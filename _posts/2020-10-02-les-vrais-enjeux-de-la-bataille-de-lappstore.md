---
title: Les vrais enjeux de la bataille de l'AppStore
subtitle: L'argent avant les libertés
date: 2020-10-02
layout: post
published: fjolain.medium.com/les-vrais-enjeux-de-la-bataille-de-lappstore-28c272762725
background: /img/2020/10/battle.jpeg
categories: [article, podcast]
tags: [apple, économie]
---

<iframe frameborder="0" loading="lazy" id="ausha-ILMC" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%233F51B5&dark=true&podcastId=Bnm2DfKYVVWG&v=3&playerId=ausha-ILMC"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>


**Fornite a déclaré la guerre contre Apple et son App Store. La société Epic Game derrière le jeu Fornite se proclame libérateur du monde mobile. On oublierait presque que derrière leurs belles intentions se cache un gain de 300 millions d’euros pour eux.**

Tous les GAFA passent aux grilles, entre la vie privée en débâcle face à leur collecte de données, leurs monopoles sur des pans entiers de l’économie ou leur évasion fiscale, les représentants des GAFA vont de commission en commission essayant de calmer les gouvernements.

## Que reproche-t-on à Apple ?

Dans ce contexte, Apple présente des spécificités. D’une part, il ne participe pas à la revente de données personnelles. Son business modèle, contrairement à Facebook ou Google, est de vendre des produits et services aux utilisateurs. Même « son monopole » est limité, le Mac ne représente que [10%](https://www.lemondeinformatique.fr/actualites/lire-les-mac-representent-10-des-pc-dans-le-monde-71294.html) du parc informatique. L’iPhone très présent sur le segment haut de gamme ne fait que [21%](https://www.lesechos.fr/tech-medias/hightech/smartphones-sur-un-marche-en-baisse-apple-et-samsung-maintiennent-la-concurrence-chinoise-a-distance-1171356) de part de marchés.

Le monopole qu’on lui reproche n’est donc pas un monopole sur un marché (comme pour Facebook ou Google), mais un monopole interne dans son écosystème. Tout propriétaire d’iPhone doit passer par l’App Store pour installer une application. Or tout achat (y compris dans l’application) est commissionné à [30%](https://www.apple.com/fr/ios/app-store/principles-practices/) par Apple. Ce monopole irrite davantage les poids lourds de l’écosystème que les gouvernements. C’est d’ailleurs Epic Game, créateur du jeu phénomène Fornite qui a déclaré la guerre contre App Store !

## Est-ce que l’App Store est différent des autres marketplaces ?

Sur les tarifs non ! Le PlayStore de Google est également à [30%](https://support.google.com/googleplay/android-developer/answer/112622?hl=fr), la plateforme Steam de jeux vidéo est aussi à [30%](https://www.phonandroid.com/steam-tuait-le-jeu-sur-pc-avant-larrivee-depic-store-selon-un-ancien-developpeur-de-valve.html), les livres numériques vendus sur Kindle sont aussi à [30%](https://kdp.amazon.com/fr_FR/help/topic/G200644210). On peut même dire qu’Apple  propose un vrai service sur son store en vérifiant les applications. Google est souvent épinglé pour accepter n’importe quoi, [y compris des arnaques](https://cyberguerre.numerama.com/6980-sur-android-une-fausse-app-wetransfer-rapporte-du-vrai-argent-a-ses-developpeurs.html)! Le service d’Amazon Kindle Direct Publisher a même été utilisé pour [blanchir de l’argent venant de fraudes internet](https://www.actualitte.com/article/lecture-numerique/amazon-verse-desormais-dans-le-blanchiment-d-argent-malgre-lui/87564).

Cependant, il a bien la « touche Apple » à savoir un environnement fermé. Google laisse installer n’importe quelle application sur Android en dehors du PlayStore. Vous pouvez télécharger n’importe quel livre numérique sur Kindle sans passer par Amazon. Pareil pour les jeux vidéo, Steam propose un environnement complet, mais tout le monde peut installer le jeu sans Steam.

L’autre « touche Apple » est l’incroyable rentabilité de l’App Store. En 2018, 65% des achats mobiles se sont faits sur l’App Store soit 46,6 milliards de dollars. Or l’App Store ne contribue que pour [28%](https://www.lesmobiles.com/actualite/26170-l-app-store-continue-de-generer-beaucoup-plus-d-argent-que-le-play-store.html) des applications téléchargées. L’App Store est une poule aux oeufs d’or pour Apple, mais aussi pour chaque développeur ! Une entreprise mobile comme Fornite, Spotify et autres génère la majorité de leur chiffre d’affaires sur iOS.

## Poker menteur.

C’est ainsi que Fornite a engrangé [un milliard de dollars de vente sur mobile, dont 96.7% sur iOS](https://sensortower.com/blog/fortnite-mobile-revenue-1-billion) et 3.3% sur Android (lancé après iOS) . Il ne faut pas chercher plus loin pour expliquer les attaques virulentes de Fornite contre Apple et non contre Google… Le besoin de liberté crié par Epic Game sur l’App Store, s’accompagne aussi de récupéré les 300 millions d’euros de vente prélevés par Apple.

Sans oublier que les pourfendeurs du monopole d’Apple comme Facebook ou Fornite, ont eux aussi leur monopole interne. Seul Epic Game peut vendre des objets virtuels dans Fornite et seul Facebook peut proposer de la pub sur Facebook, Messanger et Instagram.

## Le seul reproche acceptable de l’App Store est …

Attaquer Apple sur sa commission n’est pas acceptable. Apple a construit un écosystème fiable et lucratif qui a fait la fortune de lui et de bien d’autres entreprises. Le seul reproche qu’on puisse lui faire est de supprimer des applications protégeant la vie privée.

Par exemple, l’application ProtonVPN,  utilisée par les manifestants de Hong Kong pour garantir anonymat sur internet, [s’est vu interdire de mise à jour](https://protonmail.com/blog/apple-app-store-antitrust/), car Apple ne voulait pas voir dans sa description la mention « débloquez des sites censurés »  . Apple a aussi [retiré deux applications](https://www.frandroid.com/marques/apple/631142_hong-kong-apple-obeit-a-la-chine-et-bannit-deux-applications-de-son-app-store#:~:text=Apple%20a%20retir%C3%A9%20deux%20applications,des%20manifestations%20de%20Hong%20Kong.&text=Alors%20que%20le%20mouvement%20protestataire,%C3%A0%20des%20choix%20politiques%20cruciaux) sur demande du gouvernement chinois contre les manifestants .

Il est même choquant de voir Apple priver les manifestants de Hong Kong d’outils indispensables pour leur liberté sur une simple demande de Pékin. Alors que dans le même temps, Apple pousse une politique pro-vie privée quitte à créer [des bras de fer avec  le FBI](https://www.lesechos.fr/2016/02/terrorisme-apple-refuse-de-collaborer-avec-le-fbi-209993).

La bataille de l’App Store a profondément dévié. Son objectif doit rester une liberté d’installation pour éviter une censure injuste. Aujourd’hui, elle devient une bataille d’argent entre géants de la tech.