---
title: Train lumineux
subtitle: Comment un simple bandeau de led peut se transformer en train électrique.
date: 2016-10-29T22:19:57+01:00
layout: post
categories: [project]
tags: [raspberry, robotique, mathématique]
---
Bonjour, aujourd&rsquo;hui je vous propose un projet un peu fou que j&rsquo;ai conçu en extra durant mon stage.
On va utiliser très finement un simple ruban de led pour donner un circuit de train lumineux.
Sur notre réseau ferroviaire on pourra faire rouler plusieurs trains séparément, mettre des aiguillages et des fins de voies.
Le tout contrôler depuis un smartphone. Je vous donne un aperçu du résultat dans la vidéo ci-dessous.


<iframe src="https://www.youtube.com/embed/R8aymMU8Hc0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

### Introduction

Il existe dans le commerce des rubans de leds de plusieurs mètres de long possédant 300 voire 500 leds.
Ces rubans sont conçus pour piloter facilement de manière informatique chaque led ainsi que sa couleur.
On peut donc décider de mettre la led 1 en rouge, la led 2 en vert, éteindre la led 3, mettre la led 4 en violet, etc.
La boutique Adafruit vend ces rubans sous le nom _neopixel_, mais on peut également les trouver sur ebay ou autre.

![adafruit](https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.adafruit.com%2Fimages%2F1200x900%2F1507-00.jpg){:width="100%"}

Ce que je souhaite développer est un train lumineux.
Sur le ruban de leds un groupe de led s&rsquo;allume et peut se déplacer donnant l&rsquo;impression d&rsquo;un train.
J&rsquo;arrive très rapidement à un montage permettant de contrôler un train sur une boucle.
Ce que je désire est de pouvoir contrôler plusieurs trains indépendamment sur un circuit comprenant plusieurs boucles, aiguillages et fins de voies.

### La difficulté

![sublight]({{ site.baseurl }} /img/2016/10/sublight.png){:width="100%"}
Pour montrer où se situe la difficulté nous allons voir un exemple de réseau simple.
Il s&rsquo;agit d&rsquo;une boucle avec un aiguillage et une fin de voie.
Visuellement on voit deux voies : la boucle et la voie de garage.
Le premier problème vient du fait que je peux contrôler uniquement un ruban.
Électriquement, il faut déjà couper la boucle pour avoir un début et un fin.
Toujours électriquement, nous devons souder par des fils cachés la fin de la boucle avec le début de la voie de garage.
La figure ci-dessus illustre le passage du circuit de principe au circuit électrique de contrôle.

Admettons que le train tourne dans le sens des indices croissants avec une longueur de 3 leds,
il est sur les leds [0 1 2] à t, il sera sur [1 2 3] à t+1, c&rsquo;est le cas simple.

Maintenant, il est sur les indices décroissants. Lorsque la tête du train arrive sur la led 19, il est [19 20 21] à t,
il sera soit sur [18 19 20] à t+1 soit sur [22 19 20] à t+1. Tout de suite ça se complique, et la question est :

> _Existe t-il une représentation mathématique efficace qui permet un calcul immédiat des nouveaux indices du train pour un circuit donné ?_

Après une prépa et une école d&rsquo;ingénieur, on finit par voir des matrices partout, y comprit sur un ruban de leds&#8230;

### Les maths pour la vie

#### D&rsquo;un train à un vecteur

L&rsquo;idée principale est de représenter un train comme une suite de leds allumées ou éteintes.
De ce fait une notation vectorielle devient évidente : 0 led éteinte, 1 led allumée.
Si on reprend le circuit d&rsquo;exemple avec notre train sur l&rsquo;aiguillage qui se dirige vers la voie de garage [22 19 20], cela nous donne :

![vector T]({{ site.baseurl }} /img/2016/10/sublight_M1.png)

La première analogie avec les notations vectoriels vues en école se retrouve dans l&rsquo;état du train synthétisé par un vecteur état,
comme le *X* en éléments finis ou le vecteur *U* en structure symbolisant l&rsquo;état du système.
Il suffit de synthétiser le réseau par une matrice *R* pour que le calcul de la position du train (i.e. des états des leds) se fasse par

![matrix R]({{ site.baseurl }} /img/2016/10/sublight_M2.png)

L&rsquo;autre analogie se trouve dans la construction de la matrice *R*.
Le calcul direct est compliqué.
Par contre le calcul d&rsquo;éléments simples comme une ligne, un aiguillage ou une fin de voie est possible.
Il suffit ensuite de reconstruire la matrice globale à l&rsquo;aide des sous matrices locales.
Le calcul de la matrice du circuit d&rsquo;exemple est après.

#### D&rsquo;un réseau à une matrice

L&rsquo;idée principale du calcul réside dans la gestion led par led. Si un train est sur une led d&rsquo;indice *x*
alors au prochain coup elle sera sur la led d'indice *y*, ce qui se traduit par une fonction *f(x) = y*.
Matriciellement, cela signifie que si un 1 est présent à l&rsquo;indice *x* du vecteur,
la matrice doit le déplacer sur l&rsquo;indice *y* du vecteur.
Nous devons simplement mettre la case à 1 de la colonne *x* à la ligne *y*, soit
![matrix R]({{ site.baseurl }} /img/2016/10/sublight_M3.png).


Il ne reste plus qu&rsquo;à concevoir les fonctions<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-a7ee323bc5a3f73ad5e066b13bed5504_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#102;&#40;&#120;&#41;" title="Rendered by QuickLaTeX.com" height="18" width="34" style="vertical-align: -4px;" /> pour les trois éléments de base : ligne, aiguillage, fin de voie.

  * Lignes : Si le train est sur les indices croissants, *f(x) = x + 1*.
    Pour les indices décroissant, on a *f(x) = x - 1*.

  * Aiguillages : Si le train arrive sur une séparation en *a* qui l&rsquo;envoie soit en *b*, soit en *c*, alors
  ![matrix R]({{ site.baseurl }} /img/2016/10/sublight_M4.png)
    
    Si le train arrive de l&rsquo;autre sens, nous avons *f(x) = a, si x = b ou c*.

    On peut aussi utiliser les aiguillages pour boucler une ligne. Dans notre exemple, on boucle entre les indices 0 et 21.
    
  * Fins de voie : Notre représentation oblige à la dynamique d&rsquo;un train, on ne peut pas le stopper.
  On va donc le boucler sur lui même pour simuler un fin de voie de manière dynamique.
  Par exemple, si notre train de 3 leds arrive à la fin de voie en 26, alors le début de train sera déplacer en queue, ici en 24.
  On peut en déduire la fonction générale *f(x) = x + 1 - longueur du train*.

Les fonction de chaque élément simple permettent de construire les matrices locales de chaque élément simple.
La matrice globale représentant le réseau est la somme de toutes les sous matrices locales.


On remarque également que les matrices locales ont plusieurs états en fonction de la direction du train,
du choix d&rsquo;aiguillage ou de la longueur du train. Il faut donc reconstruire la matrice globale à chacun de ces changements.
    

A titre d&rsquo;exemple, voici la matrice globale du circuit pour un train roulant dans le sens horaire avec l&rsquo;aiguillage vers la voie de garage et une longueur de 3 leds.
    

![matrix]({{ site.baseurl }} /img/2016/10/matrice.png)


### Programmes

Les codes sources sont disponibles sur [mon github](https://github.com/FrancoisJ/SubLight)
    

Le programme principal est écrit en python.
Il fonctionne sur Raspberry pi où une bibliothèque Adafruit existe déjà pour contrôler le ruban.
Il fonctionne comme une API, un serveur TCP/IP écoute les ordres, puis le ruban est mis à jour.
    
![program]({{ site.baseurl }} /img/2016/10/sublight_schema.png){:width="100%"}

    * L&rsquo;ordre (message sous forme de String) est reçu par le serveur TCP/IP.
    * En fonction de la demande, on appelle une méthode du TrainManager.
    * Celui-ci met à jour les informations du train, et demande une nouvelle matrice à Network
    * Network assemble une nouvelle matrice depuis les éléments de bases
    
Et voici le déroulement pour le déplacement :
    
    1. Clock envoie un signal toute les 5ms à TrainManager
    2. TrainManager calcule pour chaque train son nouveau vecteur à l&rsquo;aide de la matrice réseau.
    3. Les différents vecteurs sont concaténés en un seul avec les indices RGB.
    4. Strip met à jour les leds sur le ruban.

L&rsquo;application android n&rsquo;est pas très intéressante, elle ajoute juste une interface graphique pour utiliser l&rsquo;API.
    
### Faire son propre circuit
    
Le programme, ainsi que la représentation matricielle vont pour n&rsquo;importes quels circuits, il y a juste quelques restrictions
    
    * les boucles pour changer de sens sont interdites
    * lorsque votre train est sur une fin de voie, il ne doit pas dépasser ! Cela veut dire que si votre fin de voie la plus petite est de 9 leds, alors la taille maximale autorisée sera de 9 leds.
    
Bien maintenant voyons la construction :
    
    1. découper votre ruban en petits segments. Les segments ont un sens, vérifier toujours que le output d&rsquo;un segment arrive sur le intput d&rsquo;un autre. (Mise à part pour le dernier)
    2. Ensuite lier électrique les segments entre eux, il faut refaire un seul ruban électriquement. Pour les petites distances j&rsquo;utilisais des pattes de résistance, pour les grandes distances j&rsquo;utilisais du fil que je cachais derrière la plaque.
    3. Repérer les indices des éléments : lignes, aiguillage, fin de voie sur un schéma. Pour rappel, une ligne ne doit pas comporter d&rsquo;aiguillage au milieu. Dans ce cas il s&rsquo;agit de deux lignes avec un aiguillages au bout.
    4. Remplisser le fichier `config.sbl`. Lorsque vous déclarer une ligne, le sens _start_ vers _end_ doit être le sens horaire. Pour vous aidez, je vous mets le fichier de l&rsquo;exemple :
```{
    "line" : [
        {"start" :  18, "end" : 0},
        {"start" : 21, "end" : 19},
        {"start" : 22, "end" : 26}
    ],    
    "junction" : [
        {"serial" : 1, "a" :  19, "b" : 18, "c" : 22},
        {"serial" : 2, "a" : 0, "b" : 21, "c" : 21}
    ],
    "end_line" : [
        {"position" : 26}
    ]
}```
    
    5. Enfin, il vous faudra modifier l&rsquo;application android en fonction du nombre d&rsquo;aiguillages sur votre circuit. Un simple copier-coller des blocs de code existants est suffisant.

