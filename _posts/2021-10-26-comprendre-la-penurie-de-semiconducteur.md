---
title: Comprendre la pénurie de semi-conducteur
subtitle: En l'espace de 20 ans, l'industrie s'est métamorphosée. Elle est passée d'entreprises intégrées à des entreprises spécialisées, rendant toute la supply chain sous tension.
date: 2021-10-26
layout: post
background: /img/2021/10/wafer.jpeg
categories: [article, podcast]
published: www.contrepoints.org/2021/10/29/409729-comprendre-la-penurie-de-semi-conducteurs
tags: [électronique, économie]
---

<iframe frameborder="0" loading="lazy" id="ausha-Gk03" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=bj9EmF1nqmdy&v=3&playerId=ausha-Gk03"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Depuis 3 ans, le monde traverse une pénurie de semi-conducteurs touchant les secteurs de l'automobile, de l'informatique, et bien d'autres. [Dans un précédent dossier](https://contrepoints.org/2021/04/15/395349-tout-comprendre-des-metaux-rares-1-3) sur les métaux rares, nous observons que la matière première : le silicium est suffisamment abondant pour couvrir nos besoins.

La pénurie provient d'une hausse spectaculaire de la demande due à l'essor des smartphones, de la 5G, des voitures intelligentes, des objets connectés, du cloud, de l'intelligence artificielle ou même des cryptomonnaies. Bref, toutes les dernières innovations réclament des montagnes de puces électroniques pour mettre le monde en mouvement.

Or en l'espace de 20 ans, l'industrie s'est métamorphosée. Elle est passée d'entreprises intégrées à des entreprises spécialisées, rendant toute la supply chain sous tension.

<h2>L'Ancien Monde : Intel</h2>

L'industrie du semi-conducteur était faite d'entreprises intégrées réalisant la R&D, la conception et la fabrication. L'exemple le plus parlant est Intel. L'entreprise a mis au point ces propres architectures de processeur x86 ([1981](https://fr.wikipedia.org/wiki/Intel)) puis x86_64 ([2001](https://fr.wikipedia.org/wiki/Itanium)). Elle a ensuite développé ses produits autour de ses brevets avec les processeurs i3, i5, i7, i9, Xeon, etc. Enfin elle fabrique ses processeurs dans ses usines.

Intel a même investi lourdement dans le marketing auprès du grand public. Ce fut un succès, puisque les consommateurs voulaient des ordinateurs "Intel Inside", sans vraiment savoir pour quelle raison technique.


Intel a même imposé son rythme au secteur par [la loi de Moore](https://fr.wikipedia.org/wiki/Loi_de_Moore) dictée par son ancien fondateur Gordon Moore : "Les performances doublent tous les 6 mois".

En liant son marketing à sa capacité d'innovation, Intel a entraîné le consommateur dans une quête de meilleures finesses de gravure : 45nm en [2008](https://fr.wikipedia.org/wiki/Intel), 32nm en [2010](https://fr.wikipedia.org/wiki/Intel), 22nm en [2012](https://fr.wikipedia.org/wiki/Intel), 14nm en [2016](https://fr.wikipedia.org/wiki/Intel); meilleure mémoire vive : DDR1 en [2000](https://ibis.org/summits/nov12a/pytel.pdf), DDR2 en [2004](https://ibis.org/summits/nov12a/pytel.pdf), DDR3 en [2007](https://ibis.org/summits/nov12a/pytel.pdf), DDR4 en [2012](https://ibis.org/summits/nov12a/pytel.pdf) ; meilleur wifi : 802.11a en [1999](https://fr.wikipedia.org/wiki/Wi-Fi#Les_diff%C3%A9rentes_normes_Wi-Fi), 802.11b en [2000](https://fr.wikipedia.org/wiki/Wi-Fi#Les_diff%C3%A9rentes_normes_Wi-Fi), 802.11g en [2003](https://fr.wikipedia.org/wiki/Wi-Fi#Les_diff%C3%A9rentes_normes_Wi-Fi), 802.11n en [2006](https://fr.wikipedia.org/wiki/Wi-Fi#Les_diff%C3%A9rentes_normes_Wi-Fi), 802.11ac en [2014](https://fr.wikipedia.org/wiki/Wi-Fi#Les_diff%C3%A9rentes_normes_Wi-Fi), 802.11ax en [2021](https://fr.wikipedia.org/wiki/Wi-Fi#Les_diff%C3%A9rentes_normes_Wi-Fi); meilleur USB : 1.0 en [1996](https://fr.wikipedia.org/wiki/USB#%C3%89volution_de_la_norme_USB), 2.0 en [2000](https://fr.wikipedia.org/wiki/USB#%C3%89volution_de_la_norme_USB), 3.2 en [2008](https://fr.wikipedia.org/wiki/USB#%C3%89volution_de_la_norme_USB), 4.0 en [2017](https://fr.wikipedia.org/wiki/USB#%C3%89volution_de_la_norme_USB).


Comme entreprise intégrée, nous pouvons aussi citer Texas Instrument ou IBM. Mais revenons à Intel, car sa stratégie d'abord payante c'est ensuite retournée contre lui par l'arrivée d'un trio gagnant.

<h2>Le Nouveau Monde : ARM, Apple, TSMC</h2>

Le modèle tout intégré d'Intel a su évoluer au fur et à mesure avec les ordinateurs, jusqu'à dominer complètement le secteur.

Mais les processeurs Intel étaient trop énergivores pour le marché naissant des smartphones. Ce vide créa un appel d'air et la montée de deux futurs poids lourds : ARM et TSMC.

Chacun est spécialisé dans un domaine. [ARM](https://fr.wikipedia.org/wiki/ARM_(entreprise)) est une entreprise anglaise qui recherchent et développent des technologies liées aux processeurs basse consommation. En soi, elle ne conçoit ni produit aucun composant. Elle vend ses connaissances, son expertise et ses brevets autour de son architecture.

Ces clients, tels Apple, Samsung ou NXP vont concevoir leurs processeurs sur mesure et rapidement en piochant dans le catalogue de brevets d'ARM. Une fois le composant conçu, il part dans les usines de TSMC. [TSMC](https://fr.wikipedia.org/wiki/Taiwan_Semiconductor_Manufacturing_Company) est une entreprise taiwanaise qui ne conçoit aucun composant, elle fabrique ceux des autres.

La séparation R&D, conception et fabrication a permis une itération ultra rapide, devançant en quelques années Intel.

Non content de distancier Intel sur le mobile, le Nouveau Monde montra sa supériorité sur les ordinateurs avec notamment le trio ARM, Apple et TSMC.

En se focalisant uniquement sur la conception, Apple est arrivé à maîtriser ses propres processeurs en quelques années. D'abord réservés aux IPhones et l'iPad, Apple a apporté le coup de grâce en retirant Intel de ses Mac au profit de ses processeurs maison Apple Silicium M1. Le plus ironique reste la communication d'Apple reprenant les règles d'Intel contre lui. La [puce M1 dépasserait](https://www.anandtech.com/show/16252/mac-mini-apple-m1-tested/7) les processeurs Intel en termes de puissance, d'autonomie, de finesse de gravure ou de mémoire vive.

<h2>Les conséquences d'un tel modèle</h2>

Pendant longtemps, on ne voyait que les bienfaits : investissements partagés entre plusieurs acteurs, innovation plus rapide, coûts en baisse, et une plus grande concurrence... ou presque.

En effet, là où les fabricants de PC ne pouvaient acheter des processeurs Intel qu'à Intel et souvent au prix fort. Les fabricants de téléphones peuvent acheter des processeurs ARM à Samsung, Qualcomm ou encore MediaTek à tous les prix. Mais en amont, il y a ARM en monopole et en aval il y a TSMC en quasi-monopole.

Dans les deux cas, ces monopoles attirent les convoitises politiques. ARM vient d'être rachetée par l'américain Nvidia. L'administration américaine pourrait ainsi interdire les brevets d'ARM à la Chine. Xiaomi ou Huawei s'en retrouveraient gravement touchés pour leurs futurs produits déjà impactés par l'interdiction d'Android de Google. De même pour une interdiction de TSMC de travailler pour la Chine, alors que l'entreprise dispose de processus de fabrication et d'usines uniques au monde.

De plus TSMC fait figure de goulot d'étranglement de la supply chain mondiale. Il n'est pas seul, un cartel de fabricants tient la production de composants high-tech comme les processeurs, les batteries, les écrans, les SSD ou mémoire vive. La demande a bondi, mais les moyens de production ne sont pas flexibles et nécessitent des milliards de dollars pour suivre la cadence.

Avant une ampoule se fabriquait dans une usine d'ampoules, maintenant les ampoules connectées sont dépendantes de TSMC. Avant les trottinettes se fabriquaient dans une usine de trottinette, maintenant les trottinettes électriques dépendantes de TSMC. Avant les compteurs de vitesse nécessitaient un petit moteur et une aiguille, maintenant ils ont besoin d'un écran produit en Chine. Avant les cartes graphiques se commandaient une par une par des joueurs, maintenant elles se commandent par milliers pour la recherche en intelligence artificielle ou le minage de cryptomonnaies.

<h2>Conclusion</h2>

La pénurie actuelle est donc un décalage entre le volume de productions et la hausse de la demande. Le décalage est accentué par le passage d'une industrie intégrée à une industrie spécialisée où toutes les conceptions finissent dans les mêmes usines saturées faisant goulot d'étranglement.

Cependant, le retour à des entreprises intégrées n'est pas envisagé. Le succès de l'Apple M1 face à Intel en atteste. À tel point qu'[Intel va lui aussi utiliser TSMC](https://www.reuters.com/business/intel-details-mixed-source-chip-strategy-tsmc-partnerships-2021-08-19/).

Cette mutation industrielle se propage à d'autres secteurs. Il est étonnant de constater que sur les dizaines de prétendants aux vaccins covid, les seuls à franchir la ligne d'arrivée sont en binômes concepteur / fabriquant : BioNTech/Pfizer, NIH/Moderna, Oxford/AstraZeneca.

Reste à se rappeler les paroles de Staline : "On mesure la résistance d'une chaîne à son maillon le plus faible". Et ainsi éviter les monopoles dans la supply chain. 

