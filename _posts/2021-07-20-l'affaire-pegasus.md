---
title: Les dessous de l'affaire Pegasus
subtitle: L'état finance et légalise des criminiels dès lors qu'ils espionnent les citoyens.
date: 2021-07-20
layout: post
background: /img/2021/07/pegasus.jpeg
published: www.contrepoints.org/2021/07/23/402131-pegasus-les-dessous-de-laffaire
categories: [article, podcast]
tags: [securité]
---

<iframe frameborder="0" loading="lazy" id="ausha-sMTq" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=BxM3vt2WpP3A&v=3&playerId=ausha-sMTq"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Forbidden Stories et Amnesty International viennent de révéler le "Projet Pegasus". Pegasus est un logiciel développé par la société israélienne NSO qui permet de pirater n'importe quel téléphone Android ou iPhone. Le téléphone se transforme ainsi en mouchard espionnant son propriétaire à son insu. Les victimes se comptent en dizaine de milliers, dont des chefs d'État comme Macron.

Devant un tel but comment se fait-il que Pegasus soit un produit légal ? Que NSO soit une entreprise prospère ayant pignon sur rue ? Que ses patrons ne soient pas déjà en prison ?

<h1>Une clientèle ravie du produit</h1>

Pour réaliser ses piratages, NSO cherche en permanence de nouvelles failles dans les smartphones. Cette recherche constante a un coût élevé pour l'entreprise qui se répercute sur son produit. Il faut compter [plusieurs dizaines de millions de dollars](https://en.wikipedia.org/wiki/Pegasus_(spyware)) pour utiliser Pegasus. Avoir une envie si poussée d'espionner les citoyens et posséder un tel budget pour se payer Pegasus, ne peut attirer que les états. Elle en revendique plus de 40 dont Israël, Le Qatar, La Turquie, le Mexique, le Maroc ou L'Arabie Saoudite.

La clientèle étatique de NSO étant ravie de son produit Pegasus, le produit bien que monstrueux devient légal de facto.

Depuis toujours NSO, comme ses clients, se cache derrière un masque hypocrite jurant que son produit sert uniquement à traquer les terroristes et les pédophiles. C'est promis, l'honnête citoyen même dans l'opposition ne sera jamais visé, NSO veille au grain !

Il est presque jovial de voir des chefs d'État si contents d'espionner leurs citoyens, se retrouver eux-mêmes espionner par d'autres pays. On assiste aujourd'hui à un grand jeu de dupes faisant mine de découvrir Pegasus et ses horribles utilisations, alors que le logiciel est connu depuis 5 ans.

<h1>Un scandale attendu depuis 2016</h1>

Pegasus se fit connaître dans le milieu de la cybersécurité dès [2016](https://fr.wikipedia.org/wiki/Pegasus_(logiciel_espion)#Utilisations_abusives_contre_des_opposants_politiques_et_journalistes). On détecta le logiciel dans le téléphone de Ahmed Mansoor, un militant émirati. Le logiciel utilisait des failles dans iOS. En [2017](https://fr.wikipedia.org/wiki/Pegasus_(logiciel_espion)) se sont des journalistes mexicains qui se retrouvent sur écoute à cause de ce logiciel. En  2019, on retrouve le logiciel [sur le smartphone de Jeff Bezos](https://en.wikipedia.org/wiki/Jeff_Bezos#Saudi_hacking_claim) et [Omar Abdulaziz, un dissident saoudien](https://fr.wikipedia.org/wiki/Pegasus_(logiciel_espion)#Utilisations_abusives_contre_des_opposants_politiques_et_journalistes). Cette fois le logiciel passait par une faille sur WhatsApp. Un simple appel à la victime permettait de l'installer discrètement.

Bref, toute la communauté de cybersécurité connaît Pegasus et le peu de moral de l'entreprise NSO. Il fallait cependant attendre plus de preuves pour faire vaciller la défense de NSO et de ses clients étatiques.

Avec le travail de Forbiddent Stories et Amnesty International, c'est chose faite. Ils viennent de publier la liste de 50 000 victimes du logiciel incluant des hommes politiques, des journalistes, des militants et des chefs d'entreprise.

<h1>L'arbre qui cache la forêt</h1>

Il faut espérer que NSO paye pour ses pratiques affreuses. Mais il ne faut pas être naïf. Les états semblent avoir un budget illimité pour espionner. Ils sont prêts à payer n'importe quelle entreprise pour se faciliter la tâche. Or de telles entreprises, il en a. On peut citer :

- [Cellebrite](https://fr.wikipedia.org/wiki/Cellebrite) société israélienne qui commercialise un boîtier capable d'aspirer toutes les données d'un smartphone. La France en a commandé pour 7 millions d'euros.

- [Palantir](https://fr.wikipedia.org/wiki/Palantir_Technologies) société américaine qui vend un service pour écouter et espionner une population sur internet, une sorte de NSA clé en main. La France a passé un contrat de 10 millions d'euros avec eux.

- [Hacking Team](https://fr.wikipedia.org/wiki/Hacking_Team) société italienne qui revend la découverte de failles de sécurités prêtes à être exploité pour le piratage.

- Et beaucoup d'autres plus discrètes.

Évidemment pour devenir un groupe criminel financé et "légalisé" par un état, il faut travailler pour lui en espionnant ses citoyens ou ses ennemis. Ce n'est pas le cas du groupe DarkSide, qui [a piraté la Colonial Pipeline](https://fr.wikipedia.org/wiki/Cyberattaque_de_Colonial_Pipeline) aux États-Unis en mai. Ils sont déclarés ennemis publics  par Biden et activement recherchés par le FBI. Tout comme le group REvil qui [a piraté le géant agroalimentaire américain JBS](https://www.cnbc.com/2021/06/02/russian-linked-cybercriminal-group-revil-behind-meatpacker-jbs-attack.html) en juin.

Tous font pourtant le même métier: ils piratent des personnes à leur insu. Pourtant certaines, car elles proposent d'espionner les citoyens, se font financer et adouber par les états.


