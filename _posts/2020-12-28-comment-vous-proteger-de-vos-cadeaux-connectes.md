---
title: Comment vous protéger de votre cadeau/jouet connecté ?
subtitle: Votre dernier cadeau est-il un objet connecté ? Dans ce cas, suivez le guide pour éviter qu’il se transforme en espion.
date: 2020-12-28
published: https://fjolain.medium.com/comment-vous-prot%C3%A9ger-de-votre-cadeau-connect%C3%A9-590096c3637f
layout: post
background: /img/2020/12/cadeaux.jpeg
categories: [article, podcast]
tags: [vie privée]
---

<iframe frameborder="0" loading="lazy" id="ausha-2xAP" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%233F51B5&dark=true&podcastId=ykj5VtRQaqlJ&v=3&playerId=ausha-2xAP"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Avant l’arrivée des objets connectés, un mauvais cadeau prenait juste la poussière sur l’étagère. Maintenant il peut vous espionner ! Deux catégories d’objets sont souvent reconnues pour leur espionnage : les assistants vocaux, tel [Alexa d’Amazon](https://www.ladepeche.fr/2019/04/12/vous-avez-une-enceinte-connectee-alexa-attention-les-salaries-damazon-vous-espionnent,8127292.php), [Google Home](https://www.lesnumeriques.com/assistant-domotique/google-home-mini-p41483/quand-google-home-mini-joue-espions-n67267.html) ou [Siri d’Apple]( https://www.rtl.fr/actu/sciences-tech/siri-ecoute-t-il-vos-conversations-privees-7798140627). Et, encore plus grave les jouets pour enfants, tels [le robot I-Que](https://www.lefigaro.fr/secteur/high-tech/2017/12/04/32001-20171204ARTFIG00098-securite-la-cnil-accuse-deux-jouets-connectes-d-atteinte-grave-a-la-vie-privee-des-enfants.php), [la poupée Cayla](https://www.lefigaro.fr/secteur/high-tech/2017/12/04/32001-20171204ARTFIG00098-securite-la-cnil-accuse-deux-jouets-connectes-d-atteinte-grave-a-la-vie-privee-des-enfants.php) ou encore [la montre pour enfant X4](https://securite.developpez.com/actu/309649/Une-porte-derobee-non-documentee-qui-prend-secretement-des-cliches-trouvee-dans-une-montre-intelligente-pour-enfants-le-X4-fabrique-et-developpe-conjointement-en-Chine-suscite-des-inquietudes/). 

Vous avez reçu ce type d’objets ? Protéger votre vie privée en 3 points importants.

## La caméra

Beaucoup d’objets connectés sont équipés de caméra. Certes, un voyant peut vous indiquer si la caméra est utilisée, mais ce système peut facilement être contourné. Même éteinte, la caméra peut fonctionner et vous espionner. Pour garantir votre vie privée, rien de plus simple, un bout de scotch suffit ! Obstruer la caméra dès que vous ne vous servez pas de l’objet. Il existe aussi [des caches](https://www.amazon.fr/Ultra-Mince-ordinateur-portable-prot%C3%A9ger-s%C3%A9curit%C3%A9/dp/B07SHRT5JZ/ref=sr_1_6) pour une utilisation plus pratique.

## Le micro

Une caméra se voit et reste facile à obstruer. Mais comment faire pour le micro ? Pour les assistants vocaux Amazon et Google, il existe [des brouilleurs](https://www.fastcompany.com/90290703/this-is-the-first-truly-great-amazon-alexa-and-google-home-hack). Ces appareils se logent sur l’enceinte et diffusent du bruit (inaudible pour l’homme) dans les micros rendant les assistants totalement sourds. Le bruit s’arrête sur simple ordre de l’utilisateur.

En dehors de ces cas précis, il faut la manière forte : débrancher l’appareil quand vous ne l’utilisez pas. Ou au moins, le placer dans une boîte ou un placard fermé pour diminuer la réception du son.

## L’application mobile

Parfois le danger n’est pas dans l’objet, mais dans son application mobile. Beaucoup d’objets connectés nécessitent une application. Dans ce cas, pensez à bien éteindre l’application après utilisation, ne pas la laisser tourner en arrière-plan. Attention aux ressources demandées, une application pour jouet ne doit pas avoir accès à vos contacts ou à votre micro. Par exemple, [l’application du robot « Sphero »](https://play.google.com/store/apps/details?id=com.sphero.sprk) demande un accès complet à votre GPS, votre caméra et votre micro. Est-ce bien nécessaire pour juste piloter le robot par Bluetooth ?
