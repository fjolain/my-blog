---
title: Finalement le monde des GAFAM a des limites, lesquelles ?
subtitle: Chiffre d'affaires en berne, licenciement, projets foireux, la Silicon Valley semble dans un cauchemar.
date: 2022-12-08
layout: post
background: /img/2022/12/silicon.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
---

<iframe frameborder="0" loading="lazy" id="ausha-MXJr" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=BMPL1SLJNj18&v=3&playerId=ausha-MXJr"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

La réalité est brutale surtout quand  on ne s'y attend pas. L'ascension des GAFAM semblait sans fin avec des taux de croissances à deux chiffres depuis 10 ans et des cours de bourse stratosphériques.

Depuis 6 mois, c'est le drame, la chute est abrupte. Facebook, Google, Amazon et Twitter licencient tour à tour. Les limites sont atteintes, mais quelles sont telles ?

<h2>Limite matérielle</h2>
<h3>L'innovation peine</h3>

En 1965, le CEO d'Intel prophétisa [la loi de Moore](https://fr.wikipedia.org/wiki/Loi_de_Moore) qui prédit qu'en informatique les performances doubleront tous les deux ans. Cette loi a été vérifiée pendant 40 ans aussi bien pour la puissance de calcul, le stockage ou le débit internet.

Malheureusement, les arbres ne montent pas au ciel et la technologie n'ont plus. Les PC se satisfont très bien de leurs caractéristiques actuelles, pareil pour les offres internet.

Alors que les performances de calcul ont permis les jeux vidéo, le montage vidéo, le graphisme ou les logiciels d'ingénierie. Le stockage a permis le multimédia et notamment les baladeurs MP3. Le débit haute vitesse a permis le streaming et les logiciels collaboratifs.

Les nouveaux processeurs n'apportent pas de nouveaux logiciels. La 5G n'apporte pas de nouveau service en ligne, comme le fit la 4G avec le streaming.

Sans nouvelle technologie disruptive, il n'y a pas de nouveau marché ni de nouveau produit.

On peut noter l'apparition des Neuronal Process Unit (NPU), des composants dédiés à l'IA qui apportent un léger souffle au marché.

Que peut-on vendre à quelqu'un qui a déjà un ordinateur, un smartphone, une télé ? Les montres et autres objectés connectés ne s'adressent pas à un marché aussi global, ils ne peuvent pas colmater le manque de dynamisme du numérique.

<h3>Incertitude sur la supply chain</h3>
On se rend compte aussi que le monde *immatériel* repose sur beaucoup de matériel. Le numérique nécessite une logistique mondiale et optimisée conçue durant la *paxa america* avec le dollar comme courroie d'entraînement de l'économie mondiale.

Les perspectives de guerre entre la Chine et Taiwan ou la dédollarisation du monde entraîneront un choc dans la supply chain. Des composants électroniques risquent de manquer. Au moindre composant indisponible, c'est tout le château de cartes qui s'effondre, les usines sont à l'arrêt, le produit ne peut plus être fabriqué.

D'autant que la pénurie peut provenir en amont de la fabrication, sur les matières premières. À mesure que le monde se numérise, des métaux comme le cobalt, le cuivre, l'aluminium ou le lithium deviennent critiques.

Macron souhaite d'ailleurs [ouvrir une mine de lithium](https://www.liberation.fr/environnement/climat/lithium-la-france-prevoit-douvrir-lune-des-plus-grandes-mines-deurope-dans-le-massif-central-20221024_GA24OYJPRZH7DNV6OGRBZ55ZXA/) en France pour anticiper la dépendance sur ce métal avec les voitures électriques.

<h2>Limite humaine</h2>

Les limites humaines rentrent dans le bilan d'une entreprise des deux côtés.

<h3>Coté chiffre d'affaires</h3>

Côté chiffre d'affaires, la source d'utilisateurs à conquérir se tarit. Cela fait bien longtemps que toute personne susceptible d'acheter un ordinateur en a un. De même avec les smartphones, tablettes ou télévision. Même les services en ligne se vident [Facebook perd des utilisateurs](https://www.lemonde.fr/economie/article/2022/02/03/le-reseau-social-facebook-perd-pour-la-premiere-fois-des-utilisateurs_6112140_3234.html) comme [Netflix](https://www.lesechos.fr/tech-medias/hightech/netflix-perd-pres-dun-million-dabonnes-mais-echappe-au-pire-1777529).

Le marché du numérique n'a pas d'autre produit révolutionnaire en stock. On tente d'ouvrir des débouchés dans la réalité virtuelle ou le métavers, mais sans succès jusqu'à présent.

Or la capacité à surfer d'un vague numérique à l'autre est la différence entre les gagnants et les perdants. Microsoft a bien failli couler, car il restait bloquer dans le monde vieillissant du PC, sans avoir réussi à prendre la vague du smartphone. Heureusement son pivotement vers le cloud avec Azure l'a sauvé. Certains n'ont pas eu cette chance, IBM a dû revendre sa branche PC à Lenovo, Nokia n'a pas réussi à passer du mobile au smartphone, Kodak est mort avec la pellicule.

Apple a réussi le grand chelem, d'abord en surfant sur le PC, puis sur les baladeurs numériques. Ensuite il a ouvert la révolution des smartphones et tablettes. Et maintenant il surfe sur le wearable avec l'Apple Watch, qui se [vend plus que toutes les montres suisses](https://www.ouest-france.fr/leditiondusoir/2022-09-01/apple-vend-deux-fois-plus-de-montres-que-les-suisses-voici-la-recette-de-son-succes-e802cc31-98c8-4931-89c9-ca62217589e2) et les earpods, qui lui [rapporte 12 milliards de dollars de bénéfice](https://headphonesaddict.com/airpods-facts-revenue/).

Les Occidentaux sont donc globalement équipés et, faute de nouvelle technologie, ne veulent pas forcément changer de smartphone ou de tablette.

En soi de nouveaux utilisateurs existent. Mais à défaut de l'Amérique ou de l'Europe qui disposaient déjà d'une infrastructure internet, les réserves d'utilisateurs se trouvent en Afrique, Asie ou en Inde. Pour les conquérir, il va fallait apporter le haut débit. [Google](https://cloud.google.com/blog/products/infrastructure/learn-about-googles-subsea-cables?hl=en) et [Facebook](https://www.cnbc.com/2020/05/14/facebook-building-undersea-cable-in-africa-to-boost-internet-access.html) se lancent dans les câbles sous-marins ou des projets plus exotiques comme [Google Loon](https://www.bbc.com/news/business-55761172). Toute fois le coût d'acquisition client vient de brusquement augmenter.

<h3>Coté charges</h3>
Du côté charges, Elon Musk vient de révéler le poteau rose. Après avoir licencié 50% de Twitter, le site est toujours en ligne. À quoi servaient les 50% virés ?

Dans les entreprises tech, combien de personnes non productives se retrouvent à gesticuler entre le donneur d'ordre et l'exécutant ?

La Silicon Valley s'est montrée particulièrement créative en bull shit job, des [executive meeting manager](https://www.linkedin.com/in/ekimova-ekaterina), des [happiness officers](https://www.linkedin.com/in/ekimova-ekaterina) ou des comités éthiques à perte de vue.

En soi quand le gâteau s'agrandit de 20% par an, on ne se soucie pas de qui est à table. Maintenant qu'il rétrécit, on se demande si tous les convives ont participé en cuisine.

Elon Musk sert de paratonnerre médiatique. Il n'y a pas que Twitter qui vire par milliers, Amazon vire [10 000](https://www.lefigaro.fr/secteur/high-tech/amazon-se-preparerait-a-licencier-environ-10-000-employes-20221114) personnes, HP [4 000](https://www.20minutes.fr/economie/4011335-20221123-hewlett-packard-entreprise-va-licencier-entre-4-000-6-000-employes-2025), Facebook [11 000](https://www.lepoint.fr/monde/meta-la-maison-mere-de-facebook-annonce-un-plan-de-licenciement-massif-09-11-2022-2497094_24.php) et Google [10 000](https://www.20minutes.fr/economie/4011857-20221125-google-chercherait-licencier-pres-10-000-employes-peu-performants).

<h2>Conclusion</h2>

L'industrie de la tech était bâtie sur des ressources illimitées qui se raréfient, une supply chain idéale qui se complexifie, un coût d'acquisition infime qui explose et une boulimie RH pour satisfaire toutes les causes médiatiques.

L'heure est venue à la rationalité, aux coupes budgétaires. L'argent doit aller dans les labos de recherche et non dans les comités woke ou écolos.

