---
title: Analayse du Grand Débat
subtitle: Tout savoir sur les réponses des français
date: 2019-09-03
layout: post
background: /img/2019/09/grand-debat.jpg
categories: [analysis]
tags: [data]
---

Qui se souvient du Grand Débat ?

Franchement c'est dommage, une super idée et personne pour en faire quoique se soit.
Sachez que les données sont en accès libre [ici](https://granddebat.fr/pages/donnees-ouvertes).
Sachez également qu'il y a des résultats [ici](https://granddebat.fr/pages/syntheses-du-grand-debat).

Mais devant le manque total de pédagogie ainsi que d'ergonomie, ils sont inexploitables.
Je rajouterai que personnellement, je ne fais pas confiance à Opinion Way pour être réglo.

Je suis donc parti pour analyser par moi même les données en toute transparence [code source](https://gitlab.com/fjolain/grand-debat).
Mais au lieu d'exporter le notebook en html, j'ai retroussé les manches pour concocter tout un site web autour des graphiques.

## [https://www.resultats-grand-debat.fr](https://www.resultats-grand-debat.fr)
