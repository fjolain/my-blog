---
title: Comment arrêter la haine en ligne
subtitle: TL;TR Il ne faut pas attendre que la haine arrive
date: 2020-10-29
published: fjolain.medium.com/comment-arr%C3%AAter-lengrenage-de-la-haine-en-ligne-ac9d0434cf51
layout: post
background: /img/2020/10/engrenage.jpg
categories: [article]
tags: [social network, société]
---

**La décapitation de Samuel Patty par un islamiste montre le terrible engrenage de la haine sur les réseaux sociaux. On peut techniquement freiner ces dérives sans entraver la liberté d’expression.**

## Filtrer la haine est impossible

Devant la masse de données produite chaque jour sur internet, la réponse est forcément algorithmique. Des humains ne peuvent pas abattre le travail seuls. Or les algorithmes sont incapables de filtrer la haine, même avec les dernières avancées en intelligence artificielle. Aujourd’hui, on peut déterminer le sentiment d’un texte entre positif et négatif. Voyons quelques exemples de leurs limites:

- **Le flou entre haine et critique.** Tous les deux sont des textes négatifs. L’algorithme ne fait pas la différence entre « tes arguments sont faux, tu dis n’importe quoi » et « tu mériterais de mourir, arrêtes de parler ». Pour faire la différence, l’algorithme doit se sentir menacer par les propos.
- **Le contexte est important.** Avoir des commentaires haineux sous une vidéo montrant des caricatures de Mohamed nous choquent. Mais doit-on aussi les retirer sous une vidéo faisant l’apologie du terrorisme ? En interdisant la haine, on l’interdit contre les valeurs de la république, mais aussi contre les ennemis de la république.
- **La langue humaine a plusieurs degrés.** Les algorithmes ont déjà du mal à trier les propos négatifs entre haine et critique, et ils se vautrent totalement devant l’ironie, le cynisme ou le sarcasme. Un tweet comme: « si je te croise dans la rue, on va discuter durement toi et moi », sera vu comme positif par un algorithme et non comme une menace physique.
- **La diversité des médias.** Si par magie, on arrive à endiguer la haine textuelle. Rien n’empêche les internautes de déguiser leur propos dans des images ou vidéos, rendant l’analyse impossible.

Les lois type Avia sont donc dangereuses, car totalement déconnectées des possibilités logicielles. L’algorithme supprimant toute menace haineuse en 24h n’existera pas avant plusieurs années. La loi ne pourra jamais être appliquée ou alors en ratissant large, en faisant taire la liberté d’expression et de critique.

## On peut freiner l’engrenage

Pourquoi attendre d’arriver à la haine ? On peut freiner l’engrenage qui mène à la haine. Cet engrenage se nomme « bulle de filtre », nous en faisons tous les frais. Les réseaux sociaux tentent de nous retenir le plus longtemps possible en nous incitant avec du nouveau contenu en lien avec notre historique.
	
Si nous aimons les photos de chats, Facebook nous injectera des photos de chats sur notre mur. Si nous regardons des vidéos islamistes, Youtube nous proposera d’autres vidéos islamistes sur notre page d’accueil. Les algorithmes enferment chaque internaute dans une bulle d’idées où plus aucune information contradictoire ne viendra tempérer les propos.

	
Freiner ces bulles est simple, on peut forcer les réseaux sociaux à mettre une majorité de contenus aléatoire en suggestion. On peut aussi borner dans le temps. Après une vidéo islamiste, la plateforme doit attendre trois jours avant d’en proposer d’autres. La personnalisation des suggestions doit être purgée tous les 20 jours.

Dégrader l’efficacité de ces bulles de filtre par plus d’aléatoires et en les cantonnant dans le temps permet de casser l’engrenage haineux. Cette solution est faisable techniquement, vérifiable et garante de la liberté d’expression.

Nous ne perdons rien à l’essayer.