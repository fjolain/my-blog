---
title: Calculs aérodinamisme pour modèle réduit
subtitle: Pré-calcul en vue de la conception d'un avion miniature
date: 2019-01-15
layout: analysis
background: /img/2019/01/plane.jpg
published: gitlab.com/fjolain/plane
categories: [analysis]
tags: [informatique, simulation, mathématique]
---

```python
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d, interp2d
from scipy.integrate import quad, odeint
from scipy.optimize import minimize
import pandas as pd
import math
```


```python
%matplotlib inline
```

Nous souhaitons avec un ami, concevoir un avion miniature de A à Z. Pour ce faire il est important avant de se lancer de prouver la fesabilité en fonction de données générales du problème. On va donc faire par étapes:
- recherche sur les coefficients de portances et de trainés sur internet.
- implémenter la fonction *portance*
- implémenter la fonction *trainée*
- implémenter la fonction *pesanteur*
- résoudre l'équation différentielle à l'aide du PFD pour trouver la vitesse de l'avion
- réinjecter la vitesse pour trouver la portance et ainsi la charge utile
- Puisque tout notre modèle est paramétrique, nous pouvons l'envoyer dans un optimisateur pour trouver la forme idéale de l'avion

# Paramétres généraux du problème

## Schéma de l'avion

<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA8AAAALQCAYAAABfdxm0AAA26UlEQVR42uzdDXTU5Z3ocUkmISQT8koSIGDkNWBUVGjBok3FFlotvmGlhVtQbMGX9X2rol16aLuwrUXsiqWLt1LFFe7i4q1APRZllYrHtZS1LptuQ8VKFStqilGjAj73/89ir1pUUMJkZj6fc56DtQIzz/ND883M/z+HHAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADppzQ3N7Et+jGkYuXm5TU7AgAAADrcpKnnj+xV2zdsfLo1JWtPCAMAAEDHGnvK6eccPWJUygK4umfvcPQnPznUSQAAANChTj797GtPGDMuZQE8YPDQMO7UM7/gJAAAAOhQX/zS5IXjz5qUsgAePur4cPZXv/Y3TgIAAIAONfrEsfd87eJvpCyATz5jYhjz+fHXOwkAAAA61HGNJz1y+XXfTVkAT5w6PZz0hdPucBIAAAB0qE+Mbvzt7HkLUxbAMy6bGcaMG/8LJwEAAECHGljf8Kcf33lPygJ41vcWhGM/Ofo3TgIAAIAO1bffgNY7Vj6YsgC+fuGScOQxn/iDkwAAAKBDVdX0emPV+k0pC+BFy1aH+sOPfMFJAAAA0NHCr55sSVkA/+yhx0NlVU2bYwAAAKAjlRYWFaUsfuO19j+eCkXJ4t2OAgAAgA7z+fFnHt2rtm9KAzhe0UMJTgMAAIAOc8KYz086esSolAdwdc/eoaSqqr8TAQAAoEOcfPrZ154wZlzKA3jA4KFh3KlnfsGJAAAA0CHGffHMG8efNSnlATx81PHhC2ecfZ4TAQAAoEOMOn7Miq9d/I2UB/DJZ0wMo08aN8eJAAAA0CGOazzpkcuv+27KA3ji1OnhpC+cdocTAQAAoEOMGHX8ptnzFu5TpNb1H/T23Zr3ecU/Z19+7RmXzQyfPunknzsRAAAAOkT/wUO3/fjOe/b5o4r2V/xz9uXXnvW9BWHYiFEbnQgAAAAd4rABg3bcsfLBlAfw9QuXhCOP/sRTTgQAAIAO0aO65+ur1m9KeQAvWrY6DBpyxPNOBAAAgA6Rk5v71q+ebEl5AP/socdDdc/erzkRAACA91FXVxdHU7AOzvooAWwdnFVbW7vDvxEAACCzBQ6OjxrAHLzz8a8DAAAQwAhgAQwAAAhgBLAABgAABDACWAADAAACWAALYAEMAAAIYAEsgAUwAAAggAUwAhgAABDAAhgBDAAACGABjAAGAAABjAAWwAAAgABGAAtgAABAACOABTAAACCAEcACGAAAEMACWAALYAAAQAALYOcjgAEAAAEsgBHAAACAABbACGAAAEAAC2AEMAAACGAEsAAGAAAEMAJYAAMAAAIYASyAAQAAAYwAFsAAAIAAFsACWAADAAACWADbOAEMAAAIYAGMAAYAAASwAEYAAwAAAlgAI4ABAEAAI4AFMAAAIIARwAIYAAAQwAhgAQwAAAhgBLAABgAABLAAFsACGAAAEMACGAEMAAAIYAGMAAYAAASwAEYAAwAAAlgAI4ABAEAAI4AFMAAAIIARwAIYAAAQwAhgAQwAAAhggSWABTAAACCABbAAFsAAAIAAFsAIYAAA4OPp0aPHrj1f+FsHYX3EKLMOwiovL3/DvxEAAAD2OGzg4C0/vvOesPHp1v1eHzWAP8rvNet7C8LhRx6zzokBAADwkdT27bf9jpUPdvoAvn7hkjCkYdgmJwYAAMBHUtmj+rVV6zd1+gBetGx16D+o/hknBgAAwEeSk5v71q+ebOn0Afyzhx4PUay/7MQAAAD4KJIF3Qp3f5QgPdgBvPY/ngrJ4u5vOjIAAAA+irqa3n3SIoDf/v0cGQAAAPutIJn89NEjRoV0CeDKqpr4I65qnRwAAAD75ejho849Ycy4jxykPaqq9/uzaeOf81F/vwGDh8a/RoOTAwAAYL984rjGb40/a9JHDtKDvYaPOj4MPuKIU50cAAAA+6XhqGNv+drF30ibAD75jIlh8JAjL3VyAAAA7JejR4xaffl1302bAJ44dXoYcdwJ850cAAAA++Xwo459dPa8hWkTwDMumxmOHnHcnU4OAACA/VLXf+Dvf3znPWkTwLO+tyDUNxz5b04OAACA/VLbt9/2O1Y+mDYBfP3CJWFIw7BNTg4AAID9Ul7Z45VV6zelTQAvWrY61PUf+LSTAwAAYL/k5Oa+9asnW9ImgH/20OOhorJqh5MDAABgfyQLuhXuTpf4jdfa/3gqJIu7v+noAAAA2B91Nb1qd6VTAMcretzB0QEAALA/Rh89YlRItwCu6FG9M3rstY4PAACAfTJgaMPkE8aMS7sA7j9oyFvRw29wggAAAOyTw4869urxZ01KuwAePur4UFJe9TknCAAAwD5pOOrYW7528TfSLoBPPmNiGDzkyEudIAAAAPvkqGM+cc/l13037QJ44tTp4ehPjrzeCQIAALBPhjQMWz973sK0C+AZl80MRxwz4nYnCAAAwD6p6z/w9z++8560C+BZ31sQ6huO/DcnCAAAwD7p1bfuT3esfDDtAvj6hUvCoCENjztBAAAA9klZRWXrqvWb0i6AFy1bHQ49rP9TThAAAIB9kpOb+9avnmxJuwD+2UOPh4rKqh1OEAAAgH2RLOhWuDvd4jdea//jqVCULH7DEQIAALAv6qp79t6ZjgEcr+jxB0cIAADAvhh99IhRIV0DuKJH9c7oOdQ6RgAAAD5Q7759v3TCmHFpG8CHDRi8K3oaDU4SAACAD9R/0NArxp81KW0DePio4+O3QDc6SQAAAD5Qw1HH3vK1i7+RtgF88hkTw+AhR17qJAEAAPhAQxqOuuvy676btgE8cer0UN8w7LtOEgAAgA80aGjDutnzFqZtAM+4bGYYesSwW50kAAAAH+jQfgM2//jOe9I2gGd9b0EYfPgRa50kAAAAH6hnbd9td6x8MG0D+PqFS0L/QUN+7SQBAAD4QGUVla2r1m9K2wBetGx16FvXb4uTBAAA4APl5Oa+9asnW9I2gH/20OOhorJqh5MEAADggyTz8vJ3p2v8xmvNhs2hKJl83VECAADwQeqqe/bemc4BHK/oeQRHCQAAwAcZ3TDs2N3pHsCl5RU7o+dS6zgBAADYq+LSitOPa/xsSPcA7nNY/ziAG5woAAAAe9V/0NArxp81Ke0DePio4+O3QDc6UQAAAPZq4JCGm8+54PK0D+Cx4yeEnn36zHCiAAAA7NWAwQ3LLr5mdtoH8ITJ00K/QfXfcqIAAADs1aChDetmz1uY9gE847KZYegRw251ogAAAOxVn7r+v1tw+4q0D+Br59wYBg4eusaJAgAAsFfVvWqfue3uB9I+gOcuWBzq+g36dycKAADAXpVVVLauWr8p7QN40bLVoW9dvy1OFAAAgL3Kyc1969Hm7WkfwCvWbghRzLc4UQAAAPYmmZeXvzvd4zdeazZsDoVFRW2OFAAAgL2pq+7Ze2cmBHC8oucTHCkAAAB7M/rwo47NmAAuKSt/M3pOtY4VAACA9zr5uMbPhkwJ4D51/eIAbnCsAAAAvEu/AYMuG3/WpIwJ4GNHjn4relqNThYAAIB3Oaz/oB+ec8HlGRPAY8dPCOXl5dOcLAAAAO8yYPDQpRdfMztjAnjC5Gmhb78Bf+dkAQAAeJeB9Q0Pzp63MGMCeMZlM8PgoUf8bycLAADAu/Tqc+h/Lbh9RcYE8LVzbgx1/Qf93MkCAADwLtW9ap+57e4HMiaA5y5YHOr6Dfp3JwsAAMC7lJZXvLxq/aaMCeBFy1aH2r51v3eyAAAAvEtObu5bjzZvz5gAXrF2Q4ii/iUnCwAAwDsl8/Lyd2dK/MZrzYbNobCoqM3RAgAA8E51VTW93sikAI5X9LyCowUAAOCdRg894uiMC+CS0rI3oudW63gBAAB428nHNX42ZFoA96nr92b03BocLwAAAO1qamsvGn/WpIwL4GM/+ald0dNrdMIAAAC0631o3bxzLrg84wJ47PgJIa+g4KtOGAAAgHYDBg9devE1szMugCdMnhb69hvwd04YAACAdnUDBt0/e97CjAvgGZfNDHX9B//ICQMAANCuplef/1xw+4qMC+Br59wYag/tt9IJAwAA0K66V+0zt939QMYF8NwFi0Ndv0H/7oQBAABoV1pWvmPV+k0ZF8CLlq0Ovfsc2uyEAQAAaJeTk7P70ebtGRfAK9ZuCFHcv+iEAQAAiCXz8vJ3Z1r8xmvNhs2hsKiozREDAAAQq+tR3fP1TAzgeEXPLzhiAAAAYqOHNAzL2AAuKS17I3qOtY4ZAACAk4/79ElvZWoA9zn0sDiAGxwzAABAlissLD5v/FmTQqYG8LDhI+MAbnTSAAAAWa5X7z7Xn3PB5RkbwGPHT4ivAZ7kpAEAALLcYQPqf3rxNbMzNoAnTJ4WanrV/q2TBgAAyHJ9Dxtw3+x5CzM2gGdcNjPU9jnsH500AABAluvdp+43C25fkbEBfO2cG0MU+f/XSQMAAGS5ml61f7jt7gcyNoDnLlgcDu0/6GEnDQAAkOW6l5S2rFq/KWMDeNGy1fE1wL910gAAAFkukcjb9Wjz9owN4BVrN4Syyh7bnTQAAEB2K8jLy9+dqfEbrzUbNoeiZPdXHTUAAEB2q6usqmnL5ACOV/Q8g6MGAADIbiOHNAx7PdMDuKS07I3oudY4bgAAgOw19pOjP/Nmpgdw7z6HtkXPtd5xAwAAZK8p48+aFDI9gI8cPjIO4EbHDQAAkKV61fade84Fl2d8AI8dPyG+BvhsJw4AAJClDu034CcXXzM74wN4wuRpobyy8jInDgAAkKV69an7+ex5CzM+gGdcNjNU9+x9gxMHAADI1gCuPfTxBbevyPgAvnbOjaHPof1WOHEAAIAsVVXd88nb7n4g4wN47oLFobbvYQ86cQAAgCzVvaS0ZdX6TRkfwIuWrY7fAt3kxAEAALJUIpG369Hm7RkfwCvWbgilFZXPO3EAAIDsVBAHcKbHb7zWbNgcioqLX3HkAAAA2amusqqmLRsCOF7R8w2OHAAAIDuNHNxw1GvZEsDdS0pfj55zjWMHAADIPmM/Mbrx9WwJ4F61h74WPed6xw4AAJB9pow/a1LIlgA+cvjItug5Nzp2AACALFPRo/o751xwedYE8OdOOWNX9LTPdvIAAABZprpn74UXXzM7awJ4wuRpIScv73wnDwAAkGV69an7+ex5C7MmgGdcNjNE0X+DkwcAAMgyNT17/3rB7SuyJoCvnXNj6Fnbd7mTBwAAyDI9qmt+f9vdD2RNAM9dsDj06t33AScPAACQZbqXlLasWr8pawJ40bLV8Vugm5w8AABAlslNJHY+2rw9awJ4xdoNoaS0/DknDwAAkF0KEom8XdkSv/Fas2FzKEwmWx09AABAdqmrrKppy6YAjlf0vIOjBwAAyC4jBw894pVsC+Di7iVt0XOvcfwAAADZY+yI4054LdsCuGdt31ej517v+AEAALLHlPFnTQrZFsBHDh8ZvwLc6PgBAACyRGEy+c1zLrg86wL4pC+c+nr09M82AQAAAFmisqrm5ouvmZ11ATxh8rT4JljTTQAAAECW6F176OrZ8xZmXQDPuGxmqKzp+QMTAAAAkCV6VPf81YLbV2RdAF8758ZQXdNrmQkAAADIEpVV1Ztvu/uBrAvguQsWh5retfebAAAAgCxR3L3kpVXrN2VdAC9atjpUVffcZAIAAACyRG4isfPR5u1ZF8Ar1m4I3UvKtpkAAACA7FCQSOTtyrb4jdeaDZtDYTLZagQAAACyQ11lj+rXsjGA4xU9/2AEAAAAssPIgfWHv5ytAZwsLnkt2oMaYwAAAJD5xo447oSsfQW4Z23fV6M9qDcGAAAAmW/KyWd++c1sDeAjjh7xSrQHjcYAAAAgw+UkEtecc8HlIVsD+MRxX4xfAT7bJAAAAGS4yqqamy++ZnbWBvCEydPim2BNNwkAAAAZrrpX73tmz1uYtQE847KZoaSs7B9MAgAAQIarqKx6dMHtK7I2gK+dc2MoK6tYYhIAAAAyXGVV9ebb7n4gawN47oLFoaZ37f0mAQAAIMMli7u/uGr9pqwN4EXLVoeKquonTAIAAECGy00kdj7avD1rA3jF2g2huHvpsyYBAAAgsxUkEnm7sjV+47Vmw+ZQmEy2GgUAAIDMVlde2eOVbA7geEX7EIwCAABAZhs5YPDQHdkewMnikteivagxDgAAAJlr7PBRx7+a7QHcs3efV6K9qDcOAAAAmWvK508/uy3bA/jwo459OdqLRuMAAACQua4654LLQ7YH8Iljvxi/Any2cQAAAMhQpWUVP7z4mtlZH8ATJp27O9qO6SYCAAAgQ5VX9PjX2fMWZn0Az7hsZsjPL/i2iQAAAMhQFZVVjy64fUXWB/C1c24MZWUVS0wEAABAhiqv7PG72+5+IOsDeO6CxaFHdc/7TAQAAECGSiaLX1i1flPWB/CiZatDeWWPx00EAABAhspNJHY+2rw96wN4xdoNIZksfs5EAAAAZKaCRCJvV7bHb7zWbNgcuhZ0e8VIAAAAZKa6sorKVgH8Pyvaj2AkAAAAMtPIAYOH7hC//7OSxSWvRXtSaywAAAAyz9hjR472CvCeVdOrtjXakwZjAQAAkHmmjB0/QQDvWYcfecyfoz1pNBYAAACZ56pzLrhc/O5ZJ479YnwTrEnGAgAAIMMUJZM3XHzNbPG7Z53xlalvRttyockAAADIMCUlpf8ye95C8btnzbhsZnwX6FkmAwAAIMNUVFY9uuD2FeJ3z7p2zo2hrKx8qckAAADIMCVl5U233f2A+N2z5i5YHMorKn9hMgAAADJMUTK5fdX6TeJ3z1q0bHUoLSvfaDIAAAAyTG4isfPR5u3id89asXZDSCaLnzMZAAAAmaUgDmDh+//Xmg2bQ9eCbq8YDQAAgMxSV1ZR2Sp8372ifQlGAwAAILOM7D9oyJ9F77tXsrjktWhvao0HAABA5hh79IhRLaL33au6Z++Xo71pMB4AAACZY8rY8RO8Bfo9a+iRR7dEe9NoPAAAADLHVeecf5mbYL1nfeZzp7RGezPJeAAAAGSI/Pyu37/4mtmi9z3r9IlfbYu250ITAgAAkCFKSkr/Zfa8haL3PWvGZTPju0DPMiEAAACZEsBl5et/uHi56H3PunbOjaEo2X2JCQEAAMgQ3UvK/usnd90net+z5i5YHKK9udeEAAAAZIiiZHL7qvWbRO971qJlq0NpWflGEwIAAJAhchOJnQ83PSd637NWrN0QipLJbSYEAAAgMxTk5OTsFrx/vdZs2Bzyuxa0GhEAAIDMUFdWUdkqePe+ov0JRgQAACAzjDy034AWsbv3VZTs/mq0R7XGBAAAIP2NbRg24iWxu/dVVdNrR7RHDcYEAAAg/U0ZO36Ct0C/zxp65NEt0R41GhMAAID0d9VXzrvwdbG79/Xpz37hz9EeTTImAAAA6W/OhX/7d2L3fdb4sya/HO3RhcYEAAAgzXUvKfs/s+ctFLvvs2ZcNjO+C/QskwIAAJDmirt3X/fDxcvF7vusa+fcGAoKCn9qUgAAANI+gEs3/eSu+8Tu+6y5CxaH4u7dV5sUAACANFdYlHx+1fpNYvd91qJlq0NpWflGkwIAAJDmcnJz33y46Tmx+z5rxdoNobCo6FmTAgAAkN4KcnJydgvd919rNmwO+fldXzYqAAAA6a2utLziZaH7wSvap2BUAAAA0tvIvnX9XhS5H7yKu5fErwDXGhcAAID0NbZh2IiXRO4Hr5petS9Fe9VgXAAAANLXlM+efJoA/pA15IhhcQA3GhcAAID0ddWXp053DfCHrMbPnfxCtFeTjAsAAED6mnPh3/6dyP2QddrEKTuivbrQuAAAAKSpbkXJO2fPWyhyP2RNv/TqndF2zTIxAAAAaaoomXzwh4uXi9wPWbO+tyDk5uX9xMQAAACkqeLupZt+ctd9IvdD1vULl4Ti7t1XmxgAAIA01a2w8E+r1m8SuR+yFi1bHbqXlm4wMQAAAGkqJzf3zYebnhO5H7J+9tDjoVth0TMmBgAAID0V5OTk7Ba4H77W/sdTIT+/68tGBgAAID3VlZaV7xC4+7ai/QpGBgAAID2NrO3bb7u43beVLO4efxZwrbEBAABIP2Mbjhr+orjdt1XTq/alaM8ajA0AAED6mTLm8+O9AryPq/7wI1+I9qzR2AAAAKSfq86eOv3P4nbf1gljPv9ctGeTjA0AAED6mXPhld90F+h9XKdNnBJfA3yhsQEAAEgzuXl5t82et1Dc7uM672++0RZt2yyTAwAAkGa6FRX92w8XLxe3+7hmfW9B/DFIi0wOAABAmkkWFz/xk7vuE7f7uK5fuCQUd+++2uQAAACkmYJuhc+tWr+p04Xmr55s6ZQBvGjZ6jiAHzM5AAB0Kr179/5z9EOwLMuyLOvgrpqamhd9JQIAB1cA0k9LS0uorKwMbW1tNgPS1J4QBgAEMPBB1qxZ0/7F87x582wGCGAAQABD5rr++utDMpkMhx56qM0AAQwACGDIXKeddlrIy8sLubm5oampyYaAAAYABDBkpqqqqr/cSOfss8+2ISCAAQABDBn9hXP7Ki0tdTMsEMAAgACGzP7iGRDAAIAABgEMCGAAQACDAAYEMAAIYEAAAwIYAAQwIIABAQwAAhgQwIAABgABDAhgQAADgAAG9nwx/K1vfUsAgwAGAAQwCGABDAIYABDAIIABAQwACGAQwIAABgB8BQICGBDAACCAAQEMCGAAEMCAAAYEMAAIYEAAAwIYAAQwIIDBn3kBDAACGASwAAYBDAAIYBDAAhgEMAAggEEAAwIYABDAIIABAQwAAvivtba2+soEDrLFixeHjRs3CmDIMG1tbQIYADpjAD/22GNh2rRpIZlM+ooF0uDVI6Dzq6ysDJdffnloamoSwACQ6gCOX+29+eabw6BBg9rDNz8/3xfWIICBA/hntVu3bqGwsDAcc8wxYenSpe2vCgtgAEhBABcVFYWcnJy3/0NsWZZlWVYHrvi/udXV1QIYAFIRwFu2bAlXX311qKioCLW1taG0tNQrS+AVYOAA/lnt0aNH6NWrV+jdu3eYO3du2LZtmwAGgFQE8Nt27twZli9fHsaOHesLaxDAwAGSSCTChAkTwr333vtXf4Z9GQIAKQrgd4pfFQYEMPDxxa/2vt+fYV+GAEAnCGBAAAMd/2fYlyEAIIBTrrm5uf0zWeMfQQADAhgABHDGiuM33pv4RxDAgAAGAAEsgEEAAwIYAASwAAYBDAhgABDAAhgEMPgzLIABQAALYBDAIIABAAEsgEEAgwAGAASwAEYAAwIYABDAAhgBDAhgABDACGAEMCCAAUAAC2AQwIAABgABLIBBAAMCGAAEcNoH8OjRo8PUqVP/al155ZU2CQEMCGAAEMCZE8Dvt+rq6mwSAhgQwAAggAEBDAhgABDAgAAGBDAACGBAAIMABgAEMCCAQQADAAIYBDAggAEAAQwZZdu2be+6K3kymQwtLS02BgQwACCAIfP06tXrLwF83nnn2RAQwACAAIbMNG3atJCfnx9yc3O9+gsCGAAQwJC5li5d2v7F84knnmgzQAADAAIYMld8HXB87e/jjz9uM0AAAwACGDL/VWBAAAMA+6i4uLj1kHfcTdayLMuyrIOzioqKdvhKBACAVEl06dJl18anW4N1YNdD//nHkJtItBoxAACAzqGuKJncLlg7Zh3i7Z4AAACdxvDqXrXPiNWOWfE3F+JvMhgzAACA1BszYPDQp8Vqx6we1T23RXs8zJgBAACk3pTjPj1ms1jtmHXYgEFboz1uNGYAAACpd8VpZ3/VK8AdtEadcGJz/E0GYwYAAJB63z7nwsufE6sds0458yvxK8BXGDMAAIDUu/W6OTe6C3QHrf/19b+JrwH+tjEDAABIvXt+uHi5WO2gFX9zIf4mgzEDAABIsUQi75Gf3HWfWO2gdcMtS+PPAb7HpAEAAKRYbm5i66r1m8RqB61Fy1aHRCKx3qQBAACkWE5u7msPNz0nVjtoxd9ciL/JYNIAAABSK9GlS5ddQrXj1kP/+ccQf5PBqAEAAKRWXVEy6Q7QHbyifY5XwrgBAACkzvCqnr3/KFI7dsXfZIi/2WDcAAAAUmdMv0H1T4nUjl2VPaqfib/ZYNwAAABSZ8qoE05sFqkdu+r6DfxD/M0G4wYAAJA6V4w/a9KTIrVjV/xNhvibDcYNAAAgdb49ZfolT4vUjl1fOP3sLfE3G4wbAABA6tx63Zwb3QW6g9fk8y6MPwf428YNAAAgde658Sf/5w2R2rEr/iZD/M0G4wYAAJAiXXJzH/7JXfeJ1A5e1//THW3xNxtMHAAAQIrk5ia2rlq/SaR28Fq0bHXo0qXLL00cAABAiuTk5Lz6cNNzIrWDV/xNhvibDSYOAAAgRbp06bIr+iFYHb9ycnOfNnEAAAApkkgkfidOD1IA5+RsMHEAAAApEkXZG21tbYGO1dTUFPLy8ppNHAAAQIrk5+c/1dzcrFA72C9+8Yv4rea/MHEAAAApkpub++DatWsVagdbuHChzwEGAABIsdtvvfXWlyVqx7rkkkv+EO31LOMGAACQOnNmzpz5jETtWCeffPLmaK+nGTcAAIDUuWTSpElbJGrHOuKII56M9nqscQMAAEidMz/1qU89KVE7VlVV1bZorxuMGwAAQOqM7tev3x8kasfKy8t7JdrrUuMGAACQOqVxnPks4I4Tf8xUtMfPGTUAAIAUSyQSD/zrv/7rG1K1Y8ycOfOFaJtvMGkAAACpN33s2LFPS9WOUV1dHQfwaGMGAACQerX5+fmv7ty5U60eYBs3bgwFBQXPGzEAAIBOIpFIrL/vvvu8DfoAu/LKK3dE2zvHhAEAAHQel5x66qnuBn2A9ejRoyXa22HGCwAAoPOoLSgoaPU26AP79udu3bptNVoAAACdTF5e3sZf/OIXu6TrAX3787dNFgAAQOdzyahRo56Vrh9fS0tLKC4ubo32tN5YAQAAdD6J/Pz8p9wM6+M799xzXy0oKFhopAAAADqvU3v27Pm8a4E/uieeeCIUFhb+OdrLUuMEAADQiSUSiYduvvnmP0vZj+aYY455OdrGaSYJAACg8xtWVFT0YnwdK/tn6dKl8bW//xV/H8EYAQAApIdF559//jOSdt+1traGioqKV6O9G2l8AAAA0kdNIpFoaW5uVrb76JprrnmjuLh4udEBAABIP1cdeeSRf3RDrA+3bt26kEwm42t/a4wNAABA+kl06dLl7gkTJmyTuO+vqakpjt+2aL8ajQwAAED6SiYSiV9/85vffF7q/rVt27aFmpqa1woKCr5qVAAAANJffD3wtn/+5392W+j33PRqyJAhbclk8ptGBAAAIHM05OXlvfDwww+/Jn1DiK+L/vSnP/16aWnp/zYaAAAAmWdst27dtjc3N+/K9gD+8pe/vLO8vPzeQ3zeLwAAQMaaHoXftq1bt2btK7+XXHLJ7mgPfhPtRdI4AAAAZLZLCgsLX7j//vuz6u3Q27dvb3/bcxS/j0Z7UGoMAAAAssOY3Nzc5+fMmZMVLwU/8sgjoaqq6vWKiop/OMTbngEAALJOXU5OzoYTTzxxc3xH5Ew1f/78UFhY+Gq0vuDIAQAAsldBtG7t0aPH01u2bMmom2PFUT9hwoQ3SktL/zuOfUcNAABA7ML4Y5J+9rOfbc+E+H3sscfCwIED2yorK5fuiXwAAAD4i8acnJzfR+G45Ze//GVavie6ubk5fOlLX3qtqKhoR3Fx8VRHCgAAwPuJbxA1JQ7ho446qunXv/71G+kSvl/5ylde69at2yvRuuoQr/oCAACwj+KAvCQK4T+dcMIJTb/73e9e74zhu2XLljB58uQ34vAtKCj49iE+3ggAAICPEcLXRiH84mc+85lNneUV4SeeeCJcdNFFrxcXF7eWlJTMEb4AAAAcKHFgzorfGl1YWPjCF7/4xd/ec889L7W1tR2U4I1/n3vvvTdMnTr15YqKih1FRUXPdO3a9fvCFwAAgI40IFpXRGttbm7uy/G1wj/4wQ9+v23btgMavVu3bg2LFi3afcIJJ7yQl5cXv9r76J7ft94RAAAAcLDFr8BOitbtXbp0+XNVVdWThx9++ObPfe5zzRdeeGHz97///afvuOOOF9atW9d+ve7OnTv/8opu/L/Xrl0bli9fHqJ/7oWLL7742VNOOeXpY4455o81NTV/6tq168v5+fl3Rr/22dFK2moAAAA6i/gO0sOjNTZaU6I1K1o/ita/RGtdIpHYGkXyruivQ05OzhtR4D5TUFDw8J7//x+jde2enxf//GG2EwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAjpFMJp+Ofgj7s7p167bFzgEAAJBuwv7aE8IAAAAggAEAAEAAAwAAgAAGAAAAAQwAAAACGAAAAAQwAAAAAlgAAwAAIIAFMAAAAAIYAAAABDAAAAAIYAAAABDAAAAAIIABAABAAAMAAIAABgAAQAALYAAAAASwAAYAAEAAAwAAgAAGAAAAAQwAAAACGAAAAAQwAAAACGAAAAAQwAAAAAhgAQwAAIAAFsAAAAAIYAAAABDAAAAAIIABAABAAAMAAIAABgAAAAEMAAAAAhgAAAABLIABAAAQwAIYAAAAAQwAAAACGAAAAAQwAAAACGAAAAAQwAAAACCAAQAAQAADAAAggAUwAAAAAlgAAwDA3hUVFf1hzxfKlmVZlmUdxFVYWPiUr0QA4OAKAMDBd4h3agCAAAYAAQwACGAAEMAAgAAGAAEMAAhgABDAACCAAQABDAACGAAQwAAggAEAAQwAAhgAEMAAIIABQAD7MgQABDAACGAAQAADgAAGAAQwZJgtW7aERx55JCxfvjzcdNNN4brrrgtTp04NjY2Nob6+PtTV1f1ljRw5sv3vT5w4MVx99dXhlltuCevWrQvbt2+3kSCAAQABDJ0veOfPnx9Gjx7d/sXx22E7YcKEcNFFF4XvfOc7YfHixWHt2rWhqamp/Z9/e8WhHP/9pUuXtv9zcSjHv05paWl7LM+YMSMsWbIkbN261UaDAAYAAQwcfE888UR7sA4bNixUVlaG8847L6xZsya0tbUdsN9j48aN7WEdh3T8e8RhHL9C7NVhEMAAIICBDhUHafx25gEDBrS/ynvppZe2v135YNi5c2e4++67w+TJk0MymWyP4scee8yhgAAGAAEMHDjNzc3t1+jW1NS0B3AcwqnU2tra/spwHOLx9cPxW6gBAQwAAhj4yFpaWsKVV17ZHr7x250P5NubD9SrwvG1xfG1wnEIx9cXAwIYAAQwsM/i0I1fYY2vu41vQrVt27ZO/XjjEF64cGH7443vJN3ZQh0EMAAggKETiu/GHL+1+LTTTku7V1TjUI+vEY6vTz5Y1yaDAAYABDCkmfjtzieddFIYPnx42l9Tu3Llyva3bcevYgMCGAAEMPAX8Su98XW08duH47cTZ4L4M4bjmI/vFh3fNAsQwACQlnJzc5/f8x9gy7I+5ho8eHCoqqpq/3zdTBPH/JQpU9o/NslZW9aBWXv+GwwAAOnlzDPP/Mfq6uq3Mv1jhG644YbdFRUVz0VPuc6pAwAAZJfEmDFjHho8ePBb8Wf8ZoP4euCioqId0XOvd/wAAABZoLGxsfToo49+ety4cVl3bey8efNCSUlJa7QNA0wCAABAZksMGTJkS/zZvplys6t9FX+8U3xn6D2fF/ynaC8qjQMAAECGOvbYY++JX/nNpvhta2sLX//61+Mb94Ta2tr2vxff7TqK4ab4GwKmAgAAIMMcd9xx1w4aNGh3Nr3t+be//W3o27fvX+4C/aUvfekv/98ZZ5yxKwriNSYDAAAgg/Tv3/8zZWVlu+LP+80W8c29unfvHoqLi9vjN3r+YcmSJe96ZXjYsGFvHnbYYdeaEAAAgMxQ06NHj7Z77703ZJv4et/q6ur2CI72IDzxxBN/FcklJSVvRnvUYEwAAADSW0GfPn2emTt3btbF72OPPRZKS0vD/fffH84///z2v96bW265Jb4e+Jl4r4wLAABAmho4cOB9kyZNeivb4jd+q3d8w6vly5e/69Xe93P22We/ddhhhy0xMQAAAGmovLz8rMGDB7+RbZ/1u7f4/TAtLS2hoqLizdLS0qNMDgAAQHpJ9OjR49mVK1dmVfxu27Yt1NXVhZtuuukjXS/cu3fv3xkdAACANFJcXHzepz71qTeyLX7r6+vD/PnzP9LPjz8bOb4rdJ8+fc41QQAAAOmhoHv37n9+5JFHsiZ+47d5x/H7ne9852P9OuvWrYtvlvXyIW6IBQAA0PlVVFRcfcopp7yZTfE7evTocOmllx6QX2/8+PG7+vTp802TBAAA0LmVFhYWvvpBdzwWvx9s48aNoaysrCXay4RxAgAA6KTKy8t/MHXq1Ky49je+ZnfChAntK/7rA+n444/fGV9HbaIAAAA6p5pu3bq9Ht8MSvx+PGvXro0/Fuk5IwUAANA5XTt9+vS2bHj1d+rUqaGxsbFD4vdtRxxxRFu0p2OMFQAAQCfTvXv3pvguxpkuvt43vu43vv63I8Ufp9SrV6//a7IAAAA6l9r45lcd+YpoZ3DllVeGkSNHdnj8xrZv3x4KCgriV4GTxgsAAKDzuPCss856KZPjN35FNv6s34N5jfNJJ530SrS3k4wXAABAJ5FMJn959913i98DbMmSJaFHjx6PmDAAAIBO0r95eXmvH4y3BafC0qVLQ01NTdiyZUtKPmc4Pz//jWiPC4wZAABA6k0aPXr085kYv8uXLw+1tbWhqakpZY9hyJAhL0Z73GjMAAAAUiw/P//Of/qnf3oz0+J35cqVobS0NKXxu+fGW/GNsGaZNAAAgNRKRNoO9rWxHS3+OKf4bc+d4WOd1q5dG4qLi39j1AAAAFJrQHl5+Qvit+O0tbWFLl267Iq/2WDcAAAAUqexvr5+a6bEb/x25zh+42t/O5Pq6uoXor2uN24AAACpM+mkk056MlPiN77hVWeL39jIkSOfifb6VOMGAACQOlece+65T6d7/MbXMMev/C5evLhTPr7p06f/Kdrrq4wbAABA6nx/9uzZ29M9fuvr68P8+fM77WP88Y9//Hq017caNwAAgNS5ffHixa+me/xef/31nfpxxneCjvZ6rXEDAABIkdzc3AfjOEtHra2tYfTo0eHSSy/t9I813uN4r00cAABAiuTn5z/V3Nwsfg/CDbqivd5s4gAAAFIkNzf3tTgm08nOnTvDuHHjwsSJE9PmMW/ZsiUkEomtJg4AACB10i5+J0yY0L7iv04XW7dujQN4m3EDAABIkZycnDfa2trSJiTjV31POeWUtIrfWPw282ivf2/iAAAAUiQ3N/fp+O256SC+3je+7vf93rK9cePG0NjYGO69995O99jvv//+Nw5xF2gAAICUWpsOd4H+sPh9x0cNhcWLF3e6x3/LLbc8Hz22240bAABA6tz505/+9MXOHL9z585t/6zf7du378tn7XbKAJ41a1b89ucbjBsAAEDq3DB79uynO2v8zp8/vz1+t23bttePQlqzZk1YuXJlaGlp6dQBPGXKlKbosV1l3AAAAFLnqnPPPbdTfhDwLbfcEmpqavYav8uXLw+lpaXtwRuv+K8vuuiiThvAJ5544n9Fj22KcQMAAEidKZ/73Oc6XQDHgVtbWxuampr2erOrRCIRRo4cGZ544on2V4Jvuumm9r/XWQN48ODB8Vugxxg3AACA1GkcOHDglnSJ39jkyZPbYzf+bN333iirswZweXn51uixNRg3AACA1KmP4mxbZwnFdevWtb/tOf7x/cRxPHz48L/6+4888kinDeCCgoKXosdWY9wAAABSpyAnJ+fV+CZS6RC/sThy48/7fa/484w7YwBHj2tXly5dnjNqAAAAqbci/pzaVEZifC1vfCOre++990P/2WQymVYB/Pd///f/HT2uW40ZAABA6k0bPXr071IViPG1vvHbmuNrf/dF9FjbY3nnzp1/de1wZwzgQYMGxR+BdKYxAwAASL3K3Nzc19ra2g56HDY3N7e/7Xnp0qX7/HPiwI1D9zvf+c67PhM4vi64swVwvKfxW8yjx5U0ZgAAAJ1AFMAP33fffa8dzDiMP9+3vr4+zJ8/f79+XvzK74QJE9pj96STTmr//N8BAwaEhoaGThfAy5cvfzZ6TL8wYQAAAJ3HFaeffvrvO3v8vjOCFy5cGMaNG9d+PfB1113Xfg3w1KlTP/QmWgfTmDFjfhPt7SXGCwAAoPMYkEwmXzoYUfj225WvvvrqkOkKCgqej/fWeAEAAHQieXl5zY899liHx298E6tLL7004+N3w4YNr3bp0mWzyQIAAOh8vj19+vStHRWE8duW47crn3feeSEbRHu5MdrT7xsrAACAzmdAfn7+jpaWlg6J3/jGVfF678cXZaL47s95eXkvRHs6zFgBAAB0Tj/6+te//gfx+/FccMEFv4r28nbjBAAA0HnVJBKJ1vguzQdK/DFF8XW/8fW/2aClpWVnbm7uS4e4+RUAAECnN+vUU089IAUc3+wqm+I3Nm3atPja31uNEQAAQOdXGl+/2tTU9LFC8Fvf+lZoaGjIqvhtaWl5PTc310cfAQAApJFLjj/++Gc/agjOnz8/1NfXhwP5Vup0cPrpp8fX/t5gfAAAANJHQdeuXZ9Zt27dfkfgTTfdFAYMGJB18fvss8++npOTsyPauxrjAwAAkF6mDB069E/7c+fm5cuXh9ra2vBx3z6djk488USv/gIAAKSpROS+888//8X3i753Xt+bzfG7aNGi/+zSpctT0Z6VGhsAAID0VNq1a9c//PSnP23byw2fQklJSbjyyivD2rVrQ2lpaXjssceyLn43btz4XE5Ozp+ivRpmXAAAANJbfX5+/o73xu3dd98dunfvHrp06RJ69eoVVq5cmXXx29raurOgoODpaI8mGRMAAIDMcHJJScmOd97Y6qtf/WqI/n776tq1a+jdu3fW3fhq4MCBj0fP/x+NBwAAQAZJJBLfPPzww1vevilWZWVl+9uey8rKwqWXXpp18Xveeec9Fm3L2nhrTAcAAECG6dat210TJ05sja//zc/Pz8rw3XPDr//u0qXLHw/xkUcAAAAZK1lUVPTbK6644vVsDN933PTqxWgvRhoHAACAzFbTrVu3psmTJ7+8P58RnAnuuuuuzXvu+HymMQAAAMgO8SvBPx8xYsSO7du3Z0X8zp49+zd73vY82vEDAABkmSiC/6Fnz56tTU1NGR2/Z5555q+j+P1t9JQHOHUAAIDsNSkK4dfuvffejPyc38MPP/zXh/zP3Z5LHTUAAACjk8nkSz/60Y92Z0r8Pvvss69UVFQ0Rc/t1kN81BEAAADvUFdcXPzkuHHjXmlubk7r+L355pufzMvLey56TrMcKwAAAHtTkEgkrikoKGi96KKLXk+3G2Q98MADz1dXVzdHz+ORQ3zMEQAAAPugMplM3hxfGzxnzpzdbW1tnTp8n3zyyVdHjBjxRJcuXZ6KHvskxwcAAMD+GlBWVvbz6urqV5csWdLpwrelpSVMmjTpNzk5OS9Gj/XaaP2/9u7ftYkogAO4JISThOQgWRwdAskUHDI24BhwcHBwyJI1ZMjgniWDQwYzCoY4uAn+DYE4CgEXQYOGkhYaUKimFCwt5zt//AVWK/j5wJfjbrnj3vTlvXt33ZABAADwO/bK5fKbWq12Mp1Ok6teGr1arc4Hg8G7KIq24dmehNwwRAAAAFymu3EcvwjF87TZbB5PJpNks9n8ldK7XC7Pe73eh0qlcpDJZA7DszwKuWVIAAAA+JPSpcZ3isXis3TDrHq9/nk0Gl1c9u7Ri8Xia7fbfVsqlY5C6X0f7jkK2fP6AQAAuArpP3Zvx3H8uFAofApl9aTRaGzb7fa23+8fj8fji/T74fl8nqzX6+TXhlq73S5dyvz9+mw2OxsOhx87nc5Bq9Xar1armyiKvoTS+/pn6TXTCwAAwD/nZlqIQ+6HPAgZh2L8PBTkV/l8/iibzZ6Fa0k4noaSu5/L5V6G86chD0P6Ifeu/Zjl9V0vAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADw3/sGxNEh2/Y2aJoAAAAASUVORK5CYII=">

## Grandeur du problème

$S [m^2]$: Surface de l'aile

$V [\frac{m}{s}]$ : Vitesse du vent relatif

$C_l$: Coefficient de portance (Lift en anglais)

$C_d$: Coefficient de trainée (Drague en anglais)

$C_x$: Coefficient de trainée pour une sphère à $Re > 10^3$

$l [m]$ : longueur à la corde

$L [m]$: envergure de l'aile

$d [m]$: diamétre du corps central

$\nu [\frac{m^2}{s}]$: viscosité dynamique de l'air

$g [\frac{m}{s^2}]$ : gravité

$\rho_{air} [\frac{kg}{m^3}]:$ densité de l'air

$\rho_{abs} [\frac{kg}{m^3}]:$ densité du plastique

$W_{***} [kg]:$ Masse


```python
# Nature properties (Immutable)
Cx = 0.5
rho_air = 1.225
rho_abs = 1195 * 0.2 # 1.195 g/cm3 with 20% density
nu = 1.5e-5
g = 9.81
```


```python
# Plan properties
Wfix = 0.5 # Masse en dehors de l'aile batterie, moteur, corps, ...
# Simulation properties
alpha_range = np.arange(0, 9, 1)
```

# Collecte d'information et de donnée sur internet

[Très bon cours sur les bases de l'aéromodelisme](http://www.xflr5.com/docs/Survol_Bases_Aero_et_XFLR5.pdf)

## Simulation des coefficients de vol

D'après le lien en haut XFLR5 est un outil très puissant est compliqué, utlisé pour faire des simulations poussées en 3D. Pour des simulations plus simple en 2D, il conseille Xfoil. Ca tombe bien car comme on utilise une aile standard, un très bon site donne directement les résultats de simulation. [Site avec simulations](http://airfoiltools.com)

Les résultats de simulations sont téléchargés dans ce dossier avec le nombre de Reynold comme nom de fichier. Ex 50k.csv = simulation avec 50 000 Re


```python
rek = [50, 100, 200, 500, 1000]
data = {}
for r in rek: # name file
  data[r] = pd.read_csv('{}k.csv'.format(r), sep=',')
```


```python
fig, (left, right) = plt.subplots(nrows=1, ncols=2, figsize=(16, 5))
for re in rek:
  left.plot(data[re]['alpha'], data[re]['CL'], label='{}k'.format(re))
  right.plot(data[re]['alpha'], data[re]['CD'], label='{}k'.format(re))

left.set_title('Coefficient de portance')
left.set_xlabel('Alpha (C)')
left.set_ylabel('Cl')

right.set_title('Coefficient de trainne')
right.set_xlabel('Alpha (C)')
right.set_ylabel('Cd')

fig.legend()
plt.show()
```


    
![png](/img/2019/01/output_16_0.png){: style="background: white" }
    


# Resolution du problème

Le but est de finir avec la fonction $Lift(F_{traction}, \alpha)$

## Construit $C_l (\alpha)$ et $C_d(\alpha)$

Reynold number = $Re = \frac{V l}{\nu}$


```python
def reynolds(V, l):
  return V * l / nu
```


```python
# On se place entre une vitesse de 0 à 20 m/S
V_range = np.arange(0, 20, 0.1)
fig, ax = plt.subplots()
ax.plot(V_range, reynolds(V_range, 0.05))
ax.set_title('Reynold number')
ax.set_xlabel('Speed (m/s)')
ax.set_ylabel('Reynold')
plt.show()
```


    
![png](/img/2019/01/output_21_0.png){: style="background: white" }
    


On peut juste se content des courbes avec 50k


```python
Cl = interp1d(data[50]['alpha'], data[50]['CL'], kind='quadratic')
Cd = interp1d(data[50]['alpha'], data[50]['CD'], kind='quadratic')
```

## Résoudre $V(F_t, \alpha)$

### Calculer la trainée de l'avion

Trainée aile  $D_{wing} [N] = \frac{1}{2} \rho_{air} l L V^2 C_d$


```python
def Dwing(V, a, l, L):
  return 0.5 * rho_air * l * L * Cd(a) * np.power(V, 2)

assert 0.00613 < Dwing(5, 5, 0.05, 0.35) < 0.00615
```

Trainée coprs  $D_{body} [N] = \frac{1}{2} \rho_{air} \left(\frac{d}{2}\right)^2 \pi V^2 C_x$


```python
def Dbody(V, d):
  return 0.5 * rho_air * math.pi * Cx * np.power(V * d /2, 2)

assert 0.015 < Dbody(5, 0.05) < 0.016
```

### Calculer la masse de l'avion

Profil de l'aile: $y = 0.5 \left[0.2969 \sqrt{\left(\frac{x}{l}\right)} - 0.1260\left(\frac{x}{l}\right) - 0.3516\left(\frac{x}{l}\right)^2 + 0.2843\left(\frac{x}{l}\right)^3 - 0.1015\left(\frac{x}{l}\right)^4\right]$


```python
y = lambda x, l: 0.5*(0.2969 * np.sqrt(x/l) - 0.1260*(x/l) - 0.3516*np.power(x/l, 2) + 0.2843*np.power(x/l, 3) - 0.1015*np.power(x/l, 4))
```


```python
fig, ax = plt.subplots(figsize=(12, 1))
t = np.linspace(0, 1, 100)
ax.plot(t, y(t, 1), 'b')
ax.plot(t, -y(t, 1), 'b')
ax.set_title("Profile de l'aile")
ax.axis('equal')
plt.show()
```


    
![png](/img/2019/01/output_33_0.png){: style="background: white" }
    


$Area_{airfoil} = 2\int^l_0{y dx}$


```python
def area(l):
  return 2 * quad(y, 0, l, args=(l,))[0]
assert 0.068 < area(1) < 0.069
```

Masse de l'avion $W (kg) = W_{fix} + A_{airfoil} * \rho_{abs} * L$


```python
def W(l, L):
  return Wfix + area(l) * rho_abs * L
# assert 41.43 < W(1, 1) < 41.44
```

Poid [N] $G = W * g$


```python
def gravity(l, L):
  return W(l, L) * g
```

### Résoudre la dynamique de l'avion

Résoudre le PFD sur l'axe de déplacement:
$$ W \frac{dV(F_t)}{dt} = F_t - D_{wing}(V(F_t), \alpha) - D_{body}(V(F_t))$$
$$\frac{dV(F_t)}{dt} = \frac{1}{W} \left[F_t - D_{wing}(V(F_t), \alpha) - D_{body}(V(F_t)) \right] $$


```python
def derivate(Y, x, a, l, L, d):
  return np.array([(x - Dwing(Y[0], a, l, L) - Dbody(Y[0], d)) / W(l, L)])
# assert -5.22 < derivate([5], 1, 5, 0.05, 0.35, 0.05) < -5.21
```


```python
def compute_speed(l, L, d, F_range):
  V = np.empty((len(alpha_range), len(F_range)), dtype=float)
  for i in range(V.shape[0]):
    V[i] = odeint(derivate, [0], F_range, args=(alpha_range[i],l, L, d)).reshape(len(F_range))
  return V
```

## Calculer la portance $L(F_m, \alpha)$

Portance [N] $L = \frac{1}{2} \rho_{air} S V^2 C_l$


```python
def lift_eq(l, L, V, a):
  return 0.5 * rho_air * l * L * np.power(V, 2) * Cl(a)

assert 1.41 < lift_eq(1, 1, 2, 5) < 1.42
```


```python
def build_lift(l, L, d, F_range):
  """Return Lift(alpha, F)"""
  V = compute_speed(l, L, d, F_range)
  P = np.empty_like(V)
  
  for i in range(P.shape[0]):
    P[i] = lift_eq(l, L, V[i], alpha_range[i])
  
  return interp2d(F_range, alpha_range, P)
```

### Final plot


```python
def plot_lift(l, L, d, F_max):
  F_range = np.arange(0, F_max, 0.2)
  lift = build_lift(l, L, d, F_range)
  
  fig, ax = plt.subplots(figsize=(12, 8))
  
  ax.plot(F_range, gravity(l, L)*np.ones_like(F_range), '--', label='weight')
  
  for a in alpha_range:
    ax.plot(F_range, [lift(F, a) for F in F_range], label='a={}'.format(a))
  
  ax.set_xlabel('Force de traction N')
  ax.set_ylabel('Force de portance N')
  ax.set_title('Force de portance en fonction Force de Traction \n l={}, L={}, d={}'.format(l, L, d))
  
  fig.legend()
  plt.show()
```


```python
plot_lift(l=0.05, L=0.35, d=0.05, F_max=10)
```


    
![png](/img/2019/01/output_50_0.png){: style="background: white" }
    


# Optimization


```python
def compute_max_lift(l, L, d, F_max):
  F_range = np.arange(0, F_max, 0.2)
  lift = build_lift(l, L, d, F_range)
  
  max_values = []
  for a in alpha_range:
    m = np.max([lift(F, a) for F in F_range])
    max_values.append(m)
  return np.mean(max_values)
```

Fonction à minimizer $f(l, L) = \frac{W(l, L)}{\max{lift(l, L, 0.05, 6)}}$


```python
def optimize_plane(x):
  return W(x[0], x[1]) / compute_max_lift(l=x[0], L=x[1], d=0.05, F_max=6)
```


```python
res = minimize(optimize_plane, [0.3, 1], method='SLSQP', bounds=((0, 0.3), (0, 1)))
```

    /Users/francoisjolain/projects/science/plane/venv/lib/python3.7/site-packages/ipykernel_launcher.py:1: RuntimeWarning: invalid value encountered in double_scalars
      """Entry point for launching an IPython kernel.
    /Users/francoisjolain/projects/science/plane/venv/lib/python3.7/site-packages/ipykernel_launcher.py:2: IntegrationWarning: Extremely bad integrand behavior occurs at some points of the
      integration interval.
      
    /Users/francoisjolain/projects/science/plane/venv/lib/python3.7/site-packages/ipykernel_launcher.py:2: RuntimeWarning: divide by zero encountered in double_scalars
      



```python
plot_lift(l=res.x[0], L=res.x[1], d=0.05, F_max=20)
```


    
![png](/img/2019/01/output_56_0.png){: style="background: white" }
    



```python

```
