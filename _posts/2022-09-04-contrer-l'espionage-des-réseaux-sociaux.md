---
title: Les réseaux sociaux vous espionnent, contrez-les !
subtitle: Les réseaux sociaux sont devenus des armes, dont nous sommes les munitions. Heureusement, nous pouvons arrêter les tirs.
date: 2022-09-04
layout: post
background: /img/2022/09/pas.webp
categories: [article, podcast]
published: www.contrepoints.org
tags: [vie privée, internet]
---

<iframe frameborder="0" loading="lazy" id="ausha-pN0y" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=b7NXvSOkJj9G&v=3&playerId=ausha-pN0y"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Lors de l'[article précédent](https://www.codable.tv/geopolitique-des-reseaux-sociaux-1/), nous décrivons les réseaux sociaux comme un hyper-média. Une arme d'espionnage et de propagande convoitée par les États. Une arme dont nous sommes les munitions, par les données que nous laissons.

Nous allons voir comment éviter l'espionnage des réseaux sociaux aussi bien sur leurs plateformes qu’en dehors.

<h2>Espionnage sur les réseaux sociaux</h2>

Bien évidemment, le meilleur moyen de ne pas se faire voler les données est encore de ne jamais aller sur les réseaux sociaux. On ne loupe pas grand-chose à se passer de Facebook, Twitter, Instagram ou Tiktok.  Toute fois même en tant qu'utilisateur, on peut limiter la casse.

**Éviter le mobile.** Une application mobile a plus de possibilités qu'une simple page web. Si vous acceptez un peu vite les permissions, c'est open-bar ! L'app peut analyser vos photos, vos contacts, vos SMS, vos appels, votre position GPS.

Toutes ces choses sont impossibles avec un simple site web. De plus, le site s'arrête une fois l'onglet fermé. Alors que les apps peuvent tourner en permanence en arrière-plan. Mieux vaut donc accéder aux réseaux sociaux par un navigateur web plutôt qu'une application mobile.

Ensuite **dites le moins de choses sur vous**. Empruntez un faux nom, une fausse date de naissance. Sur un réseau social, il faut considérer toutes vos données comme publiques, même celles dites "privées". Elles sont forcément publiques à l'entreprise ([Google analyse les mails qui passent par Gmail](https://www.nytimes.com/2022/08/21/technology/google-surveillance-toddler-photo.html)). De plus les réseaux sociaux revendent les données à leurs partenaires ([Facebook a revendu des millions de données à Cambridge Analytica](https://fr.wikipedia.org/wiki/Cambridge_Analytica)).

Les réseaux sociaux ne tentent même pas de résister aux gouvernements, qui peuvent demander n'importe quelles données ([Facebook a transmis à la police américaine une conversation entre une mère et sa fille](https://www.letemps.ch/monde/facebook-transmis-echanges-prives-dune-mere-fille-accusee-davortement-illegal)). Enfin, les piratages sont légion, vos données peuvent donc se retrouver en libre circulation ([Le site de rencontre extra-congugale Ashley Madison a été piraté et l'identité de ses clients révélée](https://fr.wikipedia.org/wiki/Ashley_Madison)).

Il faut donc éviter tout *like*, *subscription*, toute publication de photo ou de commentaire trop personnel.

Vous pouvez aussi **embrouiller les algorithmes**, en vous inscrivant à tout et son contraire. Allez suivre les comptes de Mélanchon comme Marine lePen, le compte de L214 comme Chasse et Péche. Les réseaux sociaux vendent des profils typés auprès des annonceurs, le votre doit éviter de rentrer dans une case.

Enfin, n'hésitez pas à vous détourner des BigTech pour des projets plus respectueux de votre vie privée. Mieux vaut quitter Messenger pour l'application chiffrée [Signal](https://signal.org/fr/), abandonner Twitter pour [Mastodon](https://joinmastodon.org/servers), arrêter Gmail pour [ProtonMail](https://proton.me/).

<h2>Espionnage hors des réseaux sociaux</h2>

Oui, Google, Facebook ou Twitter vous pistent même en dehors de leurs sites. Ils proposent des services de métriques pour inciter les gérants de sites web à installer leur script sur les pages. On peut citer [Google Analytics](https://fr.wikipedia.org/wiki/Google_Analytics) ou [Facebook Pixel](https://daniloduchesnes.com/blog/pixel-facebook-comment-installer/). Une fois le script sur la page, il va collecter les clics et les visiteurs en essayant de les lier avec les utilisateurs de leurs plateformes.

Les BigTech sont donc clairement capables de vous suivre à travers internet. Google Analytics est installé sur 80% des sites web.

Pour éviter ce pistage, il faut déjà **installer un bloqueur de publicité et de scripts** comme Adblock. Il existe aussi le navigateur [Brave](https://brave.com/fr/) qui incorpore tout un tas de bloqueurs par défaut.

Ce que je conseille aussi est de **séparer les navigateurs.** Un navigateur, tel Firefox, pour vous connecter aux GAFAM et y rester connecté. Un autre navigateur, tel Brave, pour naviguer sur le reste d'internet. Ainsi les BigTech n'ont aucune chance de lier votre navigation à votre compte.

Enfin, en plus de visiter internet sans être connecté à un compte des GAFAM, on peut accroître sa vie privée en **activant un VPN et la navigation privée.**

Là encore les logiciels open sources et communautaires sont recommandés. Mieux vaut utiliser les navigateurs [Brave](https://brave.com/fr/) ou [Firefox](https://www.mozilla.org/en-US/firefox/new/) que Chrome ou Safari. Mieux vaut utiliser le gestionnaire de mots de passe [Bitwarden](https://bitwarden.com/) que LastPass. Mieux vaut utiliser [ProtonVPN](https://proton.me/) que NordVPN.


Conclusion, face à des entreprises et des états devenus avides de vos données, nous devons adopter une *hygiène numérique*, afin de ne pas servir de munitions dans cette nouvelle guerre des réseaux sociaux.

