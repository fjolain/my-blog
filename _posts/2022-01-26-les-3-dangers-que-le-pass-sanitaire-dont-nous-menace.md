---
title: Les 3 pièges mortels codés dans le pass sanitaire
subtitle: L'arbitraire. La surveillance. L'extension
date: 2022-01-26
layout: post
background: /img/2022/01/covid.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [société, survaillance]
---

<iframe frameborder="0" loading="lazy" id="ausha-d3XZ" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=bV8VWtlQ29kq&v=3&playerId=ausha-d3XZ"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Les choix techniques lors de la conception du pass-sanitaire ne protègent pas le citoyen face aux abus de l'état. Bien au contraire, il facilite 3 types d'abus dont 2 sont déjà exploités.

<h2>Faciliter les décisions arbitraires</h2>

L'état a changé le pass-sanitaire en pass-vaccinal. En pratique, il n'a techniquement aucun changement de pass. Car le pass-sanitaire ne contient aucun "laisser-passer". Le pass-sanitaire doit en fait plutôt s'appeler le "certificat sanitaire". Car il contient uniquement votre parcours de santé.

Il n'y a pas écrit "valide" ou "invalide" dedans. Il y a écrit : "test antigénique négatif du 4 janvier 2022 à 10h45" ou encore "seconde dose vaccin Pfizer injectée le 6 décembre 2021 à 18h30".

Toutes les informations sont lisibles sur le QRCode, il suffit de le scanner. Nous parlons déjà de son contenu dans un [précédent post](https://www.codable.tv/qui-a-t-il-dans-le-pass-sanitaire/). J'ai aussi mis à disposition un scanner pour lire son contenu à greenpass.codable.tv .

Le pass certifie juste le parcours sanitaire du porteur. Tant que le porteur ne modifie pas son parcours sanitaire, le QRCode reste inchangé.

Ainsi, le *valide* ou *invalide* a lieu sur l'application gouvernementale TousAntiCovidVerif. C'est l'algorithme présent dedans qui applique les règles gouvernementales *valide* ou *invalide*.

Ces règles peuvent donc être changées très rapidement, il suffit de mettre à jour l'application, sans changer aucun pass.

Lors du *passage* du pass-sanitaire au pass-vaccinal, les nouvelles règles de l'application refuseront désormais tous les pass-sanitaires venant d'un test.

Lorsque l'état voudra rendre la 3e dose obligatoire, encore une fois il faudra juste mettre à jour l'application. Et tous les pass-sanitaires 2e doses deviendront invalides sans avoir besoin de changer le moindre QRCode.

Ce dispositif est donc parfaitement adapté à la tyrannie du gouvernement, à ses changements de décisions fréquents, aux règles arbitraires.

Le pass-vaccinal ou la 3e dose montrent que cette menace est exploitée.

<h2>Faciliter la surveillance de masse</h2>

Laisser des données de santé en libre accès traitable par n'importe qui est déjà un délit condamné par la [RGPD](https://datalegaldrive.com/donnees-sante-rgpd/#:~:text=La%20protection%20des%20donn%C3%A9es%20de%20sant%C3%A9&text=Les%20donn%C3%A9es%20de%20sant%C3%A9%20sont%20prot%C3%A9g%C3%A9es%20par%20la%20loi%20Informatique,sauf%20exception%20particuli%C3%A8re%20l'autorisant.). Mais le gouvernement semble n'avoir que faire de l'état de droit.

Bien pire pour notre vie privée, le scan de notre QRCode part dans l'application gouvernementale. De là, tout peut se passer. Comme déjà expliqué dans un [précédent post](https://www.codable.tv/nous-sommes-a-15-jours-de-developpement-dune-dictature/), le résultat du scan pourra à tout moment être rapatrié sur un serveur central, par une simple mise à jour.

Encore une fois cette menace montre le bout de son nez. En Allemagne, les restaurateurs conservent les scans sur l'application Luca pour pouvoir rappeler les cas contacts.

Ces informations ont été [récupérées par la police](https://www.bfmtv.com/tech/allemagne-la-police-a-utilise-illegalement-les-donnees-de-l-application-anti-covid-dans-une-enquete_AD-202201110397.html) qui souhaitait savoir qui avait fréquenté un restaurant afin de résoudre une enquête.

La possibilité d'utiliser le QRCode comme arme de surveillance est donc avérée. Certes l'app n'était pas étatique et les intentions des policiers semblaient louables. Cependant on constate qu'un pied a été mis dans la porte de nos vies privées. Or, ces temps-ci, les portes ne se referment pas une fois le pied étatique mis au travers.

<h2>Faciliter l'extension du pass à d'autres maladies</h2>

C'est ce qui ressort de la structure des données du pass. Elle peut enregistrer d'autres maladies. En fait, quand on analyse le code, le parcours sanitaire est lié au champ `v/tg`, qui désigne [une maladie cible](https://ec.europa.eu/health/system/files/2021-06/covid-certificate_json_specification_en_0.pdf). Pour l'instant, il n'y qu'un `840539006` pour le COVID, mais rien n'interdit dans mettre d'autres. Le pass-sanitaire est conçu pour être multimaladies.

Cette menace n'a pas encore été exploitée... pour l'instant.

<h2>Conclusion</h2>

Le pass sanitaire est donc avant tout un certificat sanitaire. On n'a pas besoin de le changer en pass-vaccinal, les QRCodes restent inchangés, il faut juste mettre à jour quelques lignes de code dans l'application gouvernementale.

Le pass-sanitaire  montre son vrai visage. Il ne sert nullement à combattre l'épidémie. Il n'est là que pour mieux nous espionner, mieux nous contrôler. Il est conçu pour le futur. Conçu pour pouvoir changer les règles tous les jours si besoin, conçu pour rapatrier les données facilement, conçu pour toutes les futures épidémies.

