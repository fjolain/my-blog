---
title: Que cache la démission de Jack Dorsey ?
subtitle: Pourquoi tous les fondateurs des géants de la Tech quittent leurs boîtes ?
date: 2021-12-14
layout: post
background: /img/2021/12/twitter.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [société]
---

<iframe frameborder="0" loading="lazy" id="ausha-KblV" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=bY8zgtG0Djxl&v=3&playerId=ausha-KblV"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Le 29 novembre dernier, Jack Dorsey [démissionnait](https://fr.wikipedia.org/wiki/Jack_Dorsey) de son poste de patron de Twitter. Il rejoint ainsi la longue liste des entreprises de la Tech ayant perdu leur patron fondateur telles Microsoft, Amazon, Google, Alibaba, Uber, WeWork, etc.

Il fut pourtant un temps, où les fondateurs restaient jusqu'au bout comme Dassault, Renault ou L'Oréal. Pourquoi une telle épidémie de démission et pour quelle raison ?

<h2>Pression des investisseurs</h2>

Avec Jack Dorsey, il faut se rappeler son précédent [départ en 2008](https://fr.wikipedia.org/wiki/Jack_Dorsey) voulu par ses actionnaires. Les investisseurs qui veulent la peau du CTO sont classiques. Il y a évidement [le départ de Steve Job en 1985](https://fr.wikipedia.org/wiki/Steve_Jobs), puis son retour des années après.

Il y a aussi les patrons sulfureux gentiment *remerciés*. Adam Neumann, fondateur de WeWork [a été licencé en 2019](https://fr.wikipedia.org/wiki/WeWork) pour avoir truqué sa comptabilité avant l'introduction en bourse. Ou encore, nous pouvons citer Travis Kalanick, fondateur d’Uber [partis de l'entreprise en 2017](https://fr.wikipedia.org/wiki/Travis_Kalanick) après une enquête sur ses pratiques de managements toxiques.

Le plus emblématique reste John MacAfee [il démissionna de son entreprise en 1994](https://fr.wikipedia.org/wiki/John_McAfee). Sa société est rachetée par Intel en 2010. Soupsonné à plusieurs reprise de trafic de stupéfiants, Intel voulait rebaptiser son logiciel en "Intel Security" pour ne plus être lié au personnage. Ce qui satisfaisait pleinement l'intéressé qui désignait l'antivirus comme le "pire logiciel au monde".

<h2>Passer à autre chose</h2>

Naturellement, certains CTO ne veulent tout simplement plus gérer l'entreprise et passer à autre chose. Comme Bill Gates qui partit s'occuper de sa fondation en [2008](https://fr.wikipedia.org/wiki/Bill_Gates). Ou encore, nous avons les deux fondateurs de Google. Serge Brin n'a pas quitté l'entreprise, mais s’est focalisé uniquement sur la partie R&D avec la création en [2010 de Google X Lab](https://fr.wikipedia.org/wiki/X_(entreprise)). Larry Page, lui, quitta son rôle de [CTO en 2019](https://fr.wikipedia.org/wiki/Larry_Page). Sans oublier, le départ de Jeff Besos [d'Amazon en 2021](https://fr.wikipedia.org/wiki/Jeff_Bezos#Carri%C3%A8re) pour se consacrer à son entreprise spatiale BlueOrigin.

En soi, Jack Dorsey peut aussi cocher cette case. Durant son absence de Twitter, il a fondé l'entreprise de paiement Square. De plus, il s'intéresse aux cryptomonnaies depuis plusieurs années. Quelques jours seulement après sa démission sur Twitter, il faisait déjà [des annonces](https://www.coindesk.com/business/2021/12/02/jack-dorsey-takes-square-deep-down-the-bitcoin-rabbit-hole/) concernant les projets de Square dans la blockchain.

<h2>Pression des gouvernements</h2>

Enfin l'état peut pousser les entrepreneurs à quitter leurs entreprises.

C'est notamment le cas en Chine. Il a l'affaire Jack Ma fondateur du Géant Alibaba qui [disparu plusieurs mois fin 2020](https://fr.wikipedia.org/wiki/Jack_Ma) après avoir critiquer le Parti. Lorsqu'il réapparut, c'était pour donner sa démission. Par la suite les fondateurs de ByteDance (TikTok), Pinduoduo, JD.com ont [tous du démissionner](https://www.lesnumeriques.com/pro/en-chine-les-mises-en-retrait-de-dirigeants-d-entreprises-tech-s-enchainent-n168061.html) suite aux pressions du Parti.

Cette envie de licencier des entrepreneurs touche également les États-Unis. Il y a de plus en plus de pression autour de [Marc Zuckerberg](https://www.lemonde.fr/economie/article/2021/03/26/zuckerberg-pichai-dorsey-les-pdg-des-reseaux-sociaux-en-accusation-au-congres_6074545_3234.html), jugé incapable de protéger les données des citoyens. Ou encore Elon Musk, que la SEC [a déjà condamné plusieurs fois](https://fr.wikipedia.org/wiki/Elon_Musk#Pol%C3%A9miques) pour manipulation du cours de Tesla à la suite de ses tweets.

Là encore, Jack Dorsey coche la case. En tant que responsable d’un réseau social (qui plus est le réseau préféré de Donald Trump), [il était accusé](https://www.lemonde.fr/economie/article/2021/03/26/zuckerberg-pichai-dorsey-les-pdg-des-reseaux-sociaux-en-accusation-au-congres_6074545_3234.html) de laisser la haine et les fausses informations circuler.

Les raisons du patron de Tweeter sont un cocktail de lassitudes et de pressions des investisseurs ou des états. La démission de Jack Dorsey illustre bien la tendance actuelle d'entreprise sans leader. Des entreprises plus stables, car moins dépendantes d'une personne, mais aussi des entreprises sans têtes et sans ambitions.

