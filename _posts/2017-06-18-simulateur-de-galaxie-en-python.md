---
title: Simulateur de galaxie en python
subtitle: Comment appliquer Newton efficacement en python
date: 2017-06-18T21:37:03+01:00
layout: post
background: /img/2017/06/galaxy.jpeg
categories: [project]
tags: [informatique, newton, simulation, mathématique]
---

Pour ceux qui ne se sont pas rendu compte, j&rsquo;adore les calculs un peu mathématiques ([théorie du chaos](https://jolain.net/?p=201), [train lumineux](https://jolain.net/?p=153), [ordinateur quantique](https://jolain.net/?p=132)), avec comme langage de prédilection le python.

Aujourd&rsquo;hui, je viens vous parler de mon dernier projet : un simulateur de gravité. Le but du programme est simple. On initialise des masses ponctuelles avec positions et vitesses. Ensuite on lance mon programme qui va calculer la force de Newton entre toutes les masses, et calculer les prochaines positions des astre.

Il peut donc simuler le système Terre-Lune, le système soleil ou même une collision de galaxies comme on peut voir dans la vidéo.

<iframe src="https://www.youtube.com/embed/aXrmRZPtSsg" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Je commence par vous donner l&rsquo;adresse du programme sur mon [github](https://github.com/FrancoisJ/galaxies_simulater).

Le programme intéressant est le fichier `processor_multi.pyx`. Contrairement à mes autres algorithmes un peu rebutants celui-ci est très simple. La force de gravité de Newton qu&rsquo;exerce la planète<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-695d9d59bd04859c6c99e7feb11daab6_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#105;" title="Rendered by QuickLaTeX.com" height="12" width="6" style="vertical-align: 0px;" /> sur la planète<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-43c82d5bb00a7568d935a12e3bd969dd_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#106;" title="Rendered by QuickLaTeX.com" height="16" width="9" style="vertical-align: -4px;" /> s&rsquo;écrit :

![M1]({{ site.baseurl }}/img/2017/06/galaxy_M1.png)

Ensuite, pour calculer la nouvelle position de l&rsquo;objet, on reste sur Newton avec sa troisième lois. Dans notre cas de simulation d&rsquo;astres massifs, cela devient :

![M2]({{ site.baseurl }}/img/2017/06/galaxy_M2.png)

### Place au code

``` python
Pour i allant de 0 à n
    Fx = 0
    Fy = 0
    Pour j allant de 0 à n
        Si i différent de j
            Fx = Fx + G*m[i]*m[j]*rx/(rx²+ry²)^1.5
            Fy = Fy + G*m[i]*m[j]*ry/(rx²+ry²)^1.5
    x[t][i] = 2*x[t-1][i] - x[t-2][i] + Fx/m[i]
    y[t][i] = 2*y[t-1][i] - y[t-2][i] + Fy/m[i]
```

Et hope le programme et fini ! C&rsquo;est tout

Ou alors il y a un petit problème.
Ce programme fait intervenir deux boucles for, une complexité *n²*.
Cela ne pose pas de problème pour la lune ou le système solaire, par contre pour simuler des galaxies,
le programme va mettre plusieurs jours (22 jours dans mon cas). Il faut donc coder plus malin.

Premièrement, on remarque que la force est symétrique.
Si on calcule la force ![M3]({{ site.baseurl }}/img/2017/06/galaxy_M3.png)
, on peut facilement calculer ![M4]({{ site.baseurl }}/img/2017/06/galaxy_M4.png).
J&rsquo;ai mis en place cette technique par les matrices Fx et Fy qui représentent :
![M5]({{ site.baseurl }}/img/2017/06/galaxy_M5.png) et ![M6]({{ site.baseurl }}/img/2017/06/galaxy_M6.png).
Ainsi je calcule que la moitié triangulaire haute des matrices, j&rsquo;obtiens les matrices entières par
![M7]({{ site.baseurl }}/img/2017/06/galaxy_M7.png).
Il me reste plus qu&rsquo;a sommer sur les lignes pour avoir la force externe pour chaque astre,
![M8]({{ site.baseurl }}/img/2017/06/galaxy_M8.png).

Deuxième, il va falloir compiler. Pour ce faire j&rsquo;ai utilisé la bibliothèque Cython qui permet de compiler du python. Elle apporte également un langage proche du python, mais fortement typé où toutes les variables doivent être déclarées au début. C&rsquo;est ce langage que j&rsquo;ai utilisé, et qui donne des fichiers `*.pyx`.

Au final après toutes ces optimisations, le programme de collision de galaxies est passé de 22 jours à 8 heures. C&rsquo;est donc un gain très significatif 😉

A bientôt et amusez vous bien avec le programme.