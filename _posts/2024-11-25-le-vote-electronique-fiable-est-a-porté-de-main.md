---
title: Pourquoi la Blockchain va permettre un système de vote incorruptible
subtitle: Les solutions étatiques échouent les une après les autres. Et si la blokchain était la solution ?
date: 2024-07-28
layout: post
background: /img/2024/12/vote.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-bb2V" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=oKQ8Vuw4lYPa&v=3&playerId=ausha-bb2V"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>


Aujourd'hui, un vote électronique fiable semble illusoire, les déboires s'accumulent.

Côté américain, les machines de votes sont notoirement corruptibles. Des hackers éthiques ont piraté l'une d'elles en seulement 24h au [dernier Def Con 2024](https://www.politico.com/news/2024/08/12/hackers-vulnerabilities-voting-machines-elections-00173668). L'administration a pris note de cette faille, mais n'a procédé à aucune mise à jour avant les élections, car le délai était trop court.

Une Américaine a écroué de 9 années de prison pour avoir [manipuler des machines](https://droit.developpez.com/actu/363604/Une-ancienne-greffiere-condamnee-a-9-ans-de-prison-pour-violation-du-systeme-des-machines-a-voter-les-experts-recommandent-l-usage-des-bulletins-de-vote-en-papier-pour-securiser-les-elections/) durant les élections de 2020. Car oui, ces machines possèdent un rôle admin capable de tout faire.

Sans oublier le vote par correspondance, lui aussi [peu fiable](https://information.tv5monde.com/international/election-aux-etats-unis-pourquoi-le-vote-par-correspondance-cristallise-les-tensions). En "oubliant" de ramasser des bulletins de vote dans certains quartiers, le facteur peut favoriser un candidat. On peut aussi plus facilement bourrer les urnes, en faisant voter des personnes qui ne se sont pas déplacées au bureau de vote.

Côté suisse, LaPoste tente depuis des années de concevoir un système de vote en ligne. Si les premiers essais se faisaient souvent [hacker facilement](https://www.24heures.ch/ils-ont-trouve-la-faille-dans-le-systeme-de-voting-553163250364). Le nouveau système semble fiable d'après les tests, à condition de faire confiance aux [multiples codes de sécurité envoyés par lettre à chaque élection](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/raw/master/System/System_Specification.pdf?inline=false), à revoir donc pour un système de vote 100% électronique et décentralisé.

Pourtant, avec des innovations comme la blockchain, les hardware wallet ou les preuves à divulgation nulle, on n'a jamais été aussi proche. Alors pourquoi cela bloque-t-il ?

Aucun des exemples n'utilise ces technologies. Elles sont relativement innovantes. Or les tentatives de vote électronique sont majoritairement soutenues par des États, qui ne veulent pas entendre parler de blockchain.

Aussi je ne prédis aucun vote électronique fiable venant d'un État ou d'une organisation étatique, si tant est qu'ils  souhaitent un système de vote incorruptible.

<h2>La blockchain résout 70% du problème</h2>

Or, la blockchain et les technologies annexes résolvent tous les problèmes liés à l'infrastructure, à savoir la transmission et le stockage des votes.

Plus en détail, cela apporte :

1. un vote immuable et horodaté. La blockchain a ces deux atouts.
2. Système de vote auditable et décentralisé pour que chacun puisse être autonome. La blockchain est auditable et décentralisée.
3. On ne peut voter au nom d'une personne et le votant ne peut répudier son vote. Avec le principe de signature numérique (cf. article) et de wallet, on répond à ce point.

La blockchain et le wallet de l'utilisateur permettent de s'assurer que le vote fait sur la machine est transmis sans altération avec sa preuve d'authenticité. Puis qu'une fois transmis, le vote devienne immuable et incensurable.

Il ne reste plus qu'à garantir les règles autour du vote :

4. Prouver que le vote est légitime. Qu'il provienne d'un votant enregistré et que le votant n'ait voté qu'une fois.
5. Garder l'anonymat du vote.
6. Garder les votes secrets jusqu'à la fin.

Dans ces cas, ni la blockchain ni le wallet ne résolvent le problème. Il faut concevoir un logiciel de cryptage pour le faire. La bonne nouvelle, c'est que nous ne sommes pas les premiers à vouloir intégrer la blockchain et le wallet utilisateur pour fournir un service en ligne. On peut déjà depuis son navigateur aller sur compound.finance pour faire un crédit, uniswap.org pour échanger des cryptoactifs, renforcer son anonymat sur tornado.cash ou parier sur polymarket.com.

Tous ces services font partie du Web3, c'est à dire, des services web reposant sur la blokchain, le wallet utilisateur et des formules mathématiques. Ils n'ont pas d’infrastructure avec un serveur centralisé quelque part, ils sont disponibles mondialement par un navigateur et directement connecté à une blockchain.

Il ne reste plus qu'à utiliser ses technologies pour concevoir notre service en ligne web3 de vote électronique en ligne.

<h2>Le projet Condorcet</h2>

Bien que conçu autour de cryptage moderne, mon projet Condorcet ressemble à un système de vote classique.

Dans un système de vote classique, le secret du vote est préservé par une enveloppe. Dans le monde du cryptage, l'enveloppe devient un chiffrement asymétrique. Le votant est capable de chiffrer son vote par une clé publique disponible sur internet. Et une personne peut le moment venu tout déchiffrer pour révéler le vote. On pense au chiffrement asymétrique RSA, mais il a mieux pour ce cas.

Car si l’on se repose sur une seule clé privée pour tout déchiffrer, une personne est en mesure de connaître les votes en avance. L'idéal est de partager la responsabilité entre plusieurs personnes. Ils doivent tous déchiffrer à leur tour pour révéler les votes. On utilise donc un chiffrement El-Gamal pour cela.

L'idée de cet algo vient d'un projet de [vote électronique de l'EPFL](https://www.epfl.ch/labs/dedis/wp-content/uploads/2020/01/report-2017-2-andrea_caforio-evoting.pdf) qui l'utilisait. Les chercheurs continuaient à utiliser El-Gamal pour anonymiser le vote ce qui les conduisit à des problèmes de performances. Le vote ne pouvait dépasser 1000 votants à cause des  temps de calcul.

Mon changement tient à utiliser les signatures en cercles pour anonymiser les votes. De même que dans le vote classique, on vérifie que les votants sont légitimes avant de mettre l'enveloppe dans l'urne. Ainsi lors du dépouillement, on ne sait pas à qui appartient l'enveloppe, mais on sait qu'elle vient d'un groupe de votants légitimes puisqu'elle provient de l'urne.

Avec des signatures en cercle, on a le même procédé. Le votant fait une signature non rattachée à lui, mais à un groupe de votants. En jouant sur la construction de la signature, le système Condorcet permet de s'assurer que le votant ne vote qu'une fois, qu'il provient bien d'un groupe de votants légitimes tout en fonctionnant avec autant de votants que nécessaire.

Pour ceux que cela intéresse, tous les détails sur ce système sont décrits dans ce PDF ici. Le projet vient juste de démarrer son code source est disponible ici.

J'espère que mon système est prometteur, mais je ne pense pas avoir la solution définitive à cette problématique. Toute fois, je parie que comme pour Bitcoin, la réponse viendra d'une initiative privée, s'appuyant sur de la créativité et les dernières innovations disponibles. Et non sur un groupe de travail bureaucratique reposant sur des normes et des technologies vieillissantes.

