---
title: Notre vie privée est l’esclave du XXI siècle ? 3/3
subtitle: L'esclavage s'est arrêté, car il était caduc face à la machine à vapeur. Comment rendre économiquement caduc le viol de notre vie privée ?
date: 2023-02-28
layout: post
background: /img/2023/02/slave.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-0zyV" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=yJeYmTKjkVRP&v=3&playerId=ausha-0zyV"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Le viol de notre vie privée, bien qu'immoral, est tout aussi banalisé que l'esclavage au XVIIe siècle. L'économie du Nouveau Monde était basée sur l'esclavage. L'économie du Nouveau Monde Numérique est basée sur le viol de notre vie privée. C'est la source d'argent la plus facile et rentable d'internet.

L'[article 4](https://www.un.org/fr/universal-declaration-human-rights/index.html) de la Déclaration des droits de l'Homme proclame que "Nul ne sera tenu en esclavage ni en servitude; l'esclavage et la traite des esclaves sont interdits sous toutes leurs formes.", peu importe qu'un contrat d'esclavage est été signé, le contrat est caduc.

Un peu plus loin, l'[article 12](https://www.un.org/fr/universal-declaration-human-rights/index.html) stipule que "Nul ne sera l'objet d'immixtions arbitraires dans sa vie privée, sa famille, son domicile ou sa correspondance", peut importe que des conditions d'utilisations ont été acceptées en ligne. Les pratiques de Google ou Facebook sont contraires aux droits de l'homme.

N'importe quel juge peut rendre caducs les contrats de Google et envoyer son PDG en prison pour non-respect des droits de l'homme. Alors pourquoi personne ne le fait ?

Pour les mêmes raisons que l'esclavage a duré des siècles, c'est une industrie rentable qui pèse dans l'économie et pèse ainsi sur le politique et la justice.

On retrouve la même chose avec le poids économique des GAFAM. Les [280 milliards de dollars](https://www.statista.com/statistics/266206/googles-annual-global-revenue/) de revenu en 2022 de Google pèsent sur la politique avec ses [8 lobbyistes](https://www.macg.co/ailleurs/2022/01/le-lobbying-de-la-derniere-chance-de-google-contre-le-digital-markets-act-europeen-126571) rien qu'au parlement européen.

Heureusement, nous sommes venus à bout de l'esclavage. Ni la morale, ni les lois, ni la politique n'ont servi. Nous avons arrêté l'esclavage après avoir inventé la machine à vapeur. Nous avons arrêté l'esclavage, car il était économique caduc face à la machine.

Alors a-t-on les technologies capables de rendre économiquement caduc le viol de notre vie privée ?

<h2>Generaliser le chiffrement</h2>

Nous naviguons déjà avec le chiffrement grâce à HTTPS. Mais le chiffrement est entre nous et le serveur en ligne. Nos données finissent en clair dans les serveurs à la merci de l'espionnage ou de la revente au plus offrant.

Ces dernières années ont vu l'émergence du chiffrement de bout en bout (End to End, E2E). Ici, les données restent chiffrées sur le serveur en ligne. Seul l'utilisateur a accès aux données en clair depuis son téléphone ou son ordinateur.

Le chiffrement E2E est la sécurité absolue, l'application [Signal](https://fr.wikipedia.org/wiki/Signal_(application)) l'utilise pour sa messagerie et des entreprises comme [Proton](https://fr.wikipedia.org/wiki/Proton_AG) ou [Apple](https://www.lebigdata.fr/chiffrement-icloud) en on fait leur marque de fabrique.

L'implémentation du chiffrement E2E est propre à chaque produit. L'outil de recherche de Google pourra difficilement passer au chiffrement E2E, nos requêtes ont besoin d'être analysées par leur serveur, mais le service Drive pourrait parfaitement chiffrer nos fichiers.

Pareil pour les objets connectés de santé. Premièrement, il n'y a aucun besoin de rapatrier les données sur le serveur, le produit fonctionnerait avec les données en local. Deuxièmement, ces données sont personnelles, elles peuvent donc être chiffrées E2E.

Les affaires de Proton ou d'Apple se portent pour le mieux. Le label chiffrement E2E pourrait devenir un avantage commercial premium justifiant un abonnement. Loin du business modèle freenium, où nous ne payons pas le produit, car nous sommes le produit !

De plus, les piratages informatiques se succèdent. Les utilisateurs sont de plus en plus nombreux à porter plainte pour demander réparation quand leurs données sont dans la nature. Le coût d'un piratage devient tel, que des assureurs envisagent de [refuser d'assurer](https://securite.developpez.com/actu/339808/Les-cyberattaques-pourraient-devenir-non-assurables-a-mesure-que-les-perturbations-causees-par-les-piratages-continueront-de-croitre-selon-le-directeur-de-la-compagnie-d-assurance-suisse-Zurich/). Or le chiffrement E2E apporte un filet de sécurité, même en cas de piratage, les données restent chiffrées. Les assureurs pourraient à l'avenir demander le chiffrement E2E sur les données critiques.

<h2>Redonner l'argent aux sites web</h2>

Sur internet le goulot d'étranglement économique reste l'acte d'achat. Il y a du monde pour acheter tous les mois un café à 5E, mais plus personne pour acheter un abonnement mensuel à 5E.

Devant l'effort pour rendre un service payant en ligne, certains jettent l'éponge comme [Salto](https://www.francetvinfo.fr/economie/medias/medias-la-plateforme-de-streaming-salto-s-arrete-annoncent-france-televisions-m6-et-tf1_5662031.html). Mais le plus souvent on se tourne vers la collecte de données et la publicité pour monétiser le trafic.

Sans attendre un changement des sites web, nous pouvons déjà utiliser des VPN, TOR ou des bloqueurs de cookies et publicités (cf. vidéo sur le sujet). En limitant la collecte de donnée, notre profil marketing en ligne devient moins complet et fait gagner moins d'argent aux entreprises du secteur.

Le modèle de rémunération actuel est loin d'être parfait. L'argent d'internet est concentré chez Google et Faceook qui redonnent juste quelques miettes aux créateurs de contenus.

Et si notre navigateur s'occupait de rémunérer nos sites favoris ? Avec la blockchain cette idée devient techniquement faisable. La blockchain permet de facilement transférer de l'argent par internet même quelques centimes.

Le navigateur [Brave](https://fr.wikipedia.org/wiki/Brave_(navigateur_web)) lance justement l'expérience. Il dispose de sa cryptomonnaie le Basic Attention Token (BAT). N'importe quel site peut réclamer de l'argent et l'utilisateur peut en un clic payer un site web, un youtubeur ou un tweet. Le navigateur peut aussi faire des dons mensuels au prorata des visites.


<h2>Conclusion</h2>

L'enjeu est posé. Pour retrouver notre vie privée en ligne, nous ne devons pas attendre une é-nième loi. Il faut rendre la collecte de données économiquement caduque. Déjà en utilisant des applications chiffrées comme Proton ou Signal. En soutenant ces entreprises pour montrer que le chiffrement E2E est un vrai argument de vente rentable. Nous pouvons aussi faire des class-actions dès que nos données sont piratées afin de rentre le stockage en clair de nos données prohibitives pour les assurances.

Pour la navigation en ligne, les micropaiements par blockchains pourraient faire gagner plus d'argent aux sites web que l'oligarchie actuelle de Google et Facebook.


