---
title: Robot Wall-e en lego
subtitle: Hacking d'un Wall-e Lego pour le motoriser
date: 2016-08-10T13:31:31+01:00
layout: post
background: '/img/2016/08/wp-1470860639992.jpg'
categories: [project]
tags: [arduino, robotique]
---
Bonjour,

J&rsquo;étais dans un centre commercial quand je suis tombé nez-à-nez avec un Walle-e en lego dans une boutique.
Comme tout geek qui se respecte, j&rsquo;ai adoré le film wall-e quand j&rsquo;étais enfant.
Je demande donc de le voir plus prêt, car à 60€ c&rsquo;est un investissement.
Et là après plusieurs minutes, le constat est sans appel : avec quelques coups de dremel et de fer à souder,
je pourrais motoriser le wall-e et le téléguider depuis mon portable.
Tous les détails et la démarche dans la suite 😉

<iframe src="https://www.youtube.com/embed/m0crW5DOj_k" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

### Présentation

**Attention aux fausses idées**, le wall-e n&rsquo;a pas vocation à être motorisé.
Il est articulé d&rsquo;origine, mais rien n&rsquo;est prévu pour l&rsquo;ajout d&rsquo;un moteur.
Il va donc falloir bricoler, couper, coller et autre.
Il ne s&rsquo;agit pas non plus de Mindstorme, la taille du wall-e est trop petite. Bref du pur hackage

Comme sur le vrai Wall-e, il y a une trappe avant qui permet d&rsquo;accéder à l&rsquo;intérieur du wall-e.
L&rsquo;espace n&rsquo;est pas très grand (pas assez pour une raspberry pi),
mais assez pour une arduino micro, une antenne bluetooth, et un circuit de commande.
L&rsquo;idéal sera de tout faire sur une plaque à souder pour optimiser l&rsquo;espace.


Je me fixe le cahier des charge suivant :

  * les deux chenilles sont motorisées dans un sens par moteur à courant continu. Le pont en H prenait trop de place, et ça libère deux sorties PWM pour autre chose.
  * Les deux bras sont motorisés par servo moteur.
  * Les deux yeux sont motorisés par servo moteur. Vu que l&rsquo;on ne met pas de marche arrière, on peut se permettre de motoriser les yeux 🙂

Voici la liste des courses si vous souhaitez le refaire. Le panier ne dépasse pas les 30€


* [servo](http://www.ebay.com/itm/4pcs-rc-Servo-mini-micro-9g-for-Rc-helicopter-Airplane-Foamy-Plane-S-/261195184549?hash=item3cd07239a5:g:ZsYAAOSw9N1Vrwrk)
* [moteur](http://www.ebay.com/itm/DC-12V-300RPM-Micro-Speed-Reduction-Gear-Motor-with-Metal-Gearbox-Wheel-Shaft/351619521747?_trksid=p2047675.c100005.m1851&_trkparms=aid=222007&algo=SIC.MBE&ao=1&asc=35852&meid=c92808a2d2524398b0279dbcfb70717e&pid=100005&rk=2&rkt=6&sd=351619521853)
* [arduino](http://www.ebay.com/itm/New-Pro-Micro-ATmega32U4-5V-16MHz-Replace-ATmega328-Arduino-Pro-Mini-/140972980117?hash=item20d2a4f795:g:dR0AAMXQsMZRjJUh)
* [bluetooth](http://www.ebay.com/itm/1pc-HC-05-6-Pin-Wireless-Bluetooth-RF-Transceiver-Module-Serial-For-Arduino-D-/172102514771?hash=item28121bf853:g:nOEAAOSwQYZWwaRu)
* [power bank](http://www.ebay.com/itm/5600mAh-Portable-External-Backup-Battery-Charger-USB-Power-Bank-for-Cell-phone-/301711457288?var=&hash=item463f677c08:m:m5JQZfEBszCN8D41_2JJNQQ)

### Electronique

Le choix d&rsquo;un powerbank est simple. Il comprend déjà tout le circuit de puissance pour délivrer du 5V continu,
et il prend aussi le circuit de recharge par USB.
Il suffit juste de retirer la coque et de venir souder votre batterie (celle de l&rsquo;annonce était au final trop grosse) ainsi que votre sortie 5V.

Sur ma carte de commande, il y a donc une carte arduino micro, 4 broches pour servos moteurs, 2 transistors MOSFET avec deux borniers, le circuit électrique du powerbank et une broches pour l&rsquo;antenne bluetooth.

Voici une photo de l&rsquo;ensemble est son schémas. La carte verte au premier plan est le circuit du powerbank.
![img]({{ site.baseurl }}/img/2016/08/wp-1470859513874.jpg){:width="100%"}

### Conception

Pour le montage du wall-e je vous laisse faire avec la notice. Pour le bricolage je n&rsquo;ai pas de plan, j&rsquo;ai fais sur le moment un peu au hasard, le résultat est plutôt pas mal. Je vous laisse les photos pour que vous situez où retirer des pièces, découper et coller.

Mise en place des moteurs pour les chenilles

![conception]({{ site.baseurl }}/img/2016/08/wp-1470859588903.jpg){:width="100%"}

Mise en place des bras
![bras]({{ site.baseurl }}/img/2016/08/wp-1470859701905.jpg){:width="100%"}

Mise en place des yeux
![yeux]({{ site.baseurl }}/img/2016/08/wp-1470859622972.jpg){:width="100%"}

Et l&rsquo;aperçu final
![final]({{ site.baseurl }}/img/2016/08/wp-1470860639992.jpg){:width="100%"}


### Informatique

L&rsquo;informatique comprend deux programmes : Arduino et Android

#### Arduino

Le programme arduino est extrêmement simple. Le module bluetooth s&rsquo;occupe de tout le boulot, il envoie les informations reçus directement sur un liaison série. Il ne reste plus qu&rsquo;à écouter les ordres. Personnellement j&rsquo;encode les données sous la forme d&rsquo;une trame de deux ou trois bytes _[commande, valeur1, (valeur2) ]_. La commande indique quels moteurs nous souhaitons contrôler.

``` c++
#include &lt;Servo.h&gt;
#include &lt;SofwareSerial.h&gt;

SoftwareSerial bluetooth(8, 7); // (RX, TX) (pin Rx BT, pin Tx BT)


// servo parameters depend to your servo runs
int left_eye_start  = 31;
int left_eye_end    = 65;

int right_eye_start = 98;
int right_eye_end = 48;

int left_arm_start  = 108;
int left_arm_end    = 40;

int right_arm_start = 95;
int right_arm_end   = 180;

Servo left_eye;
Servo right_eye;
Servo left_arm;
Servo right_arm;
int left_caterpillar = 5;
int right_caterpillar = 3;



void setup() {
  // Setup connections
  Serial.begin(9600);
  bluetooth.begin(9600);

  // Setup motors with start values
  left_eye.attach(6);
  left_eye.write(left_eye_start);
  right_eye.attach(9);
  right_eye.write(right_eye_start);
  left_arm.attach(10);
  left_arm.write(left_arm_start);
  right_arm.attach(11);
  right_arm.write(right_arm_start);


  pinMode(left_caterpillar, OUTPUT);
  pinMode(right_caterpillar, OUTPUT);
}

void loop() 
{
    if (bluetooth.available()) {
      
        // Read bluetooth value from serial
        int id = (int) bluetooth.read();
        int value1 = (int) bluetooth.read();
        while (value1 == -1)
          value1 = (int) bluetooth.read();
        int value2 = (int) bluetooth.read();
        while (value2 == -1)
          value2 = (int) bluetooth.read();
        

        // match the correct action by ID command
        switch(id) {
          // left eye
          case 1 :
            if(value1)
              left_eye.write(left_eye_start);
             else
              left_eye.write(left_eye_end);
          break;

          // right eye
          case 2 :
            if(value1)
              right_eye.write(right_eye_start);
             else
              right_eye.write(right_eye_end);
          break;

          // left arm
          case 3 :
            value1 = left_arm_end + value1*(left_arm_start - left_arm_end)/100;
            left_arm.write(value1);
          break;

          // right arm
          case 4 :
            value1 = right_arm_end + value1*(right_arm_start - right_arm_end)/100;
            right_arm.write(value1);
          break;

          // caterpillar
          case 5 :
            analogWrite(left_caterpillar, value1);
            analogWrite(right_caterpillar, value2);
          break;
          
        }
        
    }

}

```
#### Android

Pour le programme android, j&rsquo;ai rajouté un petit plus : on pilote l&rsquo;avance du robot par l&rsquo;orientation du portable, donc par les retours du gyroscope et de l’accéléromètre. Les bras sont pilotés par des JSlideBar (ils sont personnalisés pour pouvoir être mis à la vertical), les yeux sont pilotés par des JRadioButton, bien que l&rsquo;on pourrait commander précisément l&rsquo;angle des yeux, j&rsquo;ai choisi un tout-ou-rien entre deux valeurs.

L&rsquo;activité principale collecte les données des capteurs et de l&rsquo;interface graphique, et les envoie vers un contrôleur, qui va s&rsquo;assurer de la conformité de données, enfin on les envoie en bluetooth à travers une classe.

code source de l&rsquo;application : <https://github.com/FrancoisJ/wall-e_app>