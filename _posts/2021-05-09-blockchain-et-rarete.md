---
title: Blockchain & Rareté
subtitle: Comment la blockchain a prodondément transformé la rareté.
date: 2021-05-09
layout: post
background: /img/2021/05/unique.jpeg
published: www.contrepoints.org/2021/05/19/397723-comment-la-blockchain-a-profondement-transforme-la-rarete
categories: [article]
tags: [bitcoin]
---

<iframe frameborder="0" loading="lazy" id="ausha-0eLd" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=oagXMsz0GlrA&v=3&playerId=ausha-0eLd"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Des monnaies entièrement numériques valant [1 000 milliards de dollars](https://coinmarketcap.com/) comme Bitcoin. Des oeuvres d'art que n'importe qui peut copier à l'identique se vendent pourtant [69 millions $](https://www.nytimes.com/2021/03/11/arts/design/nft-auction-christies-beeple.html). Au-delà des possibles bulles, les cryptoactifs (actifs existant sur une blockchain) ont bien une valeur, car ils ont bien une rareté. Nous allons voir comment la rareté a évolué au cours des derniers siècles pour arriver jusqu'aux blockchains avec leur rareté toute particulière.


<h1>Pré Industriel</h1>

La rareté, selon [Wikipédia](https://fr.wikipedia.org/wiki/Raret%C3%A9), *exprime la difficulté de trouver une chose particulière, du fait qu'elle existe en faibles quantités ou sous forme d'exemplaires en nombre limité.*. **Jusqu'à l'arrivée de l'industrie la rareté était subie.**  Toute chose nécessitait beaucoup d'heures de travail et l'humanité avait peu de travailleurs pour réaliser toutes les tâches.


Un moine copiste prenait [3 ans](https://fr.wikipedia.org/wiki/Bible_de_Gutenberg#Fabrication) pour recopier une bible. Au XIII siècle, 200 mètres de tissus nécessitaient [6000 heures de travail](https://www.persee.fr/doc/ahess_0395-2649_1971_num_26_6_422413#:~:text=La%20productivit%C3%A9%20du%20filage%20dans,selon%20les%20titres%20de%20fil.). La cathédrale de Paris que Macron veut reconstruire en 5 ans, a nécessité [182 ans](https://fr.wikipedia.org/wiki/Cath%C3%A9drale_Notre-Dame_de_Paris) à l'époque. Un hectare de blé produisait seulement [570 kg](http://medieval.mrugala.net/Paysan/Agriculture.txt) de blé juste assez pour nourrir deux personnes par an.

Cette rareté avait aussi quelques bénéfices. La monnaie nécessitait des métaux précieux tels l'or ou l'argent. La planche à billets gouvernementale était donc limitée. L'art était le fruit d'un temps humain considérable, telle [la Cène](https://fr.wikipedia.org/wiki/La_C%C3%A8ne_%28L%C3%A9onard_de_Vinci%29) de Léonard de Vinci, peinte en 4 ans.


La rareté venait donc d'une pénurie de temps humain à une époque où tout nécessite énormément de temps humain. Cette quantité donnait également une valeur à chaque objet. Au point de lier prix et rareté comme dans l'expression *ce qui est rare est cher*.


<h1>L'Industrie</h1>

**La révolution industrielle n'a eu de cesse de réduire le temps humain nécessaire.** En 3 ans, Gutemberg put imprimer [180 exemplaires](https://fr.wikipedia.org/wiki/Bible_de_Gutenberg#Fabrication) de sa Bible de 1455. La machine à tisser Jacquard de 1801 permettait à un seul ouvrier de tisser [autant que 5](https://global-industrie.com/fr/actualites/153/un-7-juillet-1752-naissait-joseph-jacquard-inventeur-du-metier-a-tisser-mecanique-programmable), ce qui provoqua la révolte des Canuts. Aujourd'hui, les productions agricoles par hectare ont été [multiplié par 10](https://www.terre-net.fr/observatoire-technique-culturale/strategie-technique-culturale/article/les-previsions-de-surfaces-et-de-rendement-en-ble-tendre-par-departement-217-169749.html). Et nous pouvons construire des batiments, tel le Viaduc de Millau, en seulement [3 ans](https://fr.wikipedia.org/wiki/Viaduc_de_Millau).

Cette volonté de produire en masse avec peu de temps humain et encore moins d'énergie humaine a permis de baisser le coût de la vie, mais aussi de faire disparaître la rareté de nos vies ! **Le point culminant d'une société aux ressources infinies arriva avec l'informatique.** Les ressources numériques se copient et partagent quasi instantanément et gratuitement.

Bien sûr, cette perte de rareté dans la société est bénéfique. [90%](https://fr.wikipedia.org/wiki/Sous-alimentation) des humains mangent à leur faim. Le savoir n'a jamais été aussi répandu et accessible. En France, nous vivons dans [90m2](https://www.20minutes.fr/societe/1596367-20150428-cinq-choses-savoir-conditions-logement-francais) en moyenne avec seulement 2.3 personnes par logement. Nous achetons [10 kg](https://www.planetoscope.com/Commerce/1545-.html) de vêtements par an et par habitant. La baisse de rareté est corrélé par une baisse du prix, toutes les deux liées au temps humain présent dans l'objet.

Cependant, ce manque de rareté présente des défauts. Premièrement, nos monnaies sont devenues des monnaies de singe, qui s'effondrent avec leur gouvernement ([liste des hyperinflations](https://fr.wikipedia.org/wiki/Hyperinflation#Les_cas_d'hyperinflation_dans_l'Histoire)). Il faut dire que la garantie de leur valeur ne vient plus d'une quelconque rareté, mais de la seule confiance dans le gouvernement... L'art aussi s'est effondré, les techniques de copie ne permettent plus de distinguer l'original du plagiat, [surtout dans l'art moderne](https://fr.wikipedia.org/wiki/Knoedler#Scandale_et_fraudes_:_la_fermeture). Les notions mêmes d'original et de propriétaire se perdent dans le format numérique où chaque oeuvre est copiable à l'infini à l'identique tel les vidéos, musiques ou photos.

<h1>Blockchain</h1>
La blockchain est une technologie qui compte bien remettre de la rareté dans nos sociétés. Elle réalise l'exploit de rendre des actifs numériques rares, c'est-à-dire de forcer leur existence en quantité limitée. Pour se faire, elle va rendre ces actifs ni falsifiable ni duplicable. (Une véritable prouesse quand on y pense)

Elle s'est d'abord attaquée aux actifs fongibles avec la monnaie Bitcoin, une monnaie où la rareté est garantie par des preuves mathématiques. Ces preuves sont plus exactement du cryptage, ce qui donne leur nom aux cryptomonnaies ou cryptoactifs.

La rareté ne provient plus d'un temps humain, mais de preuves mathématiques. Chaque actif est bien rare sur la blockchain, mais on peut créer autant d'actifs différents que l'on souhaite. Ce premier cas d'usage des actifs fongibles, a donné plein d'autres cryptoactifs comme [Ethereum](https://ethereum.org/fr/) (monnaie), [Tether](https://tether.to/) (tracker valant un dollar), [VeraOne](https://veraone.io/) (tracker valant un gramme d'or) ou encore UNI (action de la compagnie [Uniswap](https://uniswap.org/)).

Puis sont venus les célèbres Non Fungible Token (NFT), pour les actifs numériques non fongibles, qu'on peut voir comme des certificats numériques d'authenticité. Ainsi des oeuvres d'art digitales, physiques ou des objets de luxe peuvent retrouver une rareté et une authenticité forte en se liant à leur NFT.


Nous entrons ainsi dans un monde à double rareté. La rareté *physique* garantie par une dépense de temps humain, perdurera encore sur les métaux précieux ou l'artisanat de luxe. Et de l'autre, tous les actifs se devant d'être rares comme la monnaie ou l'art, mais qui ont perdu leur rareté par le passage de l'industrie puis du numérique. Ceux-ci trouveront refuge sur la blockchain, et sa rareté *numérique* garantie par des preuves mathématique.

Dépourvue de temps humain, la rareté numérique n'a intrinsèquement pas de valeur. **Ce qui est rare n'est plus forcément cher** pour les cryptoactifs, seuls l'usage et la demande de l'actif créeront sa valeur. Ainsi le *vrai* Bitcoin culmine à [50 000 €](https://coinmarketcap.com/), alors qu'une copie : Bitcoin SV n'est qu'à [270 €](https://coinmarketcap.com/), les deux ont pourtant la même rareté (21 millions d'unités) et les mêmes garanties mathématiques. La valeur des cryptoactifs n'est donnée que par le marché. Mais au moins, la blockchain permet la rareté donc la possibilité d'une valeur.


