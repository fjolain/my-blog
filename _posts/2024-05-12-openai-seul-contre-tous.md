---
title: OpenAI face à une concurrence frugale
subtitle: OpenAI, avec sa vision d'une IA géante et coûteuse, se retrouve face à des concurrents proposant des IA plus économiques.
date: 2024-05-12
layout: post
background: /img/2024/05/server.png
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-paR9" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=B19mZFVDJ1Mn&v=3&playerId=ausha-paR9"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>



OpenAI persiste et signe, pour le futur il faut une IA encore [plus grosse que chatGPT-4](https://www.bfmtv.com/tech/intelligence-artificielle/chat-gpt-5-la-nouvelle-version-du-chatbot-d-open-ai-devrait-arriver-cet-ete_AV-202403200480.html). Son CEO, Sam Altman, réclame donc [50 milliards de dollars par an](https://intelligence-artificielle.developpez.com/actu/357315/Sam-Altman-je-me-fiche-de-savoir-si-nous-depensons-50-milliards-de-dollars-par-an-nous-construisons-l-AGI-et-cela-en-vaudra-la-peine-ce-montant-depasse-largement-le-PIB-de-certains-pays), après avoir déjà englouti [11 milliards](https://tracxn.com/d/companies/openai/__kElhSG7uVGeFk1i71Co9-nwFtmtyMVT7f-YHMn4TFBg/funding-and-investors) dont [700,000$ par jour](https://www.businessinsider.com/how-much-chatgpt-costs-openai-to-run-estimate-report-2023-4?r=US&IR=T) rien que pour les coûts des serveurs.

Le pape de l'IA est surtout de plus en plus seul dans sa vision d'IA gigantesque et coûteuse. La startup française [Mistral](https://mistral.ai/fr/) a marqué le début d'une autre voie, celle de petite IA plus économique. Google avec [Gemini](https://gemini.google.com/) et Facebook avec [LLama](https://llama.meta.com/) tentent de satisfaire tout le monde en proposant des versions petites ou grosses de leurs IA.

Plus surprenant, Microsoft pourtant propriétaire d'OpenAI vient de révéler [ses propres IA](https://azure.microsoft.com/en-us/blog/introducing-phi-3-redefining-whats-possible-with-slms/), des petites à l'opposé de chatGPT.

Dans cette fournée de nouvelles IA économes face à l'ogre ChatGPT, on aura presque oublié les annonces d'Apple. Apple a lui aussi révélé [ses modestes IA](https://www.macrumors.com/2024/04/24/apple-ai-open-source-models/) qui ne s'annoncent ni spécialement innovantes, ni plus intelligentes que les autres. Pourtant elles risquent de faire toute la différence.

Car dans le même temps, Apple a sorti son dernier [processeur M4](https://www.apple.com/chfr/newsroom/2024/05/apple-unveils-stunning-new-ipad-pro-with-m4-chip-and-apple-pencil-pro/) six mois seulement après les M3 avec pour ambition de l'orienter vers l'IA. Apple suit une stratégie de longs termes, ses premiers processeurs avec "moteur d'IA", appelé Neuronal Engine par la marque existe depuis [2017](https://en.wikipedia.org/wiki/Apple_A11). Apple utilise déjà des fonctionnalités IA en local, notamment sur les photos. On peut retirer le fond, extraire le texte, rechercher par mot-clé ou reconnaître les visages.

De telles IA demandent peu en puissance de calcul. Apple enchaîne les nouveaux processeurs pour être capable de suivre les nouvelles IA types chatGPT ou Midjourney directement dans l'appareil.

Le cloud, terre promise de la flexibilité et d'optimisation des coûts, est un enfer pour l'IA. Un serveur pour héberger un site web coûte 20€ / mois, un serveur pour héberger une IA coûte [2000 € / mois]([2000 CHF / mois](https://www.ovhcloud.com/fr/public-cloud/prices/). Aussi pouvoir utiliser l'appareil du consommateur pour déployer un produit autour IA est un gain économique conséquent.

Devant la réalité des factures vertigineuses qui s'entassent pour toute entreprise d'IA. OpenAI continue de brûler toujours plus de cash. Tout le monde tente de réduire l'hémorragie avec des IA davantage frugales en puissance de calcul.

Apple va être la seule entreprise proposant des services IA sans surcoût supplémentaire pour lui. En utilisant directement l'appareil de l'utilisateur pour faire fonctionner l'IA, Apple récupère un gain économique pour lui et une raison d'acheter le dernier produit pour le consommateur.

La guerre de l'IA grand public fait rage. OpenAI reste le leader, mais un leader dispendieux face à une concurrence proposant des alternatives bien moins coûteuses pour eux.

