---
title: Le temps, cette escroquerie
subtitle: (accompagné d'un super projet)
date: 2019-05-10T20:49:34+01:00
layout: post
background: /img/2019/05/temps.jpg
categories: [project]
tags: [société]
---
Après maintenant un siècle de travail, la coupe est pleine, plusieurs scientifiques souhaitent abandonner le temps. Mais quelle mouche les a piqué ? Supprimer le temps de nos vie est-il possible ? Nous allons voir que le temps que nous connaissons n&rsquo;a rien d&rsquo;universel, et s&rsquo;apparente plus à un escroquerie.

### Mais quelle mouche les a piqué ?

Pourquoi des scientifiques comme Carlo Rovelli souhaitent-il retirer le temps de leurs équations ? Pour eux, le temps se réfère directement à la variable « _t_ » que l&rsquo;on retrouve dans de nombreuses équations. On la trouve dans la mécanique quantique où elle est aléatoire, on ne peut pas connaître précisément le temps d&rsquo;une particule. On la retrouve aussi dans la mécanique de la relativité générale où elle est étirable et n&rsquo;existe pas en certains points comme le centre des trous noirs ou le BigBang.

Voilà un siècle que des scientifiques tentent de fusionner ces deux mécaniques pour n&rsquo;en former qu&rsquo;une seule. C&rsquo;est peut être la même lettre « _t_« , derrière se cachent des temps différents. Devant cette épineuse question. Certains scientifiques envisagent de supprimer le temps des équations, plus de « _t_« , plus de problème.

Mais qui est le coupable d&rsquo;avoir insérer « _t_ » dans les équations ? Il s&rsquo;agit d&rsquo;un coup de génie de Newton. Pour décrire le mouvement des objets, de n&rsquo;importe quel objet, Newton avait besoin d&rsquo;un grand ordonnanceur. Une variable « imaginaire », comme il le dit lui même, qui donnera le tempo à toutes les autres. Il la nomma _temps_, et l&rsquo;inséra par petit _t_ dans sa célèbre équation.

![newton]({{ site.baseurl }}/img/2019/05/newton.png)

Oui pour Newton, il s&rsquo;agit d&rsquo;un temps imaginaire pas de doute :

  * son temps est linéaire, alors que le notre est cyclique (jour/nuit, printemps/été/automne/hiver, &#8230;)
  * son temps est homogène, alors que le notre ne s’écoule pas toujours pareil. Dans les réunions il semble interminable. Devant un bon film, on ne le voit pas passer.
  * son temps est parfaitement dénombrable, le notre est insaisissable

Bref son temps est purement mathématique, le notre est spirituel&#8230; Mais pour nous, faisons nous encore la différence entre ce temps mécanique et notre temps naturel ?

### Comment l&rsquo;imaginaire est devenu réel ?

Si tout le monde au début était d&rsquo;accord pour dire que ce temps _t_ que l&rsquo;on peut mesurer, compter, découper en heures, minutes, secondes, était un faux temps. Pourquoi sommes-nous aujourd&rsquo;hui réglés dessus ? Il faut arriver au boulot à 9h30, manger à 12h30. Commencer le cours de tennis à 18h en faire pendant une heure, avoir fini de manger avant 21 pour regarder notre série jusqu&rsquo;à 22h30&#8230;

#### Les horloges

Les horloges sont des coupables idéaux. L&rsquo;équation de Newton prend appui sur _t_ pour décrire des mouvements. Il ne restait plus qu&rsquo;à concevoir un mouvement autour de _t_. Huygens, le premier, compris que le mouvement d&rsquo;un pendule est régulier, en choisissant bien les caractéristiques de son pendules, celui-ci pouvait battre à 1s, et rendre le temps _t_ concret.

Les horloges ne donnent pas le temps, elles le créent. Comment peuvent-elles mesurer un temps imaginaire ? Le mouvement du pendule dans l&rsquo;horloge, du ressort dans la montre ou des atomes de césiums dans les horloges atomiques permettent de mettre en lumière le grand ordonnanceur de Newton. Une fois cette ordonnanceur visible, le temps imaginaire devient réel.

J&rsquo;insiste sur cette escroquerie planétaire, les horloges ne peuvent mesurer le temps, elles ne font que le matérialiser.

#### Les machines

Si seulement, les horloges étaient restées des bizarrerie de chercheurs servant juste à montrer le temps imaginaire, un peu comme toutes ces expériences servant à mettre en avant les principes physiques, on serait tranquille.

En plus des horloges, les scientifiques ont inventé un tas de machines : machines pour rouler sur des rails, machines pour plier la tôle, machine pour extraire le pétrole, machines pour chauffer. Toutes ces machines sont fabriquer d&rsquo;après l&rsquo;équation de Newton, elles fonctionnent donc avec le temps imaginaire.

Le hic, avec ces machines, c&rsquo;est qu&rsquo;elle sont peu flexibles et nécessitent des humains pour fonctionner. C&rsquo;était donc aux humains de s&rsquo;adapter à eux et à leur temps. C&rsquo;est le début de l&rsquo;ère industrielle, du travail à la chaîne, du pointage et des 3-8. Les travailleurs étant maintenant parfaitement cadencés, toute la société devait l&rsquo;être. Les enfants aussi devait venir en classe avec une ponctualité de machine. Pareil pour l&rsquo;organisation des transports.

Le temps imaginaire est devenu le temps par défaut.

### L&rsquo;occasion manquée de l&rsquo;informatique

Ce problème est maintenant un paradoxe. Les machines informatiques ont remplacé les machines mécaniques dans notre vie de tout les jours. Le temps imaginaire appliquait à la mécanique paraissait horrible, maintenant qu&rsquo;il est appliqué à l&rsquo;informatique, il est totalement inhumain. Nos ordinateurs et smartphones traitent les informations dans la seconde, ils nous sollicitent constamment à toute heure du jour et de la nuit. 

Aujourd’hui, beaucoup d&rsquo;humains tentent de les suivre en étant « connectés » à travers eux. Le carnage ne fait que commencer. L&rsquo;horrible pression exerçait par l&rsquo;industrialisation sur les humains va s&rsquo;accélérer avec l&rsquo;informatique.

Pourtant, les machines ont changé, elles ne sont plus dépendantes de nous, et elles sont totalement flexibles. Votre ordinateur ne s&rsquo;allume pas à heure fixe et même si c&rsquo;est le cas, il n&rsquo;a pas besoin de vous pour démarrer. Vous pouvez laisser votre ordinateur ou votre smartphone sans surveillance, rien de grave va arriver. Nos anciennes machines mécaniques sont maintenant entièrement pilotées par informatique, elles n&rsquo;ont plus besoin d&rsquo;un humain à coté. Le paradoxe de notre société est ici : avec l&rsquo;arrivé de l&rsquo;informatique, on a l&rsquo;occasion de se séparer du temps imaginaire qui a fait tant de mal au XX siècle. Au lieu de ça, on s&rsquo;est encore un peu plus éloigné de notre temps naturel.

Jusqu&rsquo;où irons-nous ? Jusqu&rsquo;à ce que nos cerveaux lâchent ? Comme les scientifiques, nous devons nous aussi sortir la variable _t_ de nos vies.

### Mon projet d&rsquo;horloge thermodynamique

Dans la physique, il y a deux temps. Le temps imaginaire mécanique, et le temps thermodynamique. Le temps thermodynamique est défini par le deuxième principe de la thermodynamique :

> Toute <a href="https://fr.wikipedia.org/wiki/Transformation_thermodynamique">transformation</a> d&rsquo;un <a href="https://fr.wikipedia.org/wiki/Syst%C3%A8me_thermodynamique">système thermodynamique</a> s&rsquo;effectue avec augmentation de l&rsquo;<a href="https://fr.wikipedia.org/wiki/Entropie_(thermodynamique)">entropie</a> globale incluant l&rsquo;entropie du système et du milieu extérieur. On dit alors qu&rsquo;il y a création d&rsquo;entropie. -- Wikipedia

Derrière cette obscure phrase se cache un principe simple. Dans nos vies, on peut voir naturellement une assiette tombait et se casser en milles morceaux. Mais on ne verra jamais les milles morceaux se regrouper pour former une assiette. L&rsquo;entropie est le nom que l&rsquo;on donne au désordre en science. On peut donc traduire le deuxième principe par:

> L&rsquo;accroissement du désordre indique le passage du temps. -- Perso

Il n&rsquo;est pas question de mesurer le temps précisément ici. La thermodynamique indique juste le passage du temps.

Je me suis mis en tête de concevoir la première horloge qui ne ment pas ! Une horloge qui ne crée pas un temps mécanique mais permet de mesurer le passage du temps thermodynamique.<figure class="wp-block-image">

![horloge]({{ site.baseurl }}/img/2019/05/horloge.jpg){:width="100%"}

Cette horloge fonctionne ainsi : On initialise l&rsquo;horloge en plaçant le tas de sable que d&rsquo;un coté. Puis le bac tourne lentement, les pignons tout autour vont, sur plusieurs jour, étaler le sable. En regardant cette horloge jour après jour, on remarque que le sable s&rsquo;étale, le désordre croit dans le bol, le passage du temps est manifeste. Quand le sable est totalement étalé, on le replace d&rsquo;un coté.

Ci-dessous un timelapse sur 20h.

<iframe src="https://www.youtube.com/embed/mZH0txAYNaM" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" width="560" height="315" frameborder="0"></iframe>