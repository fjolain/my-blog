---
title: L'Etat feint d'être le berger pour mieux nous manger. Enjeux sur nos données 2/3
subtitle: Chaque État vise le monopole sur nos données et le chiffrement. Est-ce réaliste ?
date: 2023-02-08
layout: post
background: /img/2023/02/masque.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-Tidc" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=brDkxfKD4jl2&v=3&playerId=ausha-Tidc"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Nos données sont convoitées. Dans [le dernier article](https://www.codable.tv/le-business-derriere-votre-vie-priv%C3%A9e), nous avons analysé l'économie derrière la violation de notre vie privée et les acteurs qui font vivre cet écosystème.

Il est temps de s'intéresser aux États, car leur jeu est trouble au plutôt double. Bien évidemment qu'ils souhaitent nous espionner, mais ils ne veulent pas qu'un autre gouvernement le fasse. Et par-dessus tout, il ne va pas être espionné.

Ce grand bluff donne une chorégraphie mondiale où les États tentent de s'accaparer un monopole sur les données des citoyens et les moyens de chiffrement.

Si vous souhaitez savoir comment protéger sa vie privée, je vous renvoie vers [une de mes vidéos](https://www.youtube.com/watch?v=Ky0JBbEeSwQ) sur le sujet.

<h2>Monopole sur les données</h2>

L'espionnage par le gouvernement n'a de limite que la technologie. Avec l'avènement d'internet et les réseaux sociaux, on peut dire qu'il n'y a plus de limite. Le gouvernement peut espionner l'ensemble de ses citoyens, leurs conversations, leurs déplacements ou leurs transactions.

Le gouvernement chinois offre un showroom de ces nouvelles technologies pour espionner ses citoyens sans pouvoir être espionné en retour. C'est le signe d'une dérive autoritaire : l'état demande toujours plus de transparence pour ses citoyens, tout en augmentant l'opacité et le secret dans ses affaires.

Cette dérive apparaît aussi aux États-Unis qui ont lancé un espionnage massif de la population avec la [NSA](https://fr.wikipedia.org/wiki/National_Security_Agency). Et qui poursuit violemment toute personne révélant les secrets d'État tels Assenge ou Snowden.

La France n'est pas en reste, [la loi renseignement de 2015](https://fr.wikipedia.org/wiki/Loi_relative_au_renseignement) oblige les fournisseurs d'accès internet à analyser les connexions de leurs utilisateurs et à garder l'historique un an.

Au même moment où les impôts utilisent les photos satellites pour [traquer les piscines](https://www.midilibre.fr/2021/10/12/le-fisc-lance-la-chasse-aux-piscines-non-declarees-9845116.php) des Français, [le maigrichon patrimoine d'Emmanuel Macron](https://www.marianne.net/politique/macron/mais-ou-est-passe-le-pognon-de-dingue-que-macron-aurait-palpe-chez-rothschild) ne semble pas lever de doute, alors qu'il criait lui-même avoir fait des millions chez Rotschild.

Durant le covid, les Français devaient être scannés pour aller au restaurant, alors que Macron prenait toutes ses décisions dans le secret des conseils de défenses.

Les JO de Paris semblent l'excuse toute trouvée pour pousser le saccage de notre vie privée en France. On envisage déjà de [collecter nos empreintes pour aller aux stades](https://generate.fr/uniris-decroche-le-label-du-csf-pour-les-jeux-olympiques-de-paris-en-2024) ou de généraliser la [caméra de surveillance avec reconnaissance faciale](https://www.francetvinfo.fr/les-jeux-olympiques/jo-2024/jo-2024-le-senat-adopte-largement-le-projet-de-loi-olympique_5632772.html). J'espère qu'au moment où le citoyen réclamera les enregistrements, ils ne vont pas disparaître comme les récentes [émeutes aux Stades de France](https://www.lepoint.fr/faits-divers/stade-de-france-les-images-de-videosurveillance-supprimees-par-la-federation-09-06-2022-2478972_2627.php).

Les États veulent nos données, mais ils les veulent pour eux uniquement. Alors que le gouvernement américain ne voyait aucun mal à la collecte de données par Google ou Facebook, il est vent debout contre celle du chinois [Tiktok](https://www.lefigaro.fr/secteur/high-tech/le-patron-de-tiktok-entendu-par-le-congres-americain-fin-mars-20230130).

Pareil pour [Huawei](https://fr.wikipedia.org/wiki/Huawei), interdit aux États-Unis, car la maison blanche ne veut pas d'appareil chinois dans son infrastructure réseau. Les États-Unis savent très bien le pouvoir d'écoute quand on exporte son matériel dans les réseaux des voisins. L'affaire Snowden a montré que le matériel américain [Cisco était vérolé par la NSA](https://www.numerama.com/politique/29353-nsa-routeurs-serveurs-backdoor.html), afin d'espionner les réseaux télécoms étrangers.

Ce que veulent les états, c'est le monopole sur l'espionnage des citoyens. Ne comptez pas sur un gouvernement pour protéger votre vie privée.

<h2>Monopole sur le chiffrement</h2>

L'objectif des États est donc d'espionner... mais sans être espionné ! Il se retrouve dans une course infinie pour concevoir des outils de chiffrement et les casser...

Par exemple, la NSA, reconnue comme le nouvel oeil de Moscou, est aussi en charge de certifier les algorithmes de chiffrement.

Un département de la NSA a par exemple sélectionné le chiffrement [AES](https://fr.wikipedia.org/wiki/Advanced_Encryption_Standard) comme nouveau standard en 2001. Et depuis ce jour, un autre département essaye de le craquer.

L'affaire Snowden a aussi montré que la CIA dépense de l'argent pour corrompre le réseau TOR. [TOR](https://fr.wikipedia.org/wiki/Tor_(r%C3%A9seau)) est un outil pour brouiller ses traces sur internet et ainsi naviguer plus anonymement. Le réseau TOR que la CIA essaye de corrompre a été initialement conçu et financé par United States Naval Research Laboratory, pour ses besoins personnels.

Un autre exemple à Bruxelles, la Commission européenne pousse depuis plusieurs années à [interdire les messageries chiffrées](https://www.developpez.com/actu/341124/-Stop-au-plan-de-surveillance-de-masse-propose-par-la-Commission-de-l-UE-lance-un-editeur-de-VPN-car-l-UE-entend-scanner-toutes-les-correspondances-privees-pour-lutter-contre-la-pedopornographie/) comme ProtonMail, Telegram ou Signal.

Ce même [Telegram utilisé par l'équipe d'Emmanuel Macron](https://www.lejdd.fr/Politique/macron-comment-la-messagerie-cryptee-telegram-est-utilisee-au-coeur-du-pouvoir-3612632). Et ce même ProtonMail que l'[UE a financé](https://cordis.europa.eu/project/id/848554/fr) dans son plan Horizon 2020.

Il est évident que le fonctionnaire qui a financé ProtonMail n'est pas le même qui pousse l'interdiction de messagerie chiffrée. Mais oui, l'EU a besoin d'éviter la surveillance américain et finance des projets européens pour se passer de Google ou Microsoft. Et oui, l'EU souhaite espionner ses citoyens.

Contrairement au monopole sur la vie privée, le monopole sur le chiffrement est impossible. Soit votre chiffrement est inviolable pour tout le monde (même pour vos citoyens), soit il est faible pour tout le monde y compris pour vous.

Espionner sans être espionné n'est pas possible technologiquement... ou presque.

L'informatique quantique rebat les cartes. D'un côté les ordinateurs quantiques sont capables de casser le chiffrement actuel. De l'autre l'internet quantique permet un nouveau chiffrement incassable.

Les États investissent donc massivement dans cette technologie ([cf précédent article](https://www.codable.tv/pourquoi-l-ordinateur-quantique-passionne-les-militaires/)). Le vainqueur pourra espionner sans être espionné, du moins pour un temps.

Dans le viol de notre vie privée par les gouvernements, les limites sont technologiques. L'informatique a rendu ces limites quasi infinies. Heureusement l'informatique apporte aussi des outils pour protéger sa vie privée. Dans le prochain article, nous verrons, comment la technologie peut mettre fin au saccage.

