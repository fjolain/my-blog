---
title: Copions Elon Musk, tenons les par les bourses !
subtitle: L'arrivée d'Elon Musk dans les actionnaires de Twitter est une leçon pour reprendre le pouvoir.
date: 2022-04-16
layout: post
background: /img/2022/04/bull.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [société]
---

<iframe frameborder="0" loading="lazy" id="ausha-CPTz" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=oagXMszROrVk&v=3&playerId=ausha-CPTz"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Entre Elon Musk et Twitter, c'est une histoire d'amour qui dure depuis des années. Le milliardaire plébiscite la plateforme pour partager ses réflexions plus ou moins abouties, ses annonces plus ou moins réalistes et ses sondages plus ou moins utiles.

Il y a eu quelques querelles comme dans tout couple. Notamment sur des tweets à propos Tesla ou Bitcoin qui ont affolé les cours de bourses. Elon Musk s'est plusieurs fois fait rappeler à l'ordre [par la SEC](https://www.lesechos.fr/industrie-services/automobile/la-sec-epingle-tesla-pour-son-manque-de-supervision-des-tweets-delon-musk-1320070).

Mais au final c'est le changement de politique de Twitter et sa censure qui a mis en péril le couple. La suppression du [compte de Donald Trump](https://blog.twitter.com/en_us/topics/company/2020/suspension) puis la [démission de Jack Dorsey](https://www.codable.tv/le-regne-des-entreprises-sans-tete/) fut la goutte de trop.

A-t-elle point que Elon Musk se demanda s'il ne fallait pas [rebâtir un nouveau Twitter](https://twitter.com/elonmusk/status/1507907130124222471?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1507907130124222471%7Ctwgr%5E%7Ctwcon%5Es1_c10&ref_url=https%3A%2F%2Fwww.journaldugeek.com%2F2022%2F03%2F29%2Felon-musk-veut-creer-son-reseau-social-et-envisage-de-racheter-twitter%2F). Cette idée germa également dans la tête d'un proche de Trump qui développa la plateforme [*Gettr*](https://fr.wikipedia.org/wiki/Gettr). Elle connaît un succès modéré. Ce qui reste supérieur à Trump lui-même, qui eut aussi l'idée, et lança sa plateforme [*Truth Social*](https://fr.wikipedia.org/wiki/Truth_Social), un flop à l'heure actuelle.

Fort de ces deux tentatives aux résultats peu probants. Elon Musk a pris une autre décision. Il [acheta](https://www.numerama.com/tech/909761-elon-musk-achete-des-millions-dactions-twitter-pour-peser-sur-son-avenir.html#:~:text=Le%2014%20mars%202022%2C%20Elon,%2C5%20millions%20d'actions.) des actions de Twitter pour devenir le premier actionnaire et faire bouger les choses de l'intérieur. Une leçon pour tous, que l'on peut réutiliser !

<h2>Une méthode à réutiliser</h2>

Elon Musk rappelle qu'avec une entreprise boursière, on n'est pas obligé de reste dehors à protester. On peut tous prendre des actions et rentrer à l'intérieur.

Évidement quand on est milliardaire, c'est plus simple. Mais des mouvements civiques portés par des associations pourraient voir le jour. Voici quelques exemples :

La Société Générale pèse [25,8 milliards d'euros](https://fr.wikipedia.org/wiki/Soci%C3%A9t%C3%A9_g%C3%A9n%C3%A9rale) et compte [10 millions de clients](https://www.societegenerale.com/fr/actualites/communiques-de-presse/societe-generale-deux-initiatives-strategiques-majeures-dans-la-banque-de-detail-en-france#:~:text=D'une%20part%20avec%20la,de%2010%20millions%20de%20clients.). Il suffit que chaque client achète 8 actions soit 180€, pour que les clients deviennent le premier actionnaire du groupe avec 7%, devant BlackRock. Une banque qui appartient à ses clients, ça change tout…

Autre exemple avec Altice que le youtubeur français Idriss Aberkane a dénoncé [sur sa chaîne](https://www.youtube.com/watch?v=GWtQN2v4yzk) suivie par 500 000 abonnés. Il suffirait que chaque abonné achète 10 actions pour 100€ afin de détenir 1% de l'entreprise et ainsi faire de leur youtubeur un actionnaire important.


De même, les anti-zemmour pourraient acheter des actions Vivendi. Ou les écolos pourraient se retrouver actionnaires de Total pour demander des comptes au groupe.


L'épisode de [WallStreetBet](https://fr.wikipedia.org/wiki/WallStreetBets#Affaire_GameStop) a montré la force de la foule en bourse. Des internautes ont réussi à mettre à genoux des fonds d'investissement en s'organisation autour de l'action de GameStop.

De plus, il n'est pas nécessaire d'être  majoritaire et de peser dans les décisions. Le simple fait d'être actionnaire nous donne [des droits](https://solutions.lesechos.fr/compta-gestion/c/droits-de-vos-actionnaires-11693/) :
- le droit d'information, y compris de documents non publics comme certains rapports
- le droit d'adresser des questions au dirigeant. Lequel doit y répondre durant l'Assemblée Générale
- le droit de présence en Assemblée Générale
- le droit de vote
- le droit de défense des intérêts de la société. Les actionnaires peuvent saisir la justice et demander l’annulation d’une décision ou la révocation d’un membre du directoire s’ils estiment qu’il y a faute grave de gestion.

On pourrait ainsi acheter un paquet d'action Pfizer et se regrouper en tant qu'actionnaires pour faire valoir nos droits sur les résultats des tests du vaccin Covid et ses effets secondaires.

Sait-on jamais, nos droits et notre vote ne valent plus rien en tant que citoyen. Peut-être valent-ils plus de choses en tant qu'actionnaires ?
