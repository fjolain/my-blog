---
title: Quand les youtubeurs détruisent la politique
subtitle: Vous ne comprenez rien aux politiciens, voici un bout d'explication
date: 2017-06-05T09:27:10+01:00
layout: post
background: /img/2017/06/youtube.jpg
categories: [article]
tags: [politique, démocratie]
---
C&rsquo;est vraiment bizarre de voir autant de petits youtubeurs devenir de grands politologues sur leur chaîne au moment de la présidentielle. Au début tout cela m&rsquo;amusait de les voir parler politique en partant toujours dans les extrêmes. On se serait cru sur une pub-arnaque d&rsquo;internet avec : « Cette homme gagne 5000€ en faisant rien. », « Je vais vous montrer comment X à détourné l&rsquo;argent des Français. » ou encore « Voici la plus grosse arnaque jamais faite et elle est légale. ». Bref du bon gros pute à cliques. Ces mêmes youtubeurs nous montraient déjà de très bonnes techniques sur Pokemen Go, et je pense qu&rsquo;ils vont vite devenir expert du fidget spinner dans peu de temps, histoire de nous montrer leurs « Ultime technique fidget spinner tourne à l&rsquo;infini ».

Le problème est que leurs analyses politiques détruisent la politique. Je m&rsquo;en suis rendu compte avec la vidéo de Macron sur _Osons Causer_. Le chef d’œuvre est [ici](https://www.youtube.com/watch?v=56hLRvsWbCA). En gros le youtubeur n&rsquo;est pas content car le pack à 5 milliards d&rsquo;euro de Macron pour les entreprises a créé zéro emploi. Un scandale ? Pas vraiment.

Aucune politique ne peut changer le pays en moins de 3 ans&#8230; Imaginez vous êtes patron d&rsquo;une TPE de 7 employés. Le gouvernement vous offre 7% des salaires de vos employés. C&rsquo;est bien, mais vous n&rsquo;êtes pas sûr que cela va durer. Les élections approchent et si Le Pen ou Fillon arrivent au pouvoir, vous pouvez dire adieu à votre aide. Alors est-ce que le lendemain de l&rsquo;annonce de l&rsquo;aide vous allez embaucher une personne ? Non, évidement, l&#8217;embauche se fera quand l&rsquo;entreprise aura investi l&rsquo;argent des 7% ailleurs, créé de nouveaux marchés ou autre.

Ces youtubeurs sont donc idiots de crier au scandale quand une loi politique ne donne pas de conséquences toute de suite. Mais ils sont également néfastes.

Imaginez maintenant que vous êtes un homme/femme politique. Vous venez d&rsquo;entrer au gouvernement comme ministre. Vous visez secrètement d&rsquo;ici 4 ans la présidence. Vous savez que ces youtubeurs ont un rôle important comme meneurs d&rsquo;opinions chez les gens. Or ils ne vont pas hésiter à vous détruire si vous faites une loi qu&rsquo;ils jugent « inutile », à savoir une loi non immédiate. Il ne vous reste plus qu&rsquo;à rejeter toutes les lois importantes pour le pays et à faire du clientélisme. Au lieu d&rsquo;aider les entreprises, on va donner des subventions aux travailleurs. Au lieu de revoir l&rsquo;éducation, on va abaisser le niveau du bac. Au lieu de construire de nouveaux logements, on va subventionner les loueurs&#8230;

Ces youtubeurs par leur débilités poussent les politiques à réfléchir dans l&rsquo;immédiat. Pour internet, une loi « utile » est une loi qui fait gagner 5 points dans les sondages en une semaine. Toutes les critiques ne sont pas bonnes à prendre, spécialement celles qui ont pour seul but de générer des cliques.