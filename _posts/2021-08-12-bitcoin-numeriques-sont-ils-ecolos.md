---
title: Bitcoin et le numérique sont-ils écologiques ?
subtitle: Le progré ne doit pas être un bouc-émissaire des écolos.
date: 2021-08-12
layout: post
background: /img/2021/08/forest.jpeg
published: www.contrepoints.org/2021/08/20/403481-bitcoin-et-le-numerique-sont-ils-ecologiques
categories: [article, podcast]
tags: [bitcoin, technologie]
---

<iframe frameborder="0" loading="lazy" id="ausha-jNE2" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=yAdaMtMxQNg5&v=3&playerId=ausha-jNE2"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Ces phrases ont comme un air de déjà vu : "Un mail [consomme autant](https://www.lefigaro.fr/decideurs/un-mail-est-aussi-energivore-qu-une-ampoule-allumee-toute-la-journee-20190516) qu'une ampoule pendant une heure", "Les datacenters [polluent autant](https://www.leparisien.fr/environnement/co2-internet-pollue-autant-que-les-avions-07-10-2015-5163373.php) que l'aviation soit 2% du CO2 mondial" ou encore "Bitcoin consomme [autant que la Suède](https://reporterre.net/Le-bitcoin-monnaie-virtuelle-mais-gouffre-environnemental-reel)".

Le numérique est devenu le bouc-émissaire des écologistes. Ce progrès extraordinaire pour l'humanité se transforme en son pire défaut par la magie des prophéties écologiques. Il est évident que tous ces services consomment et polluent comme toutes activités humaines. La question n'est pas de savoir si elles polluent, mais de savoir le ratio entre utilités / pollution et de comparer avec d'autres industries.

<h1>Numérique</h1>
<h2>Un ratio utilité / pollution exceptionnel</h2>

Reprenons un exemple du début : "Les datacenters [polluent autant](https://www.leparisien.fr/environnement/co2-internet-pollue-autant-que-les-avions-07-10-2015-5163373.php) que l'aviation soit 2% du CO2 mondial". Le but de cette phrase est de rattacher le numérique à une industrie polluante comme l'aviation. Or en comparant les industries, on se rend compte de l'incroyable efficacité énergétique d'Internet.

Premièrement en termes d'usagers. Pour une même pollution, l'aviation sert [845 millions](https://www.lesechos.fr/industrie-services/tourisme-transport/aerien-1-de-la-population-mondiale-cause-la-moitie-des-emissions-de-co2-1266064) de clients par an, avec 1% de la population mondiale responsable de la moitié des trajets. Alors qu'Internet est utilisé par [4.6 milliards](https://www.blogdumoderateur.com/30-chiffres-internet-reseaux-sociaux-mobile-2021/) de personnes. À pollution égale, Internet sert à 6 fois plus d'humains que l'aviation.

Deuxièmement en termes d'usages. L'aviation ne sert qu'à voyager dont [75%](https://www.tourmag.com/Aerien-38-des-voyages-d-affaires-remplaces-par-la-visioconference_a109133.html) pour le tourisme. Internet, lui, sert à tout : communication (visio, mails, messageries), financier (paiement, épargne, investissement), organisation (réservation d'hôtels, restaurant, outils de collaboration, prise de rendez-vous médical, administratif, etc).

Qui peut encore comparer les deux comme une même pollution ? L'aviation sert en grande partie à une élite pour partir en vacances. Le numérique et Internet apportent des services essentiels à plus de la moitié de la planète.

<h2>Un Miracle de mutualisation</h2>
Permettez que j'insiste sur le miracle de mutualisation offert par le numérique. Le numérique transforme n'importe quels contenus ou services en bytes abstraits. Un seul smartphone peut ainsi gérer des centaines de fonctionnalités, un seul datacenter peut gérer des milliers de sites Internet. Un processeur, câble sous-marin ou disque dur sert pour des milliers d'applications en même temps.

BNP Paribas a besoin de [2 202](https://fr.wikipedia.org/wiki/BNP_Paribas) agences bâties, chauffées et éclairées en France pour satisfaire [7 millions](https://fr.wikipedia.org/wiki/BNP_Paribas) de clients. Boursoramas n'a qu'une plateforme cloud répartie dans quelques datacenters pour gérer [2.5 millions](https://billetdebanque.panorabanques.com/boursorama-banque/nombre-clients-boursorama-banque) de clients.

LaPoste dispose de [7 700](https://fr.wikipedia.org/wiki/La_Poste_(entreprise_fran%C3%A7aise)) agences bâties, chauffées et éclairées pour servir 68 millions de Français. Gmail repose sur [13](https://www.digora.com/fr/blog/infographie-11-faits-sur-les-datacenters-de-google) datacenters pour gérer [1,5 milliard](https://www.cnetfrance.fr/news/gmail-compte-desormais-15-milliard-d-utilisateurs-39875717.htm) d'utilisateurs.

Internet et le numérique offrent un niveau de mutualisation des ressources extraordinaire. Internet est devenu un besoin quasi vital utilisé par 4.6 milliards de personne, or il n'est responsable *que* de 2% du CO2 mondial, contre [14.3%](https://www.wri.org/data/world-greenhouse-gas-emissions-2005) pour le transport, [16.9%](https://www.wri.org/data/world-greenhouse-gas-emissions-2005) pour le chauffage, [13.8%](https://www.wri.org/data/world-greenhouse-gas-emissions-2005) pour l'agriculture.

<h2>Marges d'amélioration</h2>
Le numérique est donc un bouc émissaire illégitime de la pollution. Il a cependant deux marges de progressions:

- **Éviter l'obsolescence programmée** Les objets électroniques ont une durée de vie limitée et sont peu réparable. Quand on sait que 80% de la pollution vient de leur fabrication, alors améliorer la longévité des objets est une marge d'amélioration toute trouvée.

- **Diminuer la vidéo** Il a d’énormes différences entre le rapport utilité / pollution au sein même des services en ligne. Tous les articles de Wikipédia France sans les images pèsent seulement [9 Go](https://www.01net.com/astuces/comment-telecharger-tout-wikipedia-sur-iphone-ou-android-1587458.html), ce qui représente le [poids d'un film sur Netflix en 4K](https://help.netflix.com/fr/node/87). Ainsi, Wikipédia est le [5e site le plus visité du monde](https://www.visualcapitalist.com/the-50-most-visited-websites-in-the-world/), alors que sa bande passante (source de pollution) est négligeable. De l'autre coté Netflix est le [17e](https://www.visualcapitalist.com/the-50-most-visited-websites-in-the-world/) en termes de visites, mais le [1er](https://www.businessinsider.com/which-services-use-the-most-bandwidth-2015-12) en bande passante en s'accaparant 37% du total.

<h1>Bitcoin</h1>

Déjà, il est important de rappeler que la prédiction de la consommation du Bitcoin est un exercice impossible, comme vu dans un précédent [article](https://www.contrepoints.org/2021/07/06/401012-pourquoi-ne-peut-on-pas-predire-la-pollution-de-bitcoin). Pour s'en rendre compte, il suffit de rappeler [une étude de 2017](https://www.newsweek.com/bitcoin-mining-track-consume-worlds-energy-2020-744036) très médiatisée qui prédisait que Bitcoin consommera toute l'énergie mondiale en 2020. Nous sommes en 2021 est la consommation actuelle est estimée à [0.37%](https://cbeci.org/cbeci/comparisons) de l'électricité mondiale ou encore [0.07%](https://cbeci.org/cbeci/comparisons) du mix énergétique mondial.

<h2>Le Ratio utilité / pollution</h2>
Le Bitcoin est un réseau qui permet d'échanger et de conserver la monnaie Bitcoin d'une manière sûre et sans État ou banque. Bitcoin apporte une alternative aux monnaies étatiques soumises à la dévaluation constante et à l'inflation. À l'heure où Biden imprime [10%](https://www.lemonde.fr/international/article/2021/03/10/que-prevoit-l-enorme-plan-de-relance-voulu-par-joe-biden-aux-etats-unis_6072643_3210.html)  du PIB américain, où la [BCE rachète en masse](https://www.latribune.fr/economie/union-europeenne/rachat-de-dettes-par-la-bce-vers-la-fin-des-mesures-d-urgence-en-mars-887844.html) sur les marchés avec son argent imaginaire, où le Venezuela vient de retirer [6 zéros sur sa monnaie](https://journalducoin.com/actualites/face-a-lhyper-inflation-le-venezuela-va-retirer-six-zeros-de-sa-monnaie-la-crypto-solution-ultime/), souhaitez-vous renoncer à un système alternatif ?

Encore une fois, comparons. On peut compare les [82 TWh](https://cbeci.org/cbeci/comparisons) que consomme Bitcoin par an au [131 TWh](https://cbeci.org/cbeci/comparisons) que consomme l'or, son équivalent physique. Bitcoin consomme 37% moins que l'or, en évitant la pollution des sols et le travail des enfants dans les mines.

Moins que Bitcoin, mais dans le même ordre de grandeur, on peut citer l'éclairage de Noël responsable de [6 TWh](https://phys.org/news/2015-12-christmas-energy-entire-countries.html) aux États-Unis. Malheureusement, aucune donnée n'indique la consommation mondiale, mais elle doit se compter en plusieurs dizaines de Terra-Watt-heures.

Qui devrait porter le titre de "catastrophe écologique" les inutiles guirlandes de Noël ou une monnaie mondiale à l'abri des faillites des états ?

<h2>Bitcoin première monnaie écologique ?</h2>

On peut même dire que Bitcoin est la seule monnaie faite pour la décroissance.

En effet, une différence importante entre nos monnaies étatiques et Bitcoins repose sur l'émission monétaire. Une monnaie étatique est créée ex nihilo par la banque centrale ([9%](https://fr.wikipedia.org/wiki/Cr%C3%A9ation_mon%C3%A9taire#Cr%C3%A9ation_mon%C3%A9taire_par_les_banques_priv%C3%A9es)) et par les banques privées via l'octroi de crédits ([91%](https://fr.wikipedia.org/wiki/Cr%C3%A9ation_mon%C3%A9taire#Cr%C3%A9ation_mon%C3%A9taire_par_les_banques_priv%C3%A9es)). Notre monnaie doit toujours être soutenue par du crédit soit vers les entreprises pour soutenir la croissance, soit vers les ménages pour stimuler la consommation et donc la croissance. De plus comme la création monétaire augmente constamment, la monnaie se dévalue provoquant l'inflation. Puisque la monnaie perd de valeur chaque année, elle encourage encore à la consommation.

Les monnaies étatiques sont basées sur une nécessité de croissance infinie et incite donc à toujours plus de consommation, sinon c'est tout le système qui s'effondre.

Pour Bitcoin, l'émission est figée à 21 millions de Bitcoins. La cryptomonnaie est indépendante d'une économie en croissance ou en décroissance ou du niveau de consommation.

<h2>Marges d'amélioration</h2>
Pour arriver à décentraliser une monnaie, il faut mettre en place un système de consensus décentralisé. Bitcoin inventa un consensus révolutionnaire appelé *Proof of Work* (PoW). Les agents participants au consensus doivent apporter une preuve de dépense d'énergie. La pollution de Bitcoin provient de cette dépense d'énergie obligatoire.

Depuis l'invention du PoW,  les développeurs ont imaginé et déployé d'autres systèmes de consensus décentralisé moins énergivores (Proof of Stack, Avalanche, etc). Tous ces consensus sont déjà testés dans des cryptomonnaies moins importantes que Bitcoin. Le PoW, principale cause de pollution des cryptomonnaies, a déjà des équivalents moins énergivores pour l'avenir.

Cependant, l'exemple de la cryptomonnaie [Chia](https://www.chia.net/) rappelle que l'enfer est pavé des meilleures intentions. Le Chia repose sur un consensus *Proof of Space*, plutôt que de dépenser de l'énergie, les agents doivent utiliser de l'espace de stockage. Très vite, cette monnaie provoqua une pénurie de disques dur. L'utilisation des disques avec cette monnaie étant très intensive, les disques neufs [ne durent que quelques mois](https://blog.scaleway.com/scaleway-and-chia/), occasionnant une pollution pire que le PoW.

<h1>Conclusion</h1>
Nous vivons dans un monde en paix, la famine est au plus bas, l'illettrisme aussi et l'esclavage également. Tout ceci est permis grâce au progrès technologique. Les écologistes, blâmant déjà le progrès d'aujourd'hui, souhaitent nous l'interdire dans le futur sous prétexte qu'il serait nocif. Le progrès fait partir de l'humanité, depuis l'invention du silex jusqu'au Bitcoin. Il faut être aveugle pour n'y voir qu'un problème et fou pour ne pas s'en servir.



