---
title: Le présent a besoin du futur
subtitle: Le monde ne tourne pas qu'aux "invisibles" même en période de crise.
date: 2020-07-06
layout: post
background: /img/2020/07/dominoes.jpeg
categories: [article]
tags: [société]
---

**Comme il semble étrange que des millions de travailleurs puissent arrêter d’aller au boulot voire d’arrêter leur activité sans instantanément impacter l’économie. La France semble continuer à tourner avec ou sans eux. Qui sont-ils ? Armée de bullshit jobs ou rouage de la société ?**


## Entre conception et réalisation

Les syndicats adeptes de la vision court-termiste nous rappellent que, quand Carlos Ghosn cesse de travailler Renault continue à produire des voitures. Mais quand les ouvriers font grève, Renault est bloqué. Ce constat si souvent répété sonne un peu faux maintenant que Renault est faillite. Le monde de l’automobile est parfait pour comprendre la révolution moderne des jobs travaillant pour le futur et non pour le présent.

Pour voir une personne conduire une voiture sur la route, il faut un effort de planification au moins à quatre échelles.

La vision 30 ans où on privilégie les technologies d’avenir à industrialiser, on va définir le secteur de la marque: berline ? Citadine ? Utilitaire ? Moyenne gamme ? Haut de gamme ? Ensuite la vision 10 ans. On va concevoir la voiture, penser à sa maintenance durant toute sa vie. Le marketing va construire l’image de la marque, son code graphique. Puis la vision un an: gérer la logistique, prendre le pouls des tendances pour communiquer le bon message dans les pubs. Enfin, le présent, la réalisation du véhicule dans les usines, sa vente dans les concessionnaires ou son entretien dans les garages.

De ce constat, on remarque les jobs de conceptions / planification travaillant dans le futur, de ceux de réalisation ancrés dans le présent. Ces derniers parce qu’ils sont dans l’instantané provoquent des secousses immédiates dans la société en cas d’arrêt. C’est la prise de conscience des « invisibles » durant le confinement. 

Mais il ne faut pas se hâter de séparer les « utiles » des « inutiles ». Un ouvrier se retrouvera sans emploi si aucune personne ne prend de directives pour l’entreprise, ne conçoit les nouvelles voitures, ne gère la logistique ou ne pousse le client à l’achat. Les jobs travaillant dans le futur peuvent s’arrêter quelques jours sans problèmes, mais pas quelques semaines.

## Une avancée majeure pour l'Humanité

Il ne faut pas voir dans cette situation un problème, mais plutôt une réussite de l'Humanité. Depuis la nuit des temps, les hommes étaient collés au présent. Toute la main d'oeuvre passait dans la réalisation pour se nourrir, se vêtir ou se chauffer. Les objets évoluaient très peu et la planification ne dépassait pas l'année.

Le progrès technologique a permis la réduction du besoin de main d'oeuvre par l'automatisation. En plus de faire baisser les prix, elle a permis de réorienter des travailleurs sur plus de conception et de planification.

Aujourd'hui, des millions de chercheurs trouvent de nouvelles molécules pour améliorer la santé future. Des millions d'experts simulent le futur pour mieux anticiper les changements climatiques ou économiques. Des millions d'ingénieurs travaillent à améliorer les objets ou tâches du quotidien. Beaucoup d'humains ne vivent plus ancrés dans le présent, au jour le jour. Ils peuvent planifier leurs vacances, leurs retraites, leurs carrières.


## La réalisation a quel prix ?
Après le confinement et la vision de notre étroite dépendance à des usines étrangères. Le besoin de rapatrier plus de réalisations sur le sol français était dans la bouche de tout les politiciens de droite comme de gauche.

Or, le passage du futur au présent n’est pas la seule différence entre ces deux mondes. Les salaires en font partie. L’automatisation a fait chuter le besoin de main-d’œuvre sur chaque objet créé. La mondialisation a provoqué une concurrence féroce sur la main d’oeuvre restant. 

La réalisation de produit de masse comme la voiture ne rapporte plus rien. Les marges suivent la valeur ajoutée et se retrouvent dans la conception. Chaque produit Apple résume très bien cette situation avec sa phrase : "Designed in California. Made in China". Apple garde la grosse valeur ajoutée dans ses bureaux. Et laisse le sous-traitant chinois, le plus offrant, fabriquer ses produits. Il n'est pas le seul dans le secteur, ARM conçoit les microprocesseurs équipant tous les smartphones, tablettes, box interne et bientôt ordinateur. Pourtant ils n'ont aucune usine, ils vendent leurs plans à des partenaires, qui eux vont s'appuyer sur cette R&D pour réaliser leurs puces.

Vouloir remettre plus de réalisation en France rencontre un problème épineux. Comment re-implanter un travail à faible marge dans un pays au coût du travail élevé ? Souhaitons vraiment retrouver des bas salaires et des usines concurrencées par n'importe quel fabricant chinois ? Aucun français ne souhaite être payé comme un chinois et aucun français n'acceptera de payer trois fois plus cher qu'un produit chinois.

Tant que l'on restera un pays riche avec un coût de travail élevé notre prospérité se trouvera davantage dans les jobs de conceptions à fortes marges que de réalisation. Promouvoir un retour de la réalisation entièrement made-in-France est donc dangereux. Cela divise la population entre les “vrais” travailleurs dans les usines et les “faux” dans les bureaux. Cela sabote l’économie tout entière en sabotant notre futur et notre croissance.
