---
title: Pourquoi l'ordinateur quantique passionne les états (et les militaires) ?
subtitle: Les bourses de l'Etat, souvent maigres pour investir dans la recherche, viennent de s'ouvrir de 1,8 milliards d'euros pour le quantique. Il faut dire que les enjeux militaires sont au rendez-vous.
date: 2021-02-18
published: www.contrepoints.org/2021/02/25/391786-pourquoi-lordinateur-quantique-passionne-les-etats-et-les-militaires
layout: post
background: /img/2021/02/quantic.jpg
categories: [article, podcast]
tags: [société, informatique, chiffrement]
---

<iframe frameborder="0" loading="lazy" id="ausha-1tiK" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=y81R8Fl1l4Er&v=3&playerId=ausha-1tiK"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Une technologie dont aucune retombée dans le civil n'est pévue, vient de recevoir [1,8 milliard d'euros](https://www.numerama.com/tech/683908-plan-quantique-france.html). En comparaison, l'Intelligence artificielle, pourtant en vogue, n'a reçu *que* [665 millions d'euros](https://www.lesechos.fr/tech-medias/intelligence-artificielle/le-gouvernement-detaille-son-plan-pour-la-recherche-en-intelligence-artificielle-150828). Il faut dire que l'informatique quantique est une histoire digne d'Hollywood dont le dénouement intéresse surtout les militaires.

# Acte I - L'espoir

**Inutile de connaître le quantique pour comprendre les pouvoirs du quantique sur l'informatique.** Il faut noter une chose: les algorithmes (quantiques ou non) appliquent des opérations sur des variables. Dans l'informatique *classique*, une variable prend une valeur à la fois.


Par exemple, si vous souhaitez mettre au carré les nombres 2, 3 et 4. Vous placez le 2 dans la variable, vous lancez l'algorithme, on récupère 4. Vous placez 3 pour récupérer 9 et enfin 4 pour 16.


Avec le quantique, votre variable prend plusieurs valeurs à la fois. C'est la [*superposition quantique*](https://fr.wikipedia.org/wiki/Principe_de_superposition_quantique). Ainsi votre variable quantique vaut 2, 3 et 4. Et en appliquant une seule fois l'algorithme quantique, elle devient 4, 9 et 16 d'un coup. **On peut donc massivement paralléliser les calculs avec un ordinateur quantique**, sa puissance est décuplée par rapport à un ordinateur classique.


# Acte II - Le Drame

En fait, il a autre chose à savoir sur le quantique. Quand on mesure un système quantique, on le perturbe irrémédiablement. Notre variable quantique a effectivement les valeurs 4, 9 et 16 à la fois. Mais si l’on décide de la lire, elle se fige dans l’une de ses valeurs, supprimant toutes les autres. Il s'agit de la [*la réduction du paquet d'onde*](https://fr.wikipedia.org/wiki/R%C3%A9duction_du_paquet_d%27onde).

De plus, la valeur conservée est choisie aléatoirement ! On peut quand même, par les équations de la physique quantique, calculer les probabilités que la variable se fige dans telle ou telle valeur.

**Tout cela réduit les applications possibles.** Notre programmeur quantique doit sélectionner un problème à résoudre où il y a beaucoup de calcul en parallèle, mais une seule valeur en sortie. Ensuite, il doit assembler les opérations quantiques pour former un algorithme où la mesure finale donne le bon résultat dans 99,9...% de cas.

Bref l'informatique quantique n'a pas tout de suite fait rêver.


# Acte III - L'Arme

Vu les obstacles, concevoir un algorithme quantique est un chemin de croix. Le premier est l'algorithme de [Deutsch-Joza](https://fr.wikipedia.org/wiki/Algorithme_de_Deutsch-Jozsa) en 1992, il permet de savoir si une fonction est constante. Plus utile, en 1996, l'algorithme de [Grover](https://fr.wikipedia.org/wiki/Algorithme_de_Grover) permet une recherche efficace dans les bases de données. On pensait pouvoir le généraliser à d'autres problèmes, mais pour l'heure ce n'est pas le cas (voir les problèmes [*P = NP*](https://fr.wikipedia.org/wiki/Probl%C3%A8me_P_%E2%89%9F_NP#Statut_du_probl%C3%A8me_par_rapport_%C3%A0_d'autres_mod%C3%A8les_de_calcul) pour les lecteurs les plus téméraires).

**Et enfin, en 1996, l'algorithme de [Shor](https://fr.wikipedia.org/wiki/Algorithme_de_Shor) débarque. Il permet de factoriser de grands nombres, un bouleversement !**

Un ordi classique n'a aucun mal à multiplier deux nombres comme 3 x 5 = 15. Mais factoriser, c'est à dire, retrouver 3 et 5 comme multiple de 15 est compliqué. Pour les petits nombres comme ici, c'est rapide. Mais pour les grands nombres, le temps se compte en millions d'années, rendant le calcul rédhibitoire. Ce *cliquet* mathématique où l'on passe facilement dans un sens, mais pas dans l'autre est la base de nos chiffrements modernes (tel HTTPS).

**Les chiffrements actuels sont robustes tant que personne ne peut factoriser de grands nombres. Or un algorithme quantique le permet. Voilà pourquoi les états veulent tous avoir leurs ordinateurs quantiques *quoiqu'il en coûte*.** Le premier qui l'obtient peut casser les secrets du voisin.

# Acte IV - Le Remède

Il reste un dernier dénouement. Vous vous rappelez de la *réduction du paquet d'ondes* ? Dès qu'on mesure un système quantique, on le perturbe irrémédiablement. C'est agaçant pour les calculs, mais redoutable pour garder une conversion secrète.

Ainsi est apparue la cryptographie quantique. Elle permet de mettre en place une communication confidentielle. Si un espion tente d'écouter discrètement, il détruit aussitôt le canal. Impossible donc de se faire écouter à son insu.

La course est ainsi lancée ! Les états, accompagnés des entreprises technologiques, se livrent une course aux armements : maîtriser le quantique pour se protéger soi-même et attaquer les autres. La France vient d'investir 1,8 milliard. L'Europe [déploie son propre réseau](https://www.futura-sciences.com/sciences/actualites/communication-quantique-europe-dote-infrastructure-telecommunications-quantiques-78759/) de communications quantiques. Les entreprises américaines [Google](https://www.lemonde.fr/sciences/article/2019/10/23/google-annonce-avoir-franchi-une-etape-technologique-majeure-en-realisant-une-percee-dans-le-calcul-quantique_6016592_1650684.html), [IBM](https://www-03.ibm.com/press/us/en/pressrelease/46725.wss) ou [D-Wave](https://fr.wikipedia.org/wiki/D-Wave) franchissent toujours plus d'étapes dans la fabrication d'un ordinateur quantique opérationnel. La Chine a déjà [lancé ses premiers satellites](https://fr.wikipedia.org/wiki/QUESS) pour mettre en place des communications quantiques et travaille sur [un ordinateur](https://www.usinenouvelle.com/editorial/un-calculateur-a-photons-chinois-atteint-l-avantage-quantique.N1040039).


Pendant ce temps, les cryptologues tentent de rendre les algorithmes de chiffrement traditionnels moins soumis à la factorisation des grands nombres. **Et le civil, lui, prie pour sa vie privée et espère que quelques retombés viendront un jour.**
