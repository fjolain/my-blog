---
title: Tout comprendre des métaux rares.
subtitle: Tour d'ensemble des métaux rares, leur usages, leurs productions, leurs conflits, leurs futurs
date: 2021-04-11
layout: post
background: /img/2021/04/mines.jpeg
published: contrepoints.org/2021/04/15/395349-tout-comprendre-des-metaux-rares-1-3
categories: [article]
tags: [métaux rares, politque, société]
---

La réalité des métaux rares est loin des idées reçues ! Les métaux rares sont d'une étonnante complexité dans notre société. Or on assiste plus à une simplification voire une confusion dans la presse. Nous allons donc tenter d'y remettre de l'ordre avec ce dossier en 3 parties :

- I - Pourquoi a-t-on besoin de métaux rares ? Quand sont-ils apparus dans nos vies ?
- II - La géopolitique des métaux rares. Etudes de cas sur les moteurs, les batteries et l'électronique.
- III - Le futur des métaux rares. À la conquête des métaux sur Terre, Mer & Ciel. A-t-on vraiment besoin de métaux rares ?


<h1>I - Pourquoi a-t-on besoin de métaux rares ?</h1> 

Les métaux et les hommes ont une histoire vieille de plusieurs millénaires. Il y a eu [l'Âge du Cuivre](https://fr.wikipedia.org/wiki/%C3%82ge_du_cuivre) en 4000 av. J.-C. Puis, l'homme découvrit l'étain ce qui démarra [l'Âge du Bronze](https://fr.wikipedia.org/wiki/%C3%82ge_du_bronze) (mélange de cuivre et d'étain) en 3000 av. J.-C. S'en suivit [l'Âge du Fer](https://fr.wikipedia.org/wiki/%C3%82ge_du_fer) en 1000 av. J.-C. La présence des métaux dans les sociétés humaines ne date pas d'hier, **mais jusqu'à l'époque moderne, l'humain se contentait de [sept métaux](https://fr.wikipedia.org/wiki/Sept_m%C3%A9taux) (l'or, l'argent, le cuivre, l'étain, le plomb et le fer) sur les [86 métaux](https://fr.wikipedia.org/wiki/M%C3%A9tal) que possède la nature.**

Le décollage de la science et de l'industrie révéla le potentiel métallurgique de notre planète. L'industrie s'intéressa, par exemple, aux caractéristiques mécaniques des métaux. À commencer par un  alliage très résistant : l'acier (mélange de fer et de carbone). [De nouvelles méthodes de fabrication](https://fr.wikipedia.org/wiki/Proc%C3%A9d%C3%A9_Bessemer) apparues au XIXe siècle firent drastiquement baisser son coût de production, et devinrent le moteur de la révolution industrielle. Puis on se mit à utiliser d'autres métaux peu connus. Tel le [Tungstène](https://fr.wikipedia.org/wiki/Tungst%C3%A8ne), découvert en 1781, qui est plus solide que l'acier et par conséquent utiliser dans les machines-outils pour usiner l'acier. Ou encore l'[Aluminium](https://fr.wikipedia.org/wiki/Aluminium) découvert en 1807, dont sa légèreté trouva sa place dans l'aviation.

**Mais c'est surtout l'électronique qui entraîna la course aux métaux rares.** D'abord pour leur qualité de conducteur électrique, comme le cuivre, l'or ou l'étain. Puis par leur qualité de semi-conducteur dont le plus célèbre : le [Silicium](https://fr.wikipedia.org/wiki/Silicium) découvert en 1823 est à la base de toute l'informatique. D'autres sont utilisés par l'industrie pour leurs propriétés chimique. Le français Gaston Planté, inventa la première batterie rechargeable au plomb en [1859](https://fr.wikipedia.org/wiki/Batterie_au_plomb#Historique). Technologie supplantée par le nickel en [1967](https://en.wikipedia.org/wiki/Nickel%E2%80%93metal_hydride_battery#History). Aujourd'hui, le [Lithium](https://fr.wikipedia.org/wiki/Lithium), découvert en 1817, s'est imposé comme le meilleur métal pour les batteries.


Ici, nous devons faire un point sémantique. Il existe les *métaux* dont ceux abondant comme le fer ou le cuivre, puis les *métaux rares* moins abondant comme le lithium ou le cobalt et enfin les fameuses *terres rares*. **Les terres rares sont une sous-catégorie des métaux rares.** Ils ont des caractéristiques similaires entre eux, l'industrie les utilise pour leur propriété magnétique. Pour une même taille, les aimants en terre rare sont [100 fois plus](https://fr.wikipedia.org/wiki/Aimant_permanent) puissants que les aimants à base de fer. Le [Yttrium](https://fr.wikipedia.org/wiki/Yttrium) découvert en 1787 fut le premier à arriver dans nos foyers, on l'utilisait dans les écrans à tube cathodiques. Autre exemple la [Néodyme](https://fr.wikipedia.org/wiki/N%C3%A9odyme), découvert en 1885 est massivement utilisé dans nos moteurs électriques depuis [1982](https://fr.wikipedia.org/wiki/Aimant_au_n%C3%A9odyme).

Des millénaires pour trouver et utiliser sept métaux, et seulement trois siècles pour trouver les 79 autres restants. Toute notre avancée technologique repose sur l'existence de ces métaux rares aux noms imprononçables tels qu’Europium, Terbium, Cobalt, Holmium, Thulium, Lithium, etc. Chaque métal disposant de propriétés mécaniques, électriques, magnétiques ou chimiques exceptionnelles, devenues indispensables à notre société. **Ainsi, votre iPhone comporte [36 métaux](https://www.sciencesetavenir.fr/high-tech/chimie-de-quoi-est-fait-votre-iphone_35674) dans le creux de votre main.**

Maintenant que la terre entière tourne aux métaux rares, la production se transforme en conflit géopolitique aux multiples facettes. Dans le prochain article, nous remonterons la fillière de trois composants : les moteurs électriques, les batteires et les microprocesseur pour comprendre les stratégies autour des métaux rares.

<h1>II - La Géopolitique des métaux</h1>

Les métaux rares sont devenus indispensables à notre industrie. À mesure que l'on embrasse le numérique et l'électrique, on accroît notre besoin en ces métaux. Ils sont dans nos batteries, nos éoliennes, nos voitures, nos smartphones ou  notre télé. Leur approvisionnement est maintenant au centre d'un combat géopolitique mondial. 

On aimerait tout simplifier, comme on l'entend souvent : "La Chine détient tous les métaux rares. Ils sont méchants et très chanceux." C'est beau, simple, ça a le mérite de créer une sensation d'injustice et de peur dans la population.

Il n'en est rien. Chaque métal a sa géopolitique, car chaque métal a sa répartition géographique, sa rareté, ses usages, son industrie. La Chine n'a pas la main sur tous les métaux rares. Elle est certes partie en bonne position grâce à ses ressources naturelles, mais sa domination actuelle est le fait de patience.

Nous allons donc analyser 3 marchés différents pour comprendre la stratégie chinoise.


<h2>Moteur électrique</h2>

Les moteurs électriques sont composés de cuivre et de fer, mais surtout de néodyme, une terre rare permettant la fabrication de super-aimants. Ce métal peut être vu comme un monopole chinois *injuste* : [80%](http://www.mineralinfo.fr/sites/default/files/upload/documents/Fiches_criticite/fichecriticiteneodyme151201.pdf) des réserves et de la production sont chinois. Dans ce cas, la Chine n'a pas eu besoin d'aller très loin. Elle a réalisé un *coup à trois bandes* parfaitement exécuté pour s'accaparer le marché et la valeur ajoutée. 

Première étape: on casse les prix. La Chine brada sa production en réalisant un dumping économique et écologique pour extraire n'importe comment le minerai le moins cher possible. En 2002, le métal était tombé à [6.67 $/kg](http://www.mineralinfo.fr/sites/default/files/upload/documents/Fiches_criticite/fichecriticiteneodyme151201.pdf). Deuxième étape: remonter la chaîne de valeur. Une fois les mines étrangères coulées par une concurrence déloyale. La Chine freina drastiquement l'exportation le prix atteignit [467 $/kg](http://www.mineralinfo.fr/sites/default/files/upload/documents/Fiches_criticite/fichecriticiteneodyme151201.pdf) en juillet 2011. Les industries qui veulent résister vont devoir délocaliser leurs usines en Chine. Il faut rendre cette délocalisation agréable en continuant le dumping sur le marché interne, conserver un marché du travail à bas coût et favoriser les *join-venture* entre entreprises chinoises et étrangères. Troisième étape: pillage. Une fois que l'entreprise chinoise a pillé les propriétés intellectuelles de l'étrangère, on fait émerger une entreprise concurrente 100% chinoise.


C'est ainsi que les américains, producteurs de néodyme et d'aimants on détruit leur industrie. En [2002](https://fr.wikipedia.org/wiki/Mine_de_Mountain_Pass) la mine Moutain Pass, principal gisement américain de néodyme, ferme. Puis en [2006](), l'usine du fleuron des aimants américains [Magnecquench](https://mqitechnology.com/) est délocalisée en Chine. C’est ainsi qu'en 2012, Lockheed Martin du reconnaître, qu'il avait des super-aimants chinois dans son F-35 <a href="#footnote-1">[1]</a>. Les aimants venaient de [ChengDu Magnetics](https://hcmagnet.en.ec21.com/) en Chine.

<h2>Les batteries </h2>

Une batterie repose essentiellement sur le lithium, et dans un second temps sur le cobalt et le nickel. Tous ont quasiment la même [abondance](https://fr.wikipedia.org/wiki/Abondance_des_%C3%A9l%C3%A9ments_dans_la_cro%C3%BBte_terrestre). Le nickel est [bien reparti](https://fr.wikipedia.org/wiki/Nickel#Production_et_%C3%A9conomie_du_nickel), la Chine détient 4% de la production. Le [lithium](https://fr.wikipedia.org/wiki/Lithium#R%C3%A9serves_terrestres_et_production) est concentré en Amérique latine (Argentine, Chili, Bolivie, Brésil), en Australie, la Chine dispose de 6% des réserves. Actuellement l'Australie s'accapare la production avec 55% de l'extraction. Le principal problème est donc le cobalt. La République Démocratique du Congo détient [72% de la production et 50% des ressources](http://www.mineralinfo.fr/sites/default/files/upload/documents/Fiches_criticite/fichecriticitecobalt-publique-210225.pdf).

Dans ce cas, il faut aussi jouer des partenariats et remonter la chaîne de valeur. La Chine, par l'entreprise [China Molybdenum](https://en.wikipedia.org/wiki/China_Molybdenum), détient par exemple 80% de [Tenke Fungurume](https://fr.wikipedia.org/wiki/Mine_de_Tenke_Fungurume), principale mine de cobalt en RDC. Ensuite, elle est devenue leader dans le raffinage du cobalt avec 
 [63%](http://www.mineralinfo.fr/sites/default/files/upload/documents/Fiches_criticite/fichecriticitecobalt-publique-210225.pdf) de part de marché (à noter que l'Europe à 19%). 
 
 La Chine qui détenait déjà des ressources en lithium et nickel a ainsi récupéré une place stratégique sur le cobalt. Avec tous les métaux "prêts à l'usage" chez elle, il suffit de *favoriser* la délocalisation des industries par le dumping puis les restrictions d'exportations et enfin les *partenariats*. Et ainsi nous voyons des entreprises chinois arrivait sur le marché des batteries avec le meilleur de la technologie telle [CATL en 2011](https://fr.wikipedia.org/wiki/Contemporary_Amperex_Technology) ou [sVOLT](https://en.svolt.cn/).
 
 <h2>Les circuits électroniques </h2>
 
 Le silicium, métal semi-conducteur, à la base de toute l'électronique et l'informatique, est le matériau [le plus abondant sur terre après l'oxygène](https://fr.wikipedia.org/wiki/Abondance_des_%C3%A9l%C3%A9ments_dans_la_cro%C3%BBte_terrestre), il constitue 25% de la croûte terrestre. Envisager une captation du marché est impossible.
 
 Comme avec le cobalt, la Chine a récupéré le polluant raffinage que l'Occident ne souhaite pas voir chez lui. Mais détenir du silicium raffiné ne vaut rien dans la chaîne de valeur. Entre le silicium pur et votre microprocesseur, il y a une technicité colossale détenue uniquement par quelques acteurs tels que Samsung, Qualcomm, Intel et le leader [TSMC](https://en.wikipedia.org/wiki/TSMC). Tous sont asiatiques à l'exception de Intel (qui envisage d'utiliser les usines de TSMC). Ces entreprises sont les seules capables de fabriquer des concentrés de technologies comme les microprocesseurs, les antennes 5G ou les écrans. 
 
En plus de nécessiter une expertise technique rare en Europe, cette industrie consomme beaucoup de capitaux. Les composants sortent de méga-usine qui ne sont rentables qu'avec de grandes économies d'échelle. Comme l'usine de Samsung à Pyeongtaek en Corée du Sud pour un investissement de [14 milliards de dollars](https://www.lefigaro.fr/secteur/high-tech/2017/11/07/32001-20171107ARTFIG00309-samsung-deploie-ses-mega-usines.php).
 
 Un pays revient souvent : Taïwan. Cette petite île est d'une importance vitale pour le reste du monde. Elle dispose des entreprises les plus qualifiées telles que TSMC, mais aussi les usines les plus perfectionnées à Hsinchu, Tainan et Taichung. Devant cette place stratégique, la Chine compte bien agir et se l'accaparer. Elle [s'entraîne au débarquement](https://taiwaninfo.nat.gov.tw/news.php?unit=47&post=180586) depuis des années pour récupérer Taïwan et son savoir-faire.
 
 En attendant l'action militaire, comme toujours la Chine s'est gentiment dévouée comme usine de monde pour Samsung, Sony ou TSMC. Puis des concurrents 100% chinois sont arrivés comme Huawei, Xiaomi ou ZTE.
 
 
 <h2>Bonus patriotique : Le Hafnium </h2>
 
 La France a son pré carré : le hafnium. C'est une terre rare utilisée pour réguler les réactions physiques dans les centrales nucléaires. La France est le premier producteur.
 
 <h2>L'arrogance occidentale</h2> 
 
 L'Occident a préparé sa chute. Depuis la découverte du Nouveau Monde et ses mines de cuivre et d'argent, nous aimons chercher les ressources à bas prix chez les autres. Puis réaliser la grosse valeur ajoutée de retour chez nous, avant de revendre nos produits au reste du monde. Nous avons transformé le coton en vêtement, les diamants en bijoux, l'uranium en électricité, le cacao en chocolat, le pétrole en essence et plastique.
 
 Nous laissons aux autres le soin de polluer en minant, raffinant et fabriquant à bas coût. Nous nous concentrons sur la haute valeur ajoutée et la vente, ainsi résumé par Apple : *Designed in California. Made in China*. C'est un modèle qui a fonctionné parfaitement en Amérique du Sud, en Afrique et au Moyen-Orient. Nous pensions l'exporter en Asie, mais la Chine vient de sonner la fin.
 
 Elle a vu dans notre mondialisation, notre plus grande faiblesse. La haute technologie, joyaux de l'Occident, nous rend vulnérables à la pénurie des métaux rares. La Chine vient de trouver le talon d'Achille de l'Occident, elle vient de nous faire passer de prédateur économique à proie.
 
 À nous de contrecarrer dans le chapitre suivant.

 
 <h1>III - Le Futur des Métaux Rares</h1>

La Chine, en incitant (voire en forçant) la délocalisation par les restrictions d'exportation, a su remonter la chaîne de valeur. Par-dessus tout, elle a donné au reste du monde un guide pour rétablir le rapport de force avec l'Occident. Aujourd'hui, l'Indonésie a appliqué la méthode sur le nickel : [embargo sur le métal en septembre 2019](https://www.lesechos.fr/finance-marches/marches-financiers/le-marche-du-nickel-en-ebullition-apres-un-embargo-indonesien-1128035), puis signature de contrats, comme [celui de LG](https://www.lesechos.fr/industrie-services/automobile/batteries-electriques-lg-signe-un-deal-a-10-milliards-de-dollars-en-indonesie-1277280) à hauteur de 10 milliards de dollars pour construire une usine de batteries . L'Arabie Saoudite souhaite ouvrir des [usines pétrochimiques](https://www.lemonde.fr/economie/article/2018/10/10/pour-l-arabie-saoudite-le-plastique-c-est-fantastique_5367033_3234.html) pour concevoir du plastique.

L'Occident devra faire avec un futur où, le pays qui extrait, est aussi le pays qui fabrique. Néanmoins, le dumping chinois ne peut plus durer tant sur le plan économique que écologique, il devient insoutenable. L'achat de minerais à des pays tiers deviendra cher et instable. Trois possibilités s'offrent à nous.


<h2>Miner Terre, Mer et Ciel</h2>

Avec le court du minerai repartant à la hausse. Certaines mines, comme la Mountain Pass pourrait réouvrir. C'est aussi le cas en France où [Emanuel Macron y est favorable](https://www.20minutes.fr/planete/2719159-20200216-o-projet-montagne-or-emmanuel-macron-dit-arrete). Les fonds marins sont aussi inspectés en vue de future extraction. La France, ayant la deuxième surface maritime au monde, se pose en bonne position.

Il faut savoir que les métaux viennent de l'espace. [Ils sont forgés](https://fr.wikipedia.org/wiki/Nucl%C3%A9osynth%C3%A8se_stellaire) dans les étoiles, et arrivent sur terre par les météorites. D'où l'idée d'aller à la source et envoyer des robots miner sur les astéroïdes. Même si le [Luxembourg vient d'autoriser](https://www.slate.fr/story/169512/espace-asteroides-exploitation-miniere-metaux-precieux-terres-rares) le minage spatial, et que certaines entreprises comme [Planetary Resources](https://fr.wikipedia.org/wiki/Planetary_Resources) émergent, il reste encore d'importants problèmes techniques et économiques.

Pour rappel [Rosetta](https://fr.wikipedia.org/wiki/Rosetta_(sonde_spatiale)) a été la première sonde à se poser sur une comète en 2016 pour un coût de [1,4 milliard d'euros](https://www.lefigaro.fr/conjoncture/2014/11/12/20002-20141112ARTFIG00176-mission-rosetta-un-pari-spatial-a-pres-de-14-milliard-d-euros.php). Ou encore, la première extraction d'astéroïdes [OSIRIS-REx](https://fr.wikipedia.org/wiki/OSIRIS-REx) est en court par la NASA et va en récupérer 60g...

De plus, à l'heure de la vague verte, quel politicien souhaite ternir son image en réouvrant les mines ? Si les écolos sont déjà vent debout contre la pollution de la 5G ou des lampadaires, vont-ils accepter la réouverture des mines ? Le consommateur occidental est pris de schizophrénie souhaitant son monde technologique, mais ne voulant pas de mine dans son jardin.

<h2>Le recyclage</h2>

Nous pouvons aussi utiliser nos consommateurs comme une force. Avec le recyclage des métaux, la production se déplace des mines vers les marchés de consommateurs. L'Occident retrouve ainsi sa souveraineté. Mais là encore, le recyclage bio n'existe pas. Et surtout la rentabilité du recyclage varie d'un métal à l'autre. L'[aluminium](https://fr.wikipedia.org/wiki/Aluminium#Recyclage) est recyclable à l'infini, il nécessite 95% d'énergie en moins que le minage, ce qui le rend rentable à recycler. Alors que les terres rares restent peu recyclées, car [trop coûteuses](https://fr.wikipedia.org/wiki/Terre_rare#Utilisation,_recyclage).

<h2>A-t-on besoin de métaux rares ?</h2>

L'être humain a toujours su se passer de ce qui lui manque. On pense notamment à l'Allemagne de 1940, qui a mis au point de l'[essence](https://fr.wikipedia.org/wiki/Essence_synth%C3%A9tique) et du [caoutchouc](https://fr.wikipedia.org/wiki/Buna_(caoutchouc)) (Buna) synthétique pour mettre son économie en autarcie et ainsi préparer le pays à la guerre. Plus proche de nous, en 1987, le [protocole de Montréal](https://fr.wikipedia.org/wiki/Destruction_de_la_couche_d%27ozone#Protocole_de_Montr%C3%A9al) interdit 96 produits chimiques utilisés dans 240 secteurs industriels pour lutter contre le trou dans la couche d'ozone.

Enfin dans notre cas, il y a Tesla. Le constructeur [a déjà réduit le néodyme](http://www.fiches-auto.fr/articles-auto/tesla/s-2406-quelle-technologie-moteur-utilise-tesla-.php) de ces moteurs pour les remplacer par des bobines de cuivre. Et il vient de [retirer le cobalt](https://www.latribune.fr/opinions/blogs/commodities-influence/le-chinois-catl-et-l-americain-tesla-ont-ils-signe-la-mort-du-cobalt-840039.html) de ses batteries pour le remplacer par plus de nickel.

<h2>Conclusion</h2>

La Chine en forçant la délocalisation, a pillé l'Occident de son innovation. Ce modèle devient la norme. Mais il montre que l'Occident est riche de matière grise plus que de métaux rares. La Chine a pillé nos entreprises françaises de leurs savoirs, mais ne voulait pas des mines françaises.

Réouvrir les mines est donc un acte courtermiste. Si l'Occident doit se relever, c'est par son innovation. Nous devons financer des projets sur du recyclage plus écologique, comme [Récylum](https://fr.wikipedia.org/wiki/R%C3%A9cylum) et financer des entreprises sur de nouvelles batteries comme [Tiamat](http://www.tiamat-energy.com/).


<h4>Bibliographie</h4>
Ce dossier, en plus des diverses sources présentes dans les liens, a été construit autour des informations présentes dans :

- Le livre de référence : [*La guerre des métaux rares*](https://www.cultura.com/la-guerre-des-metaux-rares-tea-9791020905796.html), Guillaume Pitron.
- Le site interministère et ses nombreux rapports : [*mineralinfo.fr*](http://www.mineralinfo.fr)
- et bien sûr [wikipedia.org](https://fr.wikipedia.org)

