---
title: Le Halving de Bitcoin, Une Onde de Choc Imminente!
subtitle: Tous les 4 ans, un évènement mystérieux se passe sur Bitcoin. En apparence rien de bien méchant, pourtant tout le monde le redoute ou le convoite.
date: 2024-02-06
layout: post
background: /img/2024/04/cake.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-0YXh" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=yOQnruXzO8eQ&v=3&playerId=ausha-0YXh"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>


Bitcoin, la première cryptomonnaie au monde fait régulièrement parlé d'elle. Le plus souvent, l'engouement se concentre sur son prix, digne des meilleures montagnes russes. Les nouvelles nous parviennent par des titres : "Deux ans après son précédent record, le bitcoin atteint de nouveaux sommets" ([Le Temps](https://www.letemps.ch/cyber/crypto/deux-ans-apres-son-precedent-record-le-bitcoin-atteint-de-nouveaux-sommets#:~:text=Le%20prix%20du%20bitcoin%20a,peine%20plus%20de%20deux%20ans.)) ou "Le bitcoin et l'ensemble du marché des cryptomonnaies chutent à nouveau." ([Le Temps](https://www.letemps.ch/economie/bitcoin-lensemble-marche-cryptomonnaies-chutent-nouveau)).

Depuis plusieurs semaines, un terme abscon s'est frayé un chemin jusqu'aux gros titres, on parle maintenant de "Avec le "halving", la frénésie s’empare du bitcoin" ([Challenges](https://www.challenges.fr/patrimoine/placements/avec-le-halving-la-frenesie-s-empare-du-bitcoin_890111)).

Quel est ce  Halving, pourquoi cette frénésie ?

Le bitcoin est la première cryptomonnaie, car contrairement à l'or qui repose sur une ressource physique finie ou le franc qui repose sur la confiance dans la Confédération suisse, le bitcoin repose sur le cryptage soit des mathématiques.

Oui, tout dans Bitcoin est mathématique, l'invention prend la forme d'un code que n'importe qui peut faire tourner sur son ordinateur connecté à internet. De là, un registre synchronisé avec tous va se construire transaction par transaction, ou plutôt bloc par bloc, car on préfère ajouter les transactions par paquet appelé bloc.

De même que les mathématiques permettent de rendre un fichier secret par du chiffrement. Les mathématiques permettent de garantir la véracité de ce registre contenant les blocs. Tout le monde peut participer aux réseaux Bitcoin soit en ajoutant les blocs, en faisant des transactions ou en vérifiant l'exactitude du registre.

Aussi la plupart des célébrations Bitcoin sont d'ordre technique, elles n'intéressent pas les gros titres.

Cependant dans ces lois mathématiques immuables codifiées dans le programme de Bitcoin, l’une porte sur l'argent et intéresse donc tout le monde.

Le registre Bitcoin avec ses blocs et ses transactions est un système en vase clos. Il n'y a que des Bitcoins que l'on s'échange entre nous en inscrivant de nouvelles transactions. Il n'y ni Bitcoin qui rentre ni Bitcoin qui sort du registre juste du Bitcoin qui s'échange... et d'autres qui se créent.

Car à un moment donné, si on veut échanger du Bitcoin, il faut bien le créer. La règle est la suivante, un participant du réseau est tiré au hasard pour ajouter un nouveau bloc contenant les nouvelles transactions. Dans ce bloc, il a également une récompense en Bitcoin pour l'heureux élu tiré au sort.

Cette récompense commence à 50 Bitcoins par blocs et doit être divisée par deux tous les 210 000 blocs. Sachant qu'un bloc est ajouté toutes les 10 minutes en moyenne. La récompense est divisée par deux tous les 4 ans environ.

Elle a commencé à 50 bitcoins au démarrage le 3 janvier 2009, puis 25 le 28 novembre 2012, enfin 12.5 le 9 juillet 2016 et 6.25 le 11 mai 2020. Nous attendons sous peu le passage aux 3.125 Bitcoins par bloc.

La volonté de diviser la récompense par deux permet de tendre vers une limite finie de 21 millions de Bitcoins dans le système.

Voici ce fameux halving ("réduction par deux" en anglais). Dans quelques jours, la création de nouveaux Bitcoins sera divisée par deux. Les spéculateurs se frottent les mains, car aux 3 précédents halving, le prix est monté en flèche, probablement car le halving réduit les bitcoins sur le marché.

C'est donc un signal guetté par tout l'écosystème qui espère une hausse du prix comme lors des derniers. Le halving va se passer, c'est certain, il est figé dans le marbre du code, espérons qu'il apporte avec lui toutes ses promesses.


