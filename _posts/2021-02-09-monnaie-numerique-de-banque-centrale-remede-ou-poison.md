---
title: Monnaies Numériques de Banque Centrale remède ou poison ?
subtitle: Les crypto-monnaies d'états vont-ils détruire Bitcoin ?
date: 2022-02-09
layout: post
background: /img/2022/02/presse.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [société]
---

<iframe frameborder="0" loading="lazy" id="ausha-W01K" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=B19mZFrAlPV1&v=3&playerId=ausha-W01K"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

La Chine vient de lancer son [crypto-yuan](https://www.lesechos.fr/idees-debats/editos-analyses/le-e-yuan-lanti-bitcoin-au-service-de-lhegemonie-de-pekin-1328168), une monnaie nationale basée sur une blockchain. D'abord regardée avec beaucoup de scepticisme par la communauté internationale, l'idée semble avoir conquis les pays occidentaux qui se penchent tous sur un crypto-euro ou un crypto-dollar.

Pourquoi un tel engouement de nos technocrates ? Ces nouvelles monnaies vont-elles être meilleures ou pires que les monnaies existantes ?

<h2>Un nom flou pour une idée contradictoire</h2>

En période de propagande généralisée, les mots se vident de leur sens. On rappelle la volonté du porte-parole du gouvernement Gabriel Attal de lier des restrictions à la notion de liberté, en disant cet été : ["Le pass vaccinal c'est la liberté"](https://www.francetvinfo.fr/sante/maladie/coronavirus/deconfinement/direct-deconfinement-restaurants-et-cafes-rouvrent-leurs-salles-aujourd-hui-le-couvre-feu-prolonge-a-23-heures_4656663.html). Une phrase qui ressemble bizarrement au slogan du Parti de BigBrother : la servitude c'est la liberté.

Or avec les monnaies numériques de banque centrale, ou les cryptomonnaies de banque centrale ou encore les crypto-yuan. Il a bien cette volonté de lier la notion de cryptomonnaies, soit une monnaie décentralisée anarchique, à la notion totalement opposée de banque centrale.

On comprend pourtant fort simplement qu'une cryptomonnaie contrôlée par un seul acteur n'est plus une cryptomonnaie, encore plus si le seul acteur est l'état.

Aussi nous allons voir que rien de bon ne peut  venir d'un tel changement soigneusement caché derrière une novlangue orwellienne.

<h2>Pourquoi les états veulent tous leur MNBC ?</h2>

Le Bitcoin est un système financier complet. Avec seulement quelques milliers de lignes de code. Le protocole bitcoin régule la création monétaire, il gère les dépôts et les comptes des usagers, il garantit les transactions entre les comptes.

Dans le système bancaire classique, ces rôles sont divisés, car trop complexes pour être gérés par une seule personne. L'état régule la création monétaire. Les banques gèrent les comptes des clients. Les réseaux Swift, Visa ou Mastercard permettent les transferts d'argent.

Bien que tous ces acteurs sont autorisés et doivent collaborer avec l'état, ce n'est pas l'état. Ainsi, la banque européenne ne garde qu'un contrôle  partiel sur l'euro, sa propre monnaie.

Xi Jinping, a donc vu dans la blockchain, le protocole derrière Bitcoin, un moyen de surveillance. En effet, si l’on conserve la blockchain, mais qu'on retire l'anonymat, l'immutabilité des transactions et la décentralisation. On retire évidemment tout ce qui fait la force libérale et anarchique d'une cryptomonnaie, mais on crée un système monétaire complet entre les mains de l'état.

Il n'a donc plus rien de *cryptomonnaie* derrière une MNBC, il a juste la volonté d'un état de regrouper entre ses mains, les rôles de création monétaire, gestion de compte et transfert d'argent. Rôles auparavant divisés entre plusieurs acteurs.

Malheureusement, aujourd'hui, les idées liberticides chinoises telles la surveillance d'internet, la reconnaissance faciale ou le crédit social s'exportent facilement dans la tête de nos technocrates européens.

Le crypto-yuan de Pékin, fait donc des envieux à Bruxelles comme à Bercy pour son potentiel de surveillance totale de la monnaie. (Afin, comme toujours, de lutter contre le trafic, le blanchiment, le terroriste ou la pédophilie)

<h2>Quel avenir pour les vraies cryptomonnaies ? </h2>

Comme toujours prédire l'avenir est compliqué, mais vu la situation économique, la perfidie de l'état durant la crise covid et les réponses des gouvernements aux  inflations passées, je veux bien me risquer à prédire l'avenir des *vraies* cryptomonnaies.

La crise sanitaire commence à partir, que déjà une nouvelle crise provoquée par l'inflation montre le bout de son nez. En effet avec une inflation record en occident ([7%](https://www.lemonde.fr/economie/article/2022/01/12/l-inflation-atteint-7-aux-etats-unis-un-niveau-inedit-depuis-1982_6109199_3234.html) aux États-Unis [5%](https://www.lemonde.fr/international/article/2022/01/07/zone-euro-l-inflation-atteint-5-en-decembre-son-plus-haut-niveau-en-vingt-cinq-ans_6108572_3210.html) dans l'UE), il est possible que cette situation dérape en hyperinflation.

Dans ce cas, comme à chaque fois que sa monnaie se dévalue, l'état crée une nouvelle en promettant que celle-ci sera bien plus solide que l'ancienne. C'est le cas au Venezuela en [2018](https://fr.wikipedia.org/wiki/Bolivar_souverain) en passant au *Boliavar Souverain* (ratio 100 000:1), en France en [1958](https://fr.wikipedia.org/wiki/Franc_fran%C3%A7ais) en passant au nouveau franc (ratio 100:1), ou en Allemagne en [1924](https://fr.wikipedia.org/wiki/Reichsmark) en passant au reichsmark (ratio 1 000 000 000 000:1). Un nouvel euro, ou un retour au franc est donc possible.

Une autre manie de l'état est de piocher dans la caisse à technologies pour en choisir une et la vendre comme solution miracle. Avec le covid, nous avons eu le droit aux vaccins comme solution technologique miraculeuse. Avec l'inflation, il est fort possible que le nouvel euro soit un "crypto-euro". Une monnaie vendue comme bien supérieure à toutes les précédentes, car reposant sur la technologie magique de la blockchain.

Avec comme argument massue, si la blockchain a permis au Bitcoin d'être une monnaie solide, alors elle permettra au crypto-euro d'être à l'abri de l'inflation.

Argument parfaitement fallacieux, car ce n'est pas la blockchain qui permet au bitcoin d'être solide, mais son bon usage. La solidité du Bitcoin vient de son émission monétaire limitée à 21 millions de Bitcoins, la décentralisation, sa transparence dans son fonctionnement.

Autant de points qui ne seront pas partagés avec une MNBC, le nouveau crypto-euro ne saura donc pas plus solide que l'euro actuel.

Autre argument implacable, maintenant que l'on a mis le meilleur de Bitcoin dans l'euro, pourquoi encore utiliser le Bitcoin ? Et c'est sur ce point que l'avenir des *vraies* cryptomonnaies se gâte.

Quand on voit à quel point, les résistants à la vaccination sont traités. Il faut se préparer à l'idée que les cryptomonnaies et leurs utilisateurs se prennent le courroux du gouvernement.

D'abord la manière douce, en propageant sur toutes les ondes, que l'utilisation d'une monnaie autre que le crypto-euro est *égoïste*, qui va *contre le bien être collective*, les résistants *ne jouant pas le jeu* des nouvelles règles monétaires.

Puis les restrictions tomberont rapidement contre les récalcitrants du nouvel euro. Et enfin, si l'inflation galope encore malgré le crypto-euro, alors il sera temps d'interdire la détention des autres monnaies.


Dans cette optique on rappellera toutes les interdictions actuelles du Bitcoin [en Égypte, en Irak, au Qatar, en Oman, au Maroc, en Algérie, en Tunisie, au Bangladesh et désormais en Chine](https://journalducoin.com/bitcoin/cryptomonnaies-bannies-monde-bilan-inquiete/).

Dans cette optique également, on rappellera toutes les interdictions sur l'or avant-guerre. Roosevelt par [l'executive Order 6102 ](https://fr.wikipedia.org/wiki/Executive_Order_6102) de 1933 qui obligea tous les citoyens à redonner leur or sous peine de prison. Suivit par l'Italie en [1935](https://fr.wikipedia.org/wiki/Executive_Order_6102) lors des opérations dites "jour de la foi, donnez l'or à la Patrie". La France suivra en [1936](https://fr.wikipedia.org/wiki/Executive_Order_6102). Toute détention de plus de 200g d'or était illégale.


Il ne faut donc voir aucun remède des MNBC. Une monnaie étatique, dont la création monétaire est entièrement décidée par l'état, ne deviendra pas plus solide en prononçant le mot blockchain. Pire une MNCB est un poison pour notre vie privée, elle annonce l'arrivée  d'un communiste 2.0 que nous parlerons plus en détail dans une deuxième partie.


