---
title: Le bruit des bottes ne viendra pas
subtitle: Comment la technologie permet une dictature sans violence
date: 2020-04-04
layout: post
background: /img/2020/04/parade.jpeg
categories: [article]
tags: [politique, démocratie, vie privée]
---

**La technologie permet une dictature sans violent et c’est ce que la France prépare à ses citoyens, trop occupés à guetter le bruit des bottes.**

L’art du magicien c’est de focaliser le public sur sa main droit, le temps que sa main gauche prépare le tour. Ici, le gouvernement fait pareil.
Sauf que dans notre cas c’est le public qui a décidé de ne pas regarder la main gauche.

Un rapide tour sur nos sites de désinformation préférés nous plonge dans le nouveau fléau de la France : la violence policière.

Alors certes, la violence policière augmente. Mais d’une part, on est encore loin d’une dictature comme le Venezuela, le Soudan ou le Zimbabwe,
n’en déplaise à Michelle Bachelet, haute commissaire aux droits de l’homme à l’ONU. Deuxièmement, la violence n’augmente pas que chez la police,
elle augmente partout ! Des manifestants venus pour casser, des pompiers qui se font caillasser dans les cités, du harcèlement dans les écoles.
La police ne calme malheureusement pas le jeu et suit le mouvement.

## L’oiseau fait son nid
Et surtout, l’enjeu n’est plus là. Le tapage médiatique sur les gilets jaunes blessés a mis sous silence des faits pourtant hallucinants :

Loi anti-haine L’Enfer est pavé des meilleures intentions. Cette loi anti-haine est digne de Xi Jinping. D’une part, elle oblige les plateformes à
retirer tous contenus haineux dans les 24h sous peine de grosses amendes. Pour tenir ces critères, les plateformes devront nécessairement déléguer
la censure à des algorithmes avec des seuils de censures très larges. La liberté d’expression n’existera plus, la moindre critique sera interprétée
comme de la haine et supprimer automatiquement. De plus, le contenu haineux sera défini par la police sans passer par le moindre juge ou par le moindre
débat public. Cette loi va devenir le bras armé de la police qui va se transformer en police des moeurs.

Cellibrite pour tout le monde Notre beau pays vient d’équiper la gendarmerie de centaines de boîtiers de la marque Cellebrite.
Cellebrite est une entreprise championne dans l’extraction de données. Et c’est justement son produit phare, capable de siphonner les données de 95%
des smartphones que vient de se payer la gendarmerie. Cellebrite se targue de pouvoir siphonner n’importe quel iPhone et Android même verrouillé par
code et chiffré. La gendarmerie se dote des moyens de faire de véritables perquisitions numériques, et ce pour toute personne en garde à vue.
La perquisition physique, elle n’est autorisée que par un mandat d’un juge et avec de solides arguments.
Sachant qu’un gendarme peut mettre n’importe quelle personne en garde à vue, l’usage de cet appareil est parfaitement contre les droits de l’homme.

Alicem Alicem pour Authentification en LIgne CErtifée par Mobile, est la volonté de l’État français d’utiliser la reconnaissance faciale pour utiliser
les services publics en ligne. Le système commence avec Android, il est dorénavant obligatoire de s’identifier avec son visage pour se connecter à la
Sécurité Sociale ou aux Impôts. Ce n’est que la première brique du système de surveillance chinois, que nos élites transposent tranquillement chez nous.
Le système va s’étendre d’Android à tout appareil puis de quelques sites à toute l’administration. Et on finira avec une note sociale comme en Chine.

Reconnaissance Faciale Non content de la mise en place d’Alicem, Cédric O, "évoque également la volonté du gouvernement d’utiliser la reconnaissance
faciale dans les gares et aéroports pour identifier les personnes…”

Est-ce que ces informations vous surprennent ? Vous n’avez jamais entendu ? C’est quand même plus problématique que l’oeil de Jérôme Rodrigues ?

## Pourquoi la violence ?
La violence n’est pas une fin en soi. Elle n’est qu’un moyen comme un autre utiliser par un régime totalitaire pour se maintenir au pouvoir.
D’ailleurs une autre technique tout aussi répondue est de “payer la paix sociale”. L’Arabie saoudite s’est pendant très longtemps abstenue de
lever la moindre taxe ou impôt pour plaire à la population. Le pays emploie aussi 60% de la population, le plus souvent, dans de faux jobs, pour
éviter les chômeurs mécontents comme durant le printemps arabe.

Si des pays réputés totalitaires, comme l’Arabie saoudite, sont prêts à dépenser de l’argent en aides plus qu’en violence. Alors il faut bien se
rendre compte que la violence n’est pas la meilleure solution pour se maintenir au pouvoir. Les budgets pour l’armée et la police sont gigantesques.
D’ailleurs, tout compte fait, le pouvoir revient plus à l’armée qu’au dictateur et il peut se retrouver dans un coup d’État.
De plus, la population voit sa colère monter au fur et à mesure. En résumé, une dictature par la violence coûte un bras et donne envie à tout le pays
de vous tuer… pas très pérenne comme solution.

Imaginons qu’un citoyen se révolte. Avant internet et l’écoute de masse, le gouvernement ne pouvait intervenir qu’une fois que le citoyen est devenu
un dissident et commence ses actions (manifestations, articles, etc.). C’est ici que la violence arrive pour punir sévèrement le citoyen.
La violence n’a qu’un rôle de dissuasion, solution du pauvre qu’en on ne peut rien prédire.

## Une dictature sans violence
Avec la technologie, dès qu’un citoyen commencera à avoir des doutes, il ira se documenter sur internet. Si internet est contrôlé par le gouvernement,
sa requête va déclencher des alertes. Au lieu d’envoyer la police, le régime préféra la manière douce, il va générer des milliers de sites basés sur
le profile du citoyen pour essayer de lui faire accepter le régime.

Le citoyen pensera mener une enquête par des faits concrets et sa raison. Alors que chaque site sera méticuleusement construit pour lui par l’État dans
le but de manipuler son jugement. Si les algorithmes sont assez performants, après plusieurs heures de recherche le citoyen se convaincra lui-même que
le gouvernement est légitime. Pour être sûr, l’État manipulera un peu plus son fil d’actualités pour atténuer aux maximums les suspicions.
Le citoyen rentrera dans le moule, et abandonnera la révolte, sans que le gouvernement ait fait preuve de la moindre violence.

Cet exemple n’est pas si futuriste. La surveillance de masse chinoise y ressemble beaucoup. L’affaire Cambridge Analytica a montré que l’on peut proposer
du contenu pour chaque citoyen.

Si on laisse l’état dicter les lois en matière d’information et de technologie, alors le futur verra les premières dictatures sans dissidents, donc sans
violence et bruits de bottes.
