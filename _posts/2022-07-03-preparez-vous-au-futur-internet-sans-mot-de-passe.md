---
title: Préparez-vous à l'internet sans mot de passe.
subtitle: Pourquoi tout le monde veut la mort des mots de passe ?
date: 2022-07-03
layout: post
background: /img/2022/07/keyboard.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [internet, sécurité]
---

<iframe frameborder="0" loading="lazy" id="ausha-RiiT" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=BMPL1SdqJ6k7&v=3&playerId=ausha-RiiT"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

C'est la dernière nouvelle de la messe Apple 2022. L'entreprise lance son service *passkey* pour permettre à des sites internet d'utiliser les capteurs biométriques FaceId ou TouchId pour s'authentifier. L'objectif est clair : tuer les mots de passe. Mais pourquoi vouloir tuer les mots de passe ? Est-ce une lubie californienne ? Les GAFAM ont-ils des arrière-pensées ?


<h2> Qui veut tuer les mots de passe ?</h2>

Un site internet gère des milliers d'utilisateurs, toutes les données sont regroupées dans un stockage commun appelé base de données. Le serveur doit donc faire le trier dans sa base entre chaque requête utilisateur. Or toutes les requêtes utilisateur se ressemblent !

Il est donc primordial d'authentifier l'utilisateur pour pouvoir sécuriser ses données. Depuis le début d'internet, on utilisait le paradigme d'un secret que *l'on sait*. Lors de votre inscription, vous donnez votre mot de passe au site, c'est le secret. Ensuite pour se connecter, il vous faut redonner ce secret.

Comme seuls vous et le site connaissez le secret, cette technique permet de vous authentifier avec certitude. Ou presque... Car avec le temps, on se rend compte que l'humain est très mauvais pour générer aléatoirement un secret et s'en souvenir. On se retrouve donc avec des utilisateurs qui utilisent des mots de passe faibles et réutilisés sur plusieurs sites.

Il faut savoir qu'une carte graphique moderne peut essayer de craquer [20 millions de mots de passe par seconde](https://tt-hardware.com/news/la-rtx-3090-peut-casser-rapidement-les-mots-de-passe/). Elle peut donc tester les 200 000 mots d'un dictionnaire 100 fois par seconde. N'importe quel mot de passe se basant sur un mot du dictionnaire y compris avec des variantes comme `Marseill13!` ou `piano123?` reste un mot de passe faible.

En théorie, la méthode par mots de passe semble parfaite. En pratique, elle rend très facile le piratage des comptes utilisateurs, car les secrets imaginés par les humains ne sont en fait pas du tout secrets et facilement retrouvables par un ordinateur.

Au final, tout le monde veut la mort des mots de passe. Les ingénieurs logiciels qui se retrouvent avec un casse-tête de sécurité sur les bras. Les utilisateurs qui en ont marre d'oublier leurs mots de passe et de les gérer. Les entreprises qui veulent ni freiner l'adhésion client avec trop de sécurité ni mettre avoir des poursuites en cas de fuite de données.

<h2> Comment faire autrement ? </h2>

Depuis plusieurs années, le paradigme reposant sur une information que *l'on sait* se voit remplacer par un nouveau paradigme basé sur ce que *l'on a*, qui peut prendre plusieurs formes.

<h3>Utiliser un compte tiers</h3>
C'est le fameux "Se connecter avec Google". L'authentification est déportée chez Google, ainsi il faut posséder un compte Google pour s'authentifier. On a donc un compte Google très sécurisé que l'on utilise pour s'authentifier sur d'autre site internet.

<h3>Capteur biométrique</h3>
Ce système est très présent sur les smartphones notamment chez Apple avec son FaceID et TouchID. Ses capteurs servaient déjà à authentifier l'utilisateur pour déverrouiller son smartphone et s'authentifier dans des applications. Maintenant, Apple souhaite le rendre accessible aux sites web avec [*passkey*](https://developer.apple.com/passkeys/).

Microsoft a quant à lui proposé la solution [*Hello*](https://support.microsoft.com/fr-fr/windows/d%C3%A9couvrir-puis-configurer-windows-hello-dae28983-8242-bb2a-d3d1-87c9d265a5f0) pour ouvrir son ordinateur Windows grâce à son empreinte digitale ou son visage.

<h3>Code à usage unique</h3>
On utilise aujourd'hui l'authentification par mail ou SMS. Pour prouver que vous possédez bien ce mail ou ce numéro, le site web va vous envoyer un code unique, qu'il faudra recopier sur la page de connexion.

<h3>Clé USB FIDO</h3>
Bien moins connu et pourtant très pratique, il existe des clés USB spéciales appelées clé [FIDO](https://fr.wikipedia.org/wiki/Alliance_FIDO) qui permettent de s'authentifier en ligne. Il faut brancher votre clé dans l'ordinateur lors de l'accès au site internet. Vous pouvez vous procurer l'originale haut de gamme (la [Yubikey](https://www.amazon.fr/Yubico-YubiKey-USB-Authentication-Security)) ou des concurrentes moins chères.

<h2>Est-ce dangereux ?</h2>
Alors d'un point de vue de la cybersécurité, toutes ces méthodes réduisent drastiquement le risque de se faire pirater par rapport au mot de passe.

Mais si l’on regarde bien, avec le paradigme de ce que *l'on a*, on remarque que notre authentification repose sur un service externe. Là où avec le mot de passe, il n'y a que vous et le site web pour faire l'authentification. Maintenant, on va avoir besoin d'un service tiers.

Avec le "Connecter avec ..." ou le *passkey* et *Hello*, vous dépendez de Google, Facebook, Apple ou Microsoft à chaque fois que vous vous authentifiez en ligne. Même chose pour recevoir les codes, il vous faut garder votre mail et votre numéro de téléphone.

Seule la clé USB évite la dépendance, puisque le système est autonome, il n'a besoin que du site web et de vous pour fonctionner.

<h2>Conclusion</h2>
Un monde sans mots de passe reste un monde meilleur pour notre sécurité en ligne. Les GAFAM qui gèrent et sont responsables de milliards de comptes utilisateurs ont donc intérêt à mettre fin aux mots de passe.

Il ne faut pas les prendre pour des enfants de chœur pour autant, chacun avance ses pions pour proposer sa technologie afin de rendre l'utilisateur encore plus dépendent d'eux.

Aussi, je recommande de se tourner davantage vers les codes par mails ou par SMS ainsi que la clé FIDO. D'autant plus que ces méthodes sont souvent disponibles simultanément, on se met donc à l'abri d'une trop grande dépendance à un service mail ou un opérateur téléphonique.


