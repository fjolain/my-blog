---
title: Pourquoi Christine Lagarde soutient Bitcoin
subtitle: Bien que ses paroles sont farouchement anti-bitcoin, chacune de ses décisions nous donne des raisons d'avoir du Bitcoin.
date: 2022-06-18
layout: post
background: /img/2022/06/cl.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [société, économie, bitcoin]
---

<iframe frameborder="0" loading="lazy" id="ausha-kQK6" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=bW6AGu3Qajgm&v=3&playerId=ausha-kQK6"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>


Plus Christine Lagarde renie Bitcoin, plus elle met en évidence les bénéfices de Bitcoin face à sa gestion monétaire. Nous verrons donc les 3 bénéfices du Bitcoin les plus détestés de Christine Lagarde.

<h2>I - Gestion apolitique et neutre</h2>

Que vous soyez de gauche comme de droite, femme ou homme, jeune ou vieux, français, russe ou chinois, en guerre ou en paix. Le Bitcoin est ouvert à tous sans aucune restriction.

Tout le monde peut utiliser le Bitcoin aussi bien comme simple usager que comme validateur de bloc. Les règles du Bitcoin ne dépendent pas d'un agenda politique, de l'idéologie du moment ou d'événements extérieurs.

Rien à voir avec Christine Lagarde qui accepte volontiers l'argent des Russes en Europe pour mieux les confisquer par la suite.

Ou encore, elle va attendre que son candidat-copain Macron soit réélu avant de remonter les taux. [Début mars](https://www.bfmtv.com/economie/economie-social/union-europeenne/la-bce-maintient-ses-taux-directeurs_AD-202203100340.html), Christine Lagarde se sentait frileuse à relever les taux. Il faut dire qu'avec 600 milliards d'euros de dette en 5 cinq ans et un Bruno LeMaire qui crie sur tous les toits que [ce n'est pas un problème ](https://www.publicsenat.fr/article/parlementaire/les-francais-n-ont-pas-d-inquietude-a-avoir-sur-le-financement-de-notre-dette) car les taux sont bas. Il ne faut pas contrarier vos copains avant l'élection. Mais une fois l'élection passée, vous pouvez faire [*une remontée des taux spectaculaire*](https://www.lemonde.fr/economie/article/2022/06/15/les-taux-remontent-fortement-en-europe_6130365_3234.html).

Face à la girouette de la BCE, qui à tout moment change les règles et distribue les bons et mauvais points, Bitcoin fait figure de temple immuable et ouvert à tous.

<h2>II - La fin de la planche à billets</h2>

Les règles bitcoin sont figées et elles ont été choisies pour protéger votre épargne d'une inflation. Bitcoin se veut comme un or numérique. Ainsi ses réserves sont fixées à 21 millions d'unités et plus le temps passe plus la *découverte* de nouveaux bitcoins tend vers zéro.

Là encore cela tranche avec l'inarrêtable planche à billets de Christine Lagarde. Depuis son entrée en fonction le 1er novembre 2019, le bilan de la BCE a quasi doublé. Sur les [4 800 milliards](https://twitter.com/Or_fr_/status/1465982997845446659?ref_src=twsrc%5Etfw) d'euro déjà déversé dans l'économie avant 2019, Christine Lagarde a rajouté à elle seule [3 700 milliards](https://twitter.com/Or_fr_/status/1465982997845446659?ref_src=twsrc%5Etfw). Ce qui culmine le bilan à 8 500 milliards d'euros soit 80% du PIB de l'EU.

L'humanité a utilisé de nombreuses monnaies tels le blé, les épices, le sel, les métaux ou même les coquillages. Mais avec les monnaies fiduciaires étatiques, c'est la première fois que les citoyens acceptent une monnaie imprimable à l'infini par leur souverain.

Cette ineptie doit cesser.

<h2>III - Etre sa propre banque</h2>

Dès que vous confiez votre argent à une banque, il cesse d'être votre argent. Si vous êtes dans le camp du bien, la banque vous octroie temporairement de jouir de votre argent, à condition de remplir toujours plus de cerfas pour prouver sa provenance.

Le problème est que la définition du bien est fixée par la banque centrale, et je vous rappelle que ses règles changent vite.

Les non-vaccinés canadiens en ont fait les frais. Puisque Trudeau a gelé leurs comptes suites aux Convoits de la Liberté. Christine Lagarde, elle a gelé les avoirs russes dès le début de la guerre en Ukraine. Pourtant la BCE n'a pas gelé les avoirs américains à la suite à l'invasion de l'Irak.

Cette volonté de prendre votre argent directement sur votre compte n'est pas un lointain cauchemar réservé à certaines dictatures. Du temps où Christine Lagarde dirigeait le FMI, celui-ci évoquait l'idée de taxer les épargnes de 10% pour rembourser les dettes ([source](https://www.francetvinfo.fr/sante/maladie/coronavirus/non-le-fmi-ne-propose-pas-de-taxer-10-de-lepargne-mondiale-pour-faire-face-a-la-crise-du-coronavirus_3963669.html), dont vous noterez au passage la mauvaise foi de franceinfo).

En France la loi Sapin 2 de 2016 [est très claire](https://fr.wikipedia.org/wiki/Loi_relative_%C3%A0_la_transparence,_%C3%A0_la_lutte_contre_la_corruption_et_%C3%A0_la_modernisation_de_la_vie_%C3%A9conomique) : *le Haut Conseil de stabilité financière est autorisé à suspendre, retarder ou limiter les retraits d’argent ou arbitrages sur l’assurance-vie* .

Et oui l'État peut geler les assurances vie à tout moment... En soit, il ne doit rien spoiler, mais bon si pendant le gèle de vos avoirs, une inflation de 10% passe dessus, c'est tout comme.

Du côté du Bitcoin tout est scellé mathématiquement. Vous êtes le seul détenteur de votre clé de cryptage. Personne ne peut geler ou voler vos Bitcoins. Dans le temple bitcoin immuable et ouvert à tous, la salle des coffres est publique et n'importe qui peut demander son coffre.

Tous les avantages du Bitcoin sur la BCE alimentent la haine de Christine Lagarde contre lui. Le pouvoir néfaste de Christine Lagarde sur l'Euro est impossible avec Bitcoin.

Merci Christine Lagarde de nous rappeler chaque jour votre dangerosité et les bonnes raisons d'avoir du Bitcoin.

