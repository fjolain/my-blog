---
title: Où est notre souveraineté numérique ?
subtitle: Notre pays a besoin du numérique autant que d'une armée. Alors où en est-on de notre souveraineté numérique française ?
date: 2022-11-31
layout: post
background: /img/2022/11/france.webp
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
---

La souveraineté renvoie souvent à l'armement, l'énergie ou la monnaie. Mais que ferait la France sans l'électronique chinoise ou les logiciels américains ? Même en disposant de nos armes, de notre pétrole ou de notre monnaie, peut-on s'en sortir sans ordinateur, smartphone ou internet ?

Une rapide réflexion débouche sur un non, notre pays comme le reste du monde a besoin du numérique autant que d'une armée. Alors où en est-on de notre souveraineté numérique française et européenne ?

La question est pointue, car les forces et faiblesses se cachent dans les détails. On commencera par diviser la dépendance au numérique en trois piliers. L'électronique que l'on fabrique (hardware), les logiciels qui tournent dessus (software) et les logiciels qui tournent à travers internet pour offrir un service en ligne (cloud).

<h2>Dépendance hardware</h2>

Sans hardware pas de numérique. Le monde *dématérialisé* repose sur beaucoup de matériels. Nous avons d'un côté les serveurs, des ordinateurs sans écran ni clavier conçu pour s'empiler dans des datacenters. C'est dans ses serveurs qu'arrivent nos requêtes Google, nos photos de vacances ou notre épargne puisque la majorité des euros sont numériques.

Les datacenters sont reliés entre eux par des fibres optiques tirées à travers le monde. Rien que ce domaine peut faire l'objet d'une analyse géopolitique détaillée. Les Etats [surveillent tous les câbles](https://fr.wikipedia.org/wiki/Tempora) qui passent par leur territoire. Des entreprises comme Facebook ou [Google](https://cloud.google.com/blog/products/infrastructure/learn-about-googles-subsea-cables?hl=en) investissent dans ces infrastructures critiques pour accroître le nombre d'internautes et donc d'utilisateurs.

Côté français, nous disposons d'[Alcatel](https://fr.wikipedia.org/wiki/Alcatel_Submarine_Networks), un leader dans la pose de câble sous-marin. Notre façade atlantique offre aussi un noeud de transit important entre l'Europe et l'Amérique.

En périphérie de ce réseau de datacenters et de câbles se trouvent nos appareils : ordinateurs, smartphone, objets connectés.

Toute cette architecture repose sur l'utilisation massive de semi-conducteurs. Ils permettent la conception de carte électronique pour piloter des écrans, aiguiller les données à travers internet, réaliser des calculs ou stocker des données.

Historiquement les fabricants de composants électroniques étaient en Occident. Certains résistent comme [ST Micro](https://en.wikipedia.org/wiki/STMicroelectronics) en France, [Bosch](https://en.wikipedia.org/wiki/Robert_Bosch_GmbH) en Allemagne, [Ericsson](https://en.wikipedia.org/wiki/Ericsson) en Suède, [Nokia](https://fr.wikipedia.org/wiki/Nokia) en Finlande, [NXP](https://en.wikipedia.org/wiki/NXP_Semiconductors) aux Pays-Bas, [Intel](https://en.wikipedia.org/wiki/Intel) ou [Texas Instruments](https://en.wikipedia.org/wiki/Texas_Instruments) aux États-Unis.

Aujourd’hui, la majeure partie des fabricants est en Asie, précisément à Taiwan et ses alentours.

On peut noter les Taiwanais [TSMC](https://en.wikipedia.org/wiki/TSMC), [Foxconn](https://en.wikipedia.org/wiki/Foxconn), [MediaTek](https://en.wikipedia.org/wiki/MediaTek); les Coréens [LG](https://en.wikipedia.org/wiki/LG_Corporation) et [Samsung](https://en.wikipedia.org/wiki/Samsung); les Japonais [Toshiba](https://en.wikipedia.org/wiki/Toshiba), [NEC](https://en.wikipedia.org/wiki/NEC), [Fujitsu](https://en.wikipedia.org/wiki/Fujitsu); et les Chinois [ZTE](https://en.wikipedia.org/wiki/ZTE), [Huawei](https://en.wikipedia.org/wiki/Huawei#Competitive_position) ou [Allwinner](https://en.wikipedia.org/wiki/Allwinner_Technology). Le poids de la Chine est prépondérant, car même les entreprises hors Chine disposent d'une part importante de leurs usines en Chine, notamment à [Shenzhen](https://en.wikipedia.org/wiki/Shenzhen).

Une autre complexité du hardware vient de la fragmentation des acteurs dans la réalisation des puces. Prenons l'exemple de la nouvelle puce M1 d'Apple qui équipe les nouveaux Mac :

Concevoir à partir de rien un processeur demande des années de développement pour mettre au point des circuits électroniques fonctionnels. L'entreprise [ARM](https://en.wikipedia.org/wiki/ARM_Architecture_(company)) a bousculé le marché en mutualisant cette phase. ARM vend uniquement des licences sur des schémas de circuit électronique pour concevoir un processeur. Ainsi, toutes les puces des tablettes, smartphones, objets connectés sont conçues sur une base ARM.

La puce M1 est basée aussi sur ARM, une petite révolution pour un processeur d'ordinateur. Ce secteur était avant la chasse gardée d'Intel.

Apple achète donc les licences à ARM pour concevoir ses processeurs. Ensuite il demande à TSMC de fabriquer le processeur, enfin l'ensemble du Mac est assemblé par Foxconn.

----------------
**La Battaille du 5nm**

Certaines batailles se jouent à un cheveu, voire même à 10 millièmes de cheveux. Plus les transistors du circuit électronique sont fins, plus on peut densifier le circuit et moins il va dissiper de chaleur. Par conséquent, un processeur avec une gravure plus fine permet d'augmenter ses performances tout en consommant moins.

La gravure en [5nm](https://en.wikipedia.org/wiki/5_nm_process) est devenue le Graal. La seule entreprise capable de fabriquer la machine est [ASML](https://en.wikipedia.org/wiki/ASML_Holding) aux Pays-Bas. Les seules entreprises capables de fabriquer des puces à 5nm sont le coréen Samsung et le taïwanais TSMC, pour quelques clients seulement comme Apple ou Huawei.

-------------------------------------

Le hardware est un secteur de leaders écrasants voire de monopole. Cela a commencé avec Intel sur le monopole des PC, en contrôlant toute la chaîne du développement à la commercialisation en passant par la fabrication.

Aujourd'hui, en dehors des PC, la structure intégrée d'Intel est fragmentée. ARM a le monopole du développement. Plusieurs marques comme Qualcoom, Texas Instrument, Apple ou NXP conçoivent des puces à base ARM pour l'industrie ou leur propre usage.

À la fin toute la fabrication se concentre sur un rayon de 500 km autour de Taiwan avec quelques entreprises notamment TSMC.

Vu la panoplie des acteurs et leurs interactions, la souveraineté totale sur le hardware est illusoire. Pourtant dépendre d'une puissance étrangère pour son hardware ouvre la porte d'une surveillance de masse. Comme les composants, de la taille d'un grain de riz, [insérés par la Chine dans les serveurs d'Amazon](https://www.cnbc.com/2018/10/04/chinese-spy-chips-are-said-to-be-found-in-hardware-used-by-apple-amazon-apple-denies-the-bloomberg-businessweek-report.html) pour espionner les données. Ou les révélations de Snowden sur [la NSA qui installe des mouchards](https://www.numerama.com/politique/29353-nsa-routeurs-serveurs-backdoor.html) dans le matériel CISCO exporté. Récemment les [États-Unis ont interdit le matériel Huawei](https://en.wikipedia.org/wiki/Huawei#Allegations_of_espionage) de leur infrastructure télécoms pour cette raison.

Il faut donc limiter la casse pour avoir notre souveraineté. Nous disposons d'entreprises capables de concevoir de l'électronique comme Sagem, Thalès ou ST Micro. Il reste à relocaliser les usines en France.

La dépendance aux licences ARM a poussé des industriels et chercheurs à développer une nouvelle base appelée [RISC-V](https://en.wikipedia.org/wiki/RISC-V). Les travaux sont publiés en open source, tout le monde peut donc utiliser cette base librement. Y compris la [Chine](https://www.scmp.com/tech/big-tech/article/3178109/tech-war-china-bets-open-source-risc-v-chip-design-minimise-potential) et la [Russie](https://www.anandtech.com/show/16827/russia-to-build-riscv-processors-for-laptops-8core-2-ghz-12nm-2025), qui ont lancé leur programme national de processeurs RISC-V pour se défaire d'Intel ou ARM.

<h2>Dépendance Software</h2>

Pour utiliser le potentiel du harware, il faut concevoir les logiciels qui vont tourner dessus. Il existe autant de logiciels que d'usage. À travers le monde, différentes entreprises conçoivent des jeux vidéo comme le français [Ubisoft](https://fr.wikipedia.org/wiki/Ubisoft), des logiciels d'ingénierie comme le français [Dassault System](https://fr.wikipedia.org/wiki/Dassault_Syst%C3%A8mes) ou encore des lecteurs multimédias comme le français [VLC](https://fr.wikipedia.org/wiki/VLC_media_player).

Cependant, certains logiciels sont plus critiques que d'autres pour notre économie et notre souveraineté. Nous allons parler du système d'exploitation, *Operating System (OS)*. Il s'agit du logiciel principal, il va gérer internet, les périphéries (clavier, souris, etc), le stockage, l'interface graphique et le cadencement de toutes les applications installées.

En économie, l’OS agit comme un loquet et tend vers le monopole. Les applications sont conçues pour un OS précis tel Window. Si une entreprise choisit initialement Window et utilise des logiciels uniquement sur Windows tels Office, SharePoint ou Access, alors l'entreprise se retrouve coincée avec Microsoft. Une fois un OS choisi et répondu, il est très coûteux d'en sortir.

Ensuite un nouvel utilisateur va choisir l'OS avec le plus d'applications disponibles. Or les éditeurs se concentrent sur l'OS de la majorité. Ainsi plus un OS a d'utilisateurs, plus il aura d'applications, plus il aura d'utilisateurs, etc.

Microsoft a su parfaitement orchestrer cette stratégie quitte à flirter avec l'illégalité. Ses pratiques douteuses, lui ont permis de s'accaparer un marché et des royalties à vie. En soi, l'amende record de [2 milliards de dollars](https://fr.wikipedia.org/wiki/Microsoft#Controverses_et_affaires_judiciaires) pour l'UE ou les [poursuites antitrust américaines](https://en.wikipedia.org/wiki/United_States_v._Microsoft_Corp.) valaient bien la peine de se passer de la légalité pour acquérir les premiers utilisateurs.

En politique, l'OS permet d'espionner son utilisateur. Le point de vue omniscient de Microsoft sur les entreprises du monde entier est [utilisé par la NSA](https://fr.wikipedia.org/wiki/PRISM_(programme_de_surveillance)) comme arme économique et politique.

Encore une fois, l'open source vient à la rescousse de la souveraineté. Dans le software, l'open source est la norme. Par exemple, Android de Google repose sur Linux.

Pour concevoir son système d'exploitation national, il ne faut donc pas tout réécrire. La [Chine](https://www.linuxadictos.com/fr/china-quiere-su-propio-sistema-operativo-y-solo-utilizar-hardware-y-software-local.html) et la [Russie](https://fr.wikipedia.org/wiki/Astra_Linux) conçoivent leur OS national à partir de Linux. La [gendarmerie française utilise Linux](https://fr.wikipedia.org/wiki/GendBuntu) comme OS principal depuis 2008.

Pour finir dans le software, les virus informatiques sont devenus de véritables armes. Le virus [Stuxnet](https://fr.wikipedia.org/wiki/Stuxnet) conçu par les États-Unis et Israël a permis de saboter le programme nucléaire iranien.  De même pour la Russie qui a plongé l'Ukraine dans le noir en [déployant un virus sur le réseau électrique](https://en.wikipedia.org/wiki/Industroyer). Un simple logiciel peut mettre à terre un site industriel tel un missile.

Pour Israël ses virus sont même devenus une ressource d'échange diplomatique. Beaucoup de pays arabes, comme le Maroc, l'Arabie Saoudite ou les Emirats ont régularisé leur relation avec Israël pour avoir accès au virus israélien [Pegasus](https://en.wikipedia.org/wiki/Pegasus_(spyware)) capable de hacker n'importe quel smartphone pour transformer l'appareil en mouchard contre son propriétaire.

Comme les armes, les États sont dans une course pour mettre au point leurs virus et contrer ceux ces autres.

<h2>Dépendance Cloud</h2>

Le dernier pilier pour la souveraineté numérique repose sur le cloud. On image d'abord les services en lignes qui aspirent nos données vers les serveurs américains ou chinois, comme le font Facebook ou Tiktok.

Cela est déjà un vaste problème, mais ne représente que la partie émergée de l'iceberg. Dans le cloud actuel, un site français hébergé en France peut se retrouver malgré tout sous la loi américaine. Comment est-ce possible ?

Il est très coûteux d'avoir ses propres datacenters. Cette activité est une véritable industrie qui nécessite des savoir-faire et des capitaux.

Aussi les datacenters sont gérés par des acteurs spécialisés qui louent leurs serveurs aux entreprises. [OVH](https://en.wikipedia.org/wiki/OVHcloud) est par exemple le leader français dans ce domaine.

Pour déployer un site web, je ne vais pas allumer un serveur dans ma cave. Non, je me connecte sur OVH, je peux commander un serveur chez OVH, aussi simplement que commander une pizza. Une fois le serveur prêt, je reçois un lien pour téléverser le site sur le serveur pour le publier.

Une seule entreprise se retrouve donc à héberger des millions de sites de milliers d'entreprises. À ce jour, les plus gros hébergeurs sont américains avec [AWS](https://en.wikipedia.org/wiki/Amazon_Web_Services) d'Amazon, GCP de Google, et Azur de Microsft.

La concurrence française existe, on peut citer OVH, [Scaleway](https://en.wikipedia.org/wiki/Scaleway) ou [Outscale](https://fr.outscale.com/) de Dassault. Il n'a donc pas de barrières techniques à notre souveraineté, mais plus un manque de volonté. Les grandes entreprises, tout comme les startups, préfèrent encore signer avec Amazon que Scaleway pour le prestige.

------------------------

**CloudWatt et Numergy sont dans un bateau. Qui tombe à l'eau ?**

Suite aux révélations de Snowden sur la NSA, Dassault presse le pas au gouvernement pour lancer un programme de cloud souverain. Après des réflexions intenses entre énarques, le projet [Andromède](https://fr.wikipedia.org/wiki/Androm%C3%A8de_(cloud)) apparaît. L'État décide de ne rien investir sur le leader actuel OVH, mais plutôt de lui créer deux concurrents.

D'un côté CloudWatt confié à Orange et Thales, de l'autre Numergy avec SFR et Bull. Quant à Dassault, le groupe a senti l'échec et préférera lancer son propre cloud Outscale.

Dassault a eu le nez creux, Andromède fut un échec total après [285 millions d'euros investis](https://www.lemondeinformatique.fr/actualites/lire-285-millions-d-euros-pour-andromede-le-cloud-souverain-francais-41990.html). Outscale, lui, se porte bien.

------------------------------------------------------


Bien évidemment, pour profiter de l'hégémonie des entreprises américaines dans l'hébergement cloud, la maison blanche a ratifié le [Cloud Act](https://fr.wikipedia.org/wiki/CLOUD_Act) en 2018, qui impose qu'un datacenter géré par une firme américaine reste sous la loi américaine y compris en dehors du territoire.

Ainsi les datacenters européens de Google, Amazon ou Microsoft sont sous la loi américaine et [l'espionnage de la NSA](https://fr.wikipedia.org/wiki/XKeyscore). Devant une telle ingérence, il est choquant de voir l'état français signer un contrat avec Microsoft pour [stocker les données de santé des Français](https://fr.wikipedia.org/wiki/Dossier_m%C3%A9dical_partag%C3%A9#Le_Health_Data_Hub).

<h2>Conclusion</h2>

La souveraineté numérique regroupe plusieurs facettes. Si avoir une souveraineté totale semble impossible, il est envisageable d'avoir une souveraineté sur les logiciels et le cloud au moins pour les acteurs critiques du pays. Nous disposons de suffisamment d'entreprises et de développeurs pour disposer de nos propres OS et Cloud.


La souveraineté hardware est plus compliquée. Nous partons avec du retard sur un domaine très complexe. Mais les enjeux justifient les efforts. Nous ne pouvons plus compter uniquement sur un approvisionnement asiatique. Que se passerait-il si plus un seul disque dur ou processeur n'arrive en France suite à une invasion de Taiwan par la Chine ?



