---
title: Pourquoi Téléperformance se retrouve à gérer des images pédophiles
subtitle: L'État profite de la technologie pour donner ses devoirs régaliens à des entreprises privées.
date: 2022-11-20
layout: post
background: /img/2022/11/teleperformance.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
---

<iframe frameborder="0" loading="lazy" id="ausha-SSIB" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=BGP0jSgdMj62&v=3&playerId=ausha-SSIB"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

<h2>Qu'est-il arrivé à Téléperformance ?</h2>


L'entreprise [Téléperformance](https://fr.wikipedia.org/wiki/Teleperformance) été connut pour sa santé de fer et ses performances boursières. Son cours a été multiplié par 10 000% depuis son introduction en bourse, au point de la faire rentrer au CAC40 en 2020.

Téléperformance emploie 420 000 personnes à travers le monde pour sous-traiter des services consommateurs distants, comme les centres d'appels pour le SAV ou la vente, ou encore le contrôle d'identité, lors de l'inscription à une néo-banque en ligne.

L'un de ses services consiste à *modérer* le contenu en ligne, ce qui signifie traquer les images pédophiles ou terroristes. La maltraitance que ce service entraîne chez ses employés a éclaté dans [un article du Time](https://time.com/6231625/tiktok-teleperformance-colombia-investigation/). Puis le vice-ministre colombien en charge des Relations du travail (l'affaire a lieu en Colombie) [a ouvert une enquête](https://twitter.com/PalmaEdwin/status/1590084521080557568?s=20&t=iER1GbWUj_ych8Ra6K2DiQ
) à l'encontre de l'entreprise.

Ce qu'il faut se demander est pour quelle raison une entreprise privée se doit d'employer des personnes pour regarder des enfants se faire violer ?

Comment Téléperformance se retrouve-t-elle à traquer les déviances humaines à la place de la police ?

<h2>GAFAM et État pour le pire</h2>

Le dérapage a commencé avec Facebook et Google. Les deux entreprises ont bâti leur empire sur la collecte des données de leurs utilisateurs.

Ils n'hésitent pas à contrevenir à l'[article 12 des Droits de l’Homme](https://www.un.org/fr/universal-declaration-human-rights/index.html) : *Nul ne sera l'objet d'immixtions arbitraires dans sa vie privée, sa famille, son domicile ou sa correspondance*. Facebook espionne les messages y compris ceux *privés*, il a par exemple donner à la police [une correspondance entre une mère et sa fille](https://www.letemps.ch/monde/facebook-transmis-echanges-prives-dune-mere-fille-accusee-davortement-illegal).

Google analyse tous vos emails. [Un père de famille a été radié de Gmail](https://www.nytimes.com/2022/08/21/technology/google-surveillance-toddler-photo.html), car il a envoyé un mail à son pédiatre contenant une photo de son enfant nu.

Or, avec les lettres, nous mettons bien une enveloppe pour garder notre correspondance privée. Il est interdit à LaPoste comme à quiconque d'ouvrir notre courrier sous peine d'une [amende de 45 000€](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000042193573#:~:text=Version%20en%20vigueur%20depuis%20le%2001%20ao%C3%BBt%202020&text=Le%20fait%2C%20commis%20de%20mauvaise,45%20000%20euros%20d'amende.).

Pourtant c'est ce que fait Facebook, Google et Cie.

Les gouvernements, au lieu d'interdire  cette pratique illégale, l'ont utilisé à leur avantage. D'abord en instaurant un espionnage de masse secret. Puis, voyant que même les révélations de Snowden ne choquaient pas le peuple, ils ont légalisé la pratique.

Il est officiellement demandé aux acteurs d'internet d'espionner tout ce qui transite chez eux ([Loi Renseignement](https://fr.wikipedia.org/wiki/Loi_relative_au_renseignement) en France, [Cloud Act](https://fr.wikipedia.org/wiki/CLOUD_Act) aux États-Unis). Les plateformes doivent donc traquer les contenus illégaux.

Encore une fois, on ne demandera jamais à LaPoste de traquer les images pédophiles dans les lettres ? Dans le monde physique, on refuse l'espionnage, la traque de contenus illégaux se fait par la police de manière ciblée.

Mais Google et Facebook ont instauré un monde numérique sans vie privée. L'État a vu une aubaine pour nous espionner. La traque est passée de la police aux compagnies privées.

En théorie, tout est parfait d'après les GAFAM. Leur intelligence artificielle est tellement performante qu'elle peut tout détecter. Il ne doit donc pas avoir d'humain dans le process. Pas d'humain, donc pas de souffrance pour lui, et pas d'espionnage pour les utilisateurs

En pratique l'IA reste défaillante, elle peut facilement se faire berner. Des humains sont donc apparus dans le process pour analyser en plus de l'IA. Ce job de *modérateur* a déjà [fait parler de lui](https://www.20minutes.fr/high-tech/2304779-20180709-moderateur-facebook-enfer-ancienne-employee-raconte), aucune personne n'est prête pour regarder un tel contenu toute la journée.

Tiktok a récemment eu [une vague de contenu sexuel](https://fr.finance.yahoo.com/actualites/tendance-tiktok-propuls%C3%A9-dick-pics-130105926.html) sur son réseau à l'insu de son IA. Le réseau sous-traite donc la modération de son réseau à Téléperformance.

Encore une fois, ce boulot de modérateur n'a pas à être fait. LaPoste ne l'a jamais fait sans qu'on y trouve à redire, pourquoi se serait différent sur internet ?

Ce qu'on attend de l'État  n'est pas un espionnage de masse délégué à des compagnies privées. Mais des enquêtes de police et de procureur, basées sur des indices et des faits, contrôlés par des juges. Or, ces temps-ci, le travail de la justice et de la police pour traquer les pédophiles semble au point mort.

Le "suicide" d'[Epstein](https://fr.wikipedia.org/wiki/Jeffrey_Epstein) reste classé sans suite, ses clients ne semple pas inquiétés. De même avec le disque dur très compromettant de [Hunter Biden](https://www.contrepoints.org/2022/03/25/423776-hunter-biden-les-complotistes-avaient-encore-raison), la justice ou le FBI ne semble pas pressé d'enquêter.

Dans le monde physique, les États semblent laisser les pédophiles tranquilles. Par contre dans le monde numérique, les États nous prennent tous pour des pédophiles à surveiller.

Loin de l'utopie libérale, Internet se transforme en régime totalitaire au-dessus du bon sens et des droits fondamentaux, pour le plus grand plaisir des États et des GAFAM. On se retrouve donc avec des jobs ignobles pour satisfaire les dirigeants.

