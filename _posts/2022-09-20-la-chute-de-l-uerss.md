---
title: La chute de l'UERSS
subtitle: L'Union Européenne des Républiques Socialistes Soviétique se précipite vers sa chute.
date: 2022-09-18
layout: post
background: /img/2022/03/eu.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
---

<iframe frameborder="0" loading="lazy" id="ausha-Wy9n" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=ow5kGtK3eQja&v=3&playerId=ausha-Wy9n"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

<h2>De l'UE à l'UERSS</h2>

Il faut se remémorer de l'UE libérale. Son histoire commence avec la communauté européenne du charbon et de l'acier en [1951](https://fr.wikipedia.org/wiki/Communaut%C3%A9_europ%C3%A9enne_du_charbon_et_de_l%27acier). Son but figure dans le titre, créer une solidarité autour du charbon et de l'acier pour accroître les économies de chacun.

Cette vision libérale centrée sur l'économie est arrivée à son maximum avec Shengen en [1985](https://fr.wikipedia.org/wiki/Accord_de_Schengen) et l'ECU (ancêtre de l'EURO) en [1979](https://fr.wikipedia.org/wiki/European_Currency_Unit). L'UE voulait que l'argent, les citoyens, les entreprises et les idées circulent le plus librement possible pour favoriser la richesse de chacun.

Aujourd'hui, UERSS cherche à nous appauvrir. Elle a sciemment réduit l'accès à l'énergie en [dissuadant le nucléaire](https://www.cnbc.com/2022/03/09/why-eu-didnt-include-nuclear-energy-in-plan-to-get-off-russian-gas.html) et en [stoppant le gaz russe](https://www.euronews.com/my-europe/2022/06/02/explained-why-an-eu-embargo-on-russian-gas-might-be-a-long-way-off). Auparavant, l'UE facilitait l'accès au charbon.

L'UERSS veut [interdire les voitures à essence](https://www.lefigaro.fr/conjoncture/le-parlement-europeen-interdit-la-vente-de-vehicules-thermiques-neufs-a-partir-de-2035-20220608), alors que cela reste une des rares industries encore rentables dans l'Europe. Auparavant, l'UE facilitait l'accès à l'acier pour aider l'industrie.

L'UERSS souhaite traquer le moindre flux financier quitte à vouloir [interdit des nouvelles technologies comme Bitcoin](https://journalducoin.com/defi/loi-mica-bitcoin-europe-interdire-stablecoins/). Auparavant, l'EU voulait une libre circulation des capitaux.

L'UERSS enferme les citoyens chez eux, et [demande un passeport sanitaire](https://ec.europa.eu/info/live-work-travel-eu/coronavirus-response/safe-covid-19-vaccines-europeans/eu-digital-covid-certificate_fr) pour passer la frontière ou aller au restaurant. Auparavant, l'EU voulait une libre circulation des citoyens.

Encore une fois, l'UE ne devait que maximiser les synergies des économies européennes. Aujourd'hui UERSS, nous dit ce qu'on [doit manger](https://food.ec.europa.eu/horizontal-topics/farm-fork-strategy_en), [quelle voiture acheter](https://www.lefigaro.fr/conjoncture/le-parlement-europeen-interdit-la-vente-de-vehicules-thermiques-neufs-a-partir-de-2035-20220608), [comment se chauffer](https://www.letemps.ch/economie/crise-energetique-lue-veut-limiter-chauffage-batiments-publics), [quelle information regarder](https://www.liberation.fr/checknews/sur-quoi-se-base-lunion-europeenne-pour-interdire-rt-et-sputnik-20220302_XXHYWOKMPVAR7GXDZHXWPWSSTM/), comment [élever nos enfants ](https://www.lefigaro.fr/international/2018/11/30/01003-20181130ARTFIG00148-l-interdiction-de-la-fessee-fait-figure-de-norme-a-l-echelle-europeenne.php) ou même [comment se laver les mains](https://www.youtube.com/watch?v=sLa_QiWulPE). L'UERSS vient de [sortir un jeu de société](https://state-of-the-union.ec.europa.eu/von-der-leyen-commission-101_en) pour expliquer la gloire Ursula von der Leyen au bas peuple.

Il semble que l'élite européenne n'a pas supporté la chute de son utopie technocratique communiste en 1991. Elle n'a eu de cesse de recréer une technocratie en Europe de l’Ouest. Cela a commencé par Maastricht en [1992](https://fr.wikipedia.org/wiki/Trait%C3%A9_de_Maastricht) et se termine par Lisbonne en [2007](https://fr.wikipedia.org/wiki/Trait%C3%A9_de_Lisbonne).

Même cause, même conséquence, la technocratie de l’Ouest est en faillite à cause de choix calamiteux, parfois déjà testés par la technocratie de l'Est.

<h2>Comment les technocratie meurent</h2>
<h3>Effondrement de leur monnaie</h3>
Rien de mieux pour saboter une économie que de perdre la confiance dans sa monnaie. Les Romains précipitèrent leur perte en rachetant leur propre monnaie en or pour la refondre avec du cuivre au [III siècles](https://www.radiofrance.fr/franceculture/podcasts/le-cours-de-l-histoire/inflations-a-rome-l-empereur-fait-son-maximum-8144065). La Chine fut un brutal retour à l'[étalon-argent au XVI](https://www.orencash.fr/blog/largent-et-la-chine-une-histoire-profondement-liee/) après avoir trop joué avec ses billets. L'[hyperinflation allemande de 1923](https://fr.wikipedia.org/wiki/Hyperinflation_de_la_r%C3%A9publique_de_Weimar) a détruit ce qui restait de son économie après la défaite.

Aujourd'hui, après une émission monétaire débridée provoquée par les taux à 0% et le [quantitative easing](https://fr.wikipedia.org/wiki/Assouplissement_quantitatif), l'euro est plus faible que jamais. Il a descendu sous la parité [avec le franc suisse](https://www.boursorama.com/bourse/devises/taux-de-change-euro-francsuisse-EUR-CHF/) puis [le dollar](https://www.boursorama.com/bourse/devises/taux-de-change-euro-dollar-EUR-USD/). On espère tous éviter le plongeon et revenir à la stabilité.

<h3>La science d'état remplace la science des faits</h3>
Le signe le plus alarmant du déclin de  l'Europe se trouve dans sa science. Autrefois, joyaux de l'occident, la science européenne rayonnait sur le monde. Maintenant, elle sert les idéologies du gouvernement.

La science a la tâche ardue de prouver que les malheureux panneaux solaire remplaceront les centrales nucléaires. Pourtant le site d'EDF permet de comprendre l'arnaque. Les [données sur le solaire](https://www.edf.fr/groupe-edf/espaces-dedies/l-energie-de-a-a-z/tout-sur-l-energie/produire-de-l-electricite/le-solaire-photovoltaique-en-chiffres) montrent qu'un panneau solaire ne produit qu'à hauteur de 14% de sa capacité. Alors que [les données du nucléaire](https://www.edf.fr/groupe-edf/espaces-dedies/l-energie-de-a-a-z/tout-sur-l-energie/produire-de-l-electricite/le-nucleaire-en-chiffres) montre qu'une centrale nucléiare produit à hauteur de 71% de sa capacité. (Calcul ci-joint)

![img](/img/2022/09/nuclear_solar.png)

Elle doit prouver qu'un homme ne se définit pas sur des faits biologiques clairs comme ses chromosomes, ses organes sexuels ou ses hormones, mais selon le point de vue de chacun. D'ailleurs [un homme peut même tomber enceinte](https://www.lefigaro.fr/actualite-france/des-hommes-aussi-peuvent-etre-enceints-une-campagne-du-planning-familial-fait-polemique-20220820) !

Durant le covid, la science d'état tournait à plein régime. Il fallait [mesurer l'efficacité](https://www.dailymotion.com/video/x7sh6ng) des masques selon le stock disponible. Convaincre du [bénéfice-risque de s'injecter trois doses](https://www.service-public.fr/particuliers/actualites/A15393) d'un vaccin expérimental quand on n’est pas à risque. Expliquer pourquoi le vaccin, le confinement ou le pass-sanitaire sont très performants pour vaincre l'épidémie, même s'ils n'arrivent pas à aplatir une courbe.

Le communisme était très friand de la science d'état. On peut citer Staline pour qui l'hérédité, même restreinte aux graines, lui était insupportable. Heureusement, la science d'état, en la personne de [Lyssenko](https://fr.wikipedia.org/wiki/Trofim_Lyssenko), prouva bien vite qu'il n'y a pas d'hérédité dans les semences. Toutes ces régressions scientifiques ont provoqué des pénuries et des famines en URSS.

Mao, était lui aussi un fin expert en semences. Il a décidé qu'il fallait [tuer tous les oiseaux](https://www.youtube.com/watch?v=ojOmUWLDG18) pour éviter qu'ils mangent les graines dans les champs. Le peuple a exécuté l'ordre promptement en tapant dans des casseroles près des nids. Les oiseaux tombèrent morts de fatigue. Sans oiseaux dans les champs, les récoltes suivantes ont été dévastées par les insectes, il eut 45 millions de morts de faim suite au Grand Bond de Mao.

<h3>Ravager l'agriculture</h3>
On remarquera le goût prononcé des technocrates pour ravager l'agriculture. L'UERSS ne fait pas exception. Alors qu'elle vient de nous priver d'engrais azoté en votant les sanctions russes. L'UERSS pousse toujours son programme [*Farm to Fork*](https://food.ec.europa.eu/horizontal-topics/farm-fork-strategy_en) pour se passer de tout produit chimique dans l'agriculture.

Pourtant c'est bien des révoltes pour la faim qui ont renversé le gouvernement du Sri Lanka en [2022](https://www.rts.ch/info/monde/13353642-le-president-dechu-du-sri-lanka-est-rentre-apres-sept-semaines-dexil.html), la Syrie en [2012](https://fr.wikipedia.org/wiki/Syrie#Guerre_civile_syrienne) ou la Tunisie, l'Égypte et la Libye en [2010](https://fr.wikipedia.org/wiki/Printemps_arabe).

On peut même remonter à la Révolution française, le peuple se soulevait pour réclamer du pain. Ce à quoi Marie Antoinette répondit : ["Qu'ils mangent de la brioche !"](https://fr.wikipedia.org/wiki/Qu%27ils_mangent_de_la_brioche_!)

<h3>Qu'ils mangent de la brioche !</h3>

Cette élite totalement déconnectée de la vie de ses sujets est visible dans l'UERSS où cette phrase est déclinée à toutes les sauces.

- Vous n'avez plus d'essence dans la voiture ? Passez à l'électrique !
- Vous n'avez plus de gaz pour vous chauffer ? Installez une pompe à chaleur !
- Vous n'avez plus de pain ? Mangez du bio !

Les technocrates de l'UERSS ont appauvri le peuple, pourtant ils persévèrent à imposer leur morale et leur idéologie. Ils continuent de jouer de la musique, comme les musiciens du Titanic, mais ont-ils au moins vu l'iceberg devant eux ?

