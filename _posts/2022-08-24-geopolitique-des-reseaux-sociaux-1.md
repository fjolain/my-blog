---
title: Géopolitiques des réseaux sociaux, naissances d'un hyper-média.
subtitle: Les réseaux sociaux sont un hyper-média, capable de (dé)informer et d'espionner les citoyens.
date: 2022-08-23
layout: post
background: /img/2022/08/network.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [vie privée, internet]
---

<iframe frameborder="0" loading="lazy" id="ausha-LtfX" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=oKQ8Vu7gPOGa&v=3&playerId=ausha-LtfX"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Simple outil de communication pour nous, enjeux de puissance pour les États. Les réseaux sociaux canalisent les tensions à mesure qu'ils montrent tout leur potentiel.

<h2>I - Médias, guerres et États</h2>

La volonté des États de contrôler l'information pour manipuler l'opinion publique ne date pas des réseaux. Et dire qu'ils se servent des médias à cette fin est un pléonasme.

En effet, le mot *média* est le pluriel du mot latin *médium*, soit un milieu, un inter*média*ire, contrairement à immédiat, qui se fait sans intermédiaire. Le média est donc devenu cet intermédiaire entre nous et la société, entre nous et la réalité.

Chaque nouveau média va devenir un outil de communication pour informer la population. Les guerres servent à dater quand un média arrive à maturité et se retrouve utiliser par l'état.

Les guerres Napoléonnienne sont les premières à utiliser massivement la presse pour (dé)informer la population. Des aveux mêmes de Napoléon qui dit, juste après avoir censuré la presse le 18 Brumaires : [*Si je lâche la bride à la presse, je ne resterai pas trois mois
au pouvoir*](https://www.arretsurimages.net/chroniques/initiales-ds/commemorer-napoleon-4-la-presse-est-tellement-bete).

La radio comme arme de propagande a été anoblie durant la 2de guerre mondiale. Aussi bien par [le discourt du roi George VI](https://fr.wikipedia.org/wiki/Le_Discours_d%27un_roi) marquant l'entrée en guerre des Anglais en 1939, que l'appel du [18 juin 1940](https://fr.wikipedia.org/wiki/Appel_du_18_Juin) du Général de Gaule, ou [des nombreux](https://fr.wikipedia.org/wiki/Du_sang,_du_labeur,_des_larmes_et_de_la_sueur) discours de Churchill.

En 1991, la propagande rentre dans une autre dimension encore plus efficace. La télévision permet de faire vivre [l'invasion de l'Irak en live](https://fr.wikipedia.org/wiki/Guerre_du_Golfe#Couverture_m%C3%A9diatique), plongeant des millions d'Américains dans une peur permanente d'un conflit se déroulant pourtant à plus de 10 000 km de chez eux.


Aujourd'hui, avec la guerre d'Ukraine, nous sommes devant la première guerre qui se déroule sur Tiktok, Facebook ou Twitter. Les réseaux sociaux sont donc arrivés à maturité pour devenir une véritable arme de propagande convoitée par les États.

<h2>Naissance d'un hyper-média</h2>

Les réseaux sociaux apportent une double rupture avec les médias traditionnels.

Premièrement, le flux d'information est personnalisable à l'infini. Chaque utilisateur d'un réseau social à son propre flux d'information.

Rien à voir avec des millions de citoyens qui lisent, écoutent ou regardent ensemble un même discours. Ici, nous pouvons concevoir un discours pour chaque utilisateur. C'est exactement tout ce potentiel qui a été utilisé par [Cambridge Analytica](https://fr.wikipedia.org/wiki/Cambridge_Analytica) pour favoriser Trump en 2017.

Deuxièmement, le flux d'informations est bidirectionnel. En plus d'apporter l'information jusqu'à l'utilisateur, le réseau social repart avec des informations sur l'utilisateur. Les réseaux sociaux sont le premier média qui est autant un outil d'espionnage que de propagande.

Cet espionnage massif par les réseaux sociaux a été mis en lumière par [Edward Snowden](https://fr.wikipedia.org/wiki/Edward_Snowden) à travers les agissements de la NSA.

Récemment [Facebook a remis à la police américaine](https://www.letemps.ch/monde/facebook-transmis-echanges-prives-dune-mere-fille-accusee-davortement-illegal), les conversations entre une mère et sa fille. La police a ainsi pu interpeller la fille pour avortement illégal.

Oui, il faut le répéter, le fameux *si ça permet d'arrêter un terrorisme, je veux bien que l'État m'espionne* permet maintenant d'arrêter une fille qui avorte. On est bien loin du terrorisme.

Et oui, les réseaux sociaux permettent à l'État d'accéder à une conversation aussi privée que celle entre une mère et fille.

Cette incroyable versatilité des réseaux sociaux en fait un hyper-média, qui attire les gouvernements. La guerre des réseaux sociaux a commencé.


L'objectif pour un État est d'avoir le sien, de l'imposer sur les smartphones des citoyens tout en évitant les réseaux sociaux étrangers.

D'un côté, Facebook a été initialement financé par [In-Q-tel](https://fr.wikipedia.org/wiki/In-Q-Tel), le fonds d'investissement de la CIA. De plus, Mark Zuckerberg, son fondateur, semble au-dessus des lois. À chaque [scandale ou procès](https://fr.wikipedia.org/wiki/Facebook#Critiques_et_controverses), Zuckerberg a juste à faire de plates excuses aux juges pour reprendre de plus belle en totale impunité.

De l'autre, ce même fonds In-Q-tel a racheté le réseau social français Viadéo. Le rachat ne marche pas toujours, ainsi le rachat de la branche américaine de [Tiktok](https://fr.wikipedia.org/wiki/TikTok) par Oracle ne suffit pas. Les législateurs veulent tout simplement l'interdire.

**Les réseaux sociaux sont donc une arme d'espionnage et de propagande. Nous en sommes les munitions. Dans un prochain article, nous verrons comment réduire le danger des réseaux sociaux dans nos vies.**

