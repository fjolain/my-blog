---
title: 'Tuto : Occidentaliser une tradition étrangère &#8211; Exemple du yoga'
subtitle: Le manuel indispensable pour comprendre les bizarries occidentales
date: 2017-06-05T21:50:58+01:00
layout: post
background: /img/2017/06/yoga.jpg
categories: [article]
tags: [société]
---
Tu souhaites importer une tradition étrangère en occident, mais tu ne sais pas comment faire ? Tu as peur que les gens la rejettent ? Je te donne un petit tuto avec comme exemple le yoga. C&rsquo;est simple et ça fonctionne !

Voici la définition Wikipédia du yoga :

> Le **yoga** ( <span class="citation">« mise au repos »</span>) est l&rsquo;une des six écoles orthodoxes de la [philosophie indienne stika](https://fr.wikipedia.org/wiki/Philosophie_indienne "Philosophie indienne") dont le but est la libération ([moksha](https://fr.wikipedia.org/wiki/Moksha "Moksha")) du cycle des renaissances ([samsara](https://fr.wikipedia.org/wiki/Samsara "Samsara"){.mw-redirect}) engendré par le [karma](https://fr.wikipedia.org/wiki/Karma "Karma") individuel. C&rsquo;est une [discipline](https://fr.wikipedia.org/wiki/Discipline_%28sp%C3%A9cialit%C3%A9%29 "Discipline (spécialité)") visant, par la [méditation](https://fr.wikipedia.org/wiki/M%C3%A9ditation "Méditation"), l&rsquo;[ascèse](https://fr.wikipedia.org/wiki/Asc%C3%A8se "Ascèse") morale et les [exercices corporels](https://fr.wikipedia.org/wiki/Asana "Asana"), à réaliser l&rsquo;unification de l&rsquo;être humain dans ses aspects physique, [psychique](https://fr.wikipedia.org/wiki/Psych%C3%A9_%28psychologie%29 "Psyché (psychologie)") et [spirituel](https://fr.wikipedia.org/wiki/Spiritualit%C3%A9 "Spiritualité").

Aïe ça semble mal parti.

  1. On entend _cycle de renaissances_, alors que l&rsquo;Eglise et l&rsquo;Antiquité nous impose une vie éternelle dans une sorte de paradis ou d&rsquo;enfert.
  2. On arrive sur _unification de l&rsquo;être humain_, Descartes l&rsquo;a justement bien séparé entre la substance pensante (âme) et substance étendue (le corps). Freud a rajouté une couche bien dure entre conscient et inconscient histoire de ne jamais rien unifier.
  3. En plus la médecine occidentale est plus dans l&rsquo;optique de _réparer_ l&rsquo;homme, alors que la médecine orientale prône plus l&rsquo;_entretien_ quotidien_._
  4. Et enfin le _karma_, grâce à lui il y a une sorte de cause à effet dans tous nos actes. Ça passait il y encore un siècle avec les penseurs déterministes comme Spinoza ou Einstein qui enseignaient qu&rsquo;il n&rsquo;y a pas de hasard, tout est une cause. Sauf que depuis, l&rsquo;occident s&rsquo;est ancré dans l&rsquo;air des probabilités et statistiques avec notamment l&rsquo;arrivé de la mécanique quantique (Bohr) (cf [article](https://jolain.net/?p=132)) ou de la théorie du chaos (Pointcaré & Lorentz) (cf <a href="https://jolain.net/?p=201" target="_blank" rel="noopener noreferrer">article</a>).

Il y a du boulot pour occidentaliser !

## Légitimation scientifique

Ce sont les scientifiques qui forment le bien et le mal en occident. Si tu veux que ta tradition perce, il faut montrer patte blanche devant la science ! Peu importe que ta tradition soit prouvée expérimentalement depuis le II siècle av. J.C, seul les expériences comptent. Le yoga a donc vu débouler, une multitude d&rsquo;études sur lui pour prouver ses bienfaits. Quelques exemples d&rsquo;articles prouvant sa légitimité :

  * [Bienfaits du yoga sur votre corps : comment cette discipline le transforme dès le premier jour](http://www.huffingtonpost.fr/2015/06/19/bienfaits-yoga-corps-discipline-transforme-premier-jour_n_7613038.html)
  * [L’effet du yoga chez les patients atteints de cancer](https://jolain.net/wp-admin/post.php?post=214&action=edit)
  * [Yoga : une nouvelle étude confirme les effets bénéfiques sur la tension artérielle de cancer](http://www.ladepeche.fr/article/2016/12/08/2474826-yoga-nouvelle-etude-confirme-effets-benefiques-tension-arterielle.html)
  * [Les bienfaits du yoga](http://www.futura-sciences.com/sante/questions-reponses/sport-bienfaits-yoga-3121/)
  * &#8230;

Un article de Courier International parlait d&rsquo;un chercheur qui en vu des connaissances sur le cortex moteur
(partie du cerveau pour les mouvements) ne pouvait expliquer les bienfaits du yoga.
Il a alors étudié le sujet pendant longtemps avant de se rendre compte que notre vision du cerveau était trop simpliste,
et qu&rsquo;un lien peu effectivement exister entre les mouvements du corps et le stress.
Maintenant qu&rsquo;il a démontré la possibilité, il a bien voulu écouter sa famille, et se mettre au yoga&#8230;

## Améliorer les choses qui comptent vraiment

En occident, ce qui compte avant tout c&rsquo;est le travail, le sommeil (pour mieux travailler ?) et le sexe.
Ta tradition doit améliorer l&rsquo;une des trois si tu veux la voir utiliser.
Au diable, le bien être corporel, l’unification, la détente.
Si 10 minutes de yoga par jour peut te faire travailler tranquillement 2h de plus, c&rsquo;est ce qui compte.

![une magazine](http://www.boutiqueyogi.com/276-thickbox_default/yoga-journal-numero-4.jpg){:width="60%"}

*Dossier complet pour en faire au boulot. Et conseils pour avoir un beau cul!*

J&rsquo;ai remarqué le _livre sage comme une grenouille_.
Ce livre apprend aux enfants à méditer, il a fait un « développement fulgurant aux États-Unis et en Europe du Nord » selon l&rsquo;éditeur.
Les commentaires des acheteurs fnac sont étonnants, professeurs et parents l&rsquo;utilisent surtout pour reposer l&rsquo;enfant,
afin qu&rsquo;il soit plus concentré après sur le travail/devoir à faire.
Si l&rsquo;enfant se sent mieux, c&rsquo;est un petit bonus en plus.

Pour ce qui est du sexe, il suffit de taper « yoga sexe » dans Google pour voir le résultat, mon meilleur est [ici](http://ici.radio-canada.ca/premiere/emissions/les-eclaireurs/segments/chronique/20760/yoga-sexe-similitudes-concepts). Je reviendrai sur l&rsquo;importance du sexe pour percer le marché occidental.

## Créer un marché

On est pas champion du capitalisme pour rien.
Pour chaque communauté, il y doit y avoir des articles et des acheteurs.
Il serait très mal vu en occident de rentrer dans un groupe sociale comme les yoguistes sans rien dépenser. Dépenser c&rsquo;est appartenir.
Là encore le yoga pose problème, on est sensé avoir besoin de rien.
Au mieux d&rsquo;un drap posé dans l&rsquo;herbe. Qu&rsquo;importe tout est bon à vendre.
Les grandes marques ont leur étagères spéciales yoga comme [decathlon.](https://www.decathlon.fr/C-397322-yoga).
Une petite pensée pour Nature & Découverte qui a fait un bon business avec le [sac](http://www.natureetdecouvertes.com/bien-etre/yoga-gym-douce/tapis-coussins/ma-panoplie-de-yoga-15193150) à 60€ ou le [coussin](http://www.natureetdecouvertes.com/bien-etre/yoga-gym-douce/tapis-coussins/zafu-voyages-immobiles-15191410) à 38€.


![sac](http://cache.natureetdecouvertes.com.s0.frz.io/Medias/Images/Articles/15193150/650?frz-v97){:width="60%"}

## Rendre beau

Alors quand un occidental parle de beauté, il veut dire sexy.
On joue sur les mots voilà tout. Encore une fois le yoga par de loin, il faut faire oublier cette image du vieillard hindou.

![hindou](http://i.f1g.fr/media/ext/805x453/www.lefigaro.fr/medias/2014/02/05/PHOa2a36ab4-8e54-11e3-aa3a-8b5a50ca9905-355x453.jpg){:width="60%"}

Alors on peut se dire : Oui, il faut vendre les leçons, vendre les produits donc on fait des pubs classiques : femme, blonde, maigres, bien foutue.
On ne bouscule par les règles marketing occidentales établies.

![pub yoga]({{ site.baseurl }}/img/2017/06/yoga_pub.jpg){:width="100%"}

Mais même cette recherche de _beauté_ se retrouve en couverture de magazines, qui se veulent plus _nature_. C&rsquo;est mieux présentable que l&rsquo;hindou !<figure style="width: 260px" class="wp-caption aligncenter">

##  Conclusion

Et voilà : du sexy, de la science, des acheteurs et de la productivité, vous avez les clés du succès pour importer une tradition complètement non occidentale en occident.
Alors je n&rsquo;ai rien contre le yoga c&rsquo;était un exemple.
Il faut juste être curieux. Et un beau jour je me suis demandé comment le yoga a réussi à percer alors qu&rsquo;il est n&rsquo;est pas fait pour notre philosophie.
Et j&rsquo;ai remarqué tous ces mécanismes propres uniquement à l&rsquo;occident, qui ont permis « d&rsquo;occidentaliser » le yoga. Or ces mécanismes nous apprennent beaucoup sur notre société.