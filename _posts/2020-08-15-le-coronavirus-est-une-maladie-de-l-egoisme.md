---
title: Le coronavirus est une maladie de l'égoïsme
subtitle: Les virus muttent pour s'adapter aux sociétés surpeuplées et égoïstes
date: 2020-08-15
layout: post
background: /img/2020/08/foule.jpg
categories: [article]
tags: [société]
---


Avec le temps, le Covid dévoile sa carte d’identité. Touchant principalement les personnes de plus de 65 ans, il a, à l’heure actuelle, une létalité inférieure à 1%, il est très en dessous de son cousin le SRAS à 11% et encore plus du MERS à 35%. Sans compter sur Ebola est sa létalité supérieure à 50%. Même son taux de contagiosité le fameux R0, ne semble pas dangereux à 2.5, comparé au SRAS à 3.5 ou même la Rougeole à 18.

Bref, le Covid-19 semble moins dangereux que le SRAS, le MERS ou Ebola pourtant il a conquis la terre entière en quelque mois. Et toujours avec les données actuelles, avec ses 416 000 décès, il pulvérise le MERS à 600, le SRAS à 774 et même Ebola à 1 130.

**Comment, le moins menaçant des virus, est devenu le plus dangereux ?**

J’y vois personnellement deux grandes raisons. Premièrement, le mode de contamination par l’air le rend favorable aux zones densément peuplées et indépendant du niveau d’hygiène du pays.

Je m’explique, un virus comme Ebola a besoin d’une contamination directe par le toucher. Ce type de contamination comme la peste ou la lèpre se répand davantage dans les pays sans eau courante, sans toilettes ou tout-à-l’égout.  Bien que le Covid ait montré les lacunes de l’Europe sur l’utilisation du masque. L’Europe s’est préservée d’Ebola par son haut niveau d’hygiène. 

Le Covid n’en a que faire de notre tout-à-l’égout, si vous fréquentez un métro bondé, vous avez une chance de l’attraper. Le mode de contamination par l’air est donc très bien adapté aux mégalopoles actuelles disposant de haut niveau d’hygiène, mais surpeuplé.

Deuxièmement, les personnes contaminées par Ebola, la Grippe, la Rougeole, la Variole ou le MERS le savent très vite. D’une part, les symptômes sont parfaitement visibles et arrivent vites. Mais plus que les symptômes, l’état du patient se dégrade soudainement, l’obligeant à rester chez lui pour se reposer.

Encore une fois, le Covid innove ! Les premiers symptômes n’apparaissent qu’après 14 jours. Voire, ils n’apparaissent jamais dans les cas asymptomatiques. Pour les jeunes, les symptômes ne sont pas assez forts pour forcer le patient à rester chez lui. Et c’est là, la force du Covid, il n’oblige  pas le patient à rester chez lui, et lui laisse la liberté de sortir…. Pour davantage contaminer.

Le Covid a donc plus que tout autre virus imposé un nouveau mode de transmission. En effet, il se propage davantage par l’indiscipline des gens que part l’air. Plus la notion d’altruisme et de collectivisme est forte dans un pays, plus son action sera faible, comme c’est le cas en Asie, en Allemagne ou en Suisse.

**Le règne du « J’ai le droit … »**

Le Covid s’appuie sur le règne du « J’ai le droit … ». Les démocraties libérales en appliquant le principe d’état providence ont réduit toute la société en deux niveaux : l’individu et l’état. Des groupes d’organisation intermédiaires comme la famille, la ville ou la région se sont retrouvés dépossédés de tout droit sur l’individu et l’individu n’a plus le moindre devoir envers elles. Seul l’état peut imposer des contraintes à l’individu et l’individu à des comptes à rendre uniquement à l’état.

Ce constat prédit par Tocqueville est parfaitement visible en France. Où la population n’est plus organisée en quoi que ce soit et se réfère à l’état pour la moindre décision. La conséquence d’une telle organisation est qu’un individu n’a pas d’ordre à recevoir d’un autre individu et n’a même pas à se préoccuper des autres individus. 

L’état providence a enfermé les individus dans des bulles d’égoïsme où le citoyen ne se préoccupe que de lui, et comment retirer le maximum de sa relation avec l’état, sans se préoccuper du reste de la société.

Déjà moche, ce culte de l’égoïsme a été davantage stimulé par les réseaux sociaux à la course au narcissisme le plus ostentatoire. Cette mentalité est présente par cette phrase « J’ai le droit de faire […], c’est aux autres / à l’état de […] ».

En voici quelques exemples :
- J’ai le droit de rouler où je veux avec ma trottinette/vélo, c’est aux autres de faire attention.
- J’ai le droit de vouloir tout instantanément, c’est aux autres de pédaler sur leur vélo pour m’apporter mon dîner, mon colis.
- J’ai le droit de récupérer le maximum d’aide, c’est à l’état de trouver l’argent et aux autres de cotiser.

Tout récemment avec la loi PMA pour les couples homosexuels:
- J’ai le droit d’avoir un enfant, c’est à l’état de l’égaliser et aux autres de me donner un enfant.

Et dans notre cas:
- J’ai le droit de sortir, partir en vacances où je veux, c’est à l’état de me soigner si je tombe malade et aux autres de rester chez eux.



Le Covid est le fruit de l’évolution, son mode de transmission le rend parfaitement adapté aux sociétés actuelles, surpeuplées et individualistes. Il est davantage une malade s’attaquant à l’altruisme qu’aux poumons, il se transmet davantage par le « J’ai le droit » que par l’air.

Les personnes qui ont tous applaudi les soignants durant des mois, peuvent revenir de vacances, on aura besoin d’eux pour applaudir durant la deuxième vague.
