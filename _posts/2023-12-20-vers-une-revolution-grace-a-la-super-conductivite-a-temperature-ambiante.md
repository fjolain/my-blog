---
title: Vers une révolution grâce à la Super-conductivité à température ambiante
subtitle: Transport, Energie, Informatique, Plasma, Medecine, tous les verroux vont sauter. Comment l'Innovation Émerge sans Etats. (3/3)
date: 2023-11-26
layout: post
background: /img/2023/12/conductor.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---
<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-wGlv" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=y0RjpS6NgGZ7&v=3&playerId=ausha-wGlv"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Pour clore ce dossier sur l'innovation, nous allons nous tourner vers l'avenir. Dans les précédents articles, nous avons vu comment la découverte en apparence minime du forage de pétrole ou du transistor ont profondément changé notre monde.

Quelle serait la prochaine découverte d'envergure comme le pétrole ou la semi-conductivité du silicium ?

Il se pourrait bien que ce soit la super-conductivité à température ambiante.

<h2>Une conduction électrique parfaite</h2>

La conductivité d'un matériau a déjà été mentionnée dans la partie II. Il s'agit de la capacité d'un matériau à résister au courant. Les matériaux conducteurs comme l'or ou le cuivre laissent facilement passent le courant. Alors que les matériaux isolants comme le caoutchouc bloquent le courant.

Toute l'électronique tient sur les matériaux semi-conducteurs, comme le silicium. Leur résistant est pilotable en jouant sur leur propriété.

Aujourd'hui, nous allons voir, les super-conducteurs, des matériaux sans la moindre résistance au courant. Ces matériaux existent déjà comme le [niobium-titane](https://fr.wikipedia.org/wiki/Niobium-titane), mais cette propriété intervient uniquement à la température de -263°C (pour le niobium-titane).

Quelle implication dans nos vies permettra un super-conducteur à température ambiante.

<h2>Courant sans limites</h2>
Premièrement un matériau sans perte de courant permet un phénomène spectaculaire appelé la lévitation magnétique. Un train est en construction au Japon ([SCMaglev](https://en.wikipedia.org/wiki/SCMaglev)) pour remplacer les trains magnétique par bobine, mais encore une fois, il faut refroidir le matériau. Un super-conducteur à température ambiante permettra de généraliser ces nouveaux trains.

Un autre phénomène spectaculaire de la superconduction est l'apparition d'effets quantiques à notre échelle telle la jonction Josephson, alors que les effets quantiques sont plutôt réservés à l'échelle de l'atome. La généralisation des matériaux super-conducteurs généralisera aussi les ordinateurs quantiques.


Même de bons conducteurs comme l'or ou le cuivre ont des pertes,  ces pertes engendrent de la chaleur par [effet Joule](https://fr.wikipedia.org/wiki/Effet_Joule). Dès lors que le courant devient trop fort, le métal fond. Ce phénomène est utilisé dans l'industrie pour usiner ([EDM](https://www.youtube.com/watch?v=feGrx29XR4Q) ou [forge par induction](https://www.youtube.com/shorts/o1Zu47b19is)). Cependant, il s'agit bien souvent d'une contrainte qui impose une limite de courant capable de transiter dans un câble.

Avec un câble super-conducteur, l’électricité de tout Paris pourrait passer dans un seul câble électrique !

Le courant peut circuler indéfiniment dans une boucle superconductrice, on peut donc s'en servir pour stocker de l'électricité ([SMES](https://en.wikipedia.org/wiki/Superconducting_magnetic_energy_storage)).  Mais encore une fois, ce réservoir à électron nécessite des températures extrêmes ce qui le rend trop couteux pour une utilisation grand public.

<h2>Forts champs magnétiques</h2>
En déverrouillant la limite du courant dans un câble, on déverrouille également les champs magnétiques à haute énergie.

Ces champs vont permettre d'accroître la précision des scanners médicaux [IRM](https://fr.wikipedia.org/wiki/Imagerie_par_r%C3%A9sonance_magn%C3%A9tique), leurs résolutions sont corrélées à la puissance du champ magnétique produit.

L'armée pourrait s'intéresser au [canon de gauss](https://fr.wikipedia.org/wiki/Canon_magn%C3%A9tique) dans lequel un projectile est catapulté par un champ magnétique.

<h2>Le monde du plasma</h2>
Enfin, le champ magnétique permet la manipulation du plasma. Le [plasma](https://fr.wikipedia.org/wiki/%C3%89tat_plasma) est le quatrième état de la matière, il se produit à haute température quand les électrons se détachent des atomes et circulent librement dans le gaz. Le gaz devient sensible au champs magnétique.

Un champ magnétique permet de manipuler un plasma aussi bien en le cloisonnant dans un volume donné, en le mettant en mouvement ou en faisant varier sa température.

Les superconducteurs à température ambiante vont donc faire avancer tous les domaines autour du plasma comme le laser, la fusion nucléaire, les canons à plasma ou la recherche fondamentale.

<h2>Conclusion</h2>
On voit maintenant que le super-conducteur à température ambiante est un verrou technologique. Il permet de faire circuler des courants sans perte et donc sans limite dans un matériau. Cela ouvre les portes des champs magnétiques haute énergie et avec eux le monde des plasmas.

À chacun de ses étages se trouvent des progrès stratégiques pour l'humanité.



