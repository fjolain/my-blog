---
title: La corruption devient le fléau de l'Amérique.
subtitle: Le déchaînement de la SEC envers l'industrie crypto montre un pays plus proche de l'URSS que du libre-échange.
date: 2023-07-05
layout: post
background: /img/2023/07/western.png
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-o7an" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=yOQnru3pZ4ZA&v=3&playerId=ausha-o7an"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

En seulement un an, les États-Unis sont passés de terre promise de l'industrie crypto à terre d'exil. Un mystérieux fléau frappe les entreprises crypto, au point que des géants patriotiques comme [Coinbase se disent prêt à partir du pays](https://www.coindesk.com/business/2023/04/18/coinbase-could-move-away-from-us-if-no-regulatory-clarity-ceo-brian-armstrong/).

Mais que s'est-il passé ?

Dans le monde des plateformes d'échanges crypto, trois entreprises nous rejouent le Bon, la Brute et le Truand.

Le Truand est FTX, la défunte plateforme a coulé en volant au passage l'argent de ses clients.

La Brute est Binance qui a préféré foncer pour prendre des parts de marché sans attendre et encore moins réclamé une régulation claire.

Et enfin le Bon est Coinbase. Dès le départ l'entreprise a cherché la légalité, quitte [à réclamer mainte fois plus de précision](https://journalducoin.com/exchanges/sec-7-jours-donner-reponse-coinbase-reglementation-crypto/) de la SEC et sans jamais avoir eu de réponse de l'administration.

Aujourd'hui [la SEC a cloué aux piloris Binance, mais aussi Coinbase](https://www.theguardian.com/technology/2023/jun/06/sec-crypto-crackdown-us-regulator-sues-binance-and-coinbase). Les plateformes se retrouvent accusées de ne pas avoir les licences financières adéquates pour proposer certaines cryptomonnaies que la SEC juge, depuis peu, comme des *securities* soient des produits financiers.

Comme *actifs* financiers, nous avons les matières premières (*commodities*) qui existent en dehors de tout contrats et de toutes entreprises.

Et de l'autre les *produits* financiers (*securities*) qui sont émis par des entreprises a travers des contrats, il peuvent avoir des dividentes.

Or cette épineuse question de trier les actifs financiers est faite par une loi de [1946](https://en.wikipedia.org/wiki/SEC_v._W._J._Howey_Co.) qui ne permet pas de trier les nouvelles cryptomonnaies.

Bitcoin est vu comme une *commodities* y compris par la SEC. La cryptomonnaie UNI de l’entreprise Uniswap est une *securities*, mais où se situe ETH de la fondation Ethereum ?

Tout ce flou est pointé depuis des années par Coinbase qui demande une loi plus claire de la part de la SEC. L'administration ne lui a jamais répondu et ne compte pas y répondre de si tôt, mais accuse Coinbase de ne pas avoir suivi la réclamation...

Ce cirque bureaucratique est déjà exaspérant, mais il faut aussi parler de l'absent FTX.

Le Truand FTX a lui profiter du laxisme de la SEC.

Alors qu'une simple audition de ses comptes ou de ses associés aurait évité la perte de milliards de dollars aux particuliers.
L'administration s'est montrée absente devant le Traund.

Après sa chute, on découvre que le Truand a arrosé [73 millions de dollars](https://www.bloomberg.com/news/articles/2022-12-12/ftx-bankruptcy-puts-73-million-of-political-donations-at-risk) dans l'administration. [Même des hauts cadres de la SEC et la CFTC ont trempé avec FTX](https://www.latimes.com/politics/story/2022-12-26/sam-bankman-fried-cftc-sec-revolving-door).

On comprend mieux le laxisme envers son généreux bienfaiteur. Cette impunité court encore. Le CEO de FTX est encore [libre chez lui](https://www.20minutes.fr/monde/4016050-20221222-faillite-ftx-sam-bankman-fried-remis-liberte-contre-caution-250-millions-dollars), [son onéreuse défense est faite avec l'argent de ses victimes](https://www.01net.com/actualites/crypto-sait-desormais-fondateur-ftx-paie-avocats.html). Les médias, si prompts à tirer au boulet rouge sur la crypto, ont su atténuer les agissements du bienfaiteur (Faillite de FTX : « Les altruistes efficaces se sentent aujourd’hui trahis par l’un des leurs »
 [LeMonde](https://www.lemonde.fr/economie/article/2022/11/21/faillite-de-ftx-les-altruistes-efficaces-se-sentent-aujourd-hui-trahis-par-l-un-des-leurs_6150880_3234.html)).

Quand on compare le traitement entre FTX et Coinbase, on comprend que les États-Unis sont devenus un pays corrompu. La prospérité de votre entreprise ne dépend pas de sa bonne gestion et de son respect des lois, mais plutôt de l'argent injecté en politique.

Et attention au courroux du régulateur contre ceux qui ne payent pas leurs tributs au chef.

Que vous respectez la réglementation comme Coinbase, que vous jouez avec comme Binance ou que vous la dépassez comme FTX, à la fin, seul le pot-de-vin vous sauvera des poursuites de l’administration.

La terre sainte libérale est frappée de la corruption bureaucratique.


