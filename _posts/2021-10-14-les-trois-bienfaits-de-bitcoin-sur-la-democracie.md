---
title: Les 3 bienfaits de Bitcoin sur la Démocratie
subtitle: Pour la première fois de l'histoire, la gestion monétaire appartient au peuple
date: 2021-10-14
layout: post
background: /img/2021/10/foule.jpeg
published: www.contrepoints.org/2021/10/08/407962-facebook-un-bug-revelateur-des-fragilites-dinternet
categories: [article, podcast]
tags: [technologie, bitcoin, société]
---

<iframe frameborder="0" loading="lazy" id="ausha-9HqC" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=brDkxfqrPElJ&v=3&playerId=ausha-9HqC"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Tiens, tiens, tiens, l'inflation revient... D'après les médias, il s'agirait d'un phénomène naturel, indépendant de la politique et de leur gestion monétaire. On constate une hausse du prix de l'énergie, comme on constate la pluie, sans pouvoir expliquer la cause. De mon côté, j'attends toujours que les mots "Biden" et "BCE" soient prononcés au JT comme cause de cette inflation.

En même temps, il est préférable de faire croire au peuple que l'inflation est un phénomène indépendant des volontés des hommes. Car pour le français moyen, c'est le cas. L'euro comme les autres monnaies sont profondément non démocratiques. Aucun citoyen dans le monde n'a voté sur une inflation cible, un taux directeur ou la mise en place de taux de change flottant.

La gestion monétaire est anachronisme. Aujourd'hui, nous avons le choix pour tout : le type de logement, de vêtement, de voiture, de politique, d'art ou de travail. Tout, sauf la monnaie, qui n'a pas bougé depuis l'Antiquité. Un gouvernement décide seul des règles monétaires et les impose par la force à son peuple.

Heureusement, ce grave manquement démocratique est la raison d'être du Bitcoin. Le Bitcoin est une monnaie prenant la forme d'un code open source. Ce qui implique trois conséquences majeures pour notre démocratie.

<h2>Une transparence totale</h2>

La gestion monétaire est écrite dans le code. Or le code étant consultable par tous, une cryptomonnaie a forcément une gestion monétaire prédictive, vérifiable et transparente.

On est loin du monde opaque des banques centrales où les règles sont floues, cachées sous des termes ambigus tels le [*Quantitative easing*](https://fr.wikipedia.org/wiki/Banque_centrale_europ%C3%A9enne#Gestion_de_crises) (qui désigne le fait d'arroser massivement l'économie avec de l'argent imaginaire), ou encore le [*Securities Market Program*](https://fr.wikipedia.org/wiki/Banque_centrale_europ%C3%A9enne#Gestion_de_crises) (qui désigne le fait de sauver les banques avec l'argent du contribuable).

Avec les cryptomonnaies, les règles monétaires deviennent des algorithmes parfaitement explicites faisant source d'unique vérité consultable par tous.

<h2>Un vote obligatoire pour chaque changement</h2>

Si la monnaie devient un logiciel, alors un changement de règles monétaires devient une mise à jour du logiciel.

Dans les cryptomonnaies, les développeurs font office de législateurs. Ils conçoivent les nouvelles règles algorithmiques. Cependant, ils ne peuvent pas imposer leurs changements, puisque ce sont aux acteurs du réseau de déployer la nouvelle version.

Ainsi les acteurs du réseau, qui valident les transactions (les mineurs pour Bitcoin), votent pour ou contre les nouvelles règles en déployant ou non la mise à jour.

Dans une blockchain, la version officielle est tout simplement la version la plus utilisée. Si la nouvelle version dépasse l'ancienne en termes d'usage, elle est alors comme ratifiée.

Les cryptomonnaies ne sont pas une démocratie parfaite puisque ceux qui votent sont les valideurs de bloc et non les utilisateurs comme vous et moi. Cela ressemble plus au Sénat romain qu'à l'Agora athénienne.

Néanmoins, ce système reste plus démocratique que les monnaies étatiques. Aussi bien en [2017](http://bitconseil.fr/bitcoin-les-mineurs-precipitent-le-bip-91/) sur Bitcoin ou [actuellement](https://www.numerama.com/tech/713345-lethereum-passe-a-la-proof-of-stake-tout-comprendre-a-cette-revolution-dans-les-cryptomonnaies.html) sur Ethereum, nous assistons à des débats enflammés autour de déploiement de nouvelles versions. Des votes et débats autour d'une gestion monétaire, c'est une première dans l'histoire !

<h2>La diversité monétaire</h2>

Un code open source peut être copié, modifié et redistribué par n'importe qui à l'infini. Ainsi le code Bitcoin donna naissance à plein de monnaies différentes. On peut noter [Ethereum](https://fr.wikipedia.org/wiki/Ethereum) qui améliora la polyvalence en apportant les Smart Contracts. Ou encore [Monero](https://fr.wikipedia.org/wiki/Monero) qui améliora l'anonymat avec les  signatures en  cercle.

Il existe aussi des monnaies contestataires. Lors d'une mise à jour de la monnaie, si après le vote, les mineurs ayant la version minoritaire ne veulent toujours pas rejoindre la majorité, alors un *fork* peut apparaître, la version minoritaire devient une nouvelle monnaie. [Bitcoin CASH](https://en.wikipedia.org/wiki/Bitcoin_Cash) vient d'une contestation de Bitcoin en 2017. Pareil pour [Ethereum Classic](https://ethereum.org/en/history/#dao-fork) qui apparut en 2016 suite à une nouvelle version d'Ethereum.

En fait, avec les cryptomonnaies n'importe quel citoyen peut créer sa monnaie en se basant sur une monnaie existante. Évidemment, la valeur de la nouvelle monnaie dépendra du prix voulu par le marché.

<h2>Conclusion</h2>

Une gestion monétaire transparente, explicite et prédictive. Des décisions monétaires nécessitant la majorité des acteurs. La possibilité de proposer sa monnaie si l'on souhaite innover ou si l’on est en désaccord avec la gestion monétaire actuelle. Les cryptomonnaies apportent un vent de démocratie et de liberté sur le pré carré étatique de la monnaie. Ce n'est pas étonnant que les gouvernements diffament voire interdisent Bitcoin.
