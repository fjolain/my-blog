---
title: L'intérêt de la voiture électrique sans écologie ni pessimisme
subtitle: Derrière une vision pessimisme ou écolo punitive, la voiture électrique a des atouts techniques et industriels.
date: 2022-10-03
layout: post
background: /img/2022/10/tesla.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
---

<iframe frameborder="0" loading="lazy" id="ausha-LcTB" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=o9DEgfeNZdNm&v=3&playerId=ausha-LcTB"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

<h2>L'intérêt de la voiture électrique</h2>
<h3>Moins de dépendance sur les carburants</h3>

La première critique sur la voiture électrique vient de la dépendance des batteries avec la Chine et les terres rares. Nous verrons plus loin que l'utilisation des terres rares se réduit grâce aux industriels. Quant à la Chine, nous sommes déjà dépendants d'elle pour une grande partie de nos produits, notamment l'électronique.

Mais cette critique semble comme oublier la situation actuelle. Nous sommes dépendants au jour le jour d'un carburant que nous ne produisons pas. Si une pénurie a lieu sur l'essence, la France s'arrête de tourner en seulement quelques jours (cf les grêves). Avec les batteries, si plus une seule batterie arrive en France, cela impacte la production, mais tout le parc continu de rouler. Les inconvénients ne seront qu'au bout de plusieurs mois voire années.

De plus, la France ne produira jamais de pétrole et une voiture à essence ne tournera jamais à autre chose qu'au pétrole. Nous n'avons aucune chance d'être un jour indépendant avec une voiture à essence.

Alors que rien n'interdit à la France de produire les batteries, voire des batteries sans terre rare. Nous avons plus de chance d'être indépendants aussi bien à la conception qu'à la consommation avec une voiture électrique.

<h3>Flexibilité de la consommation du parc</h3>

Un moteur à explosion est une cathédrale technologique qui ne tolère aucune flexibilité. Le simple fait de changer de combustible comme du diesel par du sans plomb, le rend inutilisable.

On ne parle pas de changer le pétrole par du gaz ou du charbon, juste de passer d'un dérivé du pétrole à l'autre. Le fioul, le diesel, l'essence ou le kérosène ne sont pas interchangeables dans les moteurs. En termes d'innovation, on se retrouve coincé avec un parc impossible à améliorer sans tout changer.

Avec un parc de voiture électrique, le parc ne consomme que de l'électricité. Or l’électricité peut venir de plusieurs sources d'énergies comme le solaire, le nucléaire ou le gaz.

Le parc automobile peut être rechargé avec des centrales au charbon et passer au nucléaire sans changer une seule voiture. Imaginez par exemple un parc tournant avec des centrales à gaz, on peut augmenter l'efficacité du parc en passant des centrales classique (30% d'efficacité) aux  centrales à cycle combiné ([+60% d'efficacité](https://fr.wikipedia.org/wiki/Cycle_combin%C3%A9)) sans changer une seule voiture.

En plus de gagner en indépendance sur le consommable de la voiture électrique. Nous pouvons répartir ces consommables entre de nombreuses sources d'énergie pour éviter toute pénurie et sécuriser le parc très facilement.

Le casse-tête du tout pétrole et de sa soumission est réduit avec la voiture électrique. Néanmoins, il reste le problème de l'autonomie et de la disponibilité des bornes de recharge.


<h2>La réponse du marché</h2>
Les problématiques des bornes et des batteries sont des problématiques minimes qui montrent surtout le pessimisme ambiant.

<h3>Les batteries</h3>
Sur l'ensemble de la voiture électrique, son seul frein technologique est l'autonomie. Blocage léger puisque les batteries actuelles permettent déjà [400km d'autonomie](https://fr.wikipedia.org/wiki/Renault_Zoe). Il faut donc augmenter les capacités d'un facteur 3.

Tout cela est minime. Le siècle dernier, nous avons dompté l'énergie nucléaire, envoyé des hommes sur la lune, créé des collisionneurs de particules, maillé le monde entier à internet. Mais concevoir une batterie de voiture 3 fois plus performante serait impossible ?

Pendant que la société reste morose, des industriels et scientifiques avancent.

Tesla vient de sortir sa [batterie sans cobalt](https://insideevs.fr/news/429207/batterie-sans-cobalt-tesla/). GMG commence la fabrication de [batterie au graphène](https://www.automobile-propre.com/gmg-annonce-des-batteries-graphene-aluminium-ion-revolutionnaires/). Toyota sort sa première [voiture Mirai à l'hydrogène](https://www.automobile-propre.com/voitures/toyota-mirai).

<h3>Les bornes</h3>

Encore une fois, la France ne produit pas une goutte de pétrole, mais nous avons des stations essence tous les 10km. Pourtant alors qu'on produit notre électricité et que nous avons déjà un réseau électrique, il serait impossible d'installer des bornes de recharge ?

Heureusement, le pessimisme ne contamine pas tout le monde. Tesla continue de mailler le territoire de ses bornes. La Suisse a déjà  [maillé le sien](https://www.tcs.ch/fr/tests-conseils/conseils/mobilite-electrique/bornes-recharges-publiques.php).

Il est affolant de voir le pessimisme actuel. Les voitures électriques paraissent un mur technologique, alors que ce n'est qu'une marche. Marche minime que le marché a pris soin de faciliter avec une gamme de produite complète.

<h3>Une offre disponible pour chacun</h3>
Il a déjà les voitures toute essence qui ont réduit leur consommation. Mercedes a justement voulu [ajouter depuis 2008 un moteur électrique](https://www.lefigaro.fr/automobile/2008/09/19/03001-20080919ARTFIG00469-mercedes-electrise-ses-voitures-a-essence-.php) pour aider à gagner en efficacité sur le moteur à essence .

Toyota propose depuis [20ans des hybrides](https://fr.wikipedia.org/wiki/Toyota#V%C3%A9hicules_hybrides) pour altérer entre une conduite à essence ou électrique, le tout sans se soucier du rechargement électrique.

Puis est venu, les hybrides rechargeables comme les [Ioniq de Hyundai](https://fr.wikipedia.org/wiki/Hyundai_Ioniq). On se retrouve avec deux voitures en une seule capable de fonctionner et de se recharger aussi bien à l'essence qu'à électrique.

On peut rappeler le produit [BMW i3](https://fr.wikipedia.org/wiki/BMW_i3). Il s'agissait d'une voiture électrique avec un petit moteur à essence de secours pour générer de l'électricité en cas de manque de borne..

Et enfin les voitures uniquement électriques comme celles de Tesla ou de Renault.

Les entreprises sont activement entrain de lever les blocages technologiques. Et en attendant, le marché propose une vaste gamme entre 100% essence et 100% électrique.

<h2>Conclusion</h2>
La voiture électrique est une nette amélioration aux voitures à essence. Elle est un vecteur d'émancipation de l'Europe à la soumission du pétrole. Le marché apporte déjà des réponses aux problèmes des batteries et des bornes. Il propose aussi une vaste gamme pour une transition en douceur.

Sauf que ni l'État ni les écolos ne font dans la douceur. Ils imposent leur calendrier avec [la fin des voitures à essence en 2035](https://www.france24.com/fr/europe/20220629-plan-climat-de-l-ue-les-27-approuvent-la-fin-des-moteurs-thermiques-en-2035), alors que ni les consommateurs ni les industriels ne sont encore prêts.

Armés de leur marteau idéologique, ils ne voient dans la voiture qu'un clou à enfoncer, l’égérie des libertés individuelles "égoistes" et polluantes à abattre. Dommage, l'État stratège pourrait voir une vision industrielle capable de redynamiser la France.





