---
title: Le danger des boites noire
subtitle: L'opacité dans nos démocratie
date: 2016-08-12T13:55:04+01:00
layout: post
categories: [article]
tags: [vie privée]
---
Alors avant de commencer une petite définition de la boite noire. Quand je parle de boîte noire, je ne parle pas des boites d&rsquo;enregistrement des avions. On va parler de la boite noire en informatique. Il s&rsquo;agit d&rsquo;un ordinateur, au sens large, dans lequel tourne un programme, que l&rsquo;on ne connaît pas. Et c&rsquo;est de là que vient le problème : Que fait ce code ? L&rsquo;actualité en est chargée : Scandale Volkswagen, loi sur le renseignement, collecte de données de la NSA, trading haute vitesse, &#8230;

<!--more-->

## Mais que fait la Police !

L’événement qui a déclenché cet article est le scandale Volkswagen. Il illustre parfaitement le problème des boites noires. Nous avons un ordinateur au centre de la voiture, toutes les données vont dans cet ordinateur, toutes les commandes en sortent, et personne ne sait comment l&rsquo;ordinateur régule tout ça. Autrement dit, il faut faire pleinement confiance aux développeurs sur les lignes de codes. Alors quand il est question d&rsquo;économiser pas mal d&rsquo;argent en contournant les lois, bizarrement les lignes de code malsaines s&rsquo;écrivent toutes seules. On se retrouve donc actuellement avec des voitures entièrement informatisées conçues pour entuber les règlements.

Et la tendance n&rsquo;est pas prête de s’arrêter à l&rsquo;heure des voitures connectées voire intelligentes, leurs ordinateurs posent des gros problèmes de sécurité. Comme c&rsquo;est le cas de la nouvelle Jeep entièrement connectée, et entièrement piratable à distance. Les pirates ont pu par exemple : contrôler les moteurs, débrancher les freins, ouvrir les portes.

Les assureurs aussi se cassent la tête sur les voitures intelligentes. En cas d&rsquo;accident qui est responsable ? Le conducteur, ou le développeur ? Comme personne n&rsquo;a accès aux lignes de code, personne ne sait comment va réagir la voiture dans différentes situations. Il faut une nouvelle fois prier pour que le développeur soit dans un bon jour lorsqu&rsquo;il code le programme.

La finance est une belle preuve de boite noire depuis que les robots spéculent en bourse. Chaque banque possède son « bébé » fait maison, son programme de décision boursière. Différents types de news (bourse, tweets, articles, …) rentrent dans la machine. L&rsquo;ordinateur en ressort des ordres de décisions envoyés sur les bourses. Personne ne régule ces programmes, donc de temps en temps, ils dérapent : En 2010 le Dow Jones (bourse américaines) a chuté de 9 % en 15min. En 2013 c&rsquo;est 136 milliards de dollars qui partent en fumé à cause de ces programmes qui se sont emballés suite à un faux tweet.

Tous ces exemples mettent le doigt sur quelque chose. Les entreprises sont souvent auditées sur leurs financements, leurs usines, le respect des droits de l&rsquo;homme dans leurs locaux, mais aucun contrôle, même interne, sur leur code. La célèbre phrase de l&rsquo;informaticien Larry Lessig : « code is law » montre que le code dicte les règles. Je suis sûr qu&rsquo;il y a des contrôles fréquents chez Volkswagen pour vérifier que les comptables ne trichent pas sur la trésorerie. Leur scandale montre, qu&rsquo;il faut désormais contrôler également les développeurs.

## Ah pardon ! La boite appartient à la Police

La boite noire de référence est notre smartphone! Les compagnies ont réussi l&rsquo;exploit de nous munir toute la journée d&rsquo;une boite noire dans notre poche. Votre utilisation, votre localisation, vos différentes applications et comptes sont autant de données qui sont triées par votre boite noire. Restent-elles sur le téléphone ? Sont-elles utilisées à des fins commerciales ? Impossible de savoir.

Il a fallut les révélations d&rsquo;Edwar Snowden pour savoir ce que ces boites contenaient, et de se rendre compte qu&rsquo;elles nous espionnaient à notre insu.

Le terme boite noire a été utilisé récemment par les médias pour parler de la loi du renseignement votée après les attentats de Janvier. Je ne rentrerai pas dans les questions démocratiques de cette loi. Je citerai juste Benjamin Franklin _: Un peuple prêt à sacrifier un peu de liberté pour un peu de sécurité ne mérite ni l&rsquo;une ni l&rsquo;autre, et finit par perdre les deux._ Et je rappellerai la construction de la ligne Maginot par nos politiciens pour stopper l&rsquo;ennemi.

L&rsquo;algorithme contenu dans cette boîte sera en mesure de détecter les extrémistes, juste en regardant les données qui transitent sur le réseau. En obligeant les fournisseurs d&rsquo;accès à internet à placer ces boites dans leur data-center, c&rsquo;est toute la France qui est mise sur écoute ! L’œil de Moscou n&rsquo;aurait pas fait mieux. Une fois de plus le code est tenu secret. Vous pouvez vous retrouver dans la catégorie « potentiel djihadiste » sans aucune raison.

Cette boite noire existe depuis le 11 septembre aux États-Unis. J&rsquo;espère que nos politiciens ont un peu changé le code. Car celle américaine n&rsquo;a visiblement pas repéré les deux terroristes qui pourtant étudiaient dans la prestigieuse MIT et qui ont fait sauter une bombe durant le marathon de Boston en avril 2013. Par contre elle a envoyé le GIGN chez un couple, car l&rsquo;un avait acheté un sac à dos et l&rsquo;autre une cocotte minute. Il y a encore quelques bugs.

La plupart des boite noires revendiquent leur opacité au nom du droit d&rsquo;auteur. Avec comme argument: si je la montre on va me la copier ! La vérité serait plus : si tu la montres on va te l&rsquo;interdire. La seul solution face à cela reste l&rsquo;open source. Un code open source est aujourd&rsquo;hui le seul moyen de préserver notre liberté.