---
title: De la Pénurie de Baleine à l'Essor de l'Automobile (1/3)
subtitle: Comment l'Innovation Émerge sans Etats.
date: 2023-09-14
layout: post
background: /img/2023/09/whale.png
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-eSMR" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=blnMNuzeQ1Qq&v=3&playerId=ausha-eSMR"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

L'innovation ne peut guère être planifiée, encore moins par un État. Par exemple, la raison pour laquelle nous roulons en voitures à essence provient d'une pénurie de baleine.

<h2>Pétrole, un éternel remède</h2>

En soi l'humanité utilise le pétrole depuis l'Antiquité. Son mode d'extraction est particulièrement laborieux. Comme son nom grec l'indique, pétrole signifie "huile de roche". Certaines roches transpirent cette mystérieuse huile, ce qui donne des boues bitumineuses.

Le pétrole est donc un produit de luxe compliqué à trouver. En 1860 aux États unis, il est surtout à l'origine de nombreux remèdes vendus par des charlatans (promettant 95% d'efficacité ?).

Les humains préfèrent utiliser l'huile de baleine pour alimenter leurs lampes et notamment les phares.

Problème, la surpêche de baleine a raréfié le cétacé. La pénurie guette.

C'est alors qu'un certain [Edwin Drake](https://fr.wikipedia.org/wiki/Edwin_Drake) décida de forer le pétrole en Pennsylvanie. Il eut l'intuition que le pétrole se trouvait dessous prêt à jaillir. Le 27 août 1859, il fora son premier puits de pétrole. L'or noir jaillit de la terre. Il venait avec son seul puits de multiplier par deux la production mondiale de pétrole.

Cette ressource a permis à toute la région de s'enrichir sauf pour Drake. N'ayant mis ni son forage ni son derick sous brevet, la concurrence le mit sur la paille.

<h2>Un moteur pour cette énergie bon marché</h2>

Autour du charbon, [Watt](https://fr.wikipedia.org/wiki/James_Watt) a conçu la première machine à vapeur. De tels moteurs ont permis la fabrication des locomotives et le début du chemin de fer.

Le charbon demande beaucoup de main-d'œuvre. Aussi bien pour l'extraire de la terre, le transporter, jusqu'aux cheminots qui devaient régulièrement alimenter la locomotive durant le trajet.

On s’est intéressé au gaz de ville. [Lenoir](https://fr.wikipedia.org/wiki/%C3%89tienne_Lenoir) a créé le premier moteur à explosion en 1860. Contrairement aux moteurs à vapeur, les explosions permettent une grande variabilité du régime moteur, on peut changer la puissance facilement.

Ensuite [Beau de Rochas](https://fr.wikipedia.org/wiki/Alphonse_Eug%C3%A8ne_Beau) a théorisé le moteur à 4 temps. Puis l'ingénieur allemand [Otto](https://fr.wikipedia.org/wiki/Nikolaus_Otto) réussit à perfectionner la machine et à l'industrialiser.

Le gaz reste dangereux. Son stockage et son transport peuvent provoquer des explosions.

Il faut attendre [Daimler](https://fr.wikipedia.org/wiki/Gottlieb_Daimler) pour concevoir un moteur 4 temps à explosion fonctionnant au pétrole. Cette énergie sort d'elle-même de terre comme le gaz, mais nécessite peu de mains-d'œuvre comme le charbon.

Daimler ne s'est pas trompé. Le fait de passer du gaz au pétrole permet de monter les moteurs sur des engins autonomes et mobiles. L'essor de la voiture pouvait commencer.



Il n'y a pas d'État ni de grand commissaire au plan pour concevoir la voiture. La montée du prix de l'huile de baleine a poussé la recherche d'un substituant. La baisse drastique du pétrole a poussé des ingénieurs à adapter leurs moteurs pour cette nouvelle énergie.

Les avantages des nouveaux moteurs essence à explosion ont permis de nouveaux usages comme la voiture.







