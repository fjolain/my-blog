---
title: Startups, innovations et mépris étatique, la crypto à la française
subtitle: Tour d'horizon de la crypto-française
date: 2022-01-04
layout: post
background: /img/2022/01/paris.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [bitcoin, économie]
---

<iframe frameborder="0" loading="lazy" id="ausha-r9m5" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=Bnm2DfDwplvd&v=3&playerId=ausha-r9m5"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Après avoir fait le tour d'horizon du cloud français, place à la cryptosphère française !

<h2>Les acteurs français</h2>

La France à très vite était un haut lieu de la crypto, aussi bien pour ses innovations, ses entreprises ou sa recherche.

On peut commencer par la licorne française de la crypto : [ledger](https://ledger.com). Créée en 2014, l'entreprise commercialise des portes-monnaies électroniques pour cryptomonnaie. Ses produits sont réputés pour leur sécurité et sont rapidement devenus leader du secteur.

Autre proposition de stockage de cryptomonnaie innovante, l'entreprise [Uniris](https://uniris.io/) a développé une technologie permettant de transformer votre empreinte digitale comme moyen identification décentralisée et porte-monnaie pour vos cryptomonnaies. [Sa technologie sera essayée lors des JO](https://uniris.io/fr/blog/paris-2024-jeux-olympiques/) de Paris afin de sécuriser l'accès aux espaces privés.

La France a aussi hébergé le premier lieu physique dédié au Bitcoin. [La Maison du Bitcoin](https://fr.wikipedia.org/wiki/Coinhouse) fut ouverte de 2014 à 2019 rue du Caire à Paris. N'importe qui pouvait y entrer librement pour se renseigner sur Bitcoin et en acheter. L'espace est maintenant en ligne avec la plateforme d'échange CoinHouse.

En termes de plateforme, on peut citer [Paymium](https://paymium.com). Elle fut créée en 2011, soit seulement deux ans après la première transaction bitcoin. Ce qui en fait une des plus anciennes encore en activité.

La France s'illustre aussi par ses recherches académiques. On peut citer le projet Blockchain Advanced Research & Technology ([BART](https://www.inria.fr/fr/site-web-du-collectif-bart)) qui fédére quatres centres de recherches : INRIA, l’IRT SystemX, Télécom ParisTech et Télécom SudParis, autour de la blockchain.

À mis chemin entre la recherche académique et l'industrie, nous retrouvons la blockchain [Tezos](https://tezos.com/), qui propose une technologie de blockchain reposant sur le Proof of Stack (PoS), ainsi que sur l'utilisation de la programmation formelle, domaine de pointe de la recherche française. Sa sûreté a notamment été sélectionnée par des banques comme [Société Générale](https://www.societegenerale.com/fr/actualites/communiques-de-presse/premier-produit-structure-sur-blockchain-publique) ou la [Arab Bank Switzerland](https://lentrepreneur.co/finance/crypto/arab-bank-a-choisi-tezos-pour-faciliter-le-service-de-garde-institutionnelle-06102021) pour lancer des projets pilotes dessus.

<h2>L'état français toujours fidèle a lui-même</h2>

Comme un air de déjà vu avec le cloud, l'état français méprise son écosystème.

D'abord  en dénigrant les cryptomonnaies telle [Christin LaGarde](https://www.bfmtv.com/economie/christine-lagarde-le-bitcoin-ce-n-est-pas-une-monnaie_AV-202102070217.html) en septembre 2021 qui déclare :

    Les cryptomonnaies ne sont pas des monnaies. Point final.

Alors même que le Bitcoin est utilisé comme monnaie au Salvador. Ou encore [Bruno LeMaire](https://twitter.com/brunolemaire/status/1336703944421232640?lang=fr) ministre de l'Économie qui ne manque jamais une occasion de relier Bitcoin au financement du terrorisme comme en décembre 2020 :

    Nous devons assécher au moindre euro tous les circuits de financement du terrorisme.

    Avec @SebLecornu et @olivierdussopt , nous avons présenté en Conseil des ministres ce matin une ordonnance permettant de renforcer la lutte contre l'anonymat des transactions en cryptoactifs.


Ces demandes ont bien été entendues et ont donné lieu aux magnifiques Presatataires Sur Actifs Numériques (PSAN). Depuis décembre 2020, n'importe quelle entreprise qui gère des actifs numériques doit, avant de faire le premier euro de chiffre d'affaires, s'armer d'un département juridique complet pour devenir [PSAN](https://www.amf-france.org/fr/espace-professionnels/fintech/mes-relations-avec-lamf/obtenir-un-enregistrement-un-agrement-psan).


On arrive parfois à des situations kafkaïennes comme avec les NFT. Le simple fait d'en créer et d'en vendre peut être vu comme "gestionnaire d'actif numérique" et donc vous oblige à être PSAN . De toute manière la France interdit la vente de bien immatériel, ce qui a le mérite de clore le débat.

Ainsi la maison d'enchère [Auguttes n'a pas vendu le NFT](https://www.youtube.com/watch?v=Acz2Mhmlo-o) du premier SMS de l'histoire. Non, elle a vendu un cadre avec le contenu du NFT dedans, le NFT était un cadeau attaché à la vente du cadre.

Les particuliers détenteur de cryptomonnaies ont aussi pour leur argent.

Le fisc français oblige les contribuables à déclarer toutes leurs cryptomonnaies... en oubliant un peu vite que le Bitcoin fut justement conçu pour éviter Big Brother.

Question fiscalité, le détenteur doit payer la flat tax à 30% sur les plus-values, y compris si l’on ne revend pas !

Ainsi si après avoir acheté un Bitcoin à 1 000€, il monte à 10 000€, vous décidez d'acheter une moto avec. Et bien il faudra remplir un CERFA au fisc pour [lui donnait les 3 000€ de la plus-value latente](https://www.village-justice.com/articles/plus-values-sur-cession-crypto-monnaies-modalites-imposition-declaratives,38912.html) présente dans l'achat de votre moto.

On peut corser l'exercice, en se demandant quelle est la fiscalité d'une revente d'un NFT, celle des cryptoactifs ou celle des oeuvres d'art ?

Cette réglementation floue et sévère où on peut vite tomber dans l'illégalité refroidit les banques françaises déjà peu attirées par l'innovation. La plateforme Paymium [a dû se résigner à prendre une banque allemande](https://bitcoin.fr/un-nouveau-partenaire-bancaire-pour-paymium-2/g) devant les déboires avec les banques françaises, pareil pour CoinHouse. Récemment c'est Bitcoin Avenue, qui malgré l'obtention du PSAN [s'est fait rejeter du jour au lendemain par sa banque](https://bitcoin.fr/le-cic-cloture-le-compte-bancaire-de-bitcoin-avenue-moins-de-dix-jours-apres-son-enregistrement-en-tant-que-psan/) (celle d'un monde qui bouge).

Encore une fois le particulier n'est pas épargné, puisque certaines banques rejettent tous virements vers les plateformes d'échanges y compris celles agrégées PSAN.

Comme avec le cloud, les restrictions ne freinent que nos jeunes pouces, les géants étrangers y trouvent un pré carré. Le géant asiatique [Binance a été reçu en personne](https://www.lesechos.fr/start-up/ecosysteme/binance-la-grogne-de-lecosysteme-crypto-francais-face-a-lattitude-du-gouvernement-1362889) par le secrétaire du numérique Cédric O à Bercy en novembre dernier. L'entreprise qui possède 70 % de part de marché veut faire de la France son siège régional, voire mondial.


Encore une fois la France est un vivier de talents, d'entrepreneurs et de chercheurs. Encore une fois, le gouvernement les soutient comme la corde soutient le pendu. Il crée une réglementation et une fiscalité étouffante, tout en aidant la concurrence étrangère à rentrer sur le marché domestique.
