---
title: Quel serait un pass sanitaire protégeant le citoyen ?
subtitle: Aucune donnée nominative. Impossible à invalider après coup. Est-ce possible ?
date: 2022-02-02
layout: post
background: /img/2022/02/qrcode.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [société]
---

<iframe frameborder="0" loading="lazy" id="ausha-gBkz" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=ykj5VtkwYLnD&v=3&playerId=ausha-gBkz"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Dans précédent article, je listais les 3 pièges pour nous citoyens dans le QRCode :
- Il permet de changer facilement les règles de validations, facilitant la tyrannie.
- Il laisse toutes nos données de santé et nom/prénom disponibles à n'importe qui. De plus tout ceci part dans une application étatique à chaque scan. De plus il serait très facile de la changer pour rapatrier les scans des Français vers un serveur central, et ainsi organiser une surveillance de masse.
- Le pass est conçu pour gérer plusieurs maladies à la fois.

La question qu'on peut se poser est : Quel serait un pass sans ces 3 dangers ? La réponse trouvée est étonnante, car en plus de devenir moins dangereux pour le citoyen, le pass deviendrait plus simple à vérifier pour le restaurateur et même plus simple à coder pour le développeur !

<h2>Eviter la tyrannie</h2>

Comme expliquer dans le précédent article, le pass contient uniquement le parcours sanitaire du citoyen. Le *valide/invalide* est donné par les règles présentes dans l'application étatique. On peut donc changer très facilement les règles de validation, sans avoir à changer de QRCode.

Pour pallier à cette conception favorisant la tyrannie et l'arbitraire, rien de plus simple ! Il suffit de remplacer tout le parcours sanitaire par une date d'expiration du pass.

On change une laborieuse structure de donnée par une seule valeur : la date d'expiration du pass.

Si le gouvernement décrète que 2 doses de vaccins vous donnent droit à 1 an de pass sanitaire. Alors on fixe la date d'expiration à dans un an. Le QRCode restera valide un an, quoique décide le gouvernement par la suite.

Le *valide/invalide* est maintenant présent dans le QRCode, une fois le pass donné au citoyen, l'état ne peut qu'honorer ses promesses, sans pouvoir changer la validité des pass après coup.

<h2>Eviter la surveillance</h2>

Nous  venons déjà de retirer les données de santé du pass. Il ne reste plus qu'à retirer le nom.

En fait pour valider le titulaire d'un pass, le nom et prénom sont des piètres indicateurs. Avec le nom vous avez juste une idée du sexe, de l'âge et des origines ethniques du titulaire.

Pour valider le nom, il faut se référer à la carte d'identité. D'abord valider le nom et prénom présent sur la carte avec celui du pass. Puis valider la photo de la carte d'identité avec le visage du porteur du pass.

Aussi, on peut  simplifier en mettant directement la photo du titulaire dans le pass à la place des noms/prénoms. Cela serait plus simplement pour la vérification du pass.

Malheureusement le contenu d'une photo ne peut pas tenir dans un QRCode. De plus, une photo précise du titulaire permettra à l'état de relier un scan à un citoyen en utilisant de la reconnaissance faciale.

Là encore, la solution revient à simplifier l'information du pass. On n'a pas besoin d'une photo précise pour vérifier l'identité, il suffit d'afficher un portrait-robot du titulaire.

Le pass contiendrait donc seulement des informations comme la couleur de peau, des yeux ou des cheveux. On peut aussi y mettre le type de visage, des cheveux, la présence de lunette ou le sexe et l'âge.

Il faut avoir suffisamment de données pour vérifier le titulaire du pass. Mais garder un certain flou pour que l'état ne puisse pas relier un portrait-robot à un citoyen précis.

Ainsi, le pass serait plus facile à vérifier, le portrait-robot s'afficherait directement sur l'application de vérification. Cependant aucune donnée présente dans le pass ne permet de le lier à un individu précis. Il n'a donc plus d'inquiétude sur une future surveillance de masse. Le contenu du scan n’est ni privé ni nominatif.

<h2>Eviter les extension</h2>
Dans le pass actuel, une variable existe pour différencier les parcours sanitaires pour chaque maladie. Dans notre nouveau pass, il suffit de ne pas la mettre.

<h2>Conclusion</h2>

En plus de protéger notre vie privée et d'obliger l'état à tenir ses promesses. Le nouveau pass est très simple.

À la place d'une complexe structure de données pour connaître le nombre de doses, de tests, d’anticorps, etc., il suffit d'une simple date d'expiration.

À la place d'un nom/prénom et d’une vérification par carte d’identité, il suffit d'une dizaine de caractéristiques physiques pour pouvoir dessiner un portrait-robot sur l'application de vérification.

Il est donc beaucoup plus simple de concevoir et de développer un pass protégeant les citoyens. Le pass actuel n'est donc pas un accident, ces pièges contre notre démocratie ne doivent rien au hasard.

Et bien sûr le meilleur pass, reste l'absence de pass.
