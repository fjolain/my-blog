---
title: Les guerres du chiffrement
subtitle: De simples formules mathématiques pourtant considérées comme des munitions il y a seulement 20 ans par la loi. Quelles guerres cachent le chiffrement ?
date: 2023-05-21
layout: post
background: /img/2023/05/soldats.png
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-Jkvi" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=B19mZFlZma0O&v=3&playerId=ausha-Jkvi"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Si l'espionnage permet la collecte de données. Le chiffrement est l'arme défensive pour éviter la collecte des données.

Domaine mathématique par excellence, son évolution est l'enjeux de guerres.
Allant jusqu'à être classé comme [munition interdite à l'exportation](https://fr.wikipedia.org/wiki/Crypto_Wars) par les Américains ou les Français durant la guerre froide.

L'informatique a décuplé ses possibilités en dehors des simples communications chiffrées.

Aujourd'hui, le chiffrement est un enjeu majeur qui enrôle les états, les armées, les scientifiques et les citoyens.

<h2>L'ère pré-informatique</h2>
<h3>César : Permutation fixe</h3>
Avant l'accès à l'informatique, le chiffrement consistait en des protocoles pour mélanger les lettres d'un message.

Comme le [chiffrement César](https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage) où l’on décale les lettres dans l'alphabet. Si l’on décale les lettres de 5, le mot CESAR devient HJXFW. Un tel protocole est peu fiable, n'importe qui peut tester les 26 possibilités pour casser le code.

On peut aussi venir à bout de n'importe quelle permutation de lettres par une [analyse fréquentielle](https://fr.wikipedia.org/wiki/Analyse_fr%C3%A9quentielle). On note la répartition des lettres dans le texte chiffré. Il suffit de comparer avec la répartition des lettres dans la langue française pour retrouver la table de permutation.

<h3>Vigenère : Permutation périodique</h3>
Une méthode plus astucieuse est venue du mathématicien français [Vigenère](https://fr.wikipedia.org/wiki/Chiffre_de_Vigen%C3%A8re) en 1586. On choisit une clé comme ARTICLE. Le décalage dans l'alphabet va varier en fonction des lettres de la clé ici : 1(A), 18(R), 20(T), 9(I), 3(C), 12(L) et 5(E). Le mot VIGENERE devient XTKGYITP.

Même si le décalage n'est plus fixe, il reste périodique. Aussi ce chiffrement fut cassé par le major Prusse Kasiski en 1863.

<h3>Enigma : Permuation apériodique</h3>
Engin les Allemands mirent au point le chiffrement par permutation le plus élaboré avec la machine [Enigma](https://fr.wikipedia.org/wiki/Enigma_(machine)). L'utilisateur tape son texte comme sur une machine à écrire, à chaque touche une lumière s'allume sur un écran pour lui donner la lettre chiffrée associée.

Entre les touches et l'écran se cache un ingénieux circuit électrique qui passe dans des rouleaux. À chaque lettre, la position des rouleaux change, ainsi l'électricité prend un autre chemin à chaque lettre. La permutation des lettres dans un message n'est plus ni fixe ni périodique.

Enigma a apporté un avantage au nazi durant la Seconde Guerre mondiale et un casse-tête aux Anglais. Il a fallu le génie d'Alain Turing pour en venir à bout avec une machine révolutionnaire qui débutera l'ère de l'informatique.

<h2>Le chiffrement aujourd'hui</h2>
<h3>AES : Successeur d'Enigma</h3>
Par sa rapidité de calcul, l'informatique a rendu obsolètes tous les précédents codes comme Enigma. Par la même occasion, cela a permis de créer de nouveaux chiffrements plus sûrs.

Aujourd'hui le successeur d'Enigma s'appelle [AES](https://fr.wikipedia.org/wiki/Advanced_Encryption_Standard), il permet à l'aide d'une clé de chiffrer et déchiffrer n'importe quel fichier numérique. Mais l'informatique a ouvert d'autres branches du chiffrement.

<h3>Signature numérique</h3>
Le cryptage informatique a apporté une innovation peu connue, les signatures numériques [ECDSA](https://fr.wikipedia.org/wiki/Elliptic_curve_digital_signature_algorithm). Elles permettent de garantir que le contenu d'un auteur n'a subi aucune altération.

Elles servent aussi bien à authentifier un message, s'assurer de l'intégrité d'un logiciel ou s'authentifier sans mots de passe.

En plus d'apporter une sécurité bien supérieure aux signatures manuscrites (impossible à falsifier, impossible à altérer après signature). Les signatures numériques ont permis des cas d'usage unique.

Les signatures [Schnorr](https://fr.wikipedia.org/wiki/Protocole_d%27authentification_de_Schnorr) permettent la construction d'une seule signature venant de plusieurs signataires. [Les signatures en cercle](https://fr.wikipedia.org/wiki/Signature_de_cercle) permettent de signer anonymement au sein d'un group.

<h3>La révolution RSA</h3>
En 1977, le chiffrement [RSA](https://fr.wikipedia.org/wiki/Chiffrement_RSA) changea le monde. Que ce soit César, Vigenère, Enigma ou AES, on utilise la même clé pour chiffrer que pour déchiffrer. Pour débuter une communication sécurisée, il faut donc s'échanger cette clé en amont.

Avec le chiffrement RSA, on se retrouve avec une clé pour déchiffrer et une clé pour chiffrer. Ainsi, on peut partager publiquement la clé pour chiffrer, seuls nous restons en mesure de déchiffrer avec l'autre clé.

Ce chiffrement est derrière la sécurité d'internet avec le HTTPS. Lorsque vous vous connectez à contrepoints.org, le serveur commence par vous donner sa clé pour chiffrer, afin de sécuriser la connexion entre vous. Un tel fonctionnement est impossible avec AES.

Un autre cas d'usage sert à l'armée. Un tel chiffrement permet d'emporter la clé pour chiffrer au bout du monde sans risque. La clé pour déchiffrer reste au siège de l'état-major.

<h3>Comment interdire une formule mathématique ?</h3>
Devant cette prouesse, l'armée américaine a tenté de stopper le chiffrement RSA. Or interdire un chiffrement revient à interdit une formule mathématique. Pour tourner en dérision cette décision [des t-shirts](https://fr.wikipedia.org/wiki/Crypto_Wars#/media/Fichier:Munitions_T-shirt_(front).jpg) avec le protocole RSA dessus furent vendus, mais interdit à l'export, car considéré comme des  munitions...

L'interdiction du chiffrement a aussi frappé la France jusqu'en [1999](https://www.lemonde.fr/planete/article/2006/10/06/jacques-stern-briseur-de-codes_820777_3244.html). Encore aujourd'hui l'Union européenne souhaite [interdire le chiffrement](https://www.developpez.com/actu/341124/-Stop-au-plan-de-surveillance-de-masse-propose-par-la-Commission-de-l-UE-lance-un-editeur-de-VPN-car-l-UE-entend-scanner-toutes-les-correspondances-privees-pour-lutter-contre-la-pedopornographie/). Le FBI demande publiquement à Apple d'[affaiblir son chiffrement](https://en.wikipedia.org/wiki/FBI%E2%80%93Apple_encryption_dispute) sur les iPhone.


Devant l'impossibilité d'interdire le chiffrement, les gouvernements ont choisi d'intervenir en amont en vérolant les logiciels.

C'est ainsi que la NSA a [voulu véroler](https://fr.wikipedia.org/wiki/Bullrun) des logiciels comme TrueCrypt ou SSL. Ou encore l'entreprise suisse [Crypto AG](https://fr.wikipedia.org/wiki/Crypto_AG) s'est faite discrètement rachetée par la CIA afin de fournir des appareils vérolés aux diplomates et hommes politiques. Récemment, la gendarmerie française a piraté le logiciel [EncroChat](https://fr.wikipedia.org/wiki/EncroChat#Infiltration_par_la_Gendarmerie_fran%C3%A7aise).

<h2>Les futures guerres du chiffrement</h2>
<h3>Garder ses données chiffrées sur internet</h3>
La première guerre du chiffrement vise à garder ses données chiffrées y compris sur les services cloud en ligne. L'hébergeur cloud n'aura plus accès aux données en claires.

RSA permet de chiffrer la communication entre nous et le serveur, mais à la fin le serveur dispose des données en claires.

Ces véritables pots de miels sont aujourd'hui au coeur des piratages, de la revente de données et surtout de l'espionnage de masse à peu de frais par les états.

Pour contrer ses dérives, on assiste déjà au chiffrement de bout en bout. Lorsque les données ne sont que stockées en ligne, on peut les chiffrer avant de les envoyer.

L'entreprise [Signal](https://fr.wikipedia.org/wiki/Signal_(application)) propose une messagerie basée sur le chiffrement de bout en bout. [Proton](https://fr.wikipedia.org/wiki/Proton_Mail) propose une boîte mail, un drive et un calendrier avec ce même principe. Dans les deux cas, les entreprises n'ont aucun accès aux données sur le serveur.

Mais que faire si l'entreprise doit vérifier une donnée (comme l'identité) ou manipuler la donnée (comme une requête Google) ?

En ce moment les labos de recherche planchent sur des [chiffrements homomorphes](https://fr.wikipedia.org/wiki/Chiffrement_homomorphe) ou des [preuves zero connaissance](https://fr.wikipedia.org/wiki/Preuve_%C3%A0_divulgation_nulle_de_connaissance) (zero knowledge proof). Ces deux technologies permettent au serveur de vérifier ou manipuler une donnée alors qu’elle reste chiffrée.

Ainsi dans le futur, un service cloud pourra vérifier votre âge sans avoir votre identité. Google pourra répondre à votre requête sans connaître votre requête.

<h3>Informatique quantique</h3>
La deuxième guerre est celle de l'informatique quantique. Comme l'informatique électronique en son temps. L'informatique quantique va rendre caducs les protocoles de chiffrement actuels et en créer de nouveaux inviolables.

La course est ainsi lancée ! Les états, accompagnés des entreprises technologiques, se livrent une course aux armements : maîtriser le quantique pour se protéger soi-même et attaquer les autres. La France vient d'investir 1,8 milliard. L'Europe [déploie son propre réseau](https://www.futura-sciences.com/sciences/actualites/communication-quantique-europe-dote-infrastructure-telecommunications-quantiques-78759/) de communications quantiques. Les entreprises américaines [Google](https://www.lemonde.fr/sciences/article/2019/10/23/google-annonce-avoir-franchi-une-etape-technologique-majeure-en-realisant-une-percee-dans-le-calcul-quantique_6016592_1650684.html), [IBM](https://www-03.ibm.com/press/us/en/pressrelease/46725.wss) ou [D-Wave](https://fr.wikipedia.org/wiki/D-Wave) franchissent toujours plus d'étapes dans la fabrication d'un ordinateur quantique opérationnel. La Chine a déjà [lancé ses premiers satellites](https://fr.wikipedia.org/wiki/QUESS) pour mettre en place des communications quantiques et travaille sur [un ordinateur](https://www.usinenouvelle.com/editorial/un-calculateur-a-photons-chinois-atteint-l-avantage-quantique.N1040039).

<h3>La Monnaie</h3>
Enfin la troisième bataille du chiffrement est arrivée subitement, c'est la monnaie. [Bitcoin](https://fr.wikipedia.org/wiki/Bitcoin) a ouvert la voie à la première forme de monnaie ne reposant ni sur un métal précieux ni sur un état, mais uniquement sur le cryptage.

Pour la première fois de l'histoire, une pièce *numérique* est frappée en dehors de toute institution politique.

Un tel basculement [enrage les banques centrales](https://www.latribune.fr/entreprises-finance/banques-finance/industrie-financiere/face-a-la-concurrence-du-bitcoin-la-bri-et-les-banques-centrales-contre-attaquent-887501.html) qui y voient une concurrence incontrôlable. Comme pour les précédents outils de cryptage, les gouvernements tentent de l'interdire, en vain.


<h2>Conclusion</h2>
Le chiffrement est donc un enjeu géopolitique. Les futurs chiffrements homomorphes ou preuves sans connaissance pourraient mettre fin à l'hégémonie américaine sur les données d'internet.

Les cryptomonnaies pourraient mettre fin à l'hégémonie du dollar ou son successeur.

Enfin la course au quantique prolonge une course vieille de milliers d'années : concevoir un chiffrement inviolable pour soi et casser le chiffrement du voisin.


