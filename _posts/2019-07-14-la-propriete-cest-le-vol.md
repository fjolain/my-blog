---
title: 'La micropropriété, c&rsquo;est le vol !'
subtitle: Arrêtons de tout acheter !
date: 2019-07-14T17:55:26+01:00
layout: post
background: /img/2019/07/chaplin.jpg
categories: [article]
tags: [société]
---

**La pleine propriété d&rsquo;un objet est alléchante à condition que l&rsquo;on puisse l&rsquo;entretenir à moindre coût. Ces objets se font rares dans notre quotidien, alors acquérir la propriété d&rsquo;un objet est-elle rentable ?**

Dans l&rsquo;histoire de l&rsquo;humanité, la propriété généralisée est une chose récente. La propriété devient un droit avec la Déclaration universelle des droits de l&rsquo;homme. Il faudra attendre la révolution industrielle pour connaître la propriété de masse. Bien que la propriété sur les hautes valeurs, comme les valeurs immobilières, ne soit pas bien répartie équitablement : 80% de la richesse mondiale va aux 1% des plus riches ([source](http://www.lefigaro.fr/conjoncture/2018/01/22/20002-20180122ARTFIG00140-plus-de-80-de-la-richesse-mondiale-va-au-1-les-plus-riches.php)). Ce qu&rsquo;on peut appeler la micropropriété, la propriété sur les objets de faibles valeurs s&rsquo;est massivement répondu dans la société. Nous possédons tous un téléphone, des meubles, des vêtements ou encore une voiture.

La micropropriété est-elle rentable ?

La question mérite d&rsquo;être posée, car la propriété implique aussi l&rsquo;entretien. Or, la révolution industrielle n&rsquo;a pas seulement apportait la propriété généralisée, elle a aussi apporté la technicité généralisée. Du simple stylo, en passant par le téléphone dans votre poche ou votre voiture, les objets du quotidien demandent des connaissances et un outillage prohibitifs pour que monsieur Tout-le-Monde puisse les réparer tout seul. Nous les possédons d&rsquo;un point de vue légal, mais nous ne pouvons nous les approprier.

![ancienne voiture]({{ site.baseurl }}/img/2019/07/ancienne.jpg){:width="100%"}
*vue éclatée de la première Renault*

![nouvelle voiture]({{ site.baseurl }}/img/2019/07/nouvelle.jpg){:width="100%"}
*vue éclatée d'une voiture moderne*

Cela change toute l&rsquo;équation économique ! Il est loin le temps où les Français pouvaient réparer tout seuls leur voiture. Maintenant, chaque pépin vous coûtera 2 000€ de garagiste. Pire encore pour le high-tech où rien ne peut être réparé. Il faut racheter le téléphone à la moindre panne.

À quoi bon jouir, au prix fort, de la propriété, si au final cet objet nous échappe totalement et nécessite un flot continu de dépense pour l&rsquo;entretenir. Autant le louer ?

Même d&rsquo;un point de vue écologique, la micropropriété est une catastrophe. Pourquoi créer un objet solide ? Un fabricant qui vendrait un objet tellement solide qu&rsquo;un seul suffit pour toute une vie mettra vite la clé sous la porte. D&rsquo;un point de vue rentable, mieux vaut mettre en place une date de péremption sur le produit par de l&rsquo;obsolescence programmée. Sans oublier les autres problèmes de la micropropriété, comme le fait qu&rsquo;en ville, les voitures restent 90% de leur temps stationnées&#8230

![wast]({{ site.baseurl }}/img/2019/07/trash.jpg){:width="100%"}

La marche du tout louable a commencé dans l&rsquo;informatique avec « XaaS », Anything as a Service, plus précisément dans le divertissement. Le récent abandon par Apple de l’iTunes montre justement la fin de l&rsquo;achat de musique ou de film. Plus personne ne souhaite acquérir la propriété sur un film ou une musique, on préfère s&rsquo;abonner sur une plateforme de streaming comme Netflix (pour les films), Deezer (pour la musique) ou Scribd (pour les livres). 

Ce goût pour la location sur plateforme web est devenu le nouveau business model de l&rsquo;informatique. Les logiciels que l&rsquo;on devait payer puis installer pour réaliser des albums photos ou convertir un fichier sont maintenant disponibles par abonnement en ligne. Même le géant des achats de logiciels, Microsoft, pousse ces utilisateurs à ne plus acheter la suite Office 2019, mais plutôt à s&rsquo;abonner à Office 365 en ligne.<figure></figure> 

<iframe width="560" height="315" src="https://www.youtube.com/embed/aR_o2uZ4sjU" allowfullscreen=""></iframe>

En tant que développeur, je peux maintenant louer en ligne des ordinateurs payables à l&rsquo;heure. Beaucoup d&rsquo;entreprises préfèrent ainsi louer un datacenter que de construire le sien.

Ce fonctionnement par location se distille dans toute l&rsquo;économie. La voiture, autrefois symbole de la propriété comme art de vivre, se transforme en pur service. Tous les constructeurs se sont mis à la location longue durée. Par exemple Renault propose maintenant sa voiture électrique Zoé pour 150€/mois, même tarif pour Peugeot avec sa 208. Elon Musk, patron de Tesla, voit même avec les voitures 100% autonomes la fin de l&rsquo;achat de voiture, tout le monde commandera des taxis autonomes.

![zoe]({{ site.baseurl }}/img/2019/07/zoe.png){:width="100%"}

À peu près tout peut être loué tel un service comme vos [meubles](https://www.semeubler.com/4-location-meubles) ou vos [vêtements](https://www.lecloset.fr/pages/location_vetement). De tout manière, vous ne pourrez jamais les réparer à quoi bon les acheter !