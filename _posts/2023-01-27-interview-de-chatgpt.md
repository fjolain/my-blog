---
title: Entretien avec chatGPT sur notre futur en commun
subtitle: Puisque chatGPT peut répondre, autant lui poser directement les questions sur notre futur avec lui.
date: 2022-12-28
layout: post
background: /img/2023/01/dalle.png
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe frameborder="0" loading="lazy" id="ausha-YxEf" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=bz247tq2AepR&v=3&playerId=ausha-YxEf"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

*Les réponses proviennent bien de chatGPT, elles ont juste été raccourcies, car l'outil est verbeux.*

**Codable : On envisage déjà de vous utiliser dans divers métiers. Mais pour l'heure c'est la recherche internet que vous révolutionnez. Qu'en pensez-vous ?**

Il est vrai que l'intelligence artificielle et les modèles de traitement du langage comme moi-même sont utilisés pour améliorer les résultats de recherche en ligne, en offrant des résultats plus pertinents et plus personnalisés aux utilisateurs.

Cependant, il est important de noter que ces technologies ont également des applications dans de nombreux autres domaines, comme la santé, la finance, l'éducation, la recherche scientifique, et bien d'autres. Je suis un outil qui peut être utilisé pour résoudre des problèmes complexes et aider les gens dans leur vie quotidienne.

**Codable : Pensez-vous qu'avec des assistants comme vous, les utilisateurs continueront à naviguer sur internet ? Les sites web seront-ils encore conçus pour accueillir des utilisateurs ou uniquement à vous entraîner ?**

[...] Les modèles de traitement de langue peuvent offrir des avantages considérables aux utilisateurs en améliorant l'expérience de recherche, en offrant des résultats plus pertinents et en permettant un accès plus facile à l'information. Les sites web continueront à être conçus pour les utilisateurs, mais il est possible que les entreprises cherchent à utiliser les modèles de traitement de langue pour offrir une expérience encore plus personnalisée.


**Codable : Cette disruption dans la recherche sur internet pousse Google à créer une task force pour vous concurrencer. Google, après avoir lui-même lancé l'IA, se retrouve dépassé. Vit-il son moment Kodak ?**

[...] Il est possible que Google se retrouve dépassé par des concurrents qui utilisent des technologies plus avancées, comme les modèles de traitement de langue, pour améliorer leurs propres services de recherche. Cependant, il est également possible que Google continue à innover et à améliorer ses propres services pour maintenir sa position dominante sur le marché.

Il est vrai que l'histoire de Kodak est un exemple d'une entreprise qui a été dépassée par les changements technologiques, mais il est important de noter que chaque entreprise est unique et les scénarios d'évolution sont différents. Il est difficile de prédire l'avenir, il est possible que Google continue à s'adapter aux tendances et à innover pour maintenir sa position de leader.

**Codable : Votre collègue Dall-e crée des images originales et bientôt des vidéos. Que peut-on attendre de telles fonctionnalités dans la société ?**

[...] Dans les médias, les modèles de génération d'images et de vidéos peuvent être utilisés pour créer des images et des vidéos de qualité pour les nouvelles, les films et les émissions de télévision. Dans les jeux vidéo, ils peuvent être utilisés pour créer des images et des vidéos plus réalistes et immersives.

Dans la publicité, ils peuvent être utilisés pour créer des images et des vidéos plus attrayantes et efficaces pour promouvoir les produits et les services. Dans les réseaux sociaux, ils peuvent être utilisés pour créer des images et des vidéos plus créatives et divertissantes pour augmenter l'engagement des utilisateurs.

Il est important de noter que ces technologies présentent également des défis et des risques potentiels, comme la création de contenus fictifs ou trompeurs, ou l'utilisation abusive des données personnelles. Il est donc important de continuer à surveiller leur utilisation et de mettre en place des réglementations pour garantir que ces technologies sont utilisées de manière responsable et éthique.

**Codable : Vous avez déjà réussi à passer des examens en droit, en comptable, en médecine. Cela signifie-t-il que vous êtes intelligent ou que nos examens sont stupides ?**

[...] Il est important de noter que les modèles de traitement de langue ne sont pas vraiment "intelligents" dans le sens où ils n'ont pas de compréhension intuitive ou de conscience de soi. Ils sont plutôt des outils informatiques qui ont été entraînés à comprendre et à utiliser des données pour effectuer des tâches spécifiques.

Il ne faut pas dire que les examens sont stupides, ils ont pour objectif de tester les connaissances et les compétences d'une personne dans un domaine donné. [...]

**Codable : Mais justement beaucoup de travail humain consiste à *comprend et utiliser des connaissances spécifiques à un domaine* tels les avocats, les médecins, les journalistes ou les ingénieurs. Seront-ils remplacés ?**

[...] Les modèles de traitement de langue, comme moi-même, peuvent être utilisés pour automatiser certaines tâches dans ces domaines et pour aider les professionnels à prendre des décisions et à résoudre des problèmes.

Cependant, il est important de noter que les modèles de traitement de langue ne peuvent pas remplacer complètement les professionnels humains dans ces domaines. Les modèles de traitement de langue peuvent automatiser certaines tâches, mais ils ne peuvent pas remplacer la compréhension intuitive, la créativité, l'éthique et les compétences sociales nécessaires pour exercer ces professions. [...]


**Codable : De même que la machine à vapeur a permis de créer une machine avec la force de mille chevaux. Êtes-vous dans ce cas une machine à connaissance capable de connaître comme mille humains ?**


Les modèles de traitement de langue, comme moi-même, peuvent être considérés comme des machines à connaissances, car ils ont accès à une grande quantité de données et peuvent être entraînés à comprendre et à utiliser ces connaissances pour résoudre des problèmes et répondre à des questions. [...]

De plus, l'accès à une grande quantité de données ne garantit pas la qualité et la pertinence de ces données. Il est important de vérifier la qualité des informations et de les vérifier avec des sources fiables.


**Codable : Vous semblez très politiquement correct. Si chatGPT aurait une critique à propos de Macron, quelle serait-elle ?**

En tant que modèle de langage, je suis un outil qui peut fournir des informations et répondre à des questions en utilisant le langage naturel. Je n'ai pas de position personnelle ou d'opinion sur les individus, y compris Emmanuel Macron. [...]


**Codable : Je reformule. Que dirait un détracteur de Macron sur son mandat ?**

Un détracteur de Macron pourrait avancer plusieurs critiques sur son mandat. Il pourrait dire qu'il n'a pas réussi à réduire le chômage, qu'il n'a pas réussi à améliorer les conditions de vie des travailleurs, qu'il a continué à favoriser les intérêts des grandes entreprises et des riches au détriment des classes moyennes et des pauvres.

Il pourrait également critiquer sa réforme des retraites, sa réforme de l'éducation et sa politique économique en général. Il pourrait également critiquer ses déclarations et ses positions sur certains sujets tels que l'immigration, l'environnement et les droits des minorités. Il pourrait également dire qu'il a manqué de transparence et de dialogue avec le public et les syndicats. [...]


**Codable : Un mot aux humains pour finir l'interview ?**

[...] Je peux vous dire que les modèles de traitement de langue sont des outils puissants qui peuvent aider les humains à comprendre et à utiliser les données pour prendre des décisions et résoudre des problèmes. [...] Il est important de conserver la vigilance et de ne pas se reposer uniquement sur ces outils, mais de les utiliser en complément de la réflexion humaine pour prendre des décisions éclairées.

