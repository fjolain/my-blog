---
title: Faut-il avoir peur de la désinformation ?
subtitle: Démeler le vrai du faux
date: 2018-06-16T18:01:07+01:00
layout: post
background: /img/2018/06/fake.jpg
categories: [article]
tags: [société, social network]
---
L’actualité racontée par les média est toujours abrupte. Visiblement les fausses informations sont apparues pendant la campagne de Trump, comme si le monde nageait en pleine vérité avant. Pourtant le mensonge est aussi vieux que l&rsquo;information. On peut même difficilement le dissocier car l&rsquo;information est forcément modifiée par celui qui l&rsquo;a traite. Il n&rsquo;a pas fallut attendre l&rsquo;arrivée de Trump pour que Ulysse mente et offre un cheval en bois piégé à Troie. Rothschild n&rsquo;a rien a envier à Poutine quand, il titra dans son journal la défaite de l&rsquo;Angleterre à Waterloo, faisant ainsi chuter la bourse pour racheter des actions à bas prix. L’Amérique qui se sent trahie par la Russie n&rsquo;a pas le sentiment d&rsquo;avoir trahit ses alliés avec les armes de destruction massive en Irak ? Il est un peu tard et hypocrite pour crier au mensonge politique. Cependant le monde a changé de deux manières : le mensonge s&rsquo;est démocratisé et les journalistes ont disparut.

### La baisse du coût du mensonge

Tous les mensonges cités ci-dessus nécessitent pour l&rsquo;époque de grands moyens, il faut posséder ou faire pression sur beaucoup de journaux. Un tel pouvoir était réservé à une élite financière et politique. Ni les grands médias détenus par les grandes fortunes, ni les politiciens ne criaient aux fake news car c’était uniquement leurs mensonges. Ils avaient le monopole du mensonge. L&rsquo;arrivée d&rsquo;internet et des réseaux sociaux a changé la donne. Le ticket d&rsquo;entrée dans le club des menteurs et manipulateurs d&rsquo;opinion est passé de plusieurs dizaines de millions à quelques milliers d&rsquo;euros. Les plus malins peuvent même faire un mensonge mondial pour presque rien grâce au buzz.

La peur des fake news dans les médias et au gouvernement reflète surtout la peur de leurs dirigeants. Il y a encore 50 ans, ils contrôlaient parfaitement l&rsquo;opinion public en petite oligarchie. Maintenant n’importe qui peut utiliser leur jouet, et surtout ils sont devenus la cible privilégiée d&rsquo;un mensonge mondial sauvage.

Le problème des fake news de Trump n&rsquo;est pas le manque de vérité. Le problème c&rsquo;est que Trump avec quelques centaines de milliers d&rsquo;euros et son compte Twitter à fait plus de bruit que tous les grands médias occidentaux qui titraient tous les jours sur les qualités d&rsquo;Hillary. Un comble que l&rsquo;establishment politique n&rsquo;accepte toujours pas !

### La baisse des journalistes dans les rédactions

Le deuxième vecteur est le métier de journaliste. Le citoyen moyen n&rsquo;a jamais vraiment appris à recouper les sources ou se méfier des informations. Il payait pour lire un article écrit par un journaliste qui effectue la besogne à sa place. Or de nos jours, le métier de journaliste consiste plus à concaténer des tweets. Les sites d&rsquo;actualités pondent des articles sans vérifier les sources, sans recul et sans critique. On se retrouve donc à lire de la mono-information. Une fake new peut être reprise par un grand nombre de journaux sans jamais être vérifiée.

Le mensonge n&rsquo;est que de la mono-information. A l&rsquo;époque il suffisait de contrôler 3~4 médias pour contrôler l&rsquo;information. Il se trouve qu&rsquo;internet, et ses multiples canaux de communications n&rsquo;ont pas changé la donne. L&rsquo;internaute se content de regarder 4 médias dans lesquels plus aucun travail de journaliste n&rsquo;existe. Cette aspect de vérification a tellement disparut du métier qu&rsquo;il a fallut le réhabilité d&rsquo;urgence au sein de nouvelles équipes. Les « décodeurs » du Monde ou les « décrypteurs » du Figaro prouvent à quel point avant Trump la vérité n&rsquo;était qu&rsquo;une option pour les rédactions. Les « décodeurs » se vantent de vérifier les sources&#8230; comme si cette compétence était incroyable et peu présente chez les autres journalistes.

Je propose donc de rebaptiser la désinformation en mono-information. Et surtout qu&rsquo;on arrête d&rsquo;avoir peur devant cette mode qui n&rsquo;a rien de nouvelle et qui fait plus peur aux dirigeants qu&rsquo;aux citoyens.