---
title: Nous sommes en Chine (la croissance en moins).
subtitle: Notre élite nous a (encore) trahis pour le communisme.
date: 2023-04-02
layout: post
background: /img/2023/04/uerss.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-xzsx" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=oR8gxtRpx1Aj&v=3&playerId=ausha-xzsx"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

À la chute de l'URSS, l'élite européenne a perdu son modèle de bonheur et de prospérité. Heureusement, elle semble avoir fait le deuil des 100 millions de morts de son idéologie préférée. Car 30 ans, seulement, après la chute de l'URSS, la voilà de nouveau en marche pour contrôler nos vies et notre économie.

Depuis maintenant 20 ans, notre élite copie sur son nouveau maître à penser chinois, qui lui souffle toutes les tendances autoritaires.

Le score social est bien arrivé en Europe, votre nombre de doses définissait vos droits. La BCE fonce vers son euro numérique qui va permettre un contrôle total de nos finances (décortiqué dans un [article](https://www.codable.tv/crypto-euo-et-communist-20/)). Il permet aussi bien de geler l'argent de n'importe quel citoyen trop critique à l'égard du Parti. Mais également imposer des quotas de transaction: seulement 50€ d'essence par semaine, 1 billet d'avion par an, etc.

Enfin la reconnaissance faciale tant convoitée par notre élite est arrivée dans notre pays. Le parlement a voté [le projet de loi](https://www.senat.fr/leg/pjl22-220.html), et notamment l'article 7 :

    À titre expérimental et jusqu’au 30 juin 2025, à la seule fin d’assurer la sécurité de manifestations sportives, récréatives ou culturelles, qui, par leur ampleur ou leurs circonstances sont particulièrement exposées à des risques d’actes de terrorisme ou d’atteinte grave à la sécurité des personnes, les images collectées au moyen de systèmes de vidéoprotection [...] peuvent faire l’objet de traitements comprenant un système d’intelligence artificielle.

Analysons ces termes.

<h2>Système d'intelligence artificielle</h2>

La fameuse Intelligence Artificielle (IA) fait beaucoup parler d'elle. Elle a réponse à tout avec [chatGPT](https://openai.com/blog/chatgpt), peut dessiner n'importe quoi avec Midjourney ou [Dall-e](https://openai.com/product/dall-e-2). Dans nos smartphones, l'IA permet déjà d'extraire du texte des images ou de détecter des objets et des visages de nos photos.

Mais l'IA va plus loin, en détectant les visages, elle peut aussi retrouver l'identité de la personne. En mesurant l'espacement des yeux, du nez et des oreilles. L'IA peut comparer ces données biométriques avec une base pour retrouver l'identité de la personne. Des bases de données regroupant les visages des personnes avec leur identité existent, il s'agit des réseaux sociaux, tels Facebook, Instagram ou Tiktok.

Le gouvernement a donc toute la technologie pour traquer tous ses citoyens à chaque instant juste en déployant des caméras dans les rues. Le Parti chinois [a déjà déployé](https://www.rts.ch/info/monde/11137943-la-chine-veut-noter-tous-ses-habitants-et-installe-600-millions-de-cameras.html) cette surveillance dans ses rues. Le Parti Européen pousse cette solution, le parlement français vient de franchir le pas.

<h2>Manifestations sportives, récréatives ou culturelles</h2>
*... qui, par leur ampleur ou leurs circonstances sont particulièrement exposées à des risques d’actes de terrorisme ou d’atteinte grave à la sécurité des personnes.*

Si ce dispositif ne sert que pour les JO de 2024, pourquoi ne pas avoir écrit "durant les JO de Paris en 2024" ? Le choix de termes flous est voulu pour pérenniser la surveillance en dehors de Paris et des JO. Nos élites profitent d'un évènement exceptionnel pour mettre en place une surveillance permanente.


On peut même imaginer que les "manifestations culturelles" soient appliquées pour toutes manifestations contre le Parti afin de traquer les citoyens opposants.

<h2>Qui croire ?</h2>

Plus loin dans l'article nous trouvons :

    Ces traitements n’utilisent aucun système d’identification biométrique, ne traitent aucune donnée biométrique et ne mettent en œuvre aucune technique de reconnaissance faciale.

Cela est en contradiction avec l'utilisation du "sytème d'intélligence artificielle" plus haut. Mais s'ils prennent la peine de se contredire, c'est qu'une fois installés, nous n'avons que cette promesse de députés pour éviter la surveillance biométrique.

Devant une caméra comment peut-on savoir si elle est allumée ou éteinte ? Si son contenu est analysé pour identifier les citoyens ou non ?

Tout ce traitement est caché du citoyen, parfaitement réfutable par le gouvernement, impossible à auditer par le citoyen.

<h2>Faisons les comptes</h2>

- l'UE souhaite [espionner nos conversations privées](https://www.developpez.com/actu/341124/-Stop-au-plan-de-surveillance-de-masse-propose-par-la-Commission-de-l-UE-lance-un-editeur-de-VPN-car-l-UE-entend-scanner-toutes-les-correspondances-privees-pour-lutter-contre-la-pedopornographie/). Mais Ursula von der Leyen [ne veut pas  rendre publique](https://www.marianne.net/politique/union-europeenne/sms-de-von-der-leyen-au-patron-de-pfizer-3-questions-pour-comprendre-la-polemique) ses conversations avec le patron de Pfizer, ni les contrats.
- l'UE [veut interdire le cash](https://journalducoin.com/bitcoin/paiements-cryptos-limites-ue-inflige-nouveau-coup-bas-bitcoin/) et les crypto pour tout achat de plus de 1 000€. Dans le même temps, on retrouve [1 500 000€ en cash venant du Qatar](https://fr.wikipedia.org/wiki/Scandale_de_corruption_par_le_Qatar_au_Parlement_europ%C3%A9en) chez une eurodéputée.
- La France [fait la chasse à l'évasion fiscale](https://www.tdg.ch/des-perquisitions-visent-cinq-banques-en-france-514254052447). Mais [le maigrichon patrimoine](https://www.contrepoints.org/2017/04/12/286636-declaration-de-patrimoine-demmanuel-macron-tortueuses-explications) de Macron reste un mystère.
- On peut se retrouver [en garde à vue](https://www.bfmtv.com/grand-littoral/ordure-une-habitante-du-pas-de-calais-interpellee-pour-avoir-insulte-emmanuel-macron-sur-facebook_AN-202303290510.html) pour insulte à Macron. Mais Macron peut lui nous ["emmerder"](https://www.letemps.ch/monde/europe/emmanuel-macron-assure-vouloir-emmerder-nonvaccines).

À cela s'ajoute la note sociale, la reconnaissance faciale, la monnaie numérique traçable, les lois arbitraires sur les logements ou les voitures. On se retrouve bien en Chine... la croissance économique en moins.
