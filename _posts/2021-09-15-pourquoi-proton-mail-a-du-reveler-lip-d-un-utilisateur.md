---
title: Pourquoi ProtonMail a dû révèler l'adresse IP d'un militant ?
subtitle: Et surtout comment ont-ils fait pour ne pas en révéler davantage
date: 2021-09-15
layout: post
background: /img/2021/09/mail.jpeg
published: www.contrepoints.org/2021/09/05/404600-qr-code-et-pass-sanitaire-vers-une-surveillance-de-masse
categories: [article, podcast]
tags: [survaillance, technologie]
---

<iframe frameborder="0" loading="lazy" id="ausha-E3Xy" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=B4kMYSj0dKNR&v=3&playerId=ausha-E3Xy"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>


L'état français a forcé l'entreprise ProtonMail à révéler l'adresse IP d'un militant écologique au nom de la lutte antiterroriste. Pour en comprendre l'importance, il faut rappeler qu'une adresse IP permet au gouvernement de remonter au domicile de l'internaute, brisant son anonymat. ProtonMail a donc contribué à l'arrestation du militant. Or ProtonMail est une entreprise de mails suisse se proclamant protectrice de la vie privée et ne *collectant par défaut aucune donnée personnelle*.

Ce **par défaut** est au coeur de l'affaire qui ternit l'image de l'entreprise, car il montre que certes, l'entreprise ne collecte aucune donnée en temps normal, mais peut s'y voir obligée sur demande du gouvernement.

En effet, que ce soit aux États-Unis après le 11 septembre 2001 ([Patriot Act](https://fr.wikipedia.org/wiki/USA_PATRIOT_Act)) ou en Fance après le 13 novembre 2015 ([Loi Rensignements](https://fr.wikipedia.org/wiki/Loi_relative_au_renseignement)), les gouvernements ont voté des lois obligeant les entreprises à livrer les données de leurs utilisateurs en cas de menace terroriste. Or la définition est très vague, [le Code pénal](https://www.senat.fr/rap/l05-117/l05-1173.html#:~:text=Le%20code%20p%C3%A9nal%20d%C3%A9finit%20l,'intimidation%20ou%20la%20terreur%20%C2%BB.) définit l'acte terroriste comme un acte se rattachant à *une entreprise individuelle ou collective ayant pour but de troubler gravement l'ordre public par l'intimidation ou la terreur*.

Ainsi, l'état se retrouve maintenant en position de forcer la collecte de donnée chez les entreprises du numérique pour de vagues prétextes.

<h2>Que peuvent faire les entreprises pour résister ?</h2>

Déjà peu d'entreprises tentent d'y résister. On peut citer quand même Signal, ProtonMail ou Apple comme entreprises essayant de s'opposer à l'intrusion de l'état dans leurs données. Mais la loi est très claire: les entreprises [*ont obligation de collaboré* (LSCPT Suisse)](https://www.fedlex.admin.ch/eli/cc/2018/31/fr).

Les entreprises résistantes ont donc opté pour le chiffrement du bout en bout. Ainsi, tout ou partie des données qui transitent sur leur serveur sont illisibles même pour l'entreprise, seul l'utilisateur possède le mot de passe de déchiffrement. En soi, l'entreprise peut toujours les collecter et les partager à l'état, mais comme personne ne dispose du mot de passe, les données restent illisibles.

L'entreprise peut aussi proposer son service à travers TOR ou un service VPN pour éviter de connaître l'adresse IP de ses utilisateurs. C'est ce que propose ProtonMail, mais qui n'a pas été utilisé par le militant.

En soi, l'histoire de ProtonMail est un exemple d'entreprise résistante, puisqu'ils n'ont pu donner *que* une adresse IP. Si le militant utilisait Gmail, LaPoste ou Orange, alors le gouvernement aurait mis la main sur l'ensemble de ses mails et de ses correspondants. Or grâce au chiffrement mis en place, ProtonMail se retrouve techniquement incapable de livrer toutes ces données.

<h2>Contre-attaques étatiques.</h2>

Évidemment, cette sensation d'impuissance agace le désir de contrôle absolu de nos gouvernements. Ils pensaient s'arroger légalement une surveillance de masse, mais voilà que des technologies comme le chiffrement, le réseau TOR, les services VPN ou même Bitcoin réduisent la portée de leurs lois.

Nous assistons donc [en France](https://www.clubic.com/messagerie-instantanee/actualite-20557-la-france-pousse-l-ue-a-affaiblir-le-chiffrement-des-messageries.html), [en Australie](https://www.developpez.com/actu/236906/La-vague-loi-anti-chiffrement-de-l-Australie-etablit-un-terrible-precedent-mondial-et-represente-un-risque-enorme-pour-notre-securite-numerique/) ou [aux États-Unis](https://www.internetsociety.org/fr/blog/2020/07/le-dernier-projet-de-loi-americain-anti-cryptage-menace-la-securite-de-millions-de-personnes/) à une volonté des gouvernements d'interdire le chiffrement et d'autres technologies, afin que plus aucune entreprise ni aucun citoyen ne puissent résister face à la surveillance.
