---
title: A quoi sert le web3 ?
subtitle: Qu'est-ce que le web3, web2, et le web1 ?
date: 2022-07-25
layout: post
background: /img/2022/07/pipe.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [blockchain, internet]
---

<iframe frameborder="0" loading="lazy" id="ausha-pnMh" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=yAdaMtL3Jx0e&v=3&playerId=ausha-pnMh"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Un nouveau mot est maintenant sur toutes les présentations des startups qui souhaitent l'argent des investisseurs. Le *web3* succède aux précédents buzz words tels BigData, Intelligence Artificielle ou Cloud Native.

Bien évidement, une fois mis sur la présentation d'une startup, ce mot ne veut plus rien dire. Pourtant techniquement, il réfère à une réalité précise. Nous allons voir ce qu'est le web3 en passant également par le web1 et le web2.

<h2>Web 1, le serveur</h2>

Le web1 est apparu avec l'apparition d'internet et des premiers serveurs hardware. On appelle serveur, un ordinateur qui va rester connecter à internet tout sa vie sans presque ne jamais s'éteindre. Il n'a pas d'écran ni de clavier, mais une énorme carte réseau et beaucoup de RAM.

Mais il y a aussi le serveur software, la brique logicielle qui tourne sur le serveur hardware pour répondre aux requêtes venant d'internet.

Le web1 vient des serveurs softwares. Des logiciels capablent de *servir*  à travers internet des sites web stockés sur le disque dur du serveur hardwware.

Ainsi le web1 est juste internet avec des sites web composés de fichiers, ils sont *statiques*, c'est-à-dire figés. Si vous souhaitez modifier le moindre contenu du site, vous devez modifier les fichiers présents sur le serveur et redéployer le site web.

<h2>Web 2, la base de données</h2>

Le web 2 est apparu avec la base de données. Le site web se retrouve séparer entre les fichiers qui forment la structure d'une page web, une sorte de coquille vide. Le contenu est déplacé dans la base de données.

On peut donc modifier le contenu du site web sans changer les fichiers du serveur. On peut même modifier le contenu du site web, directement depuis le site web grâce à la base de données !

Ainsi avec le web2, nous voyons apparaître des sites modifiables par les utilisateurs. Ils sont devenus *dynamiques*. Les forums, les sites de presse, blog, Wikipédia, Facebook, YouTube sont tous regroupés dans le web2.

Même si entre Wikipédia, un site de documentation en ligne et Google Drive, une véritable suite bureautique en ligne, il y a un gouffre technologique, les deux sont pourtant web2. Car le serveur Wikipédia n'utilise que le contenu de la base de données Wikipédia, et pareil pour Google.

Il s'agit d'une limitation des bases de données du web2, elles sont stupides et malléables. Celui qui a accès à la base peut tout faire. La base d'un site web est donc accessible uniquement au serveur qui va y contrôler l'intégrité des données et les permissions utilisateur.

Les sites web2 fonctionnent donc en silo, chaque serveur héberge son site et accède à sa base. Aucun site web ne peut se greffer sur la base de données d'un autre site, au risque de corrompre les données.

<h2>Web 3, la blockchain</h2>

Le web3 est apparu avec la blockchain. Une blockchain est une base donnée sécurisée et intelligente conçue pour stocker des actifs (certificats, actions, monnaies, autres tokens). Elle est sécurisée, car personne ne peut la réécrire, on ne fait qu’ajouter des transactions, elle est immutable. Elle est intelligente, car elle gère l'intégrité des actifs et les permissions utilisateur par elle-même.

Contrairement aux bases de données web2, la blockchain peut donc être utilisée par de nombreux sites web, car aucun site ne peut la corrompre. Une blockchain fait donc office de base de données partagée à l'ensemble d'internet. Les sites peuvent lister les actifs détenus par l'utilisateur et ajouter une transaction validée par l'utilisateur.

Ainsi les données (actifs) peuvent *circuler* entre les différents sites web à travers la blockchain.

Si vous écoutez un album en boucle, Spotify peut émettre un certificat vous désignant comme fan. Ce certificat peut vous permettre d'accéder à la billetterie avant tout le monde pour acheter une place de concert de votre groupe.

Si vous achetez une guitare, le fabricant peut enregistrer votre achat sur la blockchain. Des chaînes Youtubes privées vous deviennent ainsi accessibles pour visionner des cours de guitare.

Dans un jeu vidéo, l'éditeur peut enregistrer votre avancement sur blockchain. La fin du jeu peut vous donner accès à des salons Discords pour parler entre joueurs.

Tous ces cas d'usages sont possibles en web3 grâce à la blockchain, mais impossibles avec l'internet en silo du web2.

Le web3 est un terme précis qui désigne un site web qui va utiliser la blokchain pour gérer des actifs au nom d’un utilisateur. Grâce à la blockchain, le web3 permet une intérropérabilité des données entre tout internet.
