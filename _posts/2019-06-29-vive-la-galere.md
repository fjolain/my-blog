---
title: Vive la galère
subtitle: Guide à mettre dans toutes les mains
date: 2019-06-29T13:06:01+01:00
layout: post
background: /img/2019/06/canotiers.jpg
categories: [article]
tags: [société]
---

**Si toi aussi tu ne fais pas grand-chose de tes week-ends, que tu n&rsquo;éprouves pas l&rsquo;envie de partir en voyage deux mois au bout du monde, que la vie des autres paraît bien chargée par rapport à la tienne. C&rsquo;est peut-être que tu es en manque de galères. Je vais te donner quelques astuces pour agrémenter ta vie de galères si plaisantes à notre société.**

L&rsquo;homme ne serait pas l&rsquo;homme s&rsquo;il ne crée pas ses propres problèmes. Une vie sans galères est perçue comme une hérésie par la société. Être surbookée est tendance. Mais comment faire pour saturer une journée là où le progrès simplifie drastiquement notre vie. On peut tout acheter à n&rsquo;importe quelle heure en un clic. On a des robots pour faire la vaisselle, le linge, le café &#8230; L&rsquo;information transite à toute vitesse. Les transports se sont accélérés par le train, l&rsquo;avion ou la voiture. 

Dans ces conditions, comment garder dans nos journées un niveau de galère respectable. Un niveau suffisamment élevé pour que l&rsquo;on puisse continuer à proclamer une vie sociale acceptable sans se voir traiter de branleur ne faisant rien du week-end.

Nous allons voir trois techniques de galère populaire qui n&rsquo;apportent rien d&rsquo;utile à votre vie.

## La galère par château de cartes

![chateau]({{ site.baseurl }}/img/2019/06/chateau.png){:width="100%"}

> Exemple : Vous partez à Barcelone, tous les guides le disent attention au pickpocket ! Vous avez un ancien téléphone portable, ça serait bien de prendre lui plutôt que l&rsquo;actuel ? Non, d&rsquo;accord&#8230; Alors au moins mettez un code de verrouillage sur l&rsquo;actuel ? Non, d&rsquo;accord&#8230; Prenez deux titres d&rsquo;identité et laissez un à l’hôtel, ça évitera de tout perdre ? Non, vous préférez prendre que le passeport. Et mettre la carte bancaire, passeport, clé de maison (avec l&rsquo;adresse écrite sur le passeport), clé de l’hôtel et téléphone dans le même sac avant d&rsquo;aller sur la Ramblas, vous n’avez pas peur ? Non, ce sont les autres qui se font voler, vous vous serez hypervigilant. Gagné, vous venez de tout perdre et de vous mettre dans une énorme galère !

Un château de cartes consiste à prendre une suite de décisions peu rationnelles dans le but de galérer dans le futur avec ou sans imprévu.

Comme autres exemples, on peut citer :

  * quitter l&rsquo;école (probable le pire château de carte)
  * toute les situations où il vaut mieux prévenir que guérir

## La galère par chaîne d&rsquo;évènements

![chaine]({{ site.baseurl }}/img/2019/06/chaine.jpg){:width="100%"}

> Exemple : Vous venez de vous réveiller, il est 10h. À 12h vous avez rendez-vous chez le dentiste. Vous devez aussi aller rendre une paire de chaussures au centre commercial. Deux heures pour tout faire, c&rsquo;est juste, le moindre imprévu va tout faire capoter. Une vie sans galère serait d&rsquo;aller au centre commercial après le dentiste, mais ça serait dommage de louper une chaîne d&rsquo;évènements._ _Aller on fonce au centre commercial ! Vous vous perdez 10 min pour retrouver le magasin, plus un métro qui stationne 5min pour régulation du trafic, et c&rsquo;est gagné vous êtes en retard pour le dentiste.

La chaîne d&rsquo;évènement est une forme précise de château de cartes. Une chaîne d&rsquo;évènements consiste à se remplir l&rsquo;agenda d&rsquo;évènements successifs serrés. Si tout se passe bien, c&rsquo;est juste une journée chargée. L&rsquo;idéal vient quand un imprévu surgit dans la chaîne faisant boule de neige. La boule de neige doit, de préférence, engendrer du retard, du stresse et des conflits&#8230; bref de la galère.

Comme autres exemples, on peut citer :

  * beaucoup de plannings de « vacances » de vos amis
  * Les soirées où il y a plus que deux déplacements prévus
  * les projets au boulot qui se finissent en retard
  * Le fonctionnement de la SNCF dans son ensemble

## La galère par moutonnerie

![moutons]({{ site.baseurl }}/img/2019/06/mouton.jpg){:width="100%"}

> Exemple : C&rsquo;est la finale de la coupe du monde. Le bar en bas de chez vous propose une soirée spéciale. Quelque chose de simple et tranquille s&rsquo;annonce. Quand soudain votre ami débarque : « Ils vont installer un écran géant à l’hôtel de ville. Il aura plein de monde, ça va être trop bien ». Arrivé à l&rsquo;écran géant, vous êtes englouti par une énorme foule rien que pour rentrer dans la zone qui visiblement est déjà complète. Au final ça fait une heure que vous essayez désespérément de rentrer dans la zone ou dans n&rsquo;importe quel bar, mais tout est complet. Il va falloir accepter de regarder le match de loin, et debout.

La galère par moutonnerie est très présente dans les villes (surtout à Paris). Elle se débusque facilement par la phrase : <em>il aura plein de monde, ça va être trop bien.</em>

Comme autres exemples, on peut citer :

  * Prendre les transports à 9h alors que la seule personne qui vous attend au boulot est votre ordinateur
  * Partir en vacances le 15 août
  * Vouloir un maillot des bleus après leur victoire
  * Vouloir le dernier iPhone le jour de sa sortie
  * Aller dans les grands magasins le premier jour des soldes

Voilà, vous pouvez maintenant encore galérer un peu plus dans vos vies. Mais n&rsquo;oubliez pas la règle d&rsquo;or, aucune galère ne doit rester silencieuse. N&rsquo;hésitez pas à vous plaindre des galères à vos collègues, vos proches ou votre mur Facebook.