---
title: 4 règles pour devenir une crypto-nation
subtitle: Le paradoxe de l'état français, il souhaite devenir une crypto-nation tout en tapant toujours plus sur la crypto.
date: 2022-11-07
layout: post
background: /img/2022/11/flag.webp
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
---

<iframe frameborder="0" loading="lazy" id="ausha-RQz8" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=yAdaMtkJ4mDY&v=3&playerId=ausha-RQz8"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Dans le monde de la cryptonation, il y a deux écoles : la France qui veut devenir une cryptonation et dont on peut voir les politiciens gesticuler et parler. Et la Suisse, dont aucun membre de la fédération n'a spécialement émis d'avis sur la cryptonation.

À votre avis qui deviendra une cryptonation ?

<h2>Législation des particuliers</h2>

En Suisse, on peut détenir des cryptos sans formalité. On peut même en acheter à de nombreux distributeurs physiques y compris avec du liquide contre diverses cryptomonnaies dont Bitcoin, Ethereum ou Monero. Même CFF (SNCF Suisse) expérimente l'achat de Bitcoin depuis [ses automates en gare](https://www.sbb.ch/fr/gare-services/a-la-gare/services-aux-distributeurs-de-billets/bitcoin.html).

Pour la fiscalité, en Suisse, il n'a [pas d'imposition sur la plus-value pour les particuliers](https://www.allnews.ch/content/points-de-vue/la-suisse-paradis-fiscal-des-crypto-monnaies-en-europe#:~:text=Le%20r%C3%A9gime%20fiscal%20de%20la,pas%20imposables%20pour%20les%20particuliers.). Il faut quand même les déclarer dans l'impôt sur la fortune, comme tout actif.

En France, le gouvernement tente de contrôler de plus en plus. L'achat de crypto nécessite une carte d'identité. Depuis [2019](https://www.economie.gouv.fr/cedef/regime-fiscal-cryptomonnaies), il faut aussi déclarer ses cryptos aux impôts, ainsi que tous ses comptes sur des plateformes d'échange.

La France adoube aussi l'Union Européenne qui souhaite à travers son projet [MICA](https://www.lepoint.fr/politique/emmanuel-berretta/mica-le-reglement-europeen-qui-veut-enchainer-les-cryptomonnaies-01-07-2022-2481756_1897.php#11) tracer chaque transaction et interdire Bitcoin au nom de l'écologie.

Quant aux impôts... Un article ne suffira pas à tout expliquer. En fonction de quelle cryptos vous achetez où vous les échanger et quand, l'impôt change. La complexité est telle que la startup [Waltio](https://waltio.co/) se propose de remplir les impôts pour vous.

Mais le gros problème pour les particuliers reste l'incertitude. Le gouvernement ne va jamais de main morte. On s'attend à une lourde régulation voire une interdiction chaque mois.

**Régle #1** : La fiscalité sert à payer le régalien, non à punir les profits. À quoi bon prendre des risques à détenir du Bitcoin, si à la fin tout va à l'État ?

**Régle #2** : Pour devenir une cryptonation, il ne faut pas considérer ses citoyens comme tous des banquiers de Daesh. Sinon la prochaine étape sera une surveillance Orweillienne.

<h2>Législation des entreprises</h2>
En Suisse, il n'a pas de procédure spéciale pour gérer des cryptos. FINMA (AMF Suisse) considère comme de la FinTech, elle laisse les startups [se créer sans licence](https://www.finma.ch/en/documentation/dossier/dossier-fintech/entwicklungen-im-bereich-fintech/) ("sandbox") tant qu'elles gèrent moins d'un million de francs de dépôts.

En France, il faut devenir Prestataire Sur Actifs Numériques ([PSAN](https://www.amf-france.org/fr/espace-professionnels/fintech/mes-relations-avec-lamf/obtenir-un-enregistrement-un-agrement-psan)) avant le premier euro. Cela prend plus d'un an et oblige à se constituer un département juridique pour commencer.

Ensuite, il faudra mettre en place une procédure de validation de l'identité des utilisateurs, ainsi que tout un tas de contrôles anti-blanchiment.

Tout cela est à faire dès le premier euro sous gestion.

**Régle #3** : Avant d’ensevelir les startups sous des licences et des contrôles anti-blanchiment d'argent... il faut attendre qu'elles fassent de l'argent.

<h2>Montrer l'exemple</h2>

En Suisse, la fédération est plutôt neutre sur le sujet. Certains cantons sont plus déterminés. À Zoug, il est possible de payer ses impôts en Bitcoin. À Neuchâtel, on peut mettre du Bitcoin dans le capital social de son entreprise.

[Plusieurs banques souhaitent](https://www.capital.fr/crypto/les-banques-suisses-pretes-a-se-mettre-aux-cryptomonnaies-1425171) proposer des services cryptos à leurs clients. Certaines banques se sont même lancées en tant que banque crypto-native ([Condensat](https://www.condensat.tech/), [Bitcoin Suisse](https://www.bitcoinsuisse.com/)).

En France le ton est donné à répétition par le gouvernement. Le dernier a été en présence de toutes les banques centrales réunies à Paris en septembre. « Les crypto-actifs sont des actifs purement spéculatifs et certainement pas des moyens de paiement ni des réserves de valeurs », a déclaré Christine Lagarde. Cela reste plus doux que Bruno le Maire qui liait directement Bitcoin et terroriste dans un [tweet](https://twitter.com/brunolemaire/status/1336703944421232640?lang=fr) en 2020.  Gare aux banquiers qui n'auraient pas compris la ligne du Parti !

On peut saluer le courage de la Société Générale qui brave les serments et prototypes sur la blockchain depuis plusieurs années. Elle est à l'origine du premier stablecoin euro [EUR-L](https://fr.lugh.io/).

Mais le constat est amer, les startups françaises doivent se faire bancariser à l'étranger, aucune banque ne veut toucher aux startups crypto.

CoinHouse est à la Fidor (Allemagne), Paymium est à [Weg Bank](https://bitcoin.fr/un-nouveau-partenaire-bancaire-pour-paymium-2/) (Allemagne), [Bitcoin Avenue](https://bitcoin.fr/le-cic-cloture-le-compte-bancaire-de-bitcoin-avenue-moins-de-dix-jours-apres-son-enregistrement-en-tant-que-psan/) a fait faillite, car le CIC l'a lâché... juste après être devenu PSAN.

Quant à la licorne française: Ledger. Elle fait attention de rester un fabriquant hardware. Toutes ses fonctionnalités financières sont déléguées à des prestataires étrangers.

On peut noter une bonne idée du gouvernement. Darmin a instauré un droit aux startups crypto d'[ouvrir un compte à la Caisse des Dépôts](https://bitcoin.fr/le-droit-au-compte-des-startups-crypto-assure-par-la-caisse-des-depots-et-consignations/) en cas de refus des banques françaises. Très bonne idée, mais jamais appliquée.

**Régles #4** : Pour inciter l'écosystème il faut montrer l'exemple et tendre la main aux acteurs. Traiter tout le monde de terroriste ou de Ponzi freine l'adhésion.

Au final, il a bien deux exemples. L'état français qui malgré ses veux pieux de cryptonation, a tout fait pour tuer l'écosystème, avec des fiscalités et procédures trop lourdes, le tout saupoudré de serment quasi mensuel sur le diable Bitcoin.

Beaucoup de Français ont fui notamment en Suisse (comme [Richard Détente](https://www.youtube.com/watch?v=fiYdDYiCzHg) ou moi-même). Un peu par hasard, les pionner se sont installés a Neuchâtel. Le canton (pourtant réputé peu entrepreneurial par les Suisses) les a incités à s'implémenter. La rumeur d'un endroit "crypto-friendly" s'est propagée, Neuchâtel devient une cryptovallée peuplée de français en exile.

