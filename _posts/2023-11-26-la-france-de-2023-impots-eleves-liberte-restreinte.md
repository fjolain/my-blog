---
title: La surveillance devient le prix à payer pour vivre en France
subtitle: La longue liste de l'espionnage voulu par notre gouvernement en 2023.
date: 2023-11-26
layout: post
background: /img/2023/11/form.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-ymEl" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=yOQnruJxlaOQ&v=3&playerId=ausha-ymEl"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Jadis, seuls les criminels se retrouvaient sur écoute. La traque du citoyen par les bureaucrates était une exception. Les surveillances de masse se trouvaient réservées aux régimes totalitaires, impensables dans nos démocraties.

Or depuis le 11 septembre, nos gouvernements nous considèrent tous comme des potentiels criminels qu'il faut espionner constamment. Et toute comparaison aux régimes totalitaires fera glousser nos fonctionnaire devant une telle allusion.

J'ai déjà longuement commenté cette dérive à travers les dernières actualités comme la volonté d'[interdire les VPN](https://www.journaldugeek.com/2023/10/04/bientot-linterdiction-des-vpn-en-france/#:~:text=Plus%20pr%C3%A9cis%C3%A9ment%2C%20les%20VPN%20sur,et%20r%C3%A9guler%20l'espace%20num%C3%A9rique), de mettre un [mouchard dans nos navigateurs](https://foundation.mozilla.org/fr/campaigns/sign-our-petition-to-stop-france-from-forcing-browsers-like-mozillas-firefox-to-censor-websites/), d'[interdire le chiffrement](https://www.bfmtv.com/tech/actualites/donnees-personnelles/des-pays-europeens-veulent-s-attaquer-au-chiffrement-de-whats-app-ou-i-message_AV-202305230347.html), de l'égaliser la [reconnaissance faciale](https://www.leparisien.fr/jo-paris-2024/paris-2024-il-y-aura-bien-des-cameras-intelligentes-pour-les-jeux-olympiques-23-03-2023-ZUGRDLKEXJDSVD6QGXKIKBYDTM.php), d'[interdire les cryptomonnaies](https://journalducoin.com/bitcoin/paiements-cryptos-limites-ue-inflige-nouveau-coup-bas-bitcoin/).

Tous ces abandons de nos droits ont été faits en 2023, vous pouvez constater l'imagination sans limites de nos bureaucrates dans nos privations de liberté.

Aujourd'hui j'aimerais porter l'attention sur une nouvelle dérive de surveillance "à la française". L'État français profite de nos nombreux impôts, taxes, redevances, cotisations, charges pour justifier une surveillance afin d'éviter les fraudes.

D'un côté, on va créer un impôt inquisiteur, pour de l'autre mettre en place une surveillance pour cet impôt. Double punition pour le citoyen.

L'impôt sur le revenu permet de légitimer une [surveillance de notre train de vie](https://www.lesechos.fr/idees-debats/cercle/opinion-comment-le-fisc-espionne-vos-reseaux-sociaux-et-votre-jardin-1344515) sur les réseaux sociaux par le fisc.

La taxe sur les piscines permet de légitimer une [surveillance par satellite](https://www.lesechos.fr/idees-debats/cercle/opinion-comment-le-fisc-espionne-vos-reseaux-sociaux-et-votre-jardin-1344515) des maisons française par le fisc.

L'URSAFF peut demander vos conversation de votre téléphone profesionel, s'il juge son usage trop personnel, votre téléphone devient un [avantage en nature dissimulé](https://www.leguiderh.fr/gestion-administrative-des-rh/telephone-notes-de-frais-et-remboursements#:~:text=Elle%20est%20en%20effet%20limit%C3%A9e,employeur%20sera%20plafonn%C3%A9%20%C3%A0%2050%25.) et vous êtes bon pour un redressement.

L'impôt sur la fortune permet à l'État de connaître tous nos comptes bancaires y compris à l'étranger ainsi tous nos biens immobiliers et mobiliers.

Vous devez maintenant [déclarer vos occupants](https://www.impots.gouv.fr/particulier/questions/quelles-informations-sont-declarer-le-nouveau-service-en-ligne-gerer-mes) dans vos logements directement aux impôts pour vérifier l'impôt sur le revenu de l'immobilier.

Et si par miracle, toute cette surveillance ne suffit, je rappelle que le fisc analyse toute transaction supérieure à [1000€ à travers Trakfin](https://www.economie.gouv.fr/tracfin/communication-systematique-dinformations-cosi-relative-aux-transmissions-fonds). Sachant que l'État a par ailleurs rendu [illégale toute transaction en liquide de plus 1000€](https://www.latribune.fr/entreprises-finance/banques-finance/banque/le-paiement-en-especes-interdit-au-dela-de-1-000-euros-a-partir-du-1er-septembre-487685.html). L'État interdit donc toute transaction non traçable par lui au-dessus de 1000€.

<h2>Deux poids, deux mesures</h2>
Forte heureusement, il existe encore des moyens d'éviter la surveillance.

Si vous continuez d'habiter d'en un HLM, alors que vous ne remplissez plus les critères. Vous pouvez dormir tranquille. L'État ne semble pas inquiet, d'ailleurs il souhaite [25%](https://fr.wikipedia.org/wiki/Loi_relative_%C3%A0_la_solidarit%C3%A9_et_au_renouvellement_urbains#%C3%89volution_de_l'article_55) d'HLM partout, plutôt que de réguler ceux existants.

Si vous réclamez votre chômage depuis vos vacances à l'étranger. Là aussi, soyez rassuré, il ne se passera rien.

L'État est capable de repérer les piscines par intelligence artificielle depuis l'espace, mais ne parvient pas à bloquer le site pôle emploi aux adresses IP en dehors de France.

Si vous fraudez la sécurité sociale, ne vous troublez pas. Aux dernières nouvelles, il y a [2,6 millions](https://www.lefigaro.fr/social/la-securite-sociale-denombre-2-6-millions-de-carte-vitales-en-trop-20200212) de cartes vitales actives qu'il y a d'inscrit à la sécurité sociale.

Le mieux est encore d'être dans l'État. Mon patrimoine intrigue le fisc, mais le [maigrichon patrimoine de Macron](https://www.hatvp.fr/livraison/dossiers/macron-emmanuel-dsp20722-president.pdf), après 4 ans de banquier d'affaires, n'a déclenché aucune investigation.

Savoir quel locataire habite chez moi, semble hautement important. Mais [Cahuzac](https://fr.wikipedia.org/wiki/J%C3%A9r%C3%B4me_Cahuzac#Carri%C3%A8re_professionnelle), qui  proposait ses services de consultant à BigPharma à travers son EURL "Cahuzac Conseil", tout en travaillant au ministère de la Santé, ne semble pas choquer. Cette double activité, en plein conflit d'intérêts, ne lui a pas été reprochée, seul un compte en banque suisse alimenté par ses « missions » auprès de BigPharma lui a value condamnation. Il ne fit qu'un an en prison, il est à l'heure actuelle médecin en Corse.

En résumé, être innocent ou coupable ne dépend plus de vos agissements. Vous serez constamment innocent si vous participez au pillage de l'État.

Mais si vous vous tenez à l'écart de l'État, alors vous voilà un citoyen présumé coupable à perpétuité. Afin de vous disculper du terroriste, pédophile, trafiquant, fraudeur qui sommeille en vous, une surveillance de tous vos faits et gestes devient nécessaire.

