---
title: Le Canular de la loi Avia
subtitle: Le début de la fin
date: 2020-06-06
layout: post
background: /img/2020/06/tintin.jpeg
categories: [article]
tags: [société, vie privée]
---

La loi Avia déchaîne les foules à juste titre. Je trouve que devant cette reculade de liberté d’expression, trop peu de journaux en parlent. Les vacances post-covid semblent plus importantes que notre liberté d’expression.

Les répercutions de cette loi ne vont pas changer nos vies, on va voir ensemble que cette loi est inapplicable et qu’elle peut même se retourner contre l’état. Le problème de cette loi est plus idéologique, une sorte de test sur la population : « Est-elle prête à se soumettre sans révolte » en vue de la docilité du déconfinement et du silence autour de cette loi, la réponse semble plutôt positive laissant rêveur le gouvernement sur de futurs projets.

# La haine n’a pas de définition précise

## Les injures sont réelles, la haine est un choix
La haine n’a pas de définition, et encore moins de cadre juridique. Par conséquent, vouloir encadrer la haine est irréaliste.  Par exemple, lorsque le TGV arriva à Bordeaux, des banderoles avec écrit « Dégagez les Parisiens » accueillaient les nouveaux venus… Ça n’a choqué personne, la haine parisienne est « normale », elle est tolérée par la nation. Si le slogan devient « Dégagez les Juifs » aussitôt ça devient un scandale. De même, un militant se filmant en injuriant des patrons ou des riches est une haine tout aussi acceptée ici. Un militant critiquant l’islam, crée cette fois un scandale.

Un contenu haineux est avant tout politique. Devant une injure, soit la cible s’en ficher ou n’est pas légitime aux yeux de la société, soit elle se crée une statue de victime. Elle transforme l’injure en contenu haineux pour amplifier son combat politique.

## La haine est une liberté d’expression
Quelle différence fait-on envers la haine et la critique ? Beaucoup de gens critiquent Macron au point de l’insulter sur des banderoles, c’est bien de la haine, mais condamner cette haine reviendrait à condamner la critique.

Et si je critique une religion et un dieu en particulier, c’est un blasphème. Est-ce qu’avec cette loi, le blasphème devient puni ? Dans un pays laïque ?

Sans même avoir parlé du technique, cette loi ouvre la boîte de pandore qu’on n’arrivera jamais à refermer.

# Les algorithmes n’ont aucune chance

Parlons technique ! Le problème est assez simple. Les plateformes doivent retirer tout contenu haineux en 24h. Vu le torrent de data transitant sur la plateforme, la censure ne pourra être qu’algorithmique, qui plus est avec des algorithmes américains.

## Une langue pleine de saveur
Prendre son plus beau crayon pour pondre une obligation de filtrer la haine par algorithmique montre la totale ignorance de l’état dans la technologie. Des algos pour distinguer la haine dans un message, ça existe, mais ils sont facilement trompés. Un algorithme peut facilement trouver la haine dans : « À mort les Juifs ». Mais il se fera complètement avoir dès lors qu’il y a de l’ironie ou du sarcasme. Un message comme « Sibeth est une championne, ses gênes lui ont donné un cerveau hors norme » trompera les algorithmes, la subtilité du langage dépasse celle des machines.

## Plus que la langue, les images.
Un message avec « Je n’aime pas la noire » accompagné d’une photo complique encore un peu plus l’exercice. Dans ce cas, le sens du message dépend exclusivement de la photo. Cela peut être une femme noire, donc raciste ou deux paires de chaussures dont une noire. Qu’à cela ne tienne, une communauté pourra s’approprier un code. Si la banane devient raciste, comment les algorithmes vont-ils le savoir ?

On peut aussi noyer le poisson, mettre un message doux dans le texte, et montrer du texte dans l’image. Les reconnaissances de texte ne sont pas parfaites. Il suffit de rajouter quelques barres ou d’utiliser une police peu utilisée pour tromper les algorithmes.


Bref, ceux qui espèrent l’arrêt de la haine grâce à cette loi vont vite être déçus. À moins d’interdire très largement tout type de message, rien n’y fera. La peur vient comme toujours des futurs tours de vis. On commence par une petite loi inutile puis à force d’ordonnance on finit en Chine…

À moins que l’arroseur se fasse prendre à son propre jeu.

# Arroseur Arosé
Les censeurs seront donc américains, sans aucun compte à rendre à un juge français. Ils auront carte blanche. On peut se mettre à imaginer la suite. Ou juste regarder l’actualité. Twitter vient de poser l’étiquette « fake new » sur un tweet de Trump. Jack Dorsey vient ainsi de faire passer la plateforme de simple passe-plat à celle de régulateur de la morale.

En France, maintenant, elle pourra faire plus qu’ajouter une étiquette. Elle pourra supprimer, les contenus qu’elle juge haineux. Des manifestants qui comparent Marc Zuckerberg à un dictateur. C’est haineux vous ne trouvez pas ? Des syndicats qui comparent Jeff Besos à un monstre et demandent de fermer les entrepôts. C’est aussi haineux non ? Des politiques qui comparent les GAFAs à des colonisateurs, c’est également haineux ?

Notre chère élite française ouvertement socialiste et anti-capitaliste vient de tendre le bâton aux compagnies les plus néo-libérales du monde. Elles risquent fort de l’utiliser, pas pour favoriser l’inclusion et réduire la haine, mais plus pour avoir le droit de vie ou mort sur chaque politicien français et promouvoir leur idéologie pas très socialiste. Le prochain député voulant réguler les GAFAs risque fort d’être mis au silence.
