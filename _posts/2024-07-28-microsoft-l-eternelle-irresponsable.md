---
title: Bug géant, Microsoft l'éternelle irresponsable
subtitle: Quand une Mise à Jour CrowdStrike Révèle les Failles du Monopole de Microsoft
date: 2024-07-28
layout: post
background: /img/2024/07/chaos.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-4FuQ" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=Bq1dZFJYV93O&v=3&playerId=ausha-4FuQ"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>


Vendredi 19 juillet, un bug mondial a planté des millions d'ordinateurs Windows après une mise à jour. Chose incroyable, la mise à jour ne vient pas de Microsoft, mais d'une entreprise de cybersécurité [CrowdStrike](https://www.lemondeinformatique.fr/actualites/lire-panne-geante-crowdstrike-reconnait-un-manque-de-tests-94375.html).

Avec beaucoup de stupéfaction, des entreprises ont compris leur mortelle dépendance à un acteur monopolistique, capable de mettre à l'arrêt leur business du jour au lendemain. Chaque jour, ils demandent à Microsoft de bien vouloir s'allumer.


Sans la moindre stupéfaction, Microsoft s'est rappelé qu'il détient un monopole avec un produit bancal. Chaque jour, il demande aux parties tierces présentes sur Windows (telle CrowdStrike) de bien vouloir démarrer.


Idéalement, le coeur de l'OS appelé kernel ne doit pas être obstrué par des app tierces. Si le module Bluetooth plante, l'ordinateur reste utilisable. Si un antivirus plante au démarrage, ce n'est pas critique pour l'OS, il doit pouvoir continuer le démarrage.

Windows a été conçu en considérant tout logiciel tiers comme fiable et honnête. Ce bug est la conséquence de ce paradigme initial que Microsoft tente de colmater.

Les virus avaient plein pouvoir avant qu'un système de droit admin demande à l'utilisateur de [confirmer certaines actions](https://en.wikipedia.org/wiki/User_Account_Control) depuis Windows Vista.


Si une seule application plantait, l'écran et l'OS se figeaient avant Windows 7, qui donna la priorité aux actions de l'OS avant celle des apps tierces ([session 0 isolation](https://techcommunity.microsoft.com/t5/ask-the-performance-team/application-compatibility-session-0-isolation/ba-p/372361)).

Windows n'a pas été conçu pour faire tourner le monde. Microsoft le sait, il apporte quelque changement au fils des ans, mais surtout il se dédouane de la mauvaise conception de son OS.

L'entreprise n'hésite pas à pointer du doigt l'Union européenne, qui l'a poussé à ouvrir davantage son OS. Ce n'est pas l'UE qui a codé Windows avec ses failles béantes. On peut très bien être ouvert et fiable comme Linux.

<h2>L'éternelle irresponsable</h2>
Microsoft s'est toujours comportée comme n’étant pas responsable de ses produits ou décisions et avec un certain succès.

À commencer, par sa position de monopole sur les ordinateurs personnels et les suites bureautiques. Apple essuie des défaites juridiques le forçant à proposer des [AppStores alternatifs](https://www.frandroid.com/marques/apple/626984_nous-avons-teste-laltstore-lalternative-a-lapp-store-pour-iphone) sur iPhone. Google doit proposer des [alternatifs au navigateur Google Chrone](https://www.frandroid.com/android/applications/google-apps/583370_fin-de-la-domination-de-chrome-et-google-android-vous-laisse-libre-de-choisir) sur Android.

Microsoft jouit d'une totale impunité. Personne ne semble lui demander d'ouvrir Windows et Office afin que des logiciels Windows ou des documents Offices puissent fonctionner sur des alternatives pleinement compatibles. Même son investissement massif dans OpenIA n’a pas levé le moindre bouclier.

Pourtant les .docx, .xlsx et .pptx pourraient faire l'objet d'un standard ouvert comme c'est le cas du [.pdf](https://fr.wikipedia.org/wiki/Portable_Document_Format) pourtant créé par l'entreprise privée Adobe pour ses besoins.

Avant Windows 10 et son Windows Defender, l'entreprise de Palo Alto avait la réputation d'avoir un produit peu sécurisé. Différentes études montraient qu'un ordinateur Windows neuf, déballé, branché à internet se faisait [pirater en 4 minutes](https://www.lemonde.fr/blog/bugbrother/2009/02/20/la-duree-de-vie-dun-ordinateur-non-protege-est-de-4-minutes/).


Sans parler des nombreuses failles planétaires comme [EternelBlue](https://en.wikipedia.org/wiki/EternalBlue) (2017),  [Double Kill](https://www.journaldunet.com/solutions/dsi/1210225-double-kill-se-democratise-et-passe-d-office-a-explorer/) (2018), [BlueKeep](https://en.wikipedia.org/wiki/BlueKeep) (2019), [SMBGhost](https://en.wikipedia.org/wiki/SMBGhost) (2020), [PrintNightmare](https://en.wikipedia.org/wiki/PrintNightmare) (2021) ou [Follina](https://www.cybermalveillance.gouv.fr/tous-nos-contenus/actualites/alerte-cyber-faille-de-securite-critique-dans-les-produits-windows-et-windows-server-follina) (2022).

Aucun système n'est sûr, mais tous les systèmes n'ont pas des failles critiques utilisables sur des millions d'appareils tous les ans.

Surtout que pour réparer ses failles, il faut mettre à jour rapidement l'OS avec les conséquences que l'on a vu récemment.

Microsoft, c'est aussi un rapport à la vie privée de plus en plus douteux. Windows envoie [par défaut](https://support.microsoft.com/fr-fr/office/les-fichiers-sont-enregistr%C3%A9s-sur-onedrive-par-d%C3%A9faut-dans-windows-33da0077-770c-4bda-b61e-8c8e8ca70ac7) tous vos documents en local dans son cloud.

Dernièrement, l'entreprise a dévoilé sa prochaine fonctionnalité appelée [Recall](https://www.lesnumeriques.com/intelligence-artificielle/windows-recall-un-danger-pour-votre-vie-privee-selon-les-experts-n222508.html), votre écran sera constamment enregistré et analysé par une IA afin de mieux vous connaître.

Tout cela semble loin du RGPD. Le consentement n'est pas formel, puisque les fonctionnalités sont activées par défaut. Les données sont traitées sur des serveurs américains. Encore une fois, Microsoft ne semble inquiétée par personne.


En conclusion, Windows n'a jamais été conçu pour faire tourner des logiciels critiques. Microsoft vous vend son produit, récupère vos données et vous laisse ses problèmes pour lesquels il se déresponsabilise. 


