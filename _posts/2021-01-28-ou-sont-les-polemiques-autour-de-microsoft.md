---
title: Où sont les polémiques autour de Microsoft ?
subtitle: Les GAFA font fassent à des polémiques et des poursuites antitrust, pourquoi Microsoft réussit à y échapper ?
date: 2021-01-28
layout: post
published: fjolain.medium.com/o%C3%B9-sont-les-pol%C3%A9miques-autour-de-microsoft-44824634558c
background: /img/2021/01/microsoft.jpg
categories: [article, podcast]
tags: [microsoft, économie]
---

<iframe frameborder="0" loading="lazy" id="ausha-iCjM" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%233F51B5&dark=true&v=3&playerId=ausha-iCjM"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

L’acronyme G.A.F.A pour Google, Apple, Facebook et Amazon trahit une vision centrée sur le mobile avec des produits comme Android, iPhone, Facebook ou autres. Cette vision écarte Microsoft des polémiques, alors que son emprise est la plus grande et dangereuse de toutes les entreprises tech.

Pour se rendre compte du poids de Microsoft dans l’économie, il suffit de décrire une journée type de Jean, employé de bureau. Jean commence sa journée en démarrant Windows depuis sa Surface Pro. Sa première tâche consiste à dépiler ces mails Outlooks. Un mail lui demande de valider le dernier rapport contenu dans la pièce jointe Words.

Ensuite, il enchaîne avec une réunion sur Teams où on lui présente le PowerPoint sur la stratégie d’entreprise. Jean profite d’une pause pour faire quelques recherches sur Bing avec Edge. Enfin, il termine sa matinée en téléchargeant depuis SharePoint le dernier budget trimestriel sur Excels pour le mettre à jour.

Où sont Google, Apple, Facebook ou Amazon ? Il n’y a que des produits Microsoft en entreprise.

Microsoft est tellement omniprésente qu’on finit par ne plus le voir. Absent des GAFA, alors qu’il mérite la première place en « MAGAF ». Il détient le monopole du PC depuis toujours avec Windows, dont il force l’installation par défaut. Beaucoup de logiciels ne fonctionnent même que sur Windows rendant toute migration vers la concurrence impossible. 

Il détient aussi le monopole des documents avec Office, un quasi standard. Toutes les entreprises ont leurs savoirs bloqués dans des .docx, .xlsx, .pptx. Là encore, toute migration est impossible sous peine de perdre la mise en page ou des fonctionnalités.

Microsoft est aussi présent sur le web avec Outlook, Internet Explorer, Edge, Bing ou Azur, son offre cloud qui [héberge actuellement les données médicales des français](https://www.lesechos.fr/tech-medias/hightech/letat-choisit-microsoft-pour-les-donnees-de-sante-et-cree-la-polemique-1208376).

En plus d’éviter les polémiques, Microsoft arrive même à [esquiver les poursuites antitrust actuelles](https://www.lesechos.fr/tech-medias/hightech/antitrust-le-congres-americain-propose-de-limiter-radicalement-le-pouvoir-des-gafa-1252447), alors qu’il détient le monopole des PC et des formats de documents.

Il est donc temps de changer GAFA par MAGAF et de se pencher sur les monopoles de Microsoft.
