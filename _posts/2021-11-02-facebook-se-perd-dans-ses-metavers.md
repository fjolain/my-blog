---
title: Facebook se perd dans ses métavers
subtitle: Avec les métavers Facebook tente de faire du neuf avec de l'ancien et surtout de faire oublier ses déboires
date: 2021-11-02
layout: post
background: /img/2021/11/minecraft.png
categories: [article, podcast]
published: www.contrepoints.org/2021/11/05/410817-facebook-se-perd-dans-ses-metavers
tags: [société]
---

<iframe frameborder="0" loading="lazy" id="ausha-zxPw" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=bz247tY5jRxe&v=3&playerId=ausha-zxPw"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Facebook se lance dans les métavers, un projet flou qui s'annonce plus marketing qu'utile. Quand on retire les annonces grandiloquentes, le projet laisse deux grandes interrogations.

<h2>Facebook n'a jamais réussi à quitter les réseaux sociaux</h2>

Marc Zuckerberg excelle dans les réseaux sociaux. Bien évidement avec Facebook, premier réseau au monde avec [2.8 milliards](https://www.journaldunet.com/ebusiness/le-net/1125265-nombre-d-utilisateurs-de-facebook-dans-le-monde/) d'utilisateurs. Il réitéra l'exploit en misant sur Instagram pour en faire le deuxième au monde avec [1,3 milliard](https://blog.digimind.com/fr/agences/instagram-chiffres-incontournables-2020-france-et-monde) d'utilisateurs. Le tout en envoyant ses adversaires au tapis tel SnapChat, Google+, Tumblr ou ClubHouse. Un nouveau combat s'ouvre maintenant avec TikTok.

Il eut aussi le flair de séparer la messagerie de Facebook pour en faire un service appart, ce qui deviendra la deuxième messagerie au monde avec [1,3 milliard](https://fr.statista.com/infographie/10326/utilisateurs-actifs-mensuels-applis-messagerie/) d'utilisateurs. Récidivant une nouvelle fois en misant sur Whatsapp pour en faire la première messagerie au monde avec [1,5 milliard](https://fr.statista.com/infographie/10326/utilisateurs-actifs-mensuels-applis-messagerie/) d'utilisateurs.

Marc Zuckerberg règne en maître sur le royaume des réseaux sociaux dont sa réussite n'est plus à démontrer. Pour tout le reste, c'est une autre histoire. Toutes les tentatives de s'étendre à un autre domaine furent un échec.

On peut commencer avec [Facebook Home](https://en.wikipedia.org/wiki/Facebook_Home), une surcouche d'Android 100% Facebook, présentée en 2013 et aussitôt retirée.

Ensuite en 2014, Facebook misa sur la réalité virtuelle (VR) en [rachetant Occulus Rift](https://en.wikipedia.org/wiki/Oculus_Rift). Cela ne déboucha sur aucune synergie ni avancée dans le domaine. Au contraire, Facebook créa un grave agacement des joueurs qui devaient se créer un compte Facebook pour continuer à utiliser leur produit.

Ensuite, Facebook misa sur l'IA en [2015 en installant un centre à Paris](https://fr.wikipedia.org/wiki/Facebook_Artificial_Intelligence_Research) avec à sa tête Yann LeCun, une pointure du domaine. Contrairement à Google avec ses exploits comme AlphaGo ou ses produits comme Google Home, ou Apple avec ses Neuronal Engines; Facebook ne tire toujours aucune avance concurrentielle de ses investissements en IA.

Facebook lança en 2019 un écran connecté avec caméra, Facebook Portal. Sa part de marché est très loin derrière les objets équivalents de chez Amazon ou Xiaomi.

La dernière tentative fut le plus gros échec avec sa cryptomonnaie Libra en [2019](https://fr.wikipedia.org/wiki/Facebook). Un projet mort-né qui eut le mérite d'énerver les états devant la fin prochaine de leur monopole monétaire.

Facebook n'a pas attendu Libra pour se mettre à dos les législateurs. [Il enchaîne les scandales](https://fr.wikipedia.org/wiki/Facebook#Affaire_Cambridge_Analytica) : Cambridge Analytica, faille de sécurité, révélations d'employées, panne géante, etc.

Voici donc le contexte dans lequel Facebook essaye d'imposer son métavers. Reste encore à regarder l'idée de près.

<h2>Les métavers sont aussi vieux qu'internet</h2>

Un métavers est selon Wikipédia [*un monde virtuel fictif*](https://fr.wikipedia.org/wiki/M%C3%A9tavers). Dans ce cas, n'importe quel jeu multijoueurs avec une forte communauté devient un métavers et il en existe déjà plusieurs :

- [World of Warcraft](https://fr.wikipedia.org/wiki/World_of_Warcraft) sorti en 1994 avec un pic à 12 millions de joueurs.

- [Dofus](https://fr.wikipedia.org/wiki/Dofus) sorti en 2004 avec 10 millions de joueurs.

- [Minecreaft](https://fr.wikipedia.org/wiki/Minecraft) créé en 2009 avec 126 millions de joueurs.

- [Fornite](https://fr.wikipedia.org/wiki/Fortnite) sorti en 2017 avec 250 millions de joueurs.

On peut aussi citer [Second Life](https://fr.wikipedia.org/wiki/Second_Life) créé en 1999, qui visait clairement la création d'une cyber-société.

Le métavers de Facebook semble donc une idée réchauffée, que l'entreprise tente de présenter comme originale.

Cela ressemble à un coup de poker pour faire diversion sur ses déboires. Facebook espère vaincre la malédiction, en visant suffisamment proche de son marché des réseaux sociaux, tout en pouvant y rattacher ses loupés en VR, IA ou blockchain au fur et à mesure.

Reste à se demander ce qui plaît le plus à Marc Zuckerberg, accélérer notre société vers la Matrice pour en devenir le maître. Ou se replier dans un monde imaginaire dans lequel il n'aura plus à répondre de ses actes ?


