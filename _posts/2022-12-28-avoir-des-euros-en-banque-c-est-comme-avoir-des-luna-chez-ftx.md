---
title: Avoir des euros en banques c'est comme avoir des Luna chez FTX.
subtitle: Les derniers scandales cryptos ne font que reproduire la finance classique.
date: 2022-12-28
layout: post
background: /img/2022/12/banque.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe frameborder="0" loading="lazy" id="ausha-5nDj" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=oKQ8VuR06jga&v=3&playerId=ausha-5nDj"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Après le Covid et l'Ukraine, les honnêtes gens sont priés de se scandaliser pour les déboires de la crypto comme Luna ou FTX. Il faut terroriser pour mieux réguler afin bien sûr que les honnêtes gens retrouvent le sommeil.

Mais qu'est-ce qu'on reproche à ces scandales ? D'avoir créé une monnaie soi-disant stable, mais particulièrement fragile ? D'avoir joué avec les dépôts des particuliers ?

Ne serait-ce pas hypocrite de condamner la crypto alors que ses dérives proviennent de la finance classique ?

<h2>Affaire Luna</h2>

[Terra](https://en.wikipedia.org/wiki/Terra_(blockchain)) est un des nombreux projets qui voulait créer une blockchain 2.0 mieux que Bitcoin. Pour ce faire cette nouvelle blockchain était conçue autour de son token principal le Luna et token stabilité à 1$, le TerraUSD. Ce token devait toujours valoir 1$, car si le prix monte, ils s'en créent plus, pour baisser le prix. S'il descend, la blockchain utilise le Luna pour acheter le TerraUSD puis le détruire. Ce fonctionnement est détaillé dans [un précédent article](https://www.codable.tv/les-dessous-de-la-chute-de-terra/).

Ce mécanisme a bien fonctionné durant plusieurs années, ce qui a favorisé le développement de tout un écosystème sur la blockchain Terra.
Puis le marché crypto a chuté et avec lui le Luna. Les personnes voulaient revendre en masse du Luna et du TerraUSD, l'algorithme ne parvient plus à stabiliser le prix du TerraUSD, la confiance des utilisateurs disparue, les liquidations massives envoyèrent tout le projet dans les abysses.

En résumé, le mécanisme de stabilité du TerraUSD n'était qu'un leurre, car il repose exclusivement sur la confiance du Luna et de tout le projet Terra.

Terra est juste le dernier échec d'une très longue série de fausse stabilité qui reposait sur une confiance des utilisateurs dans le système. Dès que les utilisateurs ont douté de la promesse, la monnaie s'est effondrée.

Le mark papier de 1914 devait toujours valoir un mark or. En 1923, il fallait [mille milliards marks papier](https://fr.wikipedia.org/wiki/Hyperinflation_de_la_r%C3%A9publique_de_Weimar) pour un mark or. En 1945, les États-Unis ont promis à [Brettenwood](https://fr.wikipedia.org/wiki/Accords_de_Bretton_Woods) que 35$ vaudrait toujours une once d'or. En 1971, ils mirent fin à leur promesse. Aujourd'hui une once d'or coûte 1800$.

En 1992, les pays européens devaient stabiliser leurs monnaies entre eux. La banque d'Angleterre s'est retrouvée en manque de liquidité pour stabiliser la livre. George Soros a profité de cette faiblesse pour mener [une attaque](https://fr.wikipedia.org/wiki/Crise_de_la_livre_sterling_de_1992), la livre sterling a dégringolé.

Terra a répliqué la promesse de tout gouvernement: "Ma monnaie ne va jamais s'écrouler, vous pouvez avoir confiance". Or c'est toujours l'inverse, c'est la confiance des citoyens qui donne une stabilité à la monnaie. Et comme beaucoup de monnaies sont créées n’importe comment, la confiance et la stabilité sont en équilibre précaire.

<h2>Affaire FTX</h2>

[FTX](https://fr.wikipedia.org/wiki/FTX_(entreprise)) était une plateforme d'échange crypto. Des particuliers déposaient des liquidités pour acheter ou vendre diverses cryptomonnaies. FTX a utilisé les dépôts de ses clients pour investir sur des paris douteux. La rumeur sur de tels investissements s'est propagée, tout le monde a voulu retirer son argent. Or FTX avait bien perdu de l'argent de ses clients, il s'est retrouvé insolvable.

Le fait qu'à un moment t, FTX n'avait plus la totalité des dépôts de ses clients, est une situation courante dans les banques.

Prenons l'exemple des investissements "sûrs", "garantis en capital" comme le livret A ou l'assurance vie compte en euro. Ces placements contiennent avant tout [des obligations d'états notamment français](https://fr.wikipedia.org/wiki/Livret_A#Collecte_et_utilisation_des_fonds_issus_du_livret_A). Le marché obligataire a connu une forte baisse de [20%](https://fr.investing.com/analysis/le-marche-obligataire-commencetil-a-prevoir-la-fin-de-la-hausse-des-taux-de-la-fed--200443928) ces derniers mois. Les obligations se revendaient en dessous de leur prix d'achat. Les banques pouvaient-elles rembourser tous les dépôts des Français ?

Malheureusement pour ceux qui préfèrent le simple compte courant, leurs dépôts ne sont pas non plus sûrs. La séparation des banques de dépôts et des banques d'affaires est dans le flou depuis 1984 et la loi [84-46](https://fr.wikipedia.org/wiki/Loi_relative_%C3%A0_l%27activit%C3%A9_et_au_contr%C3%B4le_des_%C3%A9tablissements_de_cr%C3%A9dit) pour "moderniser" la finance. On ne sait donc pas vraiment où est l'argent de votre compte courant.

De plus, il ne faut pas oublier que les banques de dépôt émettent des crédits avec le principe de [réserve fractionnaire](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_r%C3%A9serves_fractionnaires) qui leur permettent d'accorder plus de crédit que de dépôts. Habituellement ce ratio est de 8%, cela signifie que si 8% des prêts ne sont pas remboursés, alors les clients de la banque perdent tous leurs dépôts.

Enfin pour ceux qui on entendu parler d'une garantit de 100,000€ par établissement bancaire, sachez que le fond de garanti est doté de [6 milliards d'euros](https://www.garantiedesdepots.fr/sites/default/files/2022-04/FGDR-Rapport_Annuel_2021_3.pdf) soit 90€ par français.

FTX comme toutes les banques jouait avec l'argent de ses clients et espère que ses clients ne viendront pas retirer leur argent en même temps.

Je ne voudrais pas insinuer que nos vénérables banques ne sont pas solvables. Avec ses produits dérivés obscurs, ces effets de levier, ces réserves fractionnaires, ses assurances et réassurances, je n'en sais rien et  je doute qu'elles-mêmes  le sachent.


En résumé, la monnaie Luna reposait sur les mêmes principes que les monnaies fiat, FTX gérait  l'argent aussi bien que les banques.


