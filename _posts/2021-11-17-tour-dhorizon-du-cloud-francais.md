---
title: Tour d'horizon du cloud français
subtitle: OVH, innovation, échec étatique. Le cloud à la française
date: 2021-11-17
layout: post
background: /img/2021/11/france.jpeg
categories: [article, podcast]
published: www.contrepoints.org/2021/11/21/414499-ovh-innovation-echec-etatique-le-cloud-a-la-francaise
tags: [société, cloud, économie]
---

<iframe frameborder="0" loading="lazy" id="ausha-wiYl" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=b7NXvSQvYRjA&v=3&playerId=ausha-wiYl"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Avec l'introduction d'OVH en bourse le 15 octobre dernier. Il est temps de regarder l'écosystème du cloud en France. Nous verrons qu'OVH n'est pas seul. Le cloud français, même derrière les États-Unis, reste bien placé en Europe.

<h2>L'histoire d'OVH reflète l'histoire d'internet</h2>

OVH est devenu le leader du cloud en Europe. C'est une success-story à la française. Un jeune entrepreneur, Octabe Klaba décide de se lancer en 1999 dans le cloud. Il créa une société à Roubaix devenue multinationale et pionnière dans son domaine.

Car oui, dans l'internet naissant, OVH faisait figure de leader. Plutôt que d'installer son serveur chez soi, OVH propose de nous louer un des siens, dont le nom de l'entreprise : On Vous Héberge (OVH). Ainsi des milliers de particuliers et professionnels purent héberger leur boîte mail ou site web facilement.

OVH prospéra avec l'hébergement des blogs ou des sites e-commerce. Le besoin se résume alors à louer pour plusieurs mois, voire années, un même serveur pour héberger le site web.

Malheureusement pour OVH, internet évolua rapidement avec l'arrivée d'Amazon en [2006](https://fr.wikipedia.org/wiki/Amazon_Web_Services) et son offre révolutionnaire de [cloud élastique](https://fr.wikipedia.org/wiki/%C3%89lasticit%C3%A9_(cloud_computing)). On pouvait ainsi louer des serveurs à la minute, les allumer et les éteindre en quelques secondes. Cette souplesse devient fondamentale pour les plateformes cloud comme Netflix. Leur infrastructure s'adapte en continu à la charge de travail.

Quelques années après, la flexibilité s'améliora encore avec le [*serverless*](https://fr.wikipedia.org/wiki/Informatique_sans_serveur). Dans ce cas, le site sera automatiquement déployé sans avoir à configurer le moindre serveur. Avec le serverless, le serveur disparaît de la vue du développeur, il dépose son code et comme par magie, l'hébergeur lui donne une URL pour accéder à la plateforme déployée.

Cette double révolution laissa les acteurs établis comme OVH sur le carreau. Amazon, Microsoft et Google furent les grands gagnants. Aujourd'hui, bien qu'OVH ai remonté la pente, le mal est fait. Une bonne partie du CAC40 a migré ver les GAFA pour leurs besoins cloud.

<h2>Le cloud français ne se résume pas qu'à OVH</h2>

Par sa taille et sa couverture médiatique (surtout durant les pannes), OVH est l'arbre qui cache la forêt du cloud français. Petit tour d'horizon de différents acteurs.

[Scaleway](https://fr.wikipedia.org/wiki/Scaleway) est un *petit* OVH. Sa taille le laissa dans l'ombre, mais lui permit de s'adapter plus rapidement aux révolutions et ainsi gagner en part de marché.

[Outscale](https://fr.wikipedia.org/wiki/Outscale) est une filliale de Dassaut Système lancée en 2010 suite aux révélations de Snowden. Ce cloud souverain est à destination des acteurs critiques. Il est certifié *SecNumCloud* pour sa sûreté par l'Agence Nationale de la Sécurité des Systèmes d'Information (ANSSI).

[Qarnot](https://qarnot.com/) propose un cloud atypique puisque ses serveurs prennent la forme de radiateur qu'on installe dans des logements. Ainsi la chaleur dégagée par le calcul sert à se chauffer.

Même Naval Group vient de mettre un pied dans le cloud. L'entreprise a conçu [le premier cloud immergé au monde](https://www.naval-group.com/fr/projet-natick-phase-2-microsoft-et-naval-group-un-premier-bilan-concluant-pour-le-datacenter) en partenariat avec Microsoft.

Il faut aussi rappeler les poids lourds du développement et de l'infogérance tel Atos, Sopra ou Capgemini.

<h2>L'état français toujours fidèle à lui-même</h2>

Pour ceux qui ont encore un doute sur la compétence de l'état en dehors du régalien, le cloud français est un sketch.

Le gouvernement commença promptement par saboter l'écosystème avec une lourde réglementation. Par exemple, en 2000, l'hébergeur [Altern](https://fr.wikipedia.org/wiki/Altern) dû fermer, car la loi française l'obligeait à contrôler les 21 893 sites hébergés chez lui. On assista donc au départ des entrepreneurs du numérique vers la Californie. La goutte d'eau (fiscale) arriva en 2012 sous Hollande et provoqua le [Mouvement des Pigeons](https://fr.wikipedia.org/wiki/Mouvement_des_Pigeons).

Par une sorte de miracle, OVH, un champion national arriva tout de même à s'en sortir. Le gouvernement qui visiblement ne voulait pas de cloud chez lui fit preuve d'une totale indifférence envers la pépite française.

Par exemple en 2011, lorsque l'état lança le projet [Andromède](https://fr.wikipedia.org/wiki/Androm%C3%A8de_(cloud)) avec pour objectif de bâtir un cloud souverain. Plutôt que de consolider le champion, l'état décida de lui créer deux concurrents : CloudWatt (Orange & Thales) et Numergy (SFR & Bull). Tous les deux après [285 millions d'euros investis](https://www.lemondeinformatique.fr/actualites/lire-285-millions-d-euros-pour-andromede-le-cloud-souverain-francais-41990.html#:~:text=285%20millions%20d'euros%20pour,souverain%20fran%C3%A7ais%20%2D%20Le%20Monde%20Informatique) mirent la clé sous la porte en quelques années.

Cette indifférence à son propre écosystème reste encore présente, puisque l'état a choisi Microsoft pour héberger les données de santé des français ([Health Data Hub](https://fr.wikipedia.org/wiki/Dossier_m%C3%A9dical_partag%C3%A9#Le_Health_Data_Hub)).


Le cloud français est donc bien présent avec sa *french touch*. Il regroupe des poids lourds et des entreprises innovantes. L'état les souteint comme la corde soutient le pendu.
