---
title: Qui est Ledger, la dernière licorne française ?
subtitle: Ledger est une entreprise qui perpétue l'excellence française dans la sécurité, mais que vend t-elle pour valoir 1,5 milliards de $
date: 2021-06-12
layout: post
background: /img/2021/06/ledger.jpeg
published: www.contrepoints.org/2021/06/18/399820-qui-est-ledger-la-derniere-licorne-francaise
categories: [article, podcast]
tags: [bitcoin]
---

<iframe frameborder="0" loading="lazy" id="ausha-N680" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=y81R8F1O2NzE&v=3&playerId=ausha-N680"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Quand on possède des Bitcoins ou autres cryptomonnaies, avoir un Ledger est une étape indispensable. L'entreprise commercialise des produits pour  sécuriser les cryptoactifs. L'invention de Ledger et des cryptomonnaies ont permis la réalisation du meilleur moyen de paiement que l'humanité ait connu, bien supérieur aux billets ou cartes bancaires. Mais pourquoi donc ? On verra ensemble les différents types de paiements et l'excellence française dans ce domaine.



<h1>Paiement par monnaie physique</h1>

Avec le paiement en physique, le principal problème reste de garantir la véracité de la monnaie. Comment être sûr que c'est le bon métal et non un alliage moins cher ? Comment vérifier un billet ou une pièce ? Le faux monnayage, même en le condamnant très fermement ([30 ans, 450 000 € d'amende](https://www.legifrance.gouv.fr/codes/id/LEGISCTA000006149855/#:~:text=La%20contrefa%C3%A7on%20ou%20la%20falsification,450%20000%20euros%20d'amende.)) représente [700 000](https://fr.wikipedia.org/wiki/Contrefa%C3%A7on_des_billets_de_banque_en_euros) billets en circulation en Europe.

La palme du paiement physique le moins sécurisé revient aux chèques, où on laisse à l'usager le soin de fabriquer son propre billet sans la moindre garantie de provision.

**Avec les paiements physiques, la fraude vient du client aux dépens du marchand. La monnaie peut être contrefaite.**


<h1>Paiement par monnaies numériques</h1>

Avec les monnaies numériques, on change de paradigme. L'argent reste sur le compte et les échanges se font entre banques. Il n'y a plus possibilité de fausses monnaies pour le marchand. À la place, le client a un *identifiant* secret qui permet de s'authentifier à sa banque et d'envoyer un paiement. De là, deux écoles existent:

<h2>Excellence Française</h2>

Les ingénieurs français [Roland Morena](https://fr.wikipedia.org/wiki/Roland_Moreno) en 1974 puis [Michel Urgon](https://fr.wikipedia.org/wiki/Roland_Moreno) en 1977 mirent au point la carte à puce qui permet de conserver des secrets. De cette innovation française naquirent les cartes bancaires, badges ou cartes SIM. Ainsi que deux leaders industriels : [Gemalto](https://fr.wikipedia.org/wiki/Gemalto) et [Oberthur Tehcnologie](https://fr.wikipedia.org/wiki/Oberthur_Technologies).

Avec une carte à puce, l'identifiant (le numéro de carte dans le cas des CB) est conservé chiffré dans la puce et seule la saisie du code PIN permet de le lire. Avec le système français, l'identifiant est sécurisé. Le client doit néanmoins rester vigilant où il insère sa carte. Un terminal de paiement frauduleux pourrait lui voler son identifiant.


<h2>Médiocrité Américaine</h2>

Nos cartes bancaires françaises pourraient être des objets sécurisés, si elles n'avaient pas dû se soumettre au standard américain.
Avec cette école, le fameux identifiant *secret* qui permet de dépenser tout l'argent de son propriétaire n'est pas protégé, bien au contraire...

- Il est codé en clair dans la bande magnétique. C'est ainsi qu'en 2021, l'Amérique signe encore ses tickets de caisse comme seul moyen d'authentification.

- Il est accessible en sans contact. Ainsi n'importe quelle personne peut [se coller à vous dans le métro et voler votre numéro de CB](https://www.capital.fr/economie-politique/montpellier-larnaque-au-paiement-sans-contact-sevit-dans-le-tram-1403451).

- Et enfin, il est écrit en toutes lettres sur la carte elle-même, avec obligation de donner l'identifiant à chaque commerçant en ligne. En espérant que le commerçant ne le conserve pas, ne le revende pas et ne prélève que le bon montant.

Rien qu'en France, un tel manque de sécurité provoque plus de [500 millions d'euros](https://www.banque-france.fr/sites/default/files/medias/documents/osmp2016_web.pdf) de fraude par an.

**Avec les paiements numériques, la fraude vient du marchand aux dépens du client. L'identifiant *secret* est en fait partagé à chaque transaction.**

<h1>Paiment par cryptomonnaies</h1>

Les cryptomonnaies reprennent l'idée des cartes bancaires. Vous ne détenez pas des bitcoins dans vos mains. Les bitcoins restent sur le registre (la blockchain). Vous possédez un identifiant secret qui permet de s'authentifier sur le registre.

Mais les cryptomonnaies vont plus loin. Lors d'un paiement, vous ne devez plus divulguer votre identifiant au marchand. Non, l'identifiant reste secret, il va vous permettre de créer un *laissez-passer*. Ce laissez-passer stipule que le marchand X a le droit de prendre Y Bitcoins sur votre compte.

**Dans un paiement par cryptomonnaie aucune donnée sensible n'est échangée entre le marchand et le client. Néanmoins, le client doit protéger son identifiant pour éviter la fraude.**

<h2>Ledger</h2>

Les fondateurs de Ledger viennent de l'industrie française des cartes à puce. Ils ont vu dans le marché émergeant des cryptomonnaies, le besoin pour les utilisateurs de protéger leurs identifiants. Ils ont donc conçu et vendu plus de [3 millions](https://www.ledger.com/ledger-raises-380-million-to-make-digital-assets-more-secure-and-accessible-to-everyone) de *hardware wallet*, comme celui-ci.

![ledger nano X](/img/2019/12/ledger.webp)

L'identifiant du client est stocké chiffré dedans. Et, sur demande, le dispositif est capable de créer des laissez-passer. L'identifiant ne quitte jamais le dispositif, il n'est jamais divulgué.

Le duo Ledger + cryptomonnaies offre ainsi des paiements d'une sécurité exceptionnelle. Le marchand a une garantie sur la monnaie grâce à la blockchain ([cf. article blockchain & rareté](https://www.contrepoints.org/2021/05/19/397723-comment-la-blockchain-a-profondement-transforme-la-rarete)). Le client n'a plus à partager son identifiant secret. Son Ledger fait office de coffre-fort sécurisé pour ses cryptoactifs.

Le savoir-faire français de Ledger a permis de se valoriser récemment à 1,5 milliard de dollars et de perdurer l'excellence française dans les systèmes de sécurité embarqués.

