---
title: Comment protéger les données des mineurs sur internet
subtitle: Les mineurs sont une cible (trop) facile dans la collecte de données
date: 2023-07-12
layout: post
background: /img/2023/07/children.png
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

Dans cet article, nous examinerons les mesures que vous pouvez prendre pour protéger les données de vos enfants et garantir leur sécurité en ligne.

<h2>Sensibiliser au risque</h2>

Les données alimentent notre traçage sur internet et parfois des trafics douteux. L’IA permet maintenant de se faire passer pour n’importe qui avec une simple photo ou un extrait de voix ([deepfake](https://fr.wikipedia.org/wiki/Deepfake)).

De plus, aucune donnée sur internet n’est confidentielle. Premièrement, car même les données « privées » sont revendues ([Facebook a revendu des millions de données à Cambridge Analytica](https://fr.wikipedia.org/wiki/Cambridge_Analytica)), mais aussi, car elles peuvent être piratées et ainsi finir accessibles à tous ([piratage de données de santé](https://www.rts.ch/info/regions/neuchatel/12981283-des-milliers-de-donnees-medicales-neuchateloises-publiees-sur-le-darkweb.html)).

Bref internet rend toutes vos données publiques et éternelles. Or chaque donnée peut vous porter préjudice pour plus tard.

Il faut donc faire preuve de parcimonie et éduquer nos enfants à mettre le minimum de données sur internet.

Surtout que les enfants ne sont pas les seuls à blâmer, beaucoup de parents n’hésitent plus à submerger TikTok, Instagram ou Facebook des photos de leurs chérubins. Les mauvaises pratiques sont souvent héréditaires !

<h2>Privilégier les plateformes destinées aux enfants</h2>

La loi est très claire : pas de collecte de données sans l’aval des parents [avant l’âge de 15 ans](https://www.cnil.fr/fr/recommandation-4-rechercher-le-consentement-dun-parent-pour-les-mineurs-de-moins-de-15-ans). Pourtant, elle est rarement respectée, car les entreprises ne peuvent vérifier l’âge en ligne.

Aussi des plateformes dédiées aux enfants sont apparues et respectent cette loi. Il existe [Youtube Kids](https://www.youtubekids.com/?hl=fr) ou encore [Kiddle](https://www.kiddle.co/) un moteur de recherche dédié aux enfants.

<h2>Filtrer le contenu</h2>

En soi, le téléphone est presque une chance pour les parents. Contrairement à l’ordinateur, le smartphone est un appareil très contrôlable. On peut parfaitement interdire des applications. Ainsi qu’interdire l’usage de l’appareil photo pour certaines apps, comme TikTok ou Instagram pour éviter que les photos de nos enfants se retrouvent en accès libre. Ou même définir un temps d’utilisation pour chaque app.

Que ce soit [Android](https://fr.wikihow.com/configurer-le-contr%C3%B4le-parental-sur-Android) ou [iPhone](https://support.apple.com/fr-fr/HT201304), il est possible d’imposer un contrôle strict.

Le filtrage peut aussi se faire à l’aide d’un part-feu. Celui-ci va filtrer l’accès aux sites internet. Il faut le mettre directement sur le téléphone. Ainsi il va filtrer aussi bien le contenu des pages web que celui des apps, en venant du wifi ou de la 4G. Il en existe pour [Android](https://play.google.com/store/search?q=firewall&c=apps&hl=fr&gl=US) et [iPhone](https://lockdownprivacy.com/).

Pour bannir TikTok, il faut filtrer les sites web : tiktokv.com, tiktok.com, tiktokcdn.com, tiktokd.org, pstatp.com,
Pour bannir Instagram, il faut filtrer les sites web : cdninstagram.com, ig.me, instagram.com, facebook.com

<h2>Changer de smartphone</h2>

De nouveaux types de téléphones ont été conçus pour les pays émergents qui n’ont ni le haut débit ni l’argent pour un smartphone. Ils fonctionnent sur [KaiOS](https://www.kaiostech.com/), un OS moins puissant qu'Android ou iPhone, mais plus fonctionnel qu’un Nokia 3310.

Sur ces téléphones, on a la 4G, le Wifi, le GPS, WhatsApp, Youtube sans Tiktok, ni Instagram ou Fornite, le tout dans un format 3310.

Les fonctionnalités indispensables d’un smartphone sans avoir l’addiction.

