---
title: Que contient le QRCode du pass sanitaire ?
subtitle: Pleins de choses, mais pas votre anonymat
date: 2021-08-01
layout: post
background: /img/2021/08/green.jpeg
published: www.contrepoints.org/2021/08/07/402940-que-contient-le-qr-code-du-pass-sanitaire
categories: [article, podcast]
tags: [vie privée]
---

<iframe frameborder="0" loading="lazy" id="ausha-RovW" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=oagXMsNO54RO&v=3&playerId=ausha-RovW"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Bien que les dernières recherches montrent [la perte d'efficacité du vaccin sur le variant delta](https://www.lesechos.fr/economie-france/social/le-variant-delta-joue-avec-les-nerfs-des-autorites-sanitaires-1336139). La vaccination quasi obligatoire risque de se maintenir dans le plus pur style [lyssenkisme](https://fr.wikipedia.org/wiki/Lyssenkisme)
où la science doit se conformer à l'idéologie gouvernementale. Ainsi le QRCode européen se généralise, il est
temps de l'analyser plus en détail.

<h1>D'où vient le QRCode ?</h1>
L'histoire a sa part d'ironie. L'invention du QRCode date de [1994](https://fr.wikipedia.org/wiki/Code_QR). Il servait à Toyota pour traquer ses pièces dans son usine. Avec l'aide du smartphone et d'une bonne dose de peur, le voilà sorti de l'usine, toujours dans le but de traquer.

Cela pourrait faire sourire, mais ce sont d’honnêtes citoyens que l'on traque selon leur niveau de conformité comme de vulgaires pièces détachées.

<h1>Que contient le QRCode européen?</h1>

Le QRCode européen regorge d'informations personnelles accessibles librement par un simple scan ([liste complète](https://ec.europa.eu/health/sites/default/files/ehealth/docs/covid-certificate_json_specification_en.pdf)).

<h2>Identitée civile</h2>

On retrouve premièrement notre identité civile en toutes lettres. Le QRCode européen partage notre nom, prénom et notre date de naissance. Il n'y a donc pas un semblant d'anonymat avec ce nouvel outil. On remarque le changement de mentalité de nos politiciens depuis un an.

Les premiers outils, tel le traçage par Bluetooth, ne divulguaient rien de l'identité du porteur. De même pour le QRCode disposé à l'entrée des restaurants, bars et salles de sport. Le gouvernement mentionnait que ["Aucune donnée nominative n'est collectée."](https://qrcode.tousanticovid.gouv.fr/) ainsi que ["Le QR Code correspond à un identifiant crypté, il ne permet de retrouver ni le nom, ni l’adresse du lieu, ni l'identité du client."](https://qrcode.tousanticovid.gouv.fr/).

Avec ce nouveau QRCode nominatif, nous entrons un peu plus sous une surveillance à la chinoise. En fait, il ne reste plus qu'un pas : que le gouvernement rapatrier les résultats des scans sur son serveur central. Actuellement, le résultat du scan est juste affiché à l'écran du scanneur. Mais si dans les prochaines mises à jour, l'état décide d'envoyer le résultat sur son serveur, il sera en mesure de traquer tous ces citoyens.

<h2>État de santé</h2>

On pourrait croire que le pass sanitaire est un simple laissez-passer avec une date d'expiration. Mais ce n'est pas ce qu'ont voulu les technocrates européens, ils ont préféré mettre toutes vos données de santés relatives au Covid :

- êtes-vous vacciné, testé ou guéri du covid
- date du vaccin, du test ou de la guérison
- nombres de doses reçues
- fabriquant du vaccin
- centre de vaccination
- etc.

On peut aisément interpréter ses données à votre insu. Si vous avez 3 doses, vous êtes forcément immunodépressif. De même si votre date de vaccination est parmi les premières, vous êtes un patient à risques.

<h1>Qui peut lire toutes ces données ?</h1>
Les données sont écrites en clair dans le QRCode sans aucun chiffrement. Ces données seront lues par tous les restaurants, agents SNCF ou votre employeur.

L'état qui [n'oblige pas](https://www.service-public.fr/particuliers/vosdroits/F1144) une femme à déclarer sa grossesse à son employeur, oblige maintenant toute la population a communiquer plusieurs fois par jour ses données de santé à n'importe qui.

Pour ce manque total d'anonymat et le partage sans consentement de données médical, La Quadrature Du Net a demandé un référé au Conseil d'État. Celui-ci l’[a rejeté](https://www.laquadrature.net/2021/07/06/passe-sanitaire-le-conseil-detat-valide-la-violation-de-la-loi/).

**Si c'est en clair pourquoi quand je le scan je trouve rien ?**

Alors oui, les données ne sont pas chiffrées, mais le format est très particulier et ne peut pas être lu par une application de scan classique. Aussi pour ceux qui souhaite regarder de plus près, je mets en ligne un site [https://greenpass.codable.tv](https://greenpass.codable.tv) pour décoder le QRCode.


<h1>Peut-on falsifier un QRCode ?</h1>

Avec le QRCode présent sur les attestations de sortie, il n'avait aucune sécurité, n'importe qui pouvait créer un QRCode conforme. Avec le QRCode européen, ceci est impossible, car il est sécurisé par une signature numérique.

En cryptographie, on peut "signer" et "vérifier" un document. Cela permet de garantir qu'une personne ou un organisme a créé ce document et que personne ne l'a modifié. L'organisme possède une clé numérique secrète pour signer le document, ensuite une autre clé publique accessible à tous permet de vérifier la signature.

Le QRCode européen utilise ce mécanisme, personne ne peut créer un QRCode conforme sans avoir la clé secrète de signature.  Ensuite n'importe qui peut vérifier en utilisant la clé de vérification présente [ici](https://verifier-api.coronacheck.nl/v4/verifier/public_keys).
