---
title: S'extraire du smartphone, le cas Morphée
subtitle: Pourquoi est-il nécessaire de s'extraire du tout smartphone pour réussir
date: 2020-10-07
layout: post
published: fjolain.medium.com/sextraire-du-smartphone-pour-r%C3%A9ussir-le-cas-morph%C3%A9e-7826e76a9cdc
background: /img/2020/10/morphee.jpeg
categories: [article, podcast]
tags: [smartphone, économie]
---

<iframe frameborder="0" loading="lazy" id="ausha-eESh" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%233F51B5&dark=true&podcastId=BDgZXsMa7Rrr&v=3&playerId=ausha-eESh"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

## Qui est Morphée ?
Morphée est à la fois une startup française, un objet d’aide à la méditation pour s’endormir et surtout une belle réussite. L’entreprise est présente dans 600 magasins, table sur 4 [millions d’euros](https://www.lesechos.fr/tech-medias/hightech/morphee-setend-dans-deux-nouveaux-pays-sans-avoir-leve-de-fonds-1242043) de chiffre d’affaire cette année, le tout après un lancement fin 2019 .

Morphée n’est pas tout seul sur cette vague. L’application américaine Calm passe le cap du [milliard de dollars](https://business.lesechos.fr/entrepreneurs/startup/0601270762860-calm-la-premiere-application-de-meditation-aux-etats-unis-se-lance-en-france-329398.php) de capitalisation, devenant la première licorne dans la catégorie santé. Le potentiel de ce nouveau eldorado intéresse aussi les poids lourds. Google a racheté le fabricant de montre connectée Fitbit pour [2,1 milliards de dollar](https://www.lesechos.fr/tech-medias/hightech/google-debourse-21-milliards-de-dollars-pour-acquerir-fitbit-1144818), et a lancé une [assurance santé](https://www.lesechos.fr/tech-medias/hightech/google-debourse-21-milliards-de-dollars-pour-acquerir-fitbit-1144818). [Apple est présent aussi](https://www.lesechos.fr/tech-medias/hightech/en-direct-apple-watch-6-ipad-air-4-suivez-la-keynote-et-les-annonces-dapple-1242706) : sa nouvelle Apple Watch 6 est toujours plus tournée vers la santé et il a lancé son nouveau service Apple Fitness +.  


## La disruption Morphée
Pourtant Morphée a une particularité, une disruption. Morphée est un objet autonome intégrant des boutons, des haut-parleur et des sons de méditation pré-enregistrés, nul besoin d’un smartphone pour l’utiliser. Or, si on veut juste écouter des sons, n’importe quel smartphone peut le faire, pas besoin d’un objet spécifique.

## Pourquoi ne pas avoir fait un application mobile ?
On peut y trouver une raison pratique, Morphée est utilisé le soir avant de se coucher pour s’endormir. Son design lui permet d’être manié rapidement dans le noir. Pas besoin de sortir son smartphone, de connecter l’enceinte, d’ouvrir l’application, et potentiellement, de se faire distraire par des notifications. Ce qui serait un comble pour un produit voulant aider l’utilisateur à s’endormir !

Cependant, une application mobile a pleins d’avantages : Le produit est plus facile à concevoir et distribuer. Le consommateur peut l’acheter tout de suite sur son store. Il peut l’utiliser chez lui ou en déplacement. Il peut brancher son enceinte ou son casque. Le catalogue de sons peut être infini et renouvelé souvent.

Tout bien calculé, le choix d’un produit physique semble gênant. Surtout que la mode est de concentrer le plus de produits dans le smartphone. Morphée prend le chemin inverse. Derrière la raison pratique, se cache surtout une raison économique.

## Quel serait le prix d’une application Morphée ?
En fait, on peut même dire : Peut-on faire payer une application avec des sons de méditations dedans ? Il en existe des centaines gratuits sur Youtube ou sur les plateforme de streaming (Apple Music, Spotify, Deezer). Vu la concurrence, le prix d’une application Morphée est de 0€. La rentabilité se fera par la pub, les données ou des offres premiums. Or le même service, mis dans un objet physique cartonne à 80€.

Morphée a cassé les code du « tout smartphone ». Il rappelle que dans certains cas, le smartphone dégrade l’expérience utilisateur. Mais surtout, que comme l’utilisateur ne dépensera pas 1€ sur son smartphone, le business model sur mobile est un casse tête de rentabilité. Alors que le même utilisateur est prêt à débourser 80€ pour le même service mis dans un objet.