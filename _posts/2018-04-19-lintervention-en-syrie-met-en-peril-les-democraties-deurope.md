---
title: 'L&rsquo;intervention en Syrie met en péril les démocraties d&rsquo;Europe'
subttitle: L'effet papillon mortel
date: 2018-04-19T16:37:42+01:00
layout: post
background: /img/2018/04/damas.jpg
categories: [article]
tags: [politique]
---

La sainte trinité a encore frappé, Amérique, Angleterre et France ont de nouveau envoyé des missiles sur la Syrie. Depuis le début de la crise en 2011, nous pouvons examiner les conséquences d&rsquo;une déstabilisation de la région. Et nous pouvons prévoir les conséquences de ces nouvelles frappes sur nos démocraties.

## Introduction

Avant de rentrer dans le vif du sujet permettez moi de relever un détail sur ces frappes. La « ligne rouge » a déjà été franchie en 2013 par l&rsquo;utilisation d&rsquo;arme chimiques. Pourquoi les armes chimiques sont dites non conventionnelles ? Parce qu&rsquo;elles sont invisibles, attaquent sans prévenir et tuent tout le monde même les enfants ? Très bien, mais c&rsquo;est aussi le cas des attaques par drone. Est-ce que Barack Obama a franchi la ligne rouge avec ses [26 171 bombes](https://francais.rt.com/international/32070-etats-unis-ont-largue-equivalent-trois-bombes-par-heure-dans-le-monde-en-2016) pour la seule année 2016 ? La condamnation par les pays occidentaux fait encore une fois preuve d&rsquo;hypocrisie.

## Les conséquences du bombardement

Pour prédire les conséquences de ces bombardements, on peut regarder sous forme de cause/conséquence les précédentes frappes :

  1. Bombardement à l&rsquo;aveugle
  2. Déstabilisation de la Syrie
  3. Important flux migratoire vers l&rsquo;Europe
  4. Déstabilisation de l&rsquo;économie des pays les plus fragiles
  5. Monté du nationalisme et du mouvement anti-UE

Cette suite s&rsquo;est parfaitement illustrée dans les scrutins en France ou en Italie, mais c&rsquo;est surtout le groupe de Visegrad (Pologne, République tchèque, Hongrie, Slovaquie) qui est à redouter. C&rsquo;est pays à l&rsquo;économie fragile basée sur une main d’œuvre bon marché ont vu dans la direction d&rsquo;Angela Merkel d’accueillir un million migrants comme un ultime affront. Ces pays ont déjà le sentiment d&rsquo;être humiliés par l&rsquo;Allemagne qui se sert d&rsquo;eux comme une « Chine » voisine pour profiter des bas salaires et produire à bas coût.

Voilà que Angela Merkel s&rsquo;octroie également leur souveraineté et leur impose leurs propres politiques migratoires. D&rsquo;autant que la chancelière n&rsquo;hésite pas à détruire leur économie, dans le seul but de récupérer quelques voix pour son parti.

Il ne faut pas s&rsquo;étonner de la montée de l&rsquo;extrême droite et de la vague anti-UE présentes dans ces pays : [Marche pour l&rsquo;indépendance](http://www.lemonde.fr/europe/article/2017/11/12/en-pologne-60-000-personnes-defilent-a-l-appel-de-l-extreme-droite_5213847_3214.html) le 13 Novembre regroupant 60 000 personnes en Pologne. [Election d&rsquo;un « Trump tchèque »](http://www.lemonde.fr/europe/article/2017/10/23/large-victoire-des-populistes-en-republique-tcheque_5204704_3214.html) comme premier ministre en République tchèque. [Deuxième reconductions pour Orban](https://www.francetvinfo.fr/monde/europe/elections-legislatives-en-hongrie-favori-du-scrutin-le-premier-ministre-viktor-orban-se-pose-en-defenseur-de-la-nation_2694730.html), le dirigeant d&rsquo;extrême droite anti-immigration en Hongrie.

_Si on continue à frapper la Syrie, jusqu&rsquo;où ira cette vague anti-EU ?_

Car quitter l&rsquo;UE est une réalité de plus en plus concrète. D&rsquo;une part l&rsquo;Angleterre a montré la voie avec le Brexit. D&rsquo;autre part l&rsquo;Allemagne a très bien montré avec la Grèce comment elle traitait les petits pays en position de faiblesse. Enfin dans nos pays aussi la discorde anti-UE gagne du terrain, comme l&rsquo;atteste les suffrages en France, en Allemagne ou en Italie. Les pays fondateurs sont plus que jamais désunis et n&rsquo;arrivent plus à fédérer autour du projet européen. Là où à l&rsquo;Est, Poutine jouit d&rsquo;une carrure d&rsquo;homme fort providentiel, le seul capable de protéger la population des dirigeant occidentaux. Il ne se cache d’ailleurs plus de ses envies de reconquête de l&rsquo;URSS comme en Crimée ou en Ukraine.

## Conclusion

En résumé tirer des missiles sur la Syrie revient à tirer des missiles directement sur les fondations de l&rsquo;UE et de donner une occasion en or pour Poutine de revenir en l&rsquo;Europe de l&rsquo;Est.