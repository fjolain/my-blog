---
title: Le capitalisme simplifie l’usage, pas le fonctionnement
subtitle: Pourquoi votre machine à café est devenue si compliquée
date: 2020-06-19
layout: post
background: /img/2020/06/nespresso.jpg
categories: [article]
tags: [politique, économie]
---

**Notre vie s’est-elle vraiment complexifiée avec le capitalisme ? Quand on voit qu’un simple petit grain de sable peut mettre à terre toute une logistique, on pourrait penser que oui. Mais c’est trompeur, oui le capitalisme a complexité les objets et leur réalisation, mais dans le but de nous simplifier la vie !**

Pour s’en rendre compte, il suffit de comparer des tâches de la vie courante entre aujourd’hui et il y a 70 ans.

Laver son linge. Avant il fallait le faire à la main. Une bassine, une planche à laver, un savon et beaucoup beaucoup d’effort. Aujourd’hui, il faut tout mettre dans une machine et appuyer sur start.

Faire un café. Avant il fallait moudre le café, faire bouillir de l’eau, attendre la filtration. Beaucoup de temps et de galère pour un café médiocre. Aujourd’hui on insère une capsule dans une machine et l’on appuie sur start pour avoir un bon expresso.

Conduire une voiture. Déjà, il fallait sortir de la voiture pour tourner une manivelle, impensable aujourd’hui ! Même la conduite était une épreuve sans direction assistée, boîte automatique ou GPS. Un vrai calvaire. Aujourd’hui, on se contente de suivre les indications du GPS, on ne gère plus que la pédale d’accélération, et on peut tourner le volant avec la force d’un doigt.

Ces exemples et bien d’autres montrent que le capitalisme a considérablement simplifié notre vie. Nous libérant de beaucoup de corvées. Cependant pour arriver à une magie où il suffit d’appuyer sur start pour démarrer une voiture, une tournée de linge ou un café, il faut ajouter toujours plus de technologie.

Nos objets qui nous simplifient la vie, sont donc devenus en interne d’une complexité monstre. Chaque machine nécessite des centaines de pièces fabriquées partout dans le monde. Cette complexité rend également les objets impossibles à réparer ([cf. article](https://blog.jolain.net/soci%C3%A9t%C3%A9/2019/07/14/la-propriete-cest-le-vol.html)). Ce gap entre simplicité d’utilisation et complexité de fonctionnement ne fait que de se creuser. Au point que quand le grain de sable finit par casser le fonctionnement ou la production mondialisée, on s’aperçoit avec effroi l’enfer du décor. Effroi d’autant plus grand, que nous sommes plus habitués à la galère. Puisque plus personne ne prend 20 min pour se faire un café, personne n’envisage une seule seconde de passer 4 heures pour réparer sa machine à café.

Il n’existe pas  d’utopie où un objet simple peut nous éviter des efforts. Toute cette technologie est utile. Si on souhaite réduire notre mondialisation et le capitalisme, est-on prêt à sacrifier notre confort et notre simplification d’usage ?
