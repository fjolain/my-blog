---
title: Transport spatial et temps universel
subtitle: Ce qu'une ancienne montre nous rappelle des problèmes de synchronisation
date: 2017-06-18T11:20:34+01:00
layout: post
background: /img/2017/06/montre.jpg
categories: [article]
tags: [société]
---
L&rsquo;arrivée du quartz puis de l&rsquo;horloge atomique, nous a fait oublié a quel point avoir un temps universel était quelque chose de compliqué.

Je me suis rendu compte du progrès en retrouvant une montre à gousset de mon grand-père. Elle fait partie des montres régulateur utilisée sur les chemins de fer. Quand le chemin de fer est arrivé, un problème souvent mortel c&rsquo;est posé. Deux trains doivent prendre le même tunnel, le train A à 10h15 et le train B à 10h25. Or à cette époque les montres se dérèglaient vite. Plusieurs accidents ont eu lieu à cause de se manque de précision. On peut imaginer nos deux trains arriver en même temps dans le tunnel car A avance de 5min et B retard de 5min.

Pour régler ce problème une course à la technicité a été lancée. Les entreprises de chemin de fer ont demandé des montres régulateur aux horlogers. Ces montres faisaient office d&rsquo;étalon de temps dans les gares et les conducteurs devaient venir vérifier leur montre toutes les semaines.  

<iframe src="https://www.youtube.com/embed/1PBfEStcNiY" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Aujourd&rsquo;hui le problème du temps se repose avec la relativité générale. Einstein a démontré que le temps se courbe sous l&rsquo;effet de la gravité. On a beau mettre des horloges ultra précises dans les satellites, leur temps propres subit une dérive « naturelle » . En effet le temps passe moins vite dans le satellite que sur Terre.

_Rien de bien méchant ? _

Suffisamment pour rendre le GPS inutilisable. La donnée GPS parcourt 20 200 km avant de nous rejoindre. Si le GPS envoie la donnée avec un léger retard, le calcul de la distance entre nous et le satellite sera biaisé. De quoi loupé notre carrefour, ou notre cible dans le cas des missiles.

De plus les satellites ont besoin d&rsquo;avoir un temps unique extrêmement précis. Sinon la constellation de satellites est inutilisable.

La solution de temps universel autour de la terre est tout simplement d&rsquo;envoyer un signal de reset avec le temps à la surface de la terre comme référence. Régulièrement les satellites sont remis à l&rsquo;heure humaine, pour contrer la dérive du temps.

Cette solution ne peut pas être prise pour les voyages spatiaux. En effet cette solution fonctionne autour de la terre car la distance du signal de reset est faible et reste la même pour tous les satellites. Or pour les voyages vers mars ou autre, il faut prendre en compte le temps de parcours du signal.

Un vaisseau partant de Mars et un vaisseau arrivant sur la Terre ne recevront pas le reset au même moment. Avoir un temps universel dans l&rsquo;univers pour la conquête spatiale va être une problématique difficile à résoudre.

Chaque trajectoire du vaisseau va engendrer une dérive différente, et l&rsquo;impossibilité d&rsquo;envoyer signal de reset instantané va encore complexifier l&rsquo;affaire.

Heureusement pour l&rsquo;instant, on envoie les vaisseaux un par un, il n&rsquo;y a donc pas de risque d&rsquo;accident. Ce problème surviendra dans un siècle peut-être.