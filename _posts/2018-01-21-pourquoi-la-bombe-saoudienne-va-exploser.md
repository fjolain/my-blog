---
title: Pourquoi la bombe Saoudienne va exploser ?
subtitle: Qui sème la religion recolte les extrêmes
date: 2018-01-21T18:03:08+01:00
layout: post
background: /img/2018/01/as.jpg
categories: [article]
tags: [politique]
---

Qui sème le vent récolte la tempête. L&rsquo;Arabie Saoudite risque de l&rsquo;apprendre au prix fort. Le royaume a joué aux apprentis sorciers depuis 20 ans en propageant sont idéologie radicale. Aujourd&rsquo;hui elle se retrouve au pieds du mur, une bombe dans les mains. Comment en sont-ils arrivés là. Que cache cette bombe ? Va-t-elle exploser ?

# Construction de la bombe

Une bonne bombe, ça se prépare. Celle de l&rsquo;Arabie Saoudite date de 1932, où Abdelaziz ben Abderrahman Al Saoud fonde le Royaume d&rsquo;Arabie Saoudite, après 30 ans de guerre et 500 000 morts. Il fit appelle à la religion en créant un état religieux qui permit de museler l&rsquo;opposition et de justifier ses actes. L&rsquo;utilisation de la religion pour assoire son pouvoir est une idée familiale. En 1744, le fondateur de la ligné Saoud, Mohammed Ibn Saoud s&rsquo;associe avec un prédicateur religieux, Mohammed Ibn Abdelwahhab et fondent le wahhabisme.

> ### Histoire rapide de l&rsquo;Islam
> 
> Comme le fondateur de l&rsquo;islam, le prophète Mahomet, n&rsquo;a pas désigné de successeur lors de sa mort en 632, l&rsquo;islam s&rsquo;est scindé en deux à tout jamais. D&rsquo;une coté la branche chiite qui déclare Ali, gendre du prophète comme successeur, aujourd&rsquo;hui 30% des musulmans dans le monde (essentiellement Liban, Syrie, Iran et Yémen). De l&rsquo;autre coté la branche sunnite qui voit en Abou Bakr, compagnon fidèle du prophète le successeur, aujourd&rsquo;hui 70% des musulmans dans le monde.  
> Le wahhabisme fait partie de la branche sunnite de l&rsquo;islam. Cette religion souhaite une lecture stricte du Coran. Elle prône la charia, et fait partie des branches les plus radicales de l&rsquo;islam.

Par chance l&rsquo;Arabie Saoudite possède les deux villes saintes de l&rsquo;islam : la Mecque et Médine. Le retour de la religion strict de ses ancêtres est donc une aubaine pour « protéger » les lieux saints de l&rsquo;islam, et prendre le pouvoir.

Un fou sans moyen ne va pas très loin, heureusement le pétrole vient aider le souverain en 1938. Il se retrouva avec une manne financière incroyable. L&rsquo;Arabie Saoudite fit ce qu&rsquo;on a appelé la diplomatie du « carnet de chèques ». En concluant d&rsquo;importants contrats avec les pays riches et émergents, le pays put acquérir une place sur la scène économique et politique internationale.  
Ce que l&rsquo;on sait moins sur ces contrats, c&rsquo;est une clause autorisant l&rsquo;Arabie Saoudite a investir dans des lieux de culte dans le pays vendeur. Des pays comme la France, l&rsquo;Angleterre, la Belgique ou l&rsquo;Indonésie se sont vues construire des mosquées « made in saoudite » où le wahhabisme était enseigné. L&rsquo;exemple le plus frappant est peut-être la Grande Mosquée de Bruxelles construire par l&rsquo;Arabie Saoudite et autorisée par l&rsquo;état en échange d&rsquo;une réduction sur la facture de pétrole.

Discrètement, l&rsquo;Arabie Saoudite a distillé son idéologie extrémiste dans le monde entier. L&rsquo;historien Charles Allen à chiffré que depuis 1979, l&rsquo;Arabie Saoudite a consacré 70 milliards de dollars à la diffusion du wahhabisme. Leur doctrine prône un islam radical avec le retour de la charia, la destruction des chiites ainsi que tous « traîtres » de sunnite qui ne protègent pas l&rsquo;islam contre les chiites. C&rsquo;est ainsi que le monde entier s&rsquo;est retrouvé avec une armée de musulman radicaux prêts à faire le djihad par la violence.  
Il fallait juste une une étincelle pour faire exploser cet extrémiste et c&rsquo;est l’État Islamique qui l&rsquo;amena. Au départ financer par l&rsquo;Arabie Saoudite pour renverser Bachar Alasad (la Syrie est un pays majoritairement sunnite, mais gouvernée par un gouvernement d&rsquo;oppresseur chiite, cela en fait donc un ennemi de choix pour le wahhabisme). Le pays s&rsquo;en est écarté quand l&rsquo;organisation devenait trop dangereuse. Il ne reste pas moins beaucoup de ressemblances entre l&rsquo;Arabie Saoudite et l&rsquo;Etat Islamique : pouvoir politique et religieux confondus, instauration de la charia, lecture rigoureuse du Coran, volonté de détruire les chiites et les « traîtres » sunnites et volonté de renouer avec le djihadisme.

**Est une coïncidence, si tous les attentats survenus en Europe ont comme centre de commandement Bruxelles, lieux même de cette Grande Mosquée « offerte » par l&rsquo;Arabie Saoudite ?**

Non ! Comme le souligne Barack Obama dans une interview dans The Atlantic : _l&rsquo;Arabie saoudite propage l’extrémisme qui a généré le terrorisme_. Il cite même l&rsquo;exemple de l&rsquo;Indonésie : _[l&rsquo;Indonésie], État musulman et tolérant, est devenu un pays extrémiste, à cause du financement par l’Arabie Saoudite des mouvements fanatiques et des écoles wahhabites_.  
En voulant jouer aux apprentis sorciers par la propagation de son idéologie radical, l&rsquo;Arabie Saoudite a créé une armée de musulmans hors de contrôle et prêts à toute forme de violence pour protéger leur islam. Elle doit donc, comme fondatrice du wahhabisme et gardienne des lieux saints, montrer un islam radical sous peine d&rsquo;avoir un retour de bâton des montres qu&rsquo;elle a créé.

# Quand le grand bienfaiteur devient le traître

Or c&rsquo;est justement là que la bas blesse. L&rsquo;Arabie Saoudite a comme « obligation » de taper sur des gouvernements chiites. Avec une importation d&rsquo;arme de 64.4 milliards de dollars d&rsquo;arme rien que pour l&rsquo;année 2014, l&rsquo;Arabie Saoudite est devenue le premier importateur d&rsquo;arme au monde. Le pays dispose d&rsquo;une force de frappe colossale. Mais sur qui taper ? La Syrie ? et son puissant ami russe, certainement pas ! L&rsquo;Iran ? et son projet nucléaire, ils voudraient bien, mais ça serait se mettre en difficulté diplomatique avec ces principaux clients occidentaux qui respectent l&rsquo;Iran. Heureusement, il y a le Yémen : pays chiite sans armé, sans amis puissants et dont personne se préoccupe.

Le Yémen a donc reçu tout la puissance de feu de l&rsquo;Arabie Saoudite. Cette guerre n&rsquo;a pas alerté les médias, car en vue des contrats d&rsquo;armement, on peut dire qu&rsquo;elle est « made in occident ». Ces chers Yéménites ont pu admirer nos rafales et missiles de très près. Le Yémen est maintenant un tas de cendre, les Saoudiens ne peuvent plus l&rsquo;utiliser pour embellir leur image de protecteur du « vrai » islam.  
Pire, le royaume sympathise même avec l’ennemi Israël. Les deux pays ont l&rsquo;Iran comme ennemi commun et multiplient les relations diplomatiques : construction d&rsquo;une ambassade saoudienne à Tel-Aviv, gestion par Israël d&rsquo;une base militaire en Arabie Saoudite, rencontres de hauts gradés. Le royaume n&rsquo;a pas hésité à soutenir financièrement Israël pour la guerre du Liban. De son coté l&rsquo;état hébreux livre arme et logistique pour la guerre au Yémen. Pas sûr que les pro-wahhabisme soient pour une entente islamo-sionite.

Face à ce problème, le nouvel homme fort du royaume Mohamed Ben Salman (MBS) joue quitte ou double. Il veut réduire l&#8217;emprise religieuse dans son pays : moins de charia, plus de libertés. Deux cas de figures s&rsquo;offrent à lui. Il modernise son pays et les « ultras » n&rsquo;ont pas le temps de s’organiser. Son armée wahhbisme présente en nombre dans son propre pays se disperse : La bombe est désamorcée. Soit ces mêmes radicaux voient en MBS et en l&rsquo;Arabie Saoudite toute entière un traître qui a retourné sa veste. Leur islam est menacé et ils partent faire le djihad face au pouvoir : La bombe explose&#8230;