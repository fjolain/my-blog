---
title: Les réseaux sociaux et le tabac
subtitle: Mêmes addictions, mêmes réactions.
date: 2018-01-01T20:01:20+01:00
layout: post
background: /img/2018/01/facebook.jpg
categories: [article]
tags: [social network, société]
---

Il y a des parallèles qui ne trompent pas. Je vais vous parler des réseaux sociaux et de leurs drôles de ressemblances avec le tabac. On y retrouve un même type de consommation, un même type de marketing et un même type de marché. Alors on commence !

## L&rsquo;art de la micro-satisfaction

Le but des réseaux sociaux est de jouer sur le besoin social de l&rsquo;humain. Comme le tabac qui joue sur notre dépendance à la nicotine, les réseaux sociaux vont jouer sur notre dépendance à la reconnaissance sociale et notre peur d&rsquo;être seul. Facebook ou Twitter apporte une sorte de micro-affection et reconnaissance par la publication de photos, de commentaires ou de likes. Ces micro-affactions vont engendrer des _micro-satisfactions_ pour l&rsquo;utilisateur. Une dépendance va se créer en voulant toujours plus de micro-satisfactions tout au long de la journée.

La forte dépendance est due aussi bien pour le tabac que pour les réseaux par la simplicité et l&rsquo;instantanéité de la micro-satisfaction. L&rsquo;action de publier comme celle de fumer ne nécessite que quelques minutes et peu d&rsquo;intelligence. Contrairement à un reconnaissance par ses proches pour un projet réussi, un prix gagné ou autre, l&rsquo;effort qui engendre une micro-affection est ridicule. De plus le résultat est immédiat, en quelques minutes nous avons des retours de likes et commentaires qui viennent flatter notre ego. On peut donc se donner ces micro-satisfactions plusieurs fois par jour, de la même manière que l&rsquo;on fume une clope plusieurs fois par jour.

## Un marketing identique

### Être le leader

Les pubs traditionnelles jouent beaucoup sur notre envie de confort ou de sexe pour nous faire acheter. Généralement on est tout seul dans ces pubs. Au mieux le groupe ne sert que d&rsquo;exemple de consommateurs simples, face au héro de la pub qui lui est au dessus des gens simples. Le marketing des réseaux sociaux comme le marketing des marchands de tabacs utilisent au contraire la motivation d&rsquo;appartenir à un groupe et d&rsquo;en devenir le learder.

Dans les anciennes pub de tabac, le fumeur sortait fièrement une cigarette pour devenir le leader charismatique du groupe. Maintenant le leader charismatique du groupe sort fièrement l&rsquo;application Facebook de sa poche pour prendre un selfie (de groupe de préférence). Les deux mise sur une image jeune où le consommateur devient le centre d&rsquo;intention grâce au produit.

### Créer des habitudes

Le piège du marketing se referme d&rsquo;autant plus sur les habitudes prises avec les produits. On va volontiers ouvrir son téléphone pour scruter son fil d&rsquo;actualité comme on fumer une cigarette à la pause. On ne vas plus demander un briquer à une fille pour démarrer la conversion, on préférera l&rsquo;envoi d&rsquo;un message ou d&rsquo;une photo. On prend en photo son assiette au début du repas avec le même réflexe qu&rsquo;on va fumer une clope après le repas.

Les addictions sont très semblables est repose sur la prise d&rsquo;habitudes qui rend difficile l&rsquo;arrêt.

![facebook]({{ site.baseurl }} /img/2018/01/facebook2.jpg){:width="60%"}
## Une oligarchie

Enfin le dernier point est plus économique. Le marché du tabac comme le marché des réseaux sociaux sont des oligarchies où seulement quelques acteurs se partagent la totalité du marché. On a Facebook, Twitter, Google et Snapchat pour les réseaux sociaux, et pour les fabricant de tabac : China National Tobacco, Philip Moris, British American Tobacco, Japan Tobacco et Imperial Tobacco.

&nbsp;

## Conclusion

Après maintenant un siècle de consommation effrénée de tabac, on se rend compte que ce produit est nocif pour la population. On a beau sensibiliser les gens, le mal est fait. Beaucoup de gens sont devenus malades à cause du tabac, et la consommation du tabac est tellement ancrée dans les habitudes que l&rsquo;on ne peut plus la chasser

**Va t-on reproduire la même erreur et attendre un siècle avant de se rendre compte de la nocivité des réseaux sociaux ?**