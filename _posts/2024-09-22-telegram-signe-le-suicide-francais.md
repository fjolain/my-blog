---
title: Telegram acte le suicide français
subtitle: Suicide diplomatique, démocratique et technologique.
date: 2024-07-28
layout: post
background: /img/2024/09/effel.jpeg
categories: [article, podcast]
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-C8wE" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=omkv1S8RraVq&v=3&playerId=ausha-C8wE"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>


<h2>Diplomatique</h2>

Le travail de sape de la diplomatie française a commencé avant l'affaire Télégram. Notre président ne se contente pas d'une logorrhée verbale vide de sens. Il peut aussi agir sans considération ni compétence sur des sujets qu'ils ne maîtrisent pas comme la diplomatie.

Alors que les chefs d'états africains voulaient plus de reconnaissance dans la relation France-Afrique. On se souvient des discours (Ouagadougou ou Burkina Faso) du président en Afrique caricaturant le personnage d'OSS 177 ([Quand la blague d’Emmanuel Macron sur un président africain passe mal](https://www.lemonde.fr/afrique/article/2017/11/29/quand-la-blague-d-emmanuel-macron-sur-un-president-africain-passe-mal_5222218_3212.html)).


On se remémore la visite à Vladimir Poutine pour négocier la paix en Ukraine. Son hôte a su lui rappeler, par une mise en scène adéquate, le rôle insignifiant de Macron dans le conflit.

Maintenant, on assiste à un président tyrannique. Qui peut donner la nationalité et l'asile à Pavel Durov, CEO de Télégram et personnage politique mondial, puis l'enferme par surprise à sa descente d'avions 3 ans après.

Qui veut encore parler ou travailler avec un tel fou ? Vous passez d'ami de la France à ennemis du Parti sur un simple appel au parquet de Paris.

<h2>Démocratique</h2>

La ressemblance avec une dictature à Parti unique ne s'arrête pas là. Le chef d'État a pris soin de décrire son Nouveau Monde [dans un tweet](https://x.com/EmmanuelMacron/status/1828073770461921594) empreint d'hypocrisie.

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Je lis ici de fausses informations concernant la France suite à l’arrestation de Pavel Durov.<br> <br>La France est plus que tout attachée à la liberté d’expression et de communication, à l’innovation et à l’esprit d’entreprise. Elle le restera.<br> <br>Dans un État de droit,…</p>&mdash; Emmanuel Macron (@EmmanuelMacron) <a href="https://twitter.com/EmmanuelMacron/status/1828073770461921594?ref_src=twsrc%5Etfw">August 26, 2024</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Notre Chef suprême rassure donc les citoyens, la France reste un "État de droit", "attachée à la liberté d’expression et de communication". "C’est à la justice, en totale indépendance, qu’il revient de faire respecter la loi". "Ce n’est en rien une décision politique".

Ce message, a portée historique digne d'un dictateur, informe donc les Français, que Macron s'en fiche promptement de la liberté d'expression et de communication, de l'état de droit et de l'indépendance de la justice.

Il est usant depuis maintenant 7 ans, de voir les opposants et médias s'inquiéter de la déviance totalitaire du président pour après en période d'élection lavée son image et le réélire.

<h2>Technologique</h2>

Pour enfoncer le clou (sur le cercueil de la France), Macron rajoute dans son tweet : être attaché "à l’innovation et à l’esprit d’entreprise". Après avoir tout de même inculpé un CEO. Il n'a pas intenté un procès à l'entreprise Télégram, il a directement mis son CEO en cellule.

Cela ressemble à l'arrestation du CEO d'Alibaba, Jack Ma par Xi Jimping en Chine. Lui aussi ne voulait pas se plier aux ordres du chef suprême.

Que feront les autres CEO ? Marc Zuckerberg avec Meta ou Elon Musk avec X remettront-ils un jour le pied en France au risque de se retrouver en prison ?

Autre cas plus complexe avec l'entreprise de cryptage Ledger. Elle vend des appareils électroniques pour sécuriser la détention de cryptoactifs. L'entreprise est même leader mondial sur son marché. Sa place de leader a été conquise par sa réputation et sa fiabilité. Ledger doit démontrer que ses appareils protègent biens vos actifs et votre anonymat.

Il y a  déjà eu des scandales à cause d'[une fuite de données client](https://www.ledger.com/fr/message-ledgers-ceo-data-leak), ainsi qu'une mise à jour [jugée dangereuse pour la sécurité](https://journalducoin.com/actualites/polemique-recovery-sous-pression-ledger-fait-machine-arriere-et-joue-la-carte-de-lopen-source/). L'entreprise doit constamment garder sa réputation intacte pour garder ses clients. Si Ledger doit collaborer avec l'État pour espionner voire siphonner les cryptoactifs de ses utilisateurs, alors elle sera foutue au moindre soupçon.


C'est vrai pour Ledger, c'est vrai pour toutes les entreprises de sécurité et de cryptages en France. Leur chiffre d'affaires dépend de la confiance des utilisateurs dans la France, avec un fou pareil mieux vaut prendre ses distances.


En conclusion, Telgram est l'étincelle qui signe le suicide français et Macron le résume dans un tweet moqueur. La France devient une terre impropre pour le business ou pour nouer des relations avec l'État.




