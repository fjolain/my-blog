---
title: Pourquoi la NASA a placé son télescope James Webb si loin de la Terre ?
subtitle: Mettre son télescope à 1,3 million de km de la Terre a fait exploser son budget de 1000%, mais avait-elle le choix ?
date: 2022-01-11
layout: post
background: /img/2022/01/webb.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [science]
---

<iframe frameborder="0" loading="lazy" id="ausha-IWFi" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=BxM3vtKaKpjn&v=3&playerId=ausha-IWFi"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Le 25 décembre dernier, la NASA avec l'aide de l'Europe lança son plus cher télescope à 10 milliards de dollars vers un mystérieux endroit situé à 1,3 million de km de nous. Pourquoi se mettre un tel défi ?


Avant de  répondre à la question, il faut déjà souligner que si possible, il ne faut pas envoyer un télescope si loin.

La première raison saute aux yeux. Cela rend l'acheminement extrêmement compliqué. À une telle distance, on ne pourra jamais réparer le télescope. Or c'est justement ce qu'a dû faire la NASA avec le télescope Hubble mis en orbite autour de la Terre en 1990. Malgré tous les soins apportés à sa conception, la NASA a dû envoyer des astronautes pour le réparer.

De plus, si loin de l'atmosphère terrestre, les conditions vont être rudes pour James Webb, il sera plongé en permanence dans le vide spatial à -233C.

On se retrouve donc devant un casse-tête d'ingénierie. Il faut concevoir un bijou de technologie, résistant à un voyage éprouvant, fonctionnant à -233C pendant des années et avec une fiabilité de 100%.

Il a donc fallu tester et retester chaque pièce, chaque assemblage dans des chambres à vide à -233C. À la fin, la NASA a dû faire d'importants travaux sur sa plus grande chambre à vide au monde, située au centre Johnson.

Toute cette complexité de conception et de test est la raison qui a fait passer le budget de 1 milliard de dollars à 10 milliards.

Donc vraiment, si vous devez installer un télescope quelque part, le mettre à 1,3 million de km de la Terre est une mauvaise idée... à moins que vous souhaitiez observer très finement les infrarouges.

<h2> Qui a-t-il à voir dans les infrarouges ? </h2>

Chaque onde électromagnétique est liée à un objet ou évènement spatial.

Il y a le domaine du visible, allant du bleu au rouge, et qui permet déjà de voir les étoiles et les galaxies.

On peut aussi scruter les ondes radio pour repérer [les pulsars](https://fr.wikipedia.org/wiki/Pulsar). Les [étoiles à neutron](https://fr.wikipedia.org/wiki/%C3%89toile_%C3%A0_neutrons), elles, rejétent des rayons X ou gamma.

Pour l'infrarouge, il y a l'embarras du choix. Les toutes premières ondes après le BigBang, voyagent depuis [14 milliards d'années](https://fr.wikipedia.org/wiki/Big_Bang) dans l'univers et finissent toutes par tendre vers l'infrarouge. Regarder les infrarouges permet donc de [remonter le temps jusqu'au BigBang](https://fr.wikipedia.org/wiki/James-Webb_(t%C3%A9lescope_spatial)#%C3%89tude_des_premi%C3%A8res_%C3%A9toiles_et_galaxies).

Les pouponnières à étoiles sont [opaques dans le visible, mais transparentes dans l'infrarouge](https://fr.wikipedia.org/wiki/James-Webb_(t%C3%A9lescope_spatial)#Formation_et_%C3%A9volution_des_galaxies). Étudier ces astres à l'infrarouge permet de mieux comprendre la naissance et la formation des étoiles et galaxies.

Les molécules d'oxygène, d'eau et de CO2 filtrent l'infrarouge, c'est une source d'informations précieuses si l’on souhaite analyser d'autres planètes. Analyser l'infrarouge d'atmosphères de planètes lointaines [permettra peut-être d'y trouver la vie](https://fr.wikipedia.org/wiki/James-Webb_(t%C3%A9lescope_spatial)#%C3%89tude_des_syst%C3%A8mes_plan%C3%A9taires_et_recherche_des_%C3%A9l%C3%A9ments_propices_%C3%A0_la_vie).

Bref, l'univers regorge d'ondes infrarouges intéressantes à étudier. Il y a même trop d'infrarouges !

<h2>L'infrarouge est partout </h2>

En fait dès qu'un corps est chaud, [il rayonne de l'infrarouge](https://fr.wikipedia.org/wiki/Corps_noir).

Chaque être humain, voiture ou maison rayonne de l'infrarouge. On pourrait donc mettre James Webb loin de la civilisation, comme le [ELT](https://fr.wikipedia.org/wiki/T%C3%A9lescope_g%C3%A9ant_europ%C3%A9en) au bout milieu du désert chilien ?

Non, notre atmosphère étant composée d'oxygène, d'eau et de CO2, [il filtre la quasi-totalité de l'infrarouge](https://fr.wikipedia.org/wiki/James-Webb_(t%C3%A9lescope_spatial)#Contexte) venant de l'univers. De plus la terre elle-même émet de l'infrarouge. Il y a même des panneaux solaires qui captent cette énergie la nuit. On pourrait alors mettre le télescope en orbite comme Hubble ?

Oui, mais ce n'est pas l'idéal. Il serait encore gêné par la Terre. Il faut donc l'envoyer loin d’elle, mais à un endroit dont il ne bougera pas par rapport à la Terre. De tels points ont été trouvés par [Lagrange en 1772](https://fr.wikipedia.org/wiki/Point_de_Lagrange), il en existe 5. Et le télescope va justement au point de Lagrange 2 (L2), situé sur la ligne Soleil-Terre à 1,3 million de km de la Terre, en s'éloignant du Soleil. De là, le télescope sera loin de la Terre et de son atmosphère, mais fixe par rapport à elle.

Une partie du télescope fera face au Soleil, celle-ci va déplier un énorme parasol pour que l'autre partie soit à l'ombre sous une température de -233C, condition idéale pour mesurer les infrarouges.

<h2>Conclusion</h2>

Voilà pourquoi la NASA a investi 10 milliards de dollars pour mettre son télescope si loin. C'est la meilleure configuration pour étudier finement les ondes infrarouges venant de l'univers.

James Webb s'inscrit sur le chemin de ces mégaprojets, qui font avancer la science à pas de géant.

[Hubble](https://fr.wikipedia.org/wiki/Hubble_(t%C3%A9lescope_spatial)#R%C3%A9sultats_scientifiques) nous a permis de mieux cartographier l'univers. Le [LHC](https://fr.wikipedia.org/wiki/Grand_collisionneur_de_hadrons#D%C3%A9couvertes) à Genève, nous a permis de mieux comprendre la matière en allant jusqu'à valider l'existence du Boson de Higgs en 2012. [Ligo](https://fr.wikipedia.org/wiki/Laser_Interferometer_Gravitational-Wave_Observatory#Observations) a permis de mieux comprendre la dynamique de l'espace-temps, en validant en 2016 l'existence d'ondes gravitationnelles.

James Webb doit quant à lui répondre aux grandes questions de l'univers : Que s'est-il passé après le BigBang ? Comment les galaxies se forment ? A-t-il d'autres vies dans l'Univers ?

