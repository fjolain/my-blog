---
title: 'Simulateur d&rsquo;ordinateur quantique'
subtitle: Concevoir un interpréteur python pour la mécanique quantique
date: 2016-08-22T16:00:19+01:00
layout: post
categories: [project]
tags: [informatique, quantique, simulation, mathématique]
---
Bonjour, en apprenant la base de la mécanique quantique, j&rsquo;ai fais un détour sur l&rsquo;ordinateur quantique. Pour bien assimiler le tout j&rsquo;ai conçu un simulateur d&rsquo;ordinateur quantique simple d&rsquo;utilisation. Il permet de simuler facilement des graphes quantiques avec la plupart des portes et autant de qubits que l&rsquo;on souhaite.

Avant toute chose, le lien du programme : [https://github.com/FrancoisJ/Quantum_Computer.](https://github.com/FrancoisJ/Quantum_Computer)

Le programme est en python, mais il n&rsquo;est pas nécessaire de connaître le langage, j&rsquo;ai coder un mini interpréteur pour une utilisation plus simple. L&rsquo;idée est de transcrire un graphe en script. Ensuite on vient exécuter le script.

<!--more-->

Le programme a deux modes : le prompteur permet d’exécuter le code ligne par ligne directement par la commande `./computer`, il permet aussi d&rsquo;accéder à l&rsquo;aide par la commande _help._ Le deuxième mode est la lecture d&rsquo;un script par la commande `./computer le_fichier`. Voyons quelques exemples d&rsquo;algorithmes connus, que l&rsquo;on peut simuler.

## Exemples

### Deutsch

<img class="aligncenter" src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Deutsch-Jozsa_Algorithm.svg/363px-Deutsch-Jozsa_Algorithm.svg.png" width="454" height="129" /> 

L&rsquo;algorithme de Deutsch permet de savoir si un fonction renvoie toujours le même résultat ou non. Ce qui a de fort avec cet algorithme c&rsquo;est qu&rsquo;il permet de connaître le résultat en mesurant une seule fois la fonction. Une belle analogie serait de vérifier si une pièce est truquée, dans ce cas elle _renvoie_ toujours la même valeur. Cet algorithme permet de vérifier la pièce en regardant que d&rsquo;un seul coté, ceci grâce au propriété de la mécanique quantique.

&nbsp;

Voici, la traduction du schémas pour mon programme, ici la fonction prend soit 0 ou 1, avec _f(x) = x_.

<pre># This exemple implements the Deutsh algorithm

# Initialize computer variable
qubit x 1 0
qubit y 0 1
# Choose balanced or not function
function f
y = x
endfunction

# Perform some gates
hadamard [x,y]
uf f x y
hadamard x

# Make a measure
# if function is balanced, result will be 1
# then, result will be -1
measure x

</pre>

### Grover

L&rsquo;algorithme de grover permet de retrouver un élément dans une base de donnée. Une porte nommée oracle viendra taguer l&rsquo;élément que l&rsquo;on souhaite et une série de porte viendra extraire l&rsquo;élément. Un tel algorithme sur un ordinateur classique a une complexité de<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-acbe22db9c70b9660b1680a85add7865_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#79;&#40;&#92;&#102;&#114;&#97;&#99;&#123;&#78;&#125;&#123;&#50;&#125;&#41;" title="Rendered by QuickLaTeX.com" height="22" width="43" style="vertical-align: -6px;" /> , avec celui de grover on obient<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-3d724c2a48e2b00c0e2d41c8f366aaf1_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#79;&#40;&#92;&#115;&#113;&#114;&#116;&#123;&#78;&#125;&#41;" title="Rendered by QuickLaTeX.com" height="20" width="58" style="vertical-align: -4px;" /> .

Ci-dessous un schéma de l&rsquo;algorithme avec 8 quibts + 1 quibt de resultat. On a donc une base de donnée de 256 éléments. Nous souhaitons extraire l&rsquo;élément 57, la fonction oracle est<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-c80cff547a5251c3ac805365e2ff36fd_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#111;&#40;&#120;&#41;&#32;&#61;&#32;&#49;&#92;&#32;&#115;&#105;&#92;&#32;&#120;&#61;&#53;&#55;&#44;&#92;&#32;&#48;&#92;&#32;&#115;&#105;&#110;&#111;&#110;" title="Rendered by QuickLaTeX.com" height="18" width="216" style="vertical-align: -4px;" /> .  
[<img class="wp-image-141 aligncenter" src="https://jolain.net/wp-content/uploads/2016/08/grover-300x127.png" alt="grover" width="550" height="233" srcset="https://blog.jolain.net/wp-content/uploads/2016/08/grover-300x127.png 300w, https://blog.jolain.net/wp-content/uploads/2016/08/grover-768x326.png 768w, https://blog.jolain.net/wp-content/uploads/2016/08/grover-604x256.png 604w, https://blog.jolain.net/wp-content/uploads/2016/08/grover.png 865w" sizes="(max-width: 550px) 100vw, 550px" />](https://jolain.net/wp-content/uploads/2016/08/grover.png)  
Voici le script correspondant :

<pre># add qubit x
qubit x1 1 0
qubit x2 1 0
qubit x3 1 0
qubit x4 1 0
qubit x5 1 0
qubit x6 1 0
qubit x7 1 0
qubit x8 1 0
# add qubit q
qubit q 0 1

# perform hadamard
hadamard [x1,x2,x3,x4,x5,x6,x7,x8,q]

# add oracle function
function o
if x==57 :
    y = 1
else :
    y = 0
endfunction


# iteration = pi/4*sqrt(2^n)
repeat 12
oracle o [x1,x2,x3,x4,x5,x6,x7,x8] q
hadamard [x1,x2,x3,x4,x5,x6,x7,x8]
s0 [x1,x2,x3,x4,x5,x6,x7,x8]
hadamard [x1,x2,x3,x4,x5,x6,x7,x8]
endrepeat

measure x1
measure x2
measure x3
measure x4
measure x5
measure x6
measure x7
measure x8</pre>

## Fonctionnement

L&rsquo;utilisation du logiciel est fini, pour plus de renseignements le logiciel dispose d&rsquo;une aide. Lancer le prompteur par `./computer` puis taper `help`. Je vais maintenant vous expliquer le fonctionnement.

Avant toute chose sachez qu&rsquo;il est simple. Le fichier computer.py fait peut-être 1000 lignes, mais seulement 300 servent pour la simulation, le reste sert pour l’interpréteur et l&rsquo;aide.

Prennons l&rsquo;exemple d&rsquo;une porte générique à un qubit :

<p class="ql-center-displayed-equation" style="line-height: 43px;">
  <span class="ql-right-eqno"> &nbsp; </span><span class="ql-left-eqno"> &nbsp; </span><img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-b663d4feec93952aef77debabf462b3d_l3.png" height="43" width="95" class="ql-img-displayed-equation quicklatex-auto-format" alt="&#92;&#91;&#80;&#32;&#61;&#32;&#92;&#98;&#101;&#103;&#105;&#110;&#123;&#112;&#109;&#97;&#116;&#114;&#105;&#120;&#125; &#97;&#32;&#38;&#32;&#98;&#32;&#92;&#92; &#99;&#32;&#38;&#32;&#100; &#92;&#101;&#110;&#100;&#123;&#112;&#109;&#97;&#116;&#114;&#105;&#120;&#125;&#92;&#93;" title="Rendered by QuickLaTeX.com" />
</p>

Le problème avec la notation matricielle, c&rsquo;est que le nombre de qubit doit être fixé à l&rsquo;avance pour construire les matrices. On peut trouver une autre méthode de calcul moins contraignante et plus procédurale.

Si nous avons notre qubit X dans l&rsquo;état :<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-cbfea4d7818fd45647cdf3d701aaa2c5_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#124;&#88;&#62;&#32;&#61;&#32;&#92;&#97;&#108;&#112;&#104;&#97;&#42;&#124;&#48;&#62;&#32;&#43;&#32;&#92;&#98;&#101;&#116;&#97;&#42;&#124;&#49;&#62;" title="Rendered by QuickLaTeX.com" height="18" width="195" style="vertical-align: -4px;" /> , alors 

<p class="ql-center-displayed-equation" style="line-height: 18px;">
  <span class="ql-right-eqno"> &nbsp; </span><span class="ql-left-eqno"> &nbsp; </span><img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-da1083ccea7900c2be04f92d98d36c0a_l3.png" height="18" width="443" class="ql-img-displayed-equation quicklatex-auto-format" alt="&#92;&#91;&#124;&#89;&#62;&#32;&#61;&#32;&#80;&#42;&#124;&#88;&#62;&#32;&#61;&#32;&#40;&#97;&#42;&#92;&#97;&#108;&#112;&#104;&#97;&#43;&#98;&#42;&#92;&#98;&#101;&#116;&#97;&#41;&#42;&#124;&#48;&#62;&#32;&#43;&#32;&#40;&#99;&#92;&#97;&#108;&#112;&#104;&#97;&#43;&#100;&#92;&#98;&#101;&#116;&#97;&#41;&#42;&#124;&#49;&#62;&#92;&#93;" title="Rendered by QuickLaTeX.com" />
</p>

Tout cela peut se traduire d&rsquo;une manière conditionnelle : Si<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-41405f9fb49bec716b983cfe64b9b7a7_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#88;&#32;&#61;&#32;&#124;&#48;&#62;" title="Rendered by QuickLaTeX.com" height="18" width="71" style="vertical-align: -4px;" /> , (<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-fde19548da081fc638b07a2e9a9d9121_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#92;&#97;&#108;&#112;&#104;&#97;&#61;&#49;&#44;&#32;&#92;&#98;&#101;&#116;&#97;&#61;&#48;" title="Rendered by QuickLaTeX.com" height="16" width="95" style="vertical-align: -4px;" />), alors l&rsquo;état<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-a5cfa515a1700b765d4eeac7becb7f5a_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#124;&#48;&#62;" title="Rendered by QuickLaTeX.com" height="18" width="30" style="vertical-align: -4px;" /> devient<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-688f6d6d790e19c7f18f5e50312c4add_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#97;&#42;&#92;&#97;&#108;&#112;&#104;&#97;" title="Rendered by QuickLaTeX.com" height="9" width="37" style="vertical-align: 0px;" /> et l&rsquo;état<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-1192c64681754a92ebc79dfdfd5e28fb_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#124;&#49;&#62;" title="Rendered by QuickLaTeX.com" height="18" width="30" style="vertical-align: -4px;" /> devient<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-6cbdcd9e7dc2e04533d614f4c4632386_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#99;&#42;&#92;&#97;&#108;&#112;&#104;&#97;" title="Rendered by QuickLaTeX.com" height="9" width="35" style="vertical-align: 0px;" /> . Si<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-2d7b29fde52b3ddad06b12087a928c1b_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#88;&#32;&#61;&#32;&#124;&#49;&#62;" title="Rendered by QuickLaTeX.com" height="18" width="71" style="vertical-align: -4px;" /> , (<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-080b4f0610f00f21ed9f41a52371e0b8_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#92;&#97;&#108;&#112;&#104;&#97;&#61;&#48;&#44;&#32;&#92;&#98;&#101;&#116;&#97;&#61;&#49;" title="Rendered by QuickLaTeX.com" height="16" width="94" style="vertical-align: -4px;" />), alors l&rsquo;état<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-a5cfa515a1700b765d4eeac7becb7f5a_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#124;&#48;&#62;" title="Rendered by QuickLaTeX.com" height="18" width="30" style="vertical-align: -4px;" /> devient<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-3b3fce557ba957682ae36ea168064cf4_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#98;&#42;&#92;&#98;&#101;&#116;&#97;" title="Rendered by QuickLaTeX.com" height="17" width="35" style="vertical-align: -4px;" /> et l&rsquo;état<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-1192c64681754a92ebc79dfdfd5e28fb_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#124;&#49;&#62;" title="Rendered by QuickLaTeX.com" height="18" width="30" style="vertical-align: -4px;" /> devient<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-d2f7a6dbae0f88308b2c833f699375b5_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#100;&#42;&#92;&#98;&#101;&#116;&#97;" title="Rendered by QuickLaTeX.com" height="17" width="37" style="vertical-align: -4px;" /> .

Lorsque l&rsquo;état de l&rsquo;ordinateur est intriqué, il suffit de suivre la démarche ci-dessus sur tous les sous-états. Prenons une exemple. L&rsquo;ordinateur possède deux qubit<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-e189ca54fe99459611787351061438c3_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#124;&#120;&#121;&#62;" title="Rendered by QuickLaTeX.com" height="18" width="40" style="vertical-align: -4px;" /> , admettons que l&rsquo;état intriqué de l&rsquo;ordinateur donne 

<p class="ql-center-displayed-equation" style="line-height: 40px;">
  <span class="ql-right-eqno"> &nbsp; </span><span class="ql-left-eqno"> &nbsp; </span><img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-a6658d602d22f669ae0518d5d9f53ee1_l3.png" height="40" width="401" class="ql-img-displayed-equation quicklatex-auto-format" alt="&#92;&#91;&#124;&#65;&#62;&#32;&#61;&#32;&#92;&#102;&#114;&#97;&#99;&#123;&#49;&#125;&#123;&#92;&#115;&#113;&#114;&#116;&#123;&#50;&#125;&#125;&#42;&#124;&#48;&#48;&#62;&#32;&#43;&#32;&#48;&#42;&#124;&#48;&#49;&#62;&#32;&#43;&#32;&#48;&#42;&#124;&#49;&#48;&#62;&#32;&#92;&#102;&#114;&#97;&#99;&#123;&#49;&#125;&#123;&#92;&#115;&#113;&#114;&#116;&#123;&#50;&#125;&#125;&#42;&#124;&#49;&#49;&#62;&#92;&#93;" title="Rendered by QuickLaTeX.com" />
</p>

Nous souhaitons appliquer sur<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-0af556714940c351c933bba8cf840796_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#121;" title="Rendered by QuickLaTeX.com" height="12" width="9" style="vertical-align: -4px;" /> la porte de Hadamard

<p class="ql-center-displayed-equation" style="line-height: 54px;">
  <span class="ql-right-eqno"> &nbsp; </span><span class="ql-left-eqno"> &nbsp; </span><img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-a45f85af534298bed02f3c29a9b7cf4e_l3.png" height="54" width="138" class="ql-img-displayed-equation quicklatex-auto-format" alt="&#92;&#91;&#32;&#72;&#32;&#61;&#32;&#92;&#98;&#101;&#103;&#105;&#110;&#123;&#112;&#109;&#97;&#116;&#114;&#105;&#120;&#125; &#92;&#102;&#114;&#97;&#99;&#123;&#49;&#125;&#123;&#92;&#115;&#113;&#114;&#116;&#123;&#50;&#125;&#125;&#32;&#38;&#32;&#92;&#102;&#114;&#97;&#99;&#123;&#49;&#125;&#123;&#92;&#115;&#113;&#114;&#116;&#123;&#50;&#125;&#125;&#32;&#92;&#92; &#92;&#102;&#114;&#97;&#99;&#123;&#49;&#125;&#123;&#92;&#115;&#113;&#114;&#116;&#123;&#50;&#125;&#125;&#32;&#38;&#32;&#45;&#92;&#102;&#114;&#97;&#99;&#123;&#49;&#125;&#123;&#92;&#115;&#113;&#114;&#116;&#123;&#50;&#125;&#125; &#92;&#101;&#110;&#100;&#123;&#112;&#109;&#97;&#116;&#114;&#105;&#120;&#125;&#92;&#93;" title="Rendered by QuickLaTeX.com" />
</p>

Soit |A> l&rsquo;état avant la porte, et |B> l&rsquo;état après. On va utiliser le choix conditionnel vu plus haut sur chaque sous-état. Pour le sous-état |00> de |A>: y est en |0>, on a donc<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-be83d27297768153917fe20cd3e871ed_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#97;&#42;&#92;&#97;&#108;&#112;&#104;&#97;&#32;&#61;&#32;&#92;&#102;&#114;&#97;&#99;&#123;&#49;&#125;&#123;&#50;&#125;" title="Rendered by QuickLaTeX.com" height="22" width="70" style="vertical-align: -6px;" /> sur l&rsquo;état |00> de |B>, et<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-dc5df11f892b0de779315896a76e3633_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#99;&#42;&#92;&#97;&#108;&#112;&#104;&#97;&#32;&#61;&#32;&#92;&#102;&#114;&#97;&#99;&#123;&#49;&#125;&#123;&#50;&#125;" title="Rendered by QuickLaTeX.com" height="22" width="68" style="vertical-align: -6px;" /> pour l&rsquo;état |01> de |B>. Ensuite les états |01> et |10> de |A> sont nuls, donc on ne fait rien. Enfin l&rsquo;état |11> de |A> : y est en |1>, on a donc<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-c564e5f53ee897e01c0c27a212778507_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#98;&#42;&#92;&#98;&#101;&#116;&#97;&#32;&#61;&#32;&#92;&#102;&#114;&#97;&#99;&#123;&#49;&#125;&#123;&#50;&#125;" title="Rendered by QuickLaTeX.com" height="22" width="68" style="vertical-align: -6px;" /> sur |10> de |B> et<img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-f2ed4cdd82dfa288a5d8f4f452bbd70b_l3.png" class="ql-img-inline-formula quicklatex-auto-format" alt="&#100;&#42;&#92;&#98;&#101;&#116;&#97;&#32;&#61;&#32;&#45;&#92;&#102;&#114;&#97;&#99;&#123;&#49;&#125;&#123;&#50;&#125;" title="Rendered by QuickLaTeX.com" height="22" width="83" style="vertical-align: -6px;" /> sur l&rsquo;état |11> de |B>. Ce qui nous donne le nouvel état :

<p class="ql-center-displayed-equation" style="line-height: 36px;">
  <span class="ql-right-eqno"> &nbsp; </span><span class="ql-left-eqno"> &nbsp; </span><img src="https://blog.jolain.net/wp-content/ql-cache/quicklatex.com-eef741515bfb0a1e765fdd718bd6ce2b_l3.png" height="36" width="415" class="ql-img-displayed-equation quicklatex-auto-format" alt="&#92;&#91;&#124;&#66;&#62;&#32;&#61;&#32;&#92;&#102;&#114;&#97;&#99;&#123;&#49;&#125;&#123;&#50;&#125;&#42;&#124;&#48;&#48;&#62;&#32;&#43;&#32;&#92;&#102;&#114;&#97;&#99;&#123;&#49;&#125;&#123;&#50;&#125;&#42;&#124;&#48;&#49;&#62;&#32;&#43;&#32;&#92;&#102;&#114;&#97;&#99;&#123;&#49;&#125;&#123;&#50;&#125;&#42;&#124;&#49;&#48;&#62;&#32;&#43;&#32;&#45;&#92;&#102;&#114;&#97;&#99;&#123;&#49;&#125;&#123;&#50;&#125;&#42;&#124;&#49;&#49;&#62;&#92;&#93;" title="Rendered by QuickLaTeX.com" />
</p>

Au final, c&rsquo;est un peu la technique que l&rsquo;on fait avec un papier et un crayon quand on fait le calcul à la main. Je vous mets le bout de code gérant les portes logiques à un qubit, il y a quelques petites choses qui changent :

  1. l&rsquo;état intriqué est stocké dans un narray de numpy `self.state`, il faut donc tout le temps convertir les indices en décimal vers le binaire. J&rsquo;utilise des tableaux de bits. Exemple : `dec2bin(5) = [1, 0, 1]`, `bin2dec([1, 1, 1]) = 7`.
  2. L&rsquo;utilisateur différencie les qubits par des lettres. On a donc un dictionnaire `self.names` qui fait la traduction nom du qubit, emplacement dans le tableau binaire. Avec notre exemple, on a `self.name = {'x' : 1, 'y': 0}`.

<pre>new_state = np.zeros(len(self.state), dtype=complex)

for i in range(0, len(self.state)) :
    binary = self.dec2bin(i)


    if binary[ self.names[n] ] == 0 :
        new_state[i] += a*self.state[i]
        binary[ self.names[n] ] = 1
        new_state[ self.bin2dec(binary) ] += c*self.state[i]

    elif binary[ self.names[n] ] == 1 :
        new_state[i] += d*self.state[i]
        binary[ self.names[n] ] = 0
        new_state[ self.bin2dec(binary) ] += b*self.state[i]

self.state = new_state 

</pre>

Le reste du programme repose sur ce procédé, et ne devrait pas poser de problème pour comprendre.