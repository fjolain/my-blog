---
title: Pour éviter les pannes, l'informatique applique le libéralisme
subtitle: L'informatique est sortie de la centralisation pour plus de résilience
date: 2021-03-15
layout: post
published: contrepoints.org/2021/03/20/393421-pour-eviter-les-pannes-informatique-applique-le-liberalisme
background: /img/2021/03/network.jpeg
categories: [article, podcast]
tags: [liberal, cloud]
---

<iframe frameborder="0" loading="lazy" id="ausha-Yoj2" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=yAdaMtzDPVOZ&v=3&playerId=ausha-Yoj2"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Un seul datacenter OVH tombe et [3,6 millions](https://www.bfmtv.com/tech/incendie-chez-ovh-3-6-millions-de-sites-web-sont-tombes-en-panne_AN-202103110385.html) de sites en ligne chutent avec lui. Comment concevoir des sites web plus résilients ?

Dans les années 2010, le web changea drastiquement. On se mit à concevoir de véritables logiciels accessibles depuis le navigateur. Des plateformes de plus en plus compliquées à développer et à déployer en datacenter. Des plateformes de plus en plus fragiles par leur taille et critiques par leurs usages.

De ces problématiques naquît le [*microservice*](https://fr.wikipedia.org/wiki/Microservices), une architecture logiciel qui n'a rien à envier aux théories libérales.

## Décentraliser les logiciels

La source de ces problèmes était la centralisation. Les sites étaient d'un seul tenant hébergés sur un seul serveur. Le moindre aléa sur le serveur ou bug dans le code et tout le site devenait hors service, comme les 3,6 millions de sites chez OVH. On décida donc de découper le logiciel en parties indépendantes, appelées *microservies*. 

Par exemple, un site marchand d'un seul bloc se retrouve découpé en micro plateformes indépendantes. Un microservice qui gère le catalogue, un qui gère le panier utilisateur, un qui gère le paiement, un qui gère le suivit de la commande, etc.

Aucun microservice est immuable, il s'en crée et détruit à chaque instant en fonction de la demande et des aléas. La plateforme est flexible, elle s'étend sur plusieurs datacenters et évolue constamment au grès des aléas. Ainsi, la plateforme Netflix, pionner dans ce domaine est composée de [1 000](https://www.geeksforgeeks.org/the-story-of-netflix-and-microservices/#:~:text=Today%20Netflix%20has%20over%20a,separate%20part%20of%20the%20site.) microservices.

![home](/img/2021/03/home.jpeg)
*séparation des tâches entre microservices pour afficher votre page d'accueil de Netflix. © Netflix*


Netflix a même érigé la résilience en art. Les ingénieurs ont conçu un [*chaos monkey*](https://fr.wikipedia.org/wiki/Chaos_Monkey), un programme *fou* qui fait exprès de débrancher des serveurs ou de supprimer des données en condition réelle pour être sûre que la plateforme est bien résiliente.

## Redonner plus d'autonomie aux parties

Ce fut déjà une révolution, mais un verrou freiner encore la résilience. Les microservices envoyaient des ordres aux autres microservices, créant une chaîne de commandement trop rigide face aux aléas. La solution fut de ne plus envoyer des ordres, mais des [*évènements*](https://fr.wikipedia.org/wiki/Architecture_orient%C3%A9e_%C3%A9v%C3%A9nements).

Avec un ordre, chaque microservice doit avoir un plan global de la plateforme pour savoir qui doit recevoir l'ordre,  et  savoir que faire si l'ordre n'est pas exécuté correctement. Un évènement évite les problèmes, vous le propagez à toute la plateforme, chaque microservice décide ce qu'il en fait. 

Ce principe par évènement ou *signal* est très présent autour de nous. La nature n'appelle pas les arbres un par un pour ordonner la floraison. Elle fait monter la température et l'ensoleillement. Chaque arbre réagit à ces évènements selon ses caractéristiques. Il n'y a que les humains pour affectionner les systèmes hiérarchiques *top-bottom*...

Chaque microservice devient autonome, il est maître de son domaine et réagit aux signaux externes. LinkedIn, première plateforme à migrer totalement vers ce type de communication traite aujourd'hui [7 billions](https://engineering.linkedin.com/blog/2019/apache-kafka-trillion-messages) de messages par jour échangés entre ses microservices.

![netflix](/img/2021/03/netflix.gif)
*fonctionnement d'une partie de la plateforme Netflix avec des microservices travaillant de concert. © Netflix*

Netflix, Amazon, Facebook ou Google, toutes les plus grosses plateformes sont composées de milliers de *microservices* : agents autonomes réagissant à des signaux externes et envoyant à leur tour d'autres signaux. Le tout s'étalant sur plusieurs datacenters et se réarrangeant au grès des aléas et de la demande. Les théories libérales appliquées à l'informatique.
