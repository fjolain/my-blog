---
title: En route vers l'hyperinflation allemande de 1923
subtitle: Un siècle après, notre situation économique ressemble à celle de l'Allemagne avant l'hyperinflation de 1923.
date: 2022-05-13
layout: post
background: /img/2022/05/mark.png
categories: [article, podcast]
published: www.contrepoints.org
tags: [société]
---

<iframe frameborder="0" loading="lazy" id="ausha-aqSo" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=b7NXvS97YGm6&v=3&playerId=ausha-aqSo"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Pénurie de [blé](https://www.francetvinfo.fr/economie/emploi/metiers/agriculture/face-a-la-penurie-de-ble-les-stocks-francais-peuvent-ils-suffire_5047768.html), de [pétrole](https://www.challenges.fr/monde/face-a-la-penurie-de-petrole-l-agence-internationale-de-l-energie-prone-une-reduction-les-transports_805363), de [gaz](https://www.midilibre.fr/2022/03/30/energie-face-au-risque-de-penurie-se-dirige-t-on-vers-des-coupures-de-gaz-ciblees-10203023.php), de [métaux](https://www.mediachimie.org/actualite/alerte-%C3%A0-la-p%C3%A9nurie-de-m%C3%A9taux) ou [d'huile](https://www.femmeactuelle.fr/cuisine/news-cuisine/penurie-pourquoi-ny-a-t-il-plus-dhuile-de-tournesol-dans-les-magasins-2132551), etc. Le tout avec une inflation de [5%](https://france-inflation.com/index.php) et une belle croissance de [0%](https://www.capital.fr/economie-politique/la-croissance-francaise-nest-pas-au-rendez-vous-de-ce-premier-trimestre-1435112). Notre situation ressemble à l'Allemagne de 1920 avant l'hyperinflation…

<h2>L'Allemagne de 1920</h2>
Durant la Grande Guerre, le gouvernement allemand [a massivement émis](https://fr.wikipedia.org/wiki/Hyperinflation_de_la_r%C3%A9publique_de_Weimar#La_guerre_et_son_impact_sur_l'%C3%A9conomie_et_la_soci%C3%A9t%C3%A9_allemande) de la monnaie pour financer sa guerre. Soit par l'émission d'obligation. Soit par la création du mark-papier à la place du mark-or, mettant fin à l'étalon-or.

Tout cet afflux de liquidités, une foi sortie des poches de l'état, est allé directement dormir sur les comptes des Allemands. La guerre est plus propice à l'épargne qu'aux dépenses des ménages. Ainsi l'inflation restait faible durant la guerre.

Ce n'est que 8 ans après la fin du conflit que cette *fausse* monnaie imprimée durant la guerre inonda l'économie allemande. Le réveil de l'argent dormant a provoqué une forte pression sur la demande qui entraîna l'hyperinflation.

Il faut aussi souligner qu'après 4 ans de guerre et d'importants tributs à payer aux vainqueurs pour réparation, l'économie allemande restait atone incapable de suivre la demande avec plus d'offres.

Nous avons donc d'un côté une demande à la hausse soutenue par le réveil d'épargnes dormantes. Et en face une offre limitée due à une longue récession de l'économie et un sous-investissement des moyens de production.

Cela a conduit à une hyperinflation. Le mark-papier à parité avec le mark-or en 1914 a finit à [1 000 000 000 000:1](https://fr.wikipedia.org/wiki/Hyperinflation_de_la_r%C3%A9publique_de_Weimar#L%E2%80%99effondrement_du_cours_du_mark) en 1923.

Or notre situation actuelle est similaire à l'Allemagne d'après guerre, mais sans la guerre...

<h2>Un sabotage de l'économie actuelle</h2>

Cela fait 10 ans que la BCE et la FED inondent l'économie de faux argents. Soit par l'émission d'obligation à [taux 0%](https://fr.wikipedia.org/wiki/Taux_directeur), soit par le [quantitative easing](https://fr.wikipedia.org/wiki/Asset_Purchase_Programme).

Pourtant comme avec l'Allemagne, cette inondation de liquidité a été [absorbée par l'épargne](https://www.banque-france.fr/statistiques/epargne-et-comptes-nationaux-financiers/epargne-des-menages/presentation-trimestrielle-de-lepargne-des-menages), limitant l'inflation ces dernières années. Même les généreux chèques donnés par Trump et Biden pour stimuler la consommation [ont finit épargné](https://www.letemps.ch/economie/aux-etatsunis-une-bonne-partie-cheques-relance-va-servir-acheter-actions) (actions, bitcoin, assurance, etc.).

Aussi la reprise économique post-covid a été l'étincelle qui mit le feu à l'inflation. D'un coup les énormes capitaux dormants inondent le marché et créent une pression sur la demande.

En soi une économie saine peut y répondre en augmentant l'offre. Mais notre économie a été sabotée par deux idéologies.

Idéologie écologique, où les bureaucrates nous chantent des lendemains sans pétrole dans lesquels la puissance d'une centrale nucléaire sera remplacée par un panneau solaire. Et au final on se retrouve [à importer du gaz de schiste](https://www.latribune.fr/entreprises-finance/industrie/energie-environnement/engie-fait-volte-face-et-se-tourne-vers-le-gaz-de-schiste-americain-malgre-l-urgence-climatique-916388.html) (très polluant) pour le liquéfier (très polluant) et le transporter par bateau (très polluant). Tout cela, on le rappelle pour éviter une centrale nucléaire...

Non content de s'attaquer à l'énergie électrique, au point que chaque hiver semble maintenant [se jouer au watt près](https://www.lesechos.fr/industrie-services/energie-environnement/crise-de-lenergie-des-groupes-electrogenes-pour-eviter-le-black-out-en-france-1400493) en France. L'écologie a aussi permis de raréfier les énergies fossiles par les taxes et freins à l'investissement. Cette situation était déjà préoccupante. Mais maintenant que l'Union européenne souhaite [tout simplement couper](https://lexpansion.lexpress.fr/actualite-economique/embargo-sur-le-petrole-russe-bruxelles-passe-aux-choses-serieuses_2172961.html) le pétrole et le gaz russe, on se dit que finalement on n’était pas à une taxe prête.

Alors, bien sûr, on se demande comment faire fonctionner toutes ces machines qui coupent du bois ou minent du fer. Je veux bien mettre un pull cet hiver pour me réchauffer sans gaz. Mais va-t-on mettre un pull sur le haut fourneau pour le [chauffer à plus de 1 200 C ](https://fr.wikipedia.org/wiki/Fabrication_de_l%27acier) afin de fabrique l'acier ?

Puis idéologie économique où seul l'état est apte à accroître le PIB par plus de dépense. Pour ce faire rien de plus simple, les taux à 0% rendent le capital caduc, il ne joue plus son rôle de moteur de la croissance par le réinvestissement des profits, puisque son rendement est à 0%. À la place, l'état s'endette toujours plus pour stimuler la consommation en espérant qu'à son tour elle stimule la croissance.

Avec les taux à 0%, la croissance est purement artificielle, elle ne reflète plus un gain de production et de richesse dans le pays, mais uniquement un gain de consommation soutenu par un endettement de l'état. Le PIB se retrouve au sommet d'une montagne de dette, éloigné de la vraie production du pays.

On se retrouve donc dans une situation semblable à l'Allemagne de 1920. L'utilisation de la planche à billets a créé une montagne de dette étatique d'un côté et de l'épargne dormant de l'autre. Le tout dans une économie atone qui ne peut pas suivre la demande.

Paradoxalement, c'est une embellie économique qui, comme en Allemagne, a déclenché le réveil de l'argent dormant et son tsunami dévastateur dans l'économie.

Pour ceux qui pensent que les politiques gèrent très mal notre monnaie, vous en avez encore une fois la preuve.

Pour ceux qui pensent que les politiques sont juste bons à ravager notre économie en démarrant des guerres inutiles. Vous pouvez constater que l'Union européen s'est contentée d'assécher notre accès à l'énergie et aux capitaux tout en inondant l'économie de son argent magique. Bien qu’efficace pour saboter notre économie, cela n’est visiblement pas suffit pour l'UE qui souhaite démarrer une guerre contre la Russie.

On se retrouve donc dans une situation semblable à l'Allemagne en sorite de guerre. À la différence que pour nous la guerre n'est pas derrière, mais devant nous.

