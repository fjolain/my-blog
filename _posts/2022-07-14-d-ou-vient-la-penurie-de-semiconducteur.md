---
title: D'où vient la pénurie de semi-conducteur ?
subtitle: Toute entreprise devient une entreprise d'objets électroniques, mais aucune d'elle ne souhaite les fabriquer
date: 2022-07-03
layout: post
background: /img/2021/10/wafer.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [internet, sécurité]
---

<iframe frameborder="0" loading="lazy" id="ausha-87Lq" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=yXzG8fKqv2Nl&v=3&playerId=ausha-87Lq"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Lors d'un [précédent article](https://www.codable.tv/comprendre-la-penurie-de-semiconducteur/), nous avons analysé la pénurie des semi-conducteurs par le prisme d'une contraction de l'offre des moyens de production.

En effet, l'industrie est passée d'entreprise comme Intel qui détient ses propres usines, ses propres moyens de production. À des entreprises comme Apple, qui n'ont pas d'usine et se tournent vers des géants asiatiques tel TSCM pour fabriquer leurs produits. Ainsi, les entreprises d'électroniques ont toutes besoin d'usine.

Or l'offre de fabrication n'est pas élastique, chaque usine nécessite des milliards de dollars d'investissement et des années de conception. La production de semi-conducteur se retrouve donc avec un goulot d'étranglement autour de Taiwan, toute la supply chain mondiale est sous tension.

L'offre est un des facteurs provoquant la pénurie, nous allons maintenant analyser les changements brutaux sur la demande en semi-conducteur.

<h2>Toute entreprise devient une entreprise d'électronique</h2>
Plus le temps passe, plus les entreprises deviennent toutes des entreprises d'électroniques. Les exemples abondent.

Une entreprise de montre dans les années 1970 était uniquement une entreprise mécanique. Puis avec la crise du quartz, l'électronique apparue dans les montres. D'abord en y ajoutant un simple oscillateur quartz et une bobine. Ensuite l'affichage digital fit entrer le premier semi-conducteur dans les montres. Enfin, nous sommes actuellement à l'ère des montres connectées. Dans cette montre nous trouvons, un microprocesseur, des antennes Wifi / Bluetooth, un écran tactile et une batterie, que des semi-conducteurs de très haute technicité.

Ainsi, une montre classique n'a besoin que de pièces mécaniques fabriquées par pleins d'entreprises horlogères. Une montre connectée à besoin de composants électroniques maîtrisés par une poigné d'entreprises asiatiques. La montre connectée a donc sa production coincée en Asie.

Pareil pour les fabricants de trottinettes, de vélos, de lampes et de tout un tas d'objets devenus connectés. Il y a encore quelques années, on avait besoin que d'acier pour faire une trottinette. Aujourd'hui, il faut une batterie (lithium et cobalt), un circuit électronique (silicium), et un moteur électrique (cuivre et néodyme). Les trottinettes modernes sont donc elles aussi coincées en Asie.

Encore un bel exemple avec les voitures. Avant même l'épineux problème des voitures électriques. Nos usines se retrouvent à l'arrêt pour moins que ça.

Les constructeurs ont changé leurs tableaux de bord à aiguille par des écrans pour faire plus jolis. Les tableaux de bord à écran se retrouvent aussi [coincés en Asie](https://www.macg.co/mobilites/2021/11/les-penuries-de-composants-depouillent-les-tableaux-de-bord-des-voitures-125345).

<h2>Que faire ?</h2>
Vu la pénurie, les constructeurs reviennent aux tableaux de bord à aiguilles, sage décision. A-t-on vraiment besoin de tous ces objets connectés ? En tant que développeur, je peux vous dire qu'ils sont remplis de failles de sécurité. Votre montre, téléviseur ou même frigo devient une menace pour votre vie privée. Sans même parler de  l'obsolescence programmée.

Heureusement, on ne sera pas obligé de revenir aux montres à gousset. Car le groupe Swatch ne connaît pas de pénurie, et pour cause toute sa production est en Suisse. Swatch a [ses propres usines](https://www.lfm.ch/actualite/suisse/swatch-em-microelectronic-fait-face-a-une-forte-demande/) pour réaliser ses circuits électroniques, c'est encore une sage décision.

Il y a peu de chance de voir des entreprises intégrées comme Intel ou Swatch redevenir la norme. Mais pour autant, on peut très bien refaire revenir de Chine des entreprises européennes comme STMicro ou Siemens, afin qu'elles fabriquent pour les autres entreprises européennes. STMicro prévoit d'ailleur de [construire une usine à Grenoble](https://www.laprovence.com/actu/en-direct/6833941/semi-conducteurs-stmicro-et-globalfoundries-vont-construire-une-usine-de-57-mds-euros-a-grenoble.html). Nous pouvons aussi, à minima, financièrement faire venir TSCM en construisant une de ses usines en Europe.

Les entreprises tendent toutes à devenir des entreprises d'électroniques, mais aucune d'elle ne souhaite gérer la fabrication de leur propre produit. La pénurie de semi-conducteurs est donc écrasée entre une offre limitée, dans les mains de quelques entreprises autour de 500km de Taiwan, et une demande exponentielle.


