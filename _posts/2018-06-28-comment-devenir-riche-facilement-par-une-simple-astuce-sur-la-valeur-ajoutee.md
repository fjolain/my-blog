---
id: 373
title: Comment devenir riche facilement par une simple astuce sur la valeur ajoutée
date: 2018-06-28T21:36:00+01:00
subtitle: Découvrez pourquoi certain sont riches et pas vous
layout: post
background: /img/2018/06/argent.jpg
categories: [article]
tags: [économie, informatique]
---
Je suis prêt à tout pour un peu de trafic, donc excusez moi du titre. On va parler de la valeur ajoutée dans cet article. Comment faire de la valeur ajoutée, peut se traduire par comment faire de l&rsquo;argent, sujet au combien tabou en France. Pourtant la valeur ajoutée est au cœur de l&rsquo;économie, qui elle-même est au cœur de la bonne santé du pays. Pour rester dans un pays prospère, il faut donc protéger cette valeur ajoutée avant de vouloir protéger le code du travail, les aides aux pauvres ou les impôts des riches. Or nous allons voir par la suite qu&rsquo;une révolution de la valeur ajoutée a déjà commencé et qu&rsquo;elle va s&rsquo;accroître dans le futur laissant l&rsquo;ancien monde sur le carreau.

## La première révolution de service

Le principe de la valeur ajoutée est simple, si vous prenez pour 10€ de tissu et que vous fabriquez une chemise revendue à 110€, alors vous venez d&rsquo;ajouter 100€ de valeur ajoutée sur le bout de tissu. En retirant les coûts (machines à coudre, table, &#8230;), les charges et les impôts vous obtenez votre salaire. La quantité de valeur ajoutée que vous apportez dans votre travail est corrélée avec votre salaire. La valeur ajoutée a déjà opéré une première révolution quand la société est passée d&rsquo;une société de produits à une société de services.

Difficile d’imaginer qu&rsquo;un entrepreneur gagnant un million d&rsquo;euros a su apporter une valeur ajoutée d&rsquo;un million d&rsquo;euros dans son travail. Écrire des mails et faire des présentations n&rsquo;ajoutent pas de valeur ajoutée diront les puristes. Et pourtant, si l&rsquo;échange de mails a permis de passer des contrats et monter une entreprise, il a bien valeur ajoutée. De même que le couturier à pris un tissu, du fil et une machine pour en faire une chemise, l&rsquo;entrepreneur a transformé de l&rsquo;argent d&rsquo;une banque inutilisée, des locaux vides et des gens sans activité en une prospère compagnie. La valeur ajoutée est telle qu&rsquo;il faudrait coudre des milliers de chemises pour l&rsquo;égaler.

Les puristes français ne comprennent pas non plus comment 5 geeks dans un garage peuvent devenir aussi riches en programmant une application.

## La révolution technologique actuelle

Il s&rsquo;agit d&rsquo;une nouvelle révolution, on passe d&rsquo;une valeur ajoutée apportée par des humains à une valeur ajoutée apportée par des programmes informatiques. Avant l&rsquo;arrivé d&rsquo;Uber, il fallait quelqu&rsquo;un pour coordonner les taxis. Cette personne était formée pour mettre en relation un client avec un taxi. Cette coordination est, comme avec l&rsquo;entrepreneur, une valeur ajoutée. Maintenant, l&rsquo;application Uber gère se service pour l&rsquo;ensemble de la ville et pour plusieurs villes dans le monde. C&rsquo;est l&rsquo;avantage de la valeur ajoutée par un programme, elle est infiniment réplicable et disponible aussitôt dans le monde entier. Cette spécificité est une aubaine pour l&rsquo;entreprise, mais un problème pour la société car elle concentre la récolte de la valeur ajoutée sur un seul acteur qui plus est étranger.

Pendant la colonisation, on achetant du cacao à bas coûts en Afrique, on opérait la grosse valeur ajoutée en France pour en faire du chocolat puis on revendait le tout cher en Afrique. La différence de gain revenait directement en France, loin du circuit économique africain. Uber fait pareil, il « achète » une main d&rsquo;oeuvre française bon marché pour conduire la voiture, ajoute 25% sur le service et le revend aussitôt à un passager français. Les 25% disparaissent immédiatement du circuit économique français. On peut donc parfaitement comparer Uber ou tout autre plateforme cloud à une seringue économique, une sorte de colonisation non pas par la mer mais par internet en plus simple et plus mondial. Pourtant le meilleur est encore devant nous.

## Intelligence artificielle et blockchain

Le meilleur est devant nous, car pour l&rsquo;instant les programmes se concentrent sur la faible valeur ajoutée. Prenons l’exemple d&rsquo;un rendez-vous chez le médecin. Aujourd&rsquo;hui, seule la prise de rendez-vous est automatisée par un logiciel, la grosse valeur ajoutée réside dans le diagnostique est encore réalisée par un humain. Mais plusieurs startups sont dans la course pour trouver une intelligence artificielle capable de diagnostiquer des maladies. Là où un nouveau médecin humain nécessite un cabinet et  7 ans d&rsquo;étude, il suffira de dupliquer le code pour avoir autant de médecins que nécessaire. La seringue sera plus grosse, une start-up chinoise pourra aspirer 25€ par consultation en Europe, Amérique et Asie.

La révolution technologique offre à n&rsquo;importe qui le moyen de créer une valeur ajoutée plus importante que n&rsquo;importe qu&rsquo;elle vente de produit. Les métiers de médecins, avocats ou journaliste sont des viviers à forte valeur ajotuée pouvant êtres automatisées par intelligence artificielle. Comme simple réservation de chauffeur, Uber est valorisé à 70 milliards d&rsquo;euros. Imaginez une startup raflant 30€ par consultation ou 1000€ par divorce !

La technologie blockchain est un autre concentrateur de valeur ajoutée, elle automatise à moindre coûts les transactions et les tiers de confiance comme huissiers ou notaires. Une startup peut aspirer la valeur ajoutée de secteur entier comme la banques, les notaires ou l&rsquo;énergie. Aujourd&rsquo;hui votre fournisseur d&rsquo;énergie gère votre consommation, il s&rsquo;occupe de relever vos compteur et d&rsquo;acheter de l&rsquo;énergie à des producteurs. Une blockchain pourrait capter toute cette valeur ajoutée en reliant directement consommateur et producteur. La startup gérant la blockchain prendrait une commission sur l&rsquo;ensemble des échanges d&rsquo;énergies d&rsquo;une ville, d&rsquo;un pays ou d&rsquo;un continent.

Pour quel gouvernement comprendrait la technologie et la notion fondamentale de valeur ajoutée, il pourrait fonder un empire. Pour les autres&#8230; il se ferait doucement coloniser numériquement. Plutôt que de toujours taper fiscalement sur les entreprises et les riches, la France devrait plutôt se demander qui crée de la valeur ajoutée, et tout faire pour qu&rsquo;ils restent dans le pays. Sans quoi elle va continuer à se faire coloniser par des entreprises étrangères comme les GAFAM faute d&rsquo;avoir retenu les entrepreneurs.