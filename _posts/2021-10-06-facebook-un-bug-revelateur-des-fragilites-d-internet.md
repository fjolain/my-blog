---
title: Facebook, un bug révélateur des fragilités d’internet
subtitle: Le DNS est un pilier d'internet, mais un pilier devenu vieux et fragilisant toute le structure.
date: 2021-10-06
layout: post
background: /img/2021/10/aiguillage.jpeg
published: www.contrepoints.org/2021/10/08/407962-facebook-un-bug-revelateur-des-fragilites-dinternet
categories: [article, podcast]
tags: [technologie]
---

<iframe frameborder="0" loading="lazy" id="ausha-zzhX" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=b5mJQfA7rKXk&v=3&playerId=ausha-zzhX"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Facebook a été hors ligne à cause d'une erreur dans la configuration de leur DNS. Voyons plus en détail ce qu’il se cache derrière.

Le Domain Name System ([DNS](https://fr.wikipedia.org/wiki/Domain_Name_System)) est un rouage essentiel d'internet qui permet de relier les humains aux ordinateurs. En effet, ce qui fait office de navigation sur internet est le protocole [TCP/IP](https://fr.wikipedia.org/wiki/Suite_des_protocoles_Internet). À chaque machine connectée à internet, est assignée une adresse IP telle que 208.76.34.154 . Cette suite de chiffres sert aux machines à s'identifier entre elles, et ainsi à faire transiter les messages à travers internet.

Ce fonctionnement est parfait pour les machines, mais inapproprié pour les humains. Il faudrait se remémorer toutes les adresses IP de chacun de nos sites web pour y accéder. On a donc mis au point le DNS, qui permet de lier un nom de domaine à une adresse IP.

Lors de votre visite sur contrepoints.org, le navigateur demande au serveur DNS, l'adresse IP liée à contrepoints.org. Dans notre cas, il s'agit de 137.74.94.12. Ensuite, le navigateur va envoyer vos requêtes au serveur de contrepoints.org.

Si maintenant, par mégarde, on supprimer la configuration DNS de contrepoints.org, le site reste intact, toujours en ligne, mais plus personne ne le retrouve sans connaître son adresse IP. Il reste fonctionnel, mais inaccessible, comme ce fut le cas pour Facebook le lundi 4 octobre.

En plus de causer des pertes à [7 milliards](https://www.cnews.fr/vie-numerique/2021-10-05/facebook-secroule-en-bourse-et-mark-zuckerberg-perd-pres-de-7-milliards-en) en cas de mauvaise configuration, le DNS a deux défauts structurels qui fragilisent internet.

<h2>Domination Américaine</h2>

Bien qu'étant un pilier d'internet, le DNS est régi par un organisme américain [ICANN](https://fr.wikipedia.org/wiki/Internet_Corporation_for_Assigned_Names_and_Numbers), qui gère les .fr, .com, .org et autres.

L'idéal serait de confier le DNS à un registre décentralisé sans tiers de confiance... aka une blockchain ! Ce fut le cas du projet [NameCoin](https://fr.wikipedia.org/wiki/Cryptomonnaie#Quelques_cryptomonnaies), un des plus vieux projets blockchain qui naquît seulement 2 ans après Bitcoin. Malheureusement, il ne décolla pas. Actuellement un autre projet : [UnstoppableDomains](https://unstoppabledomains.com/), s'annonce plus prometteur.

<h2>Aucune sécurité & vie privée</h2>

Lorsque votre navigateur demande en votre nom l'adresse IP derrière contrepoint.org, il le fait [en clair et accepte la première réponse retournée](https://fr.wikipedia.org/wiki/Domain_Name_System#S%C3%A9curit%C3%A9_du_DNS). Une requête DNS équivaut à crier en public "Où  se trouve Contrepoints ?", puis se laisser guider par le premier venu.

Ce fonctionnement permet à l'état ou à un pirate de surveiller les sites visités sur son réseau, voire d'en interdire l'accès ou encore de détourner l'utilisateur. Cet espionnage n'est nullement théorique puisqu'il est en vigueur dans [la Loi Renseignement](https://fr.wikipedia.org/wiki/Loi_relative_au_renseignement) depuis 2015.

Les navigateurs déploient depuis peu la fonctionnalité [DNS-over-HTTPS](https://en.wikipedia.org/wiki/DNS_over_HTTPS) pour resécuriser une requête DNS. Sinon l'utilisation de TOR ou d'un VPN permet aussi d'éviter cet espionnage.

*Pour allez plus loin, je vous recommande [ma dernière vidéo](https://www.youtube.com/watch?v=Ky0JBbEeSwQ) sur comment se protéger de la surveillance en ligne.*

Le DNS est donc une technologie indispensable à internet, mais aussi extrêmement bancale. Une simple erreur rend les sites inaccessibles. Son organisation est le symbole de la domination américaine. De plus, son fonctionnement est très naïf, ce qui a permis un espionnage par l'état.

Le bug de Facebook montre la nécessité de faire émerger de meilleures alternatives à cette technologie.

