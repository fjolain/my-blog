---
title: 'L&rsquo;ère de l&rsquo;individualisme'
subtitle: C'est fini la solidarité, maintenant on pense à nous !
date: 2016-08-11T14:24:53+01:00
layout: post
categories: [article]
tags: [société]
---
C&rsquo;est fini la solidarité, maintenant on pense à nous. A nous, rien qu&rsquo;à nous ! La vision selon laquelle l&rsquo;individu ne vaut pas grand-chose, mais un ensemble d&rsquo;individu comme une société représente la perfection est dépassée. Dorénavant l&rsquo;individu existe pour lui, pas pour les autres. Il n&rsquo;a plus besoin d&rsquo;appartenir à des groupes pour prendre de la valeur. Et toute une nouvelle économie s&rsquo;est créée pour extraire cette valeur présente dans chaque individu.

<!--more-->

### L&rsquo;Individualisme dans l&rsquo;économie

L&rsquo;économie a été sans doute la première représentation visible de cette nouvelle ère. Elle s&rsquo;est manifestée de trois manières différentes.  

Les personnes les plus visionnaires de notre époque sont Larry Lapage et Sergueï Brin, les fondateurs de Google. Ce sont les premiers à avoir accordé la plus grande importance à l&rsquo;individu et à sa valeur. Tout le monde connaît, les conditions de travail chez Google, chaque employé est chouchouté par l&rsquo;entreprise, il a à sa disposition de nombreux services et dispose de 25 % de son temps de travail pour lui. Ce n&rsquo;est plus la santé de l&rsquo;entreprise qui importe, c&rsquo;est celle de ses employés. Une idée folle ? Au contraire ! Un employé heureux est un individu qui va donner tout ce dont il est capable à l&rsquo;entreprise. Les 25 % de temps laissés libre ont donné lieu à des projets comme Gmail ou Google Drive.

Cesser de faire peser une administration lourde sur l&#8217;employé (ou l&rsquo;élève?), lui permet de libérer sa valeur individuelle, et tout l&rsquo;or qu&rsquo;elle contient.

Une autre manière d&rsquo;extraire l&rsquo;or de l&rsquo;individu est traduit par la phrase : si tu ne payes pas pour le produit, c&rsquo;est que tu es le produit. Pourquoi le moteur de recherche de Google est gratuit ? Car comme l&rsquo;a dit Larry Lapage en 2002 : Google n&rsquo;est pas un moteur de recherche c&rsquo;est un entreprise qui collecte les données de l’individu pour alimenter ses algorithmes d&rsquo;apprentissages. Le journaliste a dû bien rire, et pourtant c&rsquo;est vrai, avec une vision juste à plus de 10 ans. Si votre Siri peut vous répondre à la question Quel temps fait-il ?. C&rsquo;est parce que des milliards d&rsquo;individus ont tapé la question dans Google, puis ont effectué un travail de recherche sur les réponses retournées. Google a collecté vos données pour savoir quel site et où sur le site se trouvait la réponse.<figure style="width: 325px" class="wp-caption alignleft">

![captcha](https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.emerce.nl%2Fcontent%2Fuploads%2F2014%2F12%2Fcaptcha.png){:width="50%"}

Un autre exemple est le Captcha, cet outil de Google permet de différencier l&rsquo;homme de la machine. On a d&rsquo;abord eu les Captchas avec les nombres. Le premier nombre est connu de Google et permet de savoir si vous êtes humain, le deuxième provient de Google Street View et demande à l&rsquo;individu de répertorier les numéros de rue. Ensuite il y a eu les mots. De même, le premier sert à différencier, le second provient de la bibliothèque numérisée de Google et permet à Google de reconnaître les mots là où son logiciel a failli. Maintenant on a les photos ! Sur 20 photos, on vous demande de cocher les photos de chats, Google connaît le contenu de 4~5 photos, le reste des photos cochées par vous viendront alimenter son algorithme d’analyse d&rsquo;image.  

Google en plus d&rsquo;avoir vu le potentiel de l&rsquo;individu chez ses employés, l&rsquo;a aussi vu chez ses utilisateurs, et a su en tirer un gisement de richesses inépuisable.

Une dernière manière d&rsquo;utiliser l&rsquo;individu est venue avec l&rsquo;économie de partage, qui s&rsquo;est transformée en économie de rentabilité. Vous n&rsquo;utilisez pas votre appartement durant l&rsquo;été : Louer le par AirBnb ! Vous avez du temps libre et une voiture? : Devenez chauffeur Uber ! Vous avez un objet inutile ? : Vendez le par LeBonCoin !

Toutes ses choses de la vie quotidienne que chaque individu possède comme sa maison, son temps, ses vieux objets étaient vus comme valeur statique. Aujourd&rsquo;hui des compagnies vous proposent de les rentabiliser, de dynamiser vos biens et compétences. Il suffit de voir Uber pour se rendre compte que laisser l&rsquo;individu rentabiliser son temps et sa voiture peut créer la plus grande société de taxi dans le monde. Ou même, sans envie de rentabilité comme Wikipédia, laisser n&rsquo;importe quel individu écrire une partie de ses connaissances, et vous avez la plus grande encyclopédie de l&rsquo;histoire !

L&rsquo;individu est une source immense de richesse, encore faut-il s&rsquo;en rendre compte. Le système de contrôle et de cloisonnement des entreprises est le meilleur moyen de se priver de cette richesse.

### L&rsquo;Individualisme dans la société

Qu’est-ce que cela signifie d&rsquo;être Français ou Norvégien ? Plus grand-chose… les gens n&rsquo;attachent plus grande importance à leur pays natal. Il n&rsquo;y a qu&rsquo;à voir le nombre d&rsquo;expatriés dans le monde. Il y a aussi, ses loups solitaires, qui sont prêts à faire des attentats en France, alors qu&rsquo;ils ont grandi en France. La loi sur la déchéance de nationalité est juste une blague, la nationalité n&rsquo;a plus de valeur, la retirer n&rsquo;est donc plus une sanction. De même qu&rsquo;il n&rsquo;y a plus de valeur de l&rsquo;entreprise. On reste 2~3 ans dans chaque entreprise, puis on part. L&rsquo;entreprise ne compte plus, ce qui compte c&rsquo;est nous. Qu&rsquo;est-ce que l&rsquo;entreprise nous apporte ?  

A cela s&rsquo;ajoute une nouvelle vague qui pourrait tout démultiplier : le statut d&rsquo;auto-entrepreneur. Uber n&rsquo;a aucun employé. Quand vous êtes chauffeur Uber, vous êtes auto-entrepreneur. De même que l&rsquo;électricien qui vient chez vous n&rsquo;est pas votre employé, il vient juste vous donner une prestation de ses services. Vous donnez votre prestation à Uber. On voit tout de suite la simplification d&#8217;embauche, vous ne payez pas l&rsquo;assurance maladie ou la retraire de votre électricien. Pareil pour Uber, si vous êtes malade, c&rsquo;est tant pis pour vous. Si vous souhaitez une retraite, à vous d&rsquo;économiser.

Maintenant, si on se projette dans 20 ans, il pourrait y avoir un statut d&rsquo;auto-entrepreneur global sans nation. Vous travaillerez 10 mois dans le Var chez Airbus, ensuite 16 mois en Californie chez Tesla, 5 mois à Montréal, et si les temps sont durs vous finirez dans les usines de Foxconn en Chine. Une liberté qui se paye par une situation précaire, sans assurance maladie, retraite ou droit du travail.

Enfin, une autre donnée qui va dans le sens de l&rsquo;Individualisme est le nombre de célibataires et de divorces qui ne fait que croître. De même que les notions de nation ou d&rsquo;entreprise ne passionnent pas la nouvelle génération, la notion de famille disparaît également. On peut vivre en couple sans se marier et sans avoir d&rsquo;enfant. Pas d&rsquo;attache, pas de vie trop collective. Notre seule préoccupation est pour notre avenir, et non celle d&rsquo;autrui.

On assiste donc à l&rsquo;émergence d&rsquo;une nouvelle ère, tuant derrière elle le travail, famille, patrie si cher à nos politiciens. Tâche à eux de réaliser et d&rsquo;accepter le changement plutôt que de se cramponner à un système obsolète.