---
title: La Maison-Blanche dicte la Constitution suisse, la face cachée de l’imposition minimum.
subtitle: Sous couvert d’une loi populaire pour museler les multinationales si détestées, c’est en fait nous citoyens qui se trouvent museler.
date: 2024-02-06
layout: post
background: /img/2024/01/lake.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-3n4q" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=bY8zgtVOgxd4&v=3&playerId=ausha-3n4q"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

L’impôt minimum pour les multinationales est passé comme une lettre à la poste. Présenté par  l’OCDE le [18 octobre 2021](https://www.oecd.org/fr/fiscalite/La-reforme-majeure-du-systeme-fiscal-international-finalisee-aujourd-hui-a-l-OCDE-permettra-de-garantir-l-application-d-un-taux-d-imposition-minimum-de-15-pourcent-aux-entreprises-multinationales-EMN-a-compter-de-2023.htm). Elle est acceptée par votation le [18 juin 2023](https://www.efd.admin.ch/efd/fr/home/le-dff/votations/imposition-minimale-ocde.html) à 78,5% par la question « Acceptez-vous une imposition particulière des grands groupes d’entreprises (Mise en œuvre du projet conjoint de l’OCDE et du G20 sur l’imposition des grands groupes d’entreprises) ».

Outre passant la lenteur suisse, l’imposition rentre en vigueur le 1er janvier 2024, par une simple [ordonnance](https://www.efd.admin.ch/efd/fr/home/impots/imposition-internationale/mise-oeuvre-imposition-minimal-ocde.html#588682908), 6 mois seulement après la votation.

On ne s’embête plus à faire voter des lois, bien sûr que tout le monde est d’accord ! Le gouvernement y voit une rentrée d’argent indolore pour ses électeurs. Les électeurs y voient une gifle pour les multinationales et leur déplorable relation publique.

Pour autant, ce changement bouleverse la Constitution Suisse en donnant trop de pouvoir à la Confédération mais aussi à Washington.

<h2>Le Delaware n’a plus de concurrent</h2>

Commençons par analyser « projet conjoint de l’OCDE et du G20 ». Derrière l’OCDE, il y a la volonté de Joe Biden, qui [ne se cache pas](https://www.letemps.ch/economie/finance/washington-defend-un-taux-dimposition-mondial-15-entreprises) d’être l’instigateur de cette idée. On pourrait penser qu’un président démocrate voulant un impôt minimum est logique. Cependant, il faut rappeler un détail, Joe Biden avant de devenir vice-président puis président était le [Sénateur du Delaware pendant 30 ans](https://fr.wikipedia.org/wiki/Joe_Biden#S%C3%A9nateur_pour_le_Delaware).

Il a fait du Delaware, un paradis fiscal « made in usa » pour les sociétés. On peut donc avoir un doute. Pourquoi le créateur d’un paradis fiscal souhaite-t-il restreindre les paradis fiscaux ?

De plus, Joe Biden ne [va pas signer](https://www.efd.admin.ch/efd/fr/home/impots/imposition-internationale/mise-oeuvre-imposition-minimal-ocde.html#588682908) son propre accord de l’OCDE. La raison ? Il a déjà un texte de loi sur une taxation minimum des sociétés étrangères ayant une activité aux États-Unis, appelé GILTY. Et même si l’[OCDE ne compte pas GILTY](https://www.osler.com/fr/ressources/reglements/2023/l-ocde-publie-des-instructions-pour-le-pilier-deux-notamment-en-ce-qui-concerne-la-coexistence-du-r) comme un équivalent à l’imposition de 15%, la Maison-Blanche va rester avec GILTY et son imposition minimum de [13,125%](https://www.vie-publique.fr/eclairage/271437-la-lutte-contre-levasion-fiscale-internationale).

Cela rappelle les pressions étasuniennes pour boycotter les avoirs des oligarques russes. UBS a renvoyé ses clients russes pour éviter les sanctions, les révélations du Cyprus Confidential ont montré que ses anciens clients sont maintenant dans [des banques américaines](https://www.letemps.ch/economie/finance/fuite-de-donnees-cyprus-confidential-l-ironique-exil-bancaire-americain-des-oligarques-russes-ejecte-par-ubs).

Les États-Unis ont déjà commencé le démarchage. Washington a créé [une aide financière](https://www.lesechos.fr/monde/etats-unis/quest-ce-que-l-inflation-reduction-act-qui-inquiete-tant-les-europeens-1883850) pour les entreprises européennes qui souhaitent traverser l’Atlantique pour trouver une énergie moins chère.

[Macron en personne](https://www.letemps.ch/monde/emmanuel-macron-deplore-washington-mesures-economiques-super-agressives-joe-biden) était venu expliquer que ce dumping fiscal avait du mal à passer venant d’un allié qui a poussé l’Europe à boycotter le gaz russe et forcé un impôt mondial à 15%. Il est cependant reparti sans avoir obtenu la moindre concession.

Le taux d’imposition était une marge de manoeuvre vitale pour des pays comme la Suisse, ne disposant pas des mêmes ressources que l’Amérique.

<h2>Pourquoi avoir changé la constitution ?</h2>

Suite à cette votation, la constitution a vu apparaître l’article [129a](https://www.fedlex.admin.ch/eli/cc/1999/404/fr#art_129_a) sur l’imposition particulière des grands groupes d’entreprises. L’article assume pleinement la non-souveraineté des cet impôt qui « tient compte des normes et règles types internationales ».

Nous voilà maintenant avec un impôt suisse dicté par Washington avec l’OCDE comme greffier.

Mais plus problématique, ce simple article permet à la Confédération de déroger à 4 articles constitutionnels pour « préserver les intérêts de l’économie suisse », que je reprends tel quel :

1. aux principes de l’universalité, de l’égalité de traitement et de la capacité économique énoncés à l’art. 127, al. 2;
2. aux taux d’imposition maximaux prévus à l’art. 128, al. 1;
3. aux dispositions sur l’exécution énoncées à l’art. 128, al. 4, 1re phrase;
4. aux exceptions à l’harmonisation fiscale prévues à l’art. 129, al. 2, 2e phrase.

Autrement dit pour faire plaisir à l’OCDE, nous avons détourné la constitution pour donner le pouvoir à la Confédération de réclamer plus d’impôts que le maximum fixé dans la Constitution, ainsi que de permettre un traitement au cas par cas non universel.

Comme dans la votation le taux de 15% n’apparaît nulle part dans l’article. Libre à la Confédération de changer par ce qui lui plaît sans demander l’avis au peuple.

Il ne faut pas jeter bébé avec l’eau du bain. La population est pour à 78%, il y a clairement une volonté du peuple de voir les multinationales plus taxées. L’important est surtout de préciser cette loi.

Nous aurions pu voter sur « Acceptez-vous un impôt sur les bénéfices de 15% pour les entreprises faisant plus de 750 millions de dollars de chiffre d’affaires. (Impôts compatibles avec la directive de l’OCDE du 18 octobre 2021) »

Dans ce cas, les règles sont fixées par le vote et dans la Constitution. La Confédération ne peut plus déroger aux articles pour « les intérêts de l’économie suisse ».

Pourquoi vouloir une loi floue qui donne tant de pouvoir à la Confédération ? Pourquoi accepter une contrainte que les États-Unis pourtant acteur de cette directive, refusent ? Pourquoi avoir accéléré la mise en oeuvre de cette votation ?

Sous couvert d’une loi populaire pour museler les multinationales si détestées, c’est en fait nous citoyens qui se trouvent museler.
