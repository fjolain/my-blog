---
title: Comparatif des alternatives à Telegram
subtitle: Vos messages ne sont pas chiffrés sur Telegram. Après l'arrestation de son CEO, mieux vaut foutre le camp !
date: 2024-07-28
layout: post
background: /img/2024/11/comparatif.webp
categories: [article, podcast]
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-8hDE" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=yvEeWs2zQkz8&v=3&playerId=ausha-8hDE"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>


L’arrestation du CEO de Telegram permet de refaire un comparatif entre les apps de messageries. Pour ce faire nous analyserons les apps entre deux critères principaux : les fonctionnalités (groupe, appel vocal ou vidéo, etc) et la vie privée (chiffrement des messages, code auditable).

Le banc de test est composé de Telegram, Signal, Messenger, WhatsApp, iMessage et Threema.

<h2>Telegram</h2>
Autant commencer par la raison de cet article, car Telegram a certes des qualités, mais aussi des défauts.

Comme qualité, l’app regorge de fonctionnalités comme les groupes de discussion, les appels, la possibilité de créer un compte sans numéro de téléphone et de partager que son pseudo.

Comme principal défaut, Telegram joue sur une illusion de vie privée. L’app se targue d’être au maximum de la sécurité au point que tout le monde se sent protégé en l’utilisant. Mais vos messages ne sont pas chiffrés en utilisant Telegram ! L’entreprise peut les lire et maintenant qu’ils se montrent plus enclins à répondre aux sollicitations, le régulateur pourra aussi les lire.

À moins que vous n’ayez démarré un « chat secret » avec votre correspond. Le chiffrement doit être activé par l’utilisateur, il n’est pas mis par défaut. Trop peu de gens le savent. L’app est donc théoriquement sécurisée, mais en pratique personne ne le met en place…

Côté open source, là encore Telegram n’est pas parfait. L’app Telegram est open source, on peut donc regarder comment fonctionne le programme et notamment la véracité de son chiffrement. Néanmoins le code du serveur n’est pas open source. On n’a donc pas la moindre idée de ce qu’il advienne de nos messages une fois envoyés.

<h2>Signal</h2>
Signal est le principal concurrent à Telgram pour la vie privée, le chiffrement est par défaut sur tout. Le code de l’app et le serveur sont open source.

Signal pêche sur un manque de fonctionnalité, le groupe de discussion est limité à [1000 personnes](https://support.signal.org/hc/en-us/articles/360007319331-Group-chats). On ne peut pas faire d’appel visio à plusieurs. L’app nécessite un numéro de téléphone pour disposer d’un compte. On peut toutefois depuis peu, partager son pseudo au lieu de son numéro.

Une récente polémique sur la sécurité de Signal a été ouverte par Telegram. La polémique vient de [Tucker Carlson](https://www.benzinga.com/news/24/02/37385407/nothing-is-secure-tucker-carlson-accuses-nsa-of-intercepting-and-leaking-encrypted-signal-chats) qui prétend avoir était écouté par la NSA alors qu’il échangeait par Signal pour préparer son interview de Putin.

Cette polémique est un peu sulfureuse de la part de Telegram pour qui le chiffrement est optionnel alors qu’il est forcé sur Signal. Mais cette polémique rappelle deux points importants en sécurité.

Oui un code open source peut avoir des failles présentes même après des années à être scruté. Le meilleur exemple reste la faille [heartbleed](https://fr.wikipedia.org/wiki/Heartbleed) apparue sur un code open source existant depuis 2 ans et scruté mondialement.

Oui une app open source peut n’avoir aucune faille et pourtant se faire pirater. Si la NSA contrôle l’OS de l’app comme iOS ou Android, elle peut contourner le chiffrement. L’app utilise bien la mémoire du téléphone, son clavier ou l’écran. Si ces éléments sont corrompus, on peut récupérer les données en clair.

<h2>WhatsApp, Messenger, iMessage</h2>
Sur le papier ces apps proposent le même chiffrement que Signal avec autant de fonctionnalité que Telegram notamment pour les groupes de discussion.

Cependant, cela reste sur le papier, car rien n’est open source. Il faut donc croire de bonne fois l’entreprise Meta pour WhatsApp et Messenger et Apple pour iMessage. En espérant, qu’elles n’ont pas installé de portes dérobées pour casser le chiffrement.

Edwar Snowden, ancien employé de la NSA, a justement [déconseillé WhatsApp](https://www.francetvinfo.fr/internet/reseaux-sociaux/facebook/video-si-quelqu-un-au-gouvernement-s-occupe-de-questions-sensibles-et-utilise-whatsapp-c-est-une-erreur-edward-snowden-met-en-garde-edouard-philippe_3618573.html) pour cela. Selon oui, l’app dispose de porte dérobée.

<h2>Threema</h2>
Pour finir, on peut citer Threema, une app open source made in swiss avec une bonne gestion de groupe et le chiffrement. Alternative alléchante, mais l’app coûte 6€ sur les stores.

Cela reste un prix acceptable pour protéger sa vie privée. Cependant, il faut se rappeler qu’on ne parle pas seul sur une app de messagerie. Convaincre ses proches de passer sur Signal peut s’avérer compliqué, leur demander de débourser de l’argent pour Threema s’annonce impossible.

Actuellement Signal représente la meilleure alternative à Telegram en prenant en compte la limite de groupe a 1000 personnes.


