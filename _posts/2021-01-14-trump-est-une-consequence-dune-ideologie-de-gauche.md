---
title: Trump est la conséquence d’une idéologie de gauche
subtitle: Le dogme du « n'offenser personne »  nous a plongés dans une censure où seuls les extrêmes résistent.
date: 2021-01-14
layout: post
background: /img/2021/01/trump.png
categories: [article]
tags: [société, démocratie]
---

## Le titre vous choque ?
Et bien, j’en suis ravi, si nous sommes aujourd’hui face à Trump et autres extrêmes, c’est parce qu’on a pas assez choqué. Choquer est devenu interdit, la censure frappe aussitôt. Elle n’est pas étatique ni forcée par les médias. Non, elle vient du peuple, de l’opinion générale. Les réseaux sociaux ont transformé l’opinion en machine à lynchage automatisée, veillant au politiquement correct à toute heure du jour et de la nuit.

Ce lynchage vient d’une utopie de la gauche : celle que plus personne ne se fera offenser à cause de sa couleur, sa religion, son sexe ou sa sexualité. Débattre oui, mais sans choquer. Et si une personne se sent offensée, l’autre doit se taire pour éviter d’autres souffrances. Avec comme argument massue, « l’offenseur » ne peut pas se mettre à la place de la victime. **Ainsi pas besoin de procès, « l’offenseur » est toujours coupable.**

## Alors comment passe-t-on de cette utopie à notre dystopie ?
Premièrement, lorsque la définition d’offense s’élargit. Ainsi la moindre critique, caricature ou simple scepticisme devient une insulte. **Or critiquer ce n’est pas diffamer**. 

Les [Droits de L'Homme de 1948](https://www.un.org/fr/universal-declaration-human-rights/index.html) séparent bien les choses. On retrouve: *la liberté de manifester sa religion ou sa conviction* (article 18) ainsi que *le droit de ne pas être inquiété pour ses opinions* (article 19). On peut parfaitement exprimer ces idées et critiquer celles des autres. Par contre dans l'article 19 : *Nul ne sera l'objet [...] d'atteintes à son honneur et à sa réputation*. Ce qui marque un trait net entre critiquer et diffamer quelqu'un.

Deuxièmement, vu que tout le monde se sent offensé, le cercle des victimes s’élargit. On ne voit plus que son malheur et on se communautarise avec autant de communautés qu’il y a de malheurs. La victimisation à tout prix atteint des paroxysmes.

Quelques exemples : [L’Oréal a retiré le mot « blanc » et « blancheur »](https://www.lexpress.fr/actualite/racisme-l-oreal-annonce-supprimer-certains-mots-comme-blanchissant-de-ses-produits_2129409.html) de ses produits pour ne pas offenser. Les hommes s’étaleraient davantage sur un siège ([« man spreading »](https://fr.wikipedia.org/wiki/Manspreading)) comme preuve de sexisme ordinaire. Le mobilier urbain tel les tourniquets du métro seraient inappropriés aux personnes obèses et donc [grossophobe](https://www.midilibre.fr/2018/05/31/nous-vivons-aujourd-hui-dans-une-societe-grossophobe,1679357.php). [Même les poches de pantalon sont « genrées »](https://www.buzzfeed.com/fr/mariekirschen/jai-verifie-les-poches-pour-hommes-et-femmes-de-6-magasins-e#.phbllZ7DE) car plus petite chez les femmes que les hommes.

Troisièmement, les réseaux sociaux servent d’explosifs. Ils vont engendrer un effet de groupe vis-à-vis de ces communautés *victimaire*. Le groupe va se radicaliser, ne tolérant plus la moindre critique. Il ne va pas hésiter à lyncher par milliers, voire millions, tous propos contraires à son idéologie. **La société se fissure et laisse place à une logique tribale**: « soit tu es avec nous, soit tu es contre nous ».

L’exemple le plus emblématique est la communauté musulmane avec des scandales comme le défunt [Samuel Patty](https://fr.wikipedia.org/wiki/Assassinat_de_Samuel_Paty). Ou [Mila](https://fr.wikipedia.org/wiki/Affaire_Mila), jeune étudiante critiquant l’islam sur les réseaux sociaux. La communauté musulmane l’a répondu avec plus de 30 000 menaces de mort. Ou encore [April Benayoum](https://www.marianne.net/agora/humeurs/miss-france-cachez-cet-israel-que-je-ne-saurais-voir), candidate Miss France lynchée après avoir mentionné ces origines israéliennes. 

On se retrouve ainsi avec un débat interdit. Une épée de Damoclès plane au-dessus de nous. N’importe qui peut avoir sa vie gâchée, si un propos heurte la moindre communauté.


## Dans ces conditions qui peut survivre ?
Personnellement, je suis partisan modéré du vaccin covid. J’adhère à l’efficacité et la sûreté des vaccins en général. Ces positions exprimées sur Tweeter me feront lyncher par les anti-vaccins. Mais, vu le manque de recul et les forts enjeux économiques, j’émets quelques doutes sur la sûreté du vaccin Covid 19. Ces positions exprimées sur Twitter me feront également lyncher, mais par les pro-vaccins. C’est ainsi que les idées modérées disparaissent des réseaux pour ne laisser que des guerres de communautés aux visions extrêmes.

Prendre la parole sur les réseaux, c’est s’exposer à un lynchage collectif, hors de contrôle que peu de personnes peuvent endurer. Il faut une estime de soi à toute épreuve pour faire face aux insultes et menaces.

Ainsi, l’idéologie de « n'offenser personne », a légitimé le communautarisme et des réponses disproportionnées face aux critiques. Créant un monde où **seules les personnes les plus folles, les plus extrémistes, les plus imbues d’eux-mêmes comme Donald Trump peuvent exister et s’exprimer**. Les plus modérés préfèrent se taire pour éviter le lynchage. Cette idéologie a donné un paysage de clans prêts à tout pour défendre leurs dogmes dépourvus de nuance.

Il faut réinstaller le droit aux idées choquantes, le droit à déplaire. Galilée a choqué en instaurant l’héliocentrisme, Les Lumières ont choqué en critiquant la monarchie et l’Église. Lincoln a choqué en arrêtant l’esclavage. Aujourd’hui, on mélange critiquer et diffamer qui lui ne mène à rien et reste puni par la loi.

**Si les plus instruits ne peuvent plus choquer par leurs idées, les plus fous choqueront par leurs actes.**

