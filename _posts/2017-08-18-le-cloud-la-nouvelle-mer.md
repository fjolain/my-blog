---
id: 262
title: Le cloud, la nouvelle mer
subtitle: La troublante ressemblance entre le cloud et la mer...
date: 2017-08-18T14:07:15+01:00
layout: post
background: https://cloudatlas.wmo.int/images/compressed/4775_main_cumulonimbus-capillatus-praecipitatio-incus-mamma-praecipitatio_clouds.jpg
categories: [article]
tags: [cloud]
---
Le cloud ça vous parle ? C&rsquo;est le mot à la mode, il désigne les services proposés sur internet pour stocker vos documents, accéder à vos mails, écrire vos documents. Derrière ce mot se cache les GAFA (Google Apple Facebook Amazon) qui a eux seuls regroupent les principaux services. Mais le cloud c&rsquo;est aussi un nouveau marché. Étrangement ce marché me rappelle celui du transport maritime, on y gère des containers, on extrait des ressources, il y a même des poissons ! Je vous propose une immersion entre ciel et mer, la nouvelle industrie du nuage raconté par les flots.

## Les serveurs comme bateaux

> Un serveur c&rsquo;est un ordinateur taillé pour les services internet. Il a une bonne connexion internet beaucoup de stockage, énormément de RAM et plusieurs processeurs. Par contre il n&rsquo;a pas d&rsquo;écran ni d&rsquo;enceinte. Tous les fabricants d&rsquo;ordinateurs ne font pas de serveurs. On retrouve Dell, HP et Lenovo comme marques connues, mais par exemple IBM vend beaucoup de serveurs et pas d&rsquo;ordinateurs. Ou Apple qui vend beaucoup d&rsquo;ordinateurs mais ne vend pas de serveurs.

Ce qui m&rsquo;a orienté dans la comparaison internet / transport maritime, c&rsquo;est _Docker_. Docker est un logiciel qui crée des conteneurs informatiques. Un conteneur informatique est un application simple, mais autonome réalisant qu&rsquo;une seule tâche. Vous pouvez l&rsquo;exécuter sur différents serveurs partout dans le monde. Si on découpe un site internet en conteneur, on peut facilement le mettre en ligne, il suffit de mettre bout à bout ses conteneurs. L&rsquo;image de docker est justement un baleine.

![docker]({{ site.baseurl }}/img/2017/08/docker.png){:width="50%"}

Comme le nombre de conteneurs informatiques grandit, on utilise un logiciel qui va nous permettre de mieux gérer les conteneurs. Il s&rsquo;agit de _Kubernetes_, et son logo est un barre de bateau&#8230;

![kubernetes]({{ site.baseurl }}/img/2017/08/kubernetes.png){:width="50%"}

La première comparaison est évidente, les serveurs sont devenus de véritable bateaux navigants sur internet.

On se retrouve donc à manier des conteneurs aux commandes d&rsquo;une barre pour éviter que le site sombre happé par le flot de visiteurs&#8230;

## Les constructeurs de PC comme constructeur naval

![naval]({{ site.baseurl }}/img/2017/08/navale.jpg){:width="100%"}


Le cloud vous promet une liberté sans fin. Plus besoin d&rsquo;avoir des disques durs, ni d&rsquo;ordinateurs puissants, tout est en ligne. En fait, avec le cloud, les fichiers et calculs sont toujours là, ils sont faits sur l&rsquo;ordinateur de quelqu&rsquo;un d&rsquo;autre. Le fichier stocké sur un ordinateur portable d&rsquo;un particulier se retrouve stocké dans un serveur d&rsquo;une entreprise. Comme pour les chantiers navals, il y a de tout. On peut avoir un simple « bateau » (serveur) pour faire tourner un petit site. Mais ce qui a le vent en poupe ce sont les portes-conteneurs d&rsquo;internet, il s&rsquo;agit des data-centers. Un seul data center peut contenir des dizaines de milliers de serveurs et des millions de conteneurs informatiques.

## Amazon comme armateur

Comme avec le transport maritime, il y a d&rsquo;un coté les constructeurs de bateaux, et de l&rsquo;autre ceux qui veulent déployer leurs conteneurs. Au milieu, il y a les armateurs. Dans le domaine du transport maritime, la compagnie Maersk fait figure de leader avec plus de 20% de part de marché du secteur. Dans le domaine de l&rsquo;internet le leader est Amazon avec plus de 40% de part de marché. Amazon est un passage obligé pour n&rsquo;importe quelle société qui  souhaite se lancer dans le cloud. On peut également citer Google, IBM, OVH et Microsoft comme autres armateurs sur internet.

![amazon](http://www.francetvinfo.fr/image/74vvdbncu-10b8/1500/843/2072060.jpg){:width="100%"}

## Les données comme ressources.

Quelle richesse navigue sur internet ? La réponse a déjà été donnée dans un précédent article (cf. [data: nouveau pétrole, nouvelle arme](https://jolain.net/?p=115)). La richesse la plus précieuse d&rsquo;internet est la donnée. Celle que l&rsquo;utilisateur laisse derrière lui.

A la différence du transport maritime, il n&rsquo;y a pas de port sur internet. Tout le flux est échangé directement de porte-conteneurs  à porte-conteneurs (serveurs). Certains porte-conteneurs vont être conçus pour recueillir les données brutes dans ce cas, le bateau gère un site internet ou une application mobile. D&rsquo;autres porte-conteneurs vont ensuite raffiner la donnée brute pour en faire une donnée statistique à forte valeur ajoutée et donc vendable, c&rsquo;est le fameux BigData. Enfin d&rsquo;autres vont être pour le stockage des données. Ce sont des serveurs disposant de plusieurs 100 To chacun pour entreposer la donnée, une sorte de hangars flottant.

Mais les gisements s&rsquo;épuisent vite, il faut trouver de nouveau lieux de forage pour accumuler la donnée. Les sous-sols de Facebook ou Instagram ont été de très bon gisements. On en découvre de nouveau chaque jour.

![naval](http://adiac-congo.com/sites/default/files/moho_nord_1_0_fileminimizer.jpg){:width="100%"}

## Les pirates

Qui dit ressources dit pirates. Les pirates informatiques ressemblent fort aux pirates des mers. D&rsquo;abord on localise le bateau sur internet. Ensuite on l&rsquo;attaque au meilleur moment, quand il est le plus faible. Les pirates arrivent sur les serveurs, ils pillent les ressources en volant la donnée. Et, s&rsquo;ils le peuvent, ils coulent le bateau en faisant tomber le service en ligne.

De même, il existe aussi des corsaires, des pirates payés par leur état pour piller les bateaux ennemis. A l&rsquo;heure actuelle, nous avons les corsaires américains, principalement ceux de la NSA. On les a vu à l’œuvre avec le virus Stuxnet conçu contre les centrales nucléaires iraniennes. Les corsaires russes sont aussi redoutables, comme le montre le piratage des mails d’Hillary Clinton.

Les pirates informatiques utilisent aussi l’ingénierie sociale pour voler les données. Il s&rsquo;agit d’imiter le bateau d&rsquo;une grande compagnie comme Facebook, pour que des visiteurs y rentre et se fasse avoir.

![naval](http://www.pirates-corsaires.com/img3/black-pearl-queen-anne-s-revenge.jpg){:width="100%"}

## Les internautes comme poissons

Et qui sont les internautes pour finir la comparaison ? A mon avis les internautes sont les poissons d&rsquo;internet. Les sites internet qui les chassent font office de chalutiers des mers. Dans le monde d&rsquo;internet l&rsquo;afflux de poissons produit de la richesse, comme si les poissons sécrétaient du pétrole derrière eux. Les chalutiers partent en mer pour essayer de capter le flux de poissons et le rediriger vers leurs gisements.

![naval](http://boe.publiatis.com/documents/%7BED5B1581-A82A-452A-A55A-70552A0F7B67%7D/accounts/%7BCF80B3DA-C843-4BA0-8CF8-342359B239C4%7D/chalut-deverse-copie.jpg){:width="100%"}