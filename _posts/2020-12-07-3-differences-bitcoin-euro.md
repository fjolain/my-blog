---
title: 3 différences à savoir entre bitcoin et vos euros
subtitle: Le bitcoin fait maintenant partie du paysage économique, un tour d’horizon s’impose.
date: 2020-12-06
layout: post
published: fjolain.medium.com/3-diff%C3%A9rences-%C3%A0-savoir-entre-bitcoin-et-vos-euros-8ddc55a77b0f
background: /img/2020/12/bitcoin.jpg
categories: [article, podcast]
tags: [bitcoin]
---

<iframe frameborder="0" loading="lazy" id="ausha-eo01" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%233F51B5&dark=true&podcastId=oKQ8VuL3GNOJ&v=3&playerId=ausha-eo01"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

## Une monnaie décentralisée

L’euro est géré par la Banque Centrale Européenne (BCE). Elle choisit les paramètres de la monnaie (création monétaire, validation des transactions, taux directeur, etc.). La BCE ne peut pas gérer l’ensemble des utilisateurs et des transactions. Elle trie donc, au compte goûte, des entreprises capables de gérer localement l’euro en son nom, ce sont les banques. Le système repose sur la confiance en la BCE de la bonne gestion de l’Euro.

Pour le Bitcoin, toutes les règles de gestions sont publiques, figées et prennent la forme d’algorithmes. De même que n’importe qui peut participer à Wikipédia à condition de respecter les règles utilisateur. N’importe qui peut participer au registre Bitcoin (appelé blockchain) à condition de suivre les *règles-algorithme*. 

Dans le cas de Wikipédia, ce sont des humains qui participent et la vérification se fait par un examen manuel de l’article. Dans le cas du Bitcoin, ce sont de machines qui participent en exécutant les *règles-algorithme*. Ces algorithmes sont le plus souvent du cryptage d’où le nom de « crypto-monnaie », ils produisent des résultats vérifiables mathématiquement.

Ainsi, n’importe qui peut participer à Bitcoin et n’importe qui peut vérifier mathématiquement l’exactitude du travail accompli. Il n’a pas besoin de « faire confiance », la monnaie se décentralise.

## Une monnaie numérique

Posséder du Bitcoin c’est posséder des identifiants pour vous authentifier auprès du registre. C’est exactement comme votre CB, il n’a pas d’argent dans la CB, mais des identifiants pour donner l’ordre de transaction à votre banque. Vos bitcoins restent donc dans le registre en ligne. Une transaction revient à un passage de propriété au sein du registre.

L’euro aussi est quasiment numérique puisque [90%](https://fr.wikipedia.org/wiki/Masse_mon%C3%A9taire#Masse_mon%C3%A9taire_euro_%E2%82%AC) des euros en circulation sont numériques. Vous ne les détenez  pas en main, vous possédez des identifiants CB ou compte en ligne pointant vers vos euros. Les 10% restant sont les billets et pièces qui pour le coup sont bien physiques.

## Une monnaie « énergétique » au stock limité

La création monétaire de l’euro reste un mystère pour beaucoup de gens. La fameuse « planche à billets » représente une minorité d’euros créés. La majorité des euros sont créés par l’émission de [crédit par les banques commerciales](https://fr.wikipedia.org/wiki/Cr%C3%A9ation_mon%C3%A9taire#Cr%C3%A9ation_mon%C3%A9taire_par_les_banques_priv%C3%A9es) . La BCE joue indirectement sur ce deuxième tableau en fixant le taux directeur. 

La masse monétaire de l’Euro grandie sans cesse en fonction des politiques monétaires. En grossissant trop, elle peut entraîner une hyperinflation et ravager la monnaie, l’économie et l’épargne de tout un pays.

Ce cas de figure est justement un critère que Bitcoin souhaite éviter. Grâce à ses *règles-algorithme*, il existe une limite et un taux d’émission monétaire infranchissables, évitant une création monétaire trop forte.

De même que pour produire de l’or, il faut nécessairement dépenser de l’énergie en minant. Pour créer du bitcoin les participants doivent suivre les *règles-algorithme* qui obligent à de lourdes dépenses énergétiques. Encore une fois, cette dépense d’énergie est parfaitement vérifiable mathématiquement par autrui.

Ce modèle souvent pointé du doigt pour sa pollution permet de « naturellement » réguler la création monétaire. Il n’existe pas un seul bitcoin créé en dehors des *règles-algorithme* et sans avoir nécessité de lourdes quantités d’énergie.

Enfin, de même que le stock d’or est à jamais fixé, le stock de bitcoins ne dépassera jamais les 21 millions de bitcoins. Nous en sommes aujourd’hui à [18,5 millions](https://coinmarketcap.com/fr/) déjà créés.

Bitcoin est une monnaie atypique au carrefour entre l’or et internet. Ces caractéristiques permettent une diversité monétaire trop longtemps cantonnée aux seules monnaies fiduciaires.
