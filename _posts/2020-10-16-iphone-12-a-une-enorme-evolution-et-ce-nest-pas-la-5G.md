---
title: L’iPhone 12 apporte une énorme évolution et ce n’est pas la 5G
subtitle: La vague des "cartes neuronales"
date: 2020-10-16
layout: post
published: fjolain.medium.com/liphone-12-apporte-une-%C3%A9norme-%C3%A9volution-et-ce-n-est-pas-la-5g-ca9a0c4ea171
background: /img/2020/10/neuron.jpeg
categories: [article, podcast]
tags: [apple, machine learning]
---
**L’iPhone 12 permet de faire tourner des réseaux de neurones plus rapidement même sans internet et ça change tout !**

<iframe frameborder="0" loading="lazy" id="ausha-j0AH" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%233F51B5&dark=true&podcastId=BQ8gZtXa4GjD&v=3&playerId=ausha-j0AH"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

La messe est dite ! Le nouveau iPhone est sorti avec quelques améliorations : meilleur écran, meilleure photo et surtout le passage de la 4G à la 5G. Derrière ces annonces, il en a une très discrète et pourtant prometteuse. L’iPhone 12 dispose d’un [nouveau Neuronal Engine](https://www.apple.com/fr/iphone-12/specs/), aussi appelé Neuronal Process Unit (NPU).

## Qu’est-ce qu’un NPU ?

Smartphone et ordinateur disposent de plusieurs outils pour réaliser les calculs. Il y a le processeur, une sorte de couteaux suisses qui sert à tout. Mais sa flexibilité se paye par une certaine lenteur. Lorsqu’on doit exécuter constamment le même type de calcul, on  préfère créer un « co-processeur » qui est spécialisé dans ce calcul en allant très vite.

C’est ainsi que votre processeur est épaulé par une carte graphique pour l’affichage, une carte réseaux pour internet, une carte son pour l’audio, etc. 

Le NPU, lui, est spécialisé dans les réseaux de neurones. Les fameux calculs qui se cachent derrière les prouesses de l’intelligence artificielle. Ce sont des réseaux de neurones qui repèrent les objets dans les photos, trient votre boîte mail évitant les spams, déverrouillent votre smartphone par votre visage ou prédisent les films que vous allez aimer sur Netflix.

## Pourquoi Apple mise de plus en plus sur ses Neuronal Engines ?

Aujourd’hui, des applications utilisent déjà des réseaux de neurones sans NPU. Mais rien ne se passe sur votre smartphone. Tous les calculs sont faits sur un serveur distant beaucoup plus puissant que votre smartphone. Ainsi, ses applications sont constamment connectées au serveur, ils envoient vos informations et reçoivent les résultats.

Les NPU évitent de passer par le serveur, et sans le besoin d’un serveur beaucoup de choses s’améliorent.

Plus besoin de développer le code du serveur, seule l’application mobile suffit. Une entreprise pourra proposer des services à base de réseaux de neurones plus facilement. Le service fonctionne même sans connexion, une application reconnaissant les oiseaux fonctionnera même au milieu de la forêt. De plus, les NPU consomment peu. Par exemple, il devient possible lors d’un appel de faire tourner un réseau de neurones pendant des heures pour filtrer le bruit sans pour autant vider la batterie.

L’arrivée de cartes graphiques performantes sur mobiles a permis l’éclosion de jeux vidéo, un marché estimé à [60 milliards de dollars en 2019](https://www.bpifrance.fr/A-la-une/Actualites/Marche-des-Jeux-video-2019-sous-le-signe-de-la-croissance-et-du-mobile-46652). De la même manière , l’arrivée de « carte neuronale » va créer un nouveau marché dédié à l’IA.