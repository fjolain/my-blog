---
title: Les JO 2024 révéleront le naufrage du socialisme français
subtitle: Les fléaux du socialisme révélés au grand jour
date: 2023-07-12
layout: post
background: /img/2023/07/wall.png
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-M1dN" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=yg2zjtN3AKRd&v=3&playerId=ausha-M1dN"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Depuis 30 ans, la France régresse sous les jougs du socialisme. Tout semble près pour que les 4 chevaux de l'apocalypse socialiste déferlent sur les JO et montre aux yeux du monde la ruine de la France.

<h2>La bureaucratie et son copinage</h2>
La date se rapproche et les nouvelles s'enchaînent: [lignes de métro retardées pour après les JO](https://www.lemonde.fr/economie/article/2021/07/13/nouveaux-retards-pour-le-metro-du-grand-paris-qui-rate-le-rendez-vous-des-jeux-olympiques-2024_6088176_3234.html), [infrastructure sportive en retard](https://www.lemonde.fr/sport/article/2022/12/07/jo-de-paris-2024-trois-mois-de-retard-pour-la-livraison-de-l-arena_6153426_3242.html), [manque d'agents de sécurité](https://www.lemonde.fr/sport/article/2022/09/22/jo-2024-face-au-manque-d-agents-de-securite-privee-le-ministere-de-l-interieur-veut-faciliter-les-recrutements_6142692_3242.html).

Il faut dire qu'un point central du socialisme consiste à mettre les copains aux manettes et non les personnes compétentes.

Ici, Ane Hidalgo fait figure de légende vivante à hauteur de l'évènement.

Personne ne lui demande à quoi peut bien servir un adjoint [à la transition écologique](https://www.paris.fr/pages/dan-lert-7973), un autre adjoint à l'[économie sociale et solidaire](https://www.paris.fr/pages/florentin-letissier-8010). Que vient faire Audrey Pulvar comme adjointe à l'[agriculture](https://www.paris.fr/pages/audrey-pulvar-7962). Et surtout pourquoi l'adjoint [en charge des finances et du budget](https://www.paris.fr/pages/paul-simondon-5355) gère aussi les affaires funéraires...

Ni [les faux emplois](https://www.capital.fr/economie-politique/emploi-fictif-le-temoignage-qui-accuse-anne-hidalgo-1255383), ni le [détournement d'argent public](https://www.lefigaro.fr/politique/anne-hidalgo-devoile-enfin-ses-notes-de-frais-20230319), ni [ses promesses non tenues](https://www.ouest-france.fr/politique/anne-hidalgo/anne-hidalgo-renonce-a-une-promesse-de-campagne-et-augmente-la-taxe-fonciere-a-paris-21d16d1e-5eac-11ed-be29-537312e67f82), ni [les finances de Paris](https://www.lefigaro.fr/conjoncture/pourquoi-la-dette-de-la-ville-de-paris-s-envole-20210924), ne semblent remettre en cause sa place.

Si toute fois Anne Hidalgo ne suffit pas, il reste encore la bureaucratie. Nous avons bien sûr le Comité National Olympique et Sportif Français ([CNOSF](https://fr.wikipedia.org/wiki/Comit%C3%A9_national_olympique_et_sportif_fran%C3%A7ais)) branche française du Comité International Olympique (CIO). A cela s'ajoute le Comité Paralympique et Sportif Français ([CPSF](https://fr.wikipedia.org/wiki/Comit%C3%A9_paralympique_et_sportif_fran%C3%A7ais)). Sans oublier le [Conseil d'Administration](https://www.paris2024.org/fr/conseil-administration/) de Paris 2024 comprenant la ville de Paris, l'Etat, la Région Île-de-France, le Grand Paris le CNOSF et CPSF. Lui-même faisant partie du [Commité d'organisation des Jeux Olympiques et Paralympiques Paris 2024](https://www.paris2024.org/fr/nos-missions/).

Mais si vous avez bien compris aucun de cela ne manie la truelle, il faut donc la [Société de Livraison des Ouvrages Olympiques](https://fr.wikipedia.org/wiki/Soci%C3%A9t%C3%A9_de_livraison_des_ouvrages_olympiques) pour gérer les travaux. Sans oublier qu'une partie des ouvrages liés aux transports restent du côté de la [Société du Grand Paris](https://fr.wikipedia.org/wiki/Soci%C3%A9t%C3%A9_du_Grand_Paris).

Bref, on croise les doigts pour que les JO avancent malgré le copinage et la bureaucratie.

<h2>Les grèves</h2>
Les camarades de la CGT n'ont jamais loupé une occasion pour avancer la lutte des classes. Ils ne reculent ni devant les [vacances de millions de français](https://www.lesechos.fr/industrie-services/tourisme-transport/sncf-sud-rail-maintient-son-preavis-de-greve-pour-les-vacances-de-noel-1887910), ni devant les [examens d'étudiants](https://www.francebleu.fr/infos/societe/bac-2023-les-eleves-touches-par-la-greve-des-transports-la-semaine-prochaine-pourront-arriver-en-retard-2752343), ni devant [les pénuries](https://www.rts.ch/info/monde/13456492-la-penurie-de-carburant-fait-rage-en-france-le-gouvernement-requisitionne-du-personnel.html).

Les JO seront-ils une occasion trop belle pour se mettre en grève ?

Je recommande aux visiteurs d'avoir un vélo pour se déplacer durant les JO.

<h2>Manifestations</h2>
Pour aller avec la grève, rien de mieux que des manifestations. Surtout qu'en ce moment entre la retraite, l'inflation ou la hausse du prix de l'énergie, les mécontents sont nombreux.

Les gilets jaunes ne profiteraient pas de la couverture médiatique pour faire le match retour ?

Je recommande aux visiteurs un casque ainsi que des chaussures de sécurité.

<h2>Insécurité</h2>
En soi; l'insécurité dans Paris est déjà préoccupante, mais nos bureaucrates ont eu la bonne idée de faire partager l'enrichissement culturel des banlieues au reste du monde.

Le village olympique, la cérémonie ainsi que les épreuves de nage et d'athlétisme sont en [Seine-Saint-Denis](https://www.lejdd.fr/Sport/jo-a-paris-la-carte-des-sites-olympiques-3435365), le même Seine-Saint-Denis où les supporters anglais se sont fait dépouiller par les locaux.

Les épreuves de gymnastique, GRS et trampoline se tiennent à [Nanterre](https://www.lejdd.fr/Sport/jo-a-paris-la-carte-des-sites-olympiques-3435365), l'endroit même où ont eu lieu les dernières émeutes.

Je recommande un gilet par balle et une bombe au poivre pour les visiteurs.

Ces quatre calamités apportées par le socialisme pourrissent la France depuis 30 ans. Malgré cela, un certain imaginaire type [Emily in Paris](https://fr.wikipedia.org/wiki/Emily_in_Paris) tient toujours dans le monde.

Les JO de Paris risquent d'être la chute du mur de Berlin pour le socialisme français. Toutes ses erreurs, toute sa gestion néfaste seront révélées au grand jour.


