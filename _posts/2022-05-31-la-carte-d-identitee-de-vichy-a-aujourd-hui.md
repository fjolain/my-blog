---
title: La carte d'identité, de Vichy à aujourd'hui
subtitle: La vie des citoyens est de plus en plus surveillée et contrôlée par ceux-là mêmes qui accroissent le secret et l'opacité autour leurs décisions.
date: 2022-05-31
layout: post
background: /img/2022/05/panoptique.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [société, surveillance]
---

<iframe frameborder="0" loading="lazy" id="ausha-FMAE" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=oagXMs0rArjX&v=3&playerId=ausha-FMAE"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Parmi les nombreuses lubies de notre Président. Il en a une très préoccupante : sa volonté de traquer ses citoyens, car selon lui: ["dans une société démocratique, il ne devrait pas avoir d'anonymat" ](https://www.lebigdata.fr/emmanuel-macron-anonymat-internet).

La crise du covid lui a donné de nombreux prétextes à cette fin. En effet d'un côté, le citoyen voyait tous ses résultats de santé conservé et centralisé par l'état pour son bien dans [le Health Data Hub](https://fr.wikipedia.org/wiki/Dossier_m%C3%A9dical_partag%C3%A9#Le_Health_Data_Hub). Ce qui est également bien pour le gouvernement américain, puisque l'hébergeur fut Microsoft. Ce qui est bien aussi pour les pirates qui y trouvèrent un moyen simple de récupérer d'un coup les résultats de [1,4 millions](https://www.codable.tv/les-entrepots-de-donnees-reve-pour-letat-cauchemar-pour-les-citoyens/) de Français.

Le summum fut bien évidemment l'instauration d'un pass-vaccinal. Une véritable note sociale à la chinoise où le nombre de doses définit le barème d'un *bon* citoyen. Cet outil avait aussi toutes les caractéristiques techniques pour [se transformer en outils de surveillance](https://www.codable.tv/nous-sommes-a-15-jours-de-developpement-dune-dictature/) massive avec seulement quelques lignes de code.

Pendant que la vie du citoyen devient plus transparente et traçable que jamais, le gouvernement et BigPharma se sont tapis dans le secret et l'opacité la plus totale. Les résultats de Pfizer prouvant l'efficacité du vaccin [sont classés secret](https://www.bloomberg.com/graphics/2021-pfizer-secret-to-whats-in-the-covid-vaccine/). Macron a quant à lui changer ses conseils de ministres en [conseils de défenses](https://www.humanite.fr/politique/emmanuel-macron/institutions-quand-macron-met-la-democratie-sous-secret-defense-716925) afin que toutes les décisions, prises durant le covid, soient protégées par le secret défense pour 50 ans.

À cela s'ajoute [l'affaire McKinsey](https://fr.wikipedia.org/wiki/McKinsey_%26_Company#En_France_sous_le_quinquennat_d%E2%80%99Emmanuel_Macron), qui au-delà de la simple évasion fiscale, jette un grand flou sur le fonctionnement de nos ministères. En effet, comme le soulignent les sénateurs, les consultants McKinsey se retrouvent à rédiger les directives à côté des agents gouvernementaux. Partant de là, les raisons derrières chaque décision et les potentiels conflits d'intérêts deviennent opaques et peu auditables.

Nous avons donc d'un côté la vie des citoyens de plus en plus épiée, fliquée et contrôlée par ceux-là mêmes qui accroissent davantage le secret défense, le secret des affaires et l'opacité de leurs décisions.

Ainsi, il n'est plus rare de voir nos élites s'en prendre directement à des technologies comme le Bitcoin, le chiffrement ou l'anonymat en ligne par VPN ou Tor. Ces technologies permettent justement aux citoyens de conserver sa vie privée malgré la pression de l'état.

Puisque visiblement l'Union européenne s'attaque à interdire le [Bitcoin](https://journalducoin.com/bitcoin/menace-bitcoin-union-europeenne-veut-regulation-mondiale-cryptomonnaies/) et le [chiffrement](https://www.macg.co/services/2021/07/une-nouvelle-loi-europeenne-veut-la-peau-du-chiffrement-des-messageries-122913), Macron se focalise actuellement sur l'anonymat en ligne,  avec comme mission d'instaurer une [identité numérique](https://www.lemonde.fr/pixels/article/2022/05/09/cinq-questions-pour-comprendre-la-future-application-d-identite-numerique_6125353_4408996.html).

<h2>La carte d'identité, une longue histoire</h2>

Pendant plusieurs siècles en France, [un passeport interne](https://fr.wikipedia.org/wiki/Carte_nationale_d%27identit%C3%A9_en_France), un passeport d'ouvrier ou un sauf-conduit étaient nécessaires pour se déplacer sur le territoire. Ces documents ont été abolis par les républicains durant le Second Empire pour éviter la surveillance de l'état.

Ce n'est que le [27 octobre 1940](https://fr.wikipedia.org/wiki/Carte_nationale_d%27identit%C3%A9_en_France) sous Vichy que la carte d'édentée que nous connaissons a été instaurée. À la différence des passeports internes qui étaient obligatoires uniquement si l’on se déplace sur le territoire ou si l’on était étranger. La carte d'identité est obligatoire pour tous les Français.

Le but était bien sûr de surveiller les Français dans leur vie et leurs déplacements. Aussi une mention "Juif" était apposée sur le carte pour aider le gouvernement d'en sa tâche.

Ensuite, comme toujours, cette mesure *provisoire*, *exceptionnelle* et *ciblée* se pérennisa et se généralisa.

Cette envie de la part de nos dirigeants de traquer sa population est donc à l'origine de la carte d'identité. Cette volonté se perpétue avec nos dirigeants actuels (à [l'Élysée](https://www.developpez.com/actu/245458/Macron-Je-ne-veux-plus-de-l-anonymat-sur-les-plateformes-internet-le-President-francais-confirme-qu-il-veut-bien-la-fin-de-l-anonymat-en-ligne/), à [Bruxelles](https://www.macg.co/services/2021/07/une-nouvelle-loi-europeenne-veut-la-peau-du-chiffrement-des-messageries-122913) ou encore à [Davos](https://youtu.be/IJcey1PPiIM?t=406)) qui veulent pousser le contrôle d'identié en permanence et partout.

Bien que l’on contrôle déjà nos identités pour n'importe quelle démarche administrative ou lors d'un contrôle dans la rue et il n'y a pas si longtemps pour aller aux restaurants. Le contrôle n'est pas permanent dans nos vies.

Or avec l'instauration de la reconnaissance faciale, notre visage devient notre carte d'identité. Le gouvernement peut ainsi nous contrôler en permanence dans la rue, dans les transports ou dans les bâtiments.

Il reste malheureusement encore un endroit, où même avec la reconnaissance faciale, on ne peut pas contrôler votre identité : internet. Internet reste le dernier bastion de l'anonymat dans nos vies. Le gouvernement a bien sûr [légalisé le traçage](https://fr.wikipedia.org/wiki/Loi_relative_au_renseignement) continu par adresse IP sur l'internet français. Mais des logiciels comme le VPN ou Tor permettent de le contourner (cf [ma vidéo sur le sujet](https://www.youtube.com/watch?v=Ky0JBbEeSwQ)). Heureusement, le gouvernement a pensé à tout !


Grâce à la carte d'identité numérique, internet ne sera plus anonyme. L'état souhaite débuter par *certains* sites comme les réseaux sociaux et [les sites pornographiques](https://www.francetvinfo.fr/societe/pornographie/protection-des-mineurs-bientot-un-controle-d-identite-pour-acceder-aux-sites-pornographiques_2971897.html). Mais il pourra la généraliser très facilement. Par exemple en liant votre carte d'identité à votre Box internet. Ainsi chaque requête faite sur internet sera liée à votre identité numérique même si vous passez par un VPN ou Tor pour vous protéger.

Ainsi dans la rue ou sur internet, votre surveillance deviendra permanente et totale. Voilà une belle mission réservée à Bruno LeMaire, nouveau ministre du Numérique.

La vision de Macron prolonge celle de Vichy en son temps. Le "père du peuple" se doit de tout savoir de ses citoyens pour les protéger contre les ennemis de la Nation. Pendant que lui-même se drape dans le culte du secret (et de la personnalité).



