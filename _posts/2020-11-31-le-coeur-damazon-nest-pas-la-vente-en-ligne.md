---
title: Le coeur d'Amazon n'est pas la vente en ligne
subtitle: Découvrez AWS, la face cachée d'Amazon
date: 2020-11-31
layout: post
published: lesechos.fr/idees-debats/cercle/opinion-le-coeur-damazon-nest-pas-la-vente-en-ligne-1269853
background: /img/2020/11/amazon.jpg
categories: [article, podcast]
tags: [amazon, économie]
---
<iframe frameborder="0" loading="lazy" id="ausha-jCym" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%233F51B5&dark=true&podcastId=BQ8gZtqvjxaP&v=3&playerId=ausha-jCym"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

**La vente en ligne ne représente que 1/3 de ses bénéfices, découvrez « AWS », la face cachée d’Amazon qui menace l’économie française**

## Un coupable idéal

En période de crise, il est important de trouver un ennemi public : Jérôme Karviel en 2008, Amazon en 2020. L’entreprise est désignée comme unique coupable de la faillite des petits commerces. Il est loin le temps où le gouvernement accueillait le géant de Seattle [à bras ouvert](https://www.liberation.fr/france/2012/06/25/montebourg-a-amazon-je-ne-cache-pas-mon-contentement_828946).

Ce n’est pourtant pas Amazon qui a accordé des permis de construire toujours plus nombreux pour des centres commerciaux toujours plus grands en dehors des villes. Ce n’est pas Amazon, qui a décidé d’une manière arbitraire quels commerces devront fermer pendant le confinement.

Il est donc assez lâche de s’en prendre à Amazon comme seul responsable. Mais quitte à le dénoncer, autant être plus utile. Car Amazon n’est pas une entreprise de vente en ligne pour particulier. Non, Amazon est avant tout une entreprise proposant des services informatiques à d’autres entreprises, il s’agit « Amazon Web Service » ([AWS](https://aws.amazon.com/fr/) ), explication !


## AWS, la face cachée d’Amazon

Toute entreprise se doit d’avoir des serveurs informatiques pour gérer ses sites internet ou stocker ses données. Elle peut acheter le matériel et gérer son propre datacenter, ou louer un datacenter clé en main. C’est ce que propose AWS, ainsi des entreprises comme Netflix, Snapshat ou Apple ne possèdent aucun serveur, ils louent les serveurs d’Amazon à travers l’offre AWS.

On y retrouve aussi une grande partie du CAC40 : [Société Générale, Renault, SNCF, TF1, Canal + ou encore, Engie](https://www.capital.fr/entreprises-marches/decouvrez-aws-lactivite-qui-rapporte-plus-a-amazon-que-le-e-commerce-1336734), tous hébergent leurs services informatiques chez Amazon ainsi que leurs données. Or, choisir de se reposer sur le AWS pour son datacenter est un dilemme.

D’un côté, les entreprises sont obligées de suivre la course technologique (cloud, IA, chatbot, mobile, etc.) sous peine de se faire distancer par la concurrence. Dans ce cas, utiliser AWS facilite grandement l’accès aux nouvelles technologies. De l’autre côté, vous devenez très dépendant à Amazon ! Vos données deviennent totalement accessibles à lui et au gouvernement américain. 

De plus, on ne migre pas d’AWS aussi facilement qu’on migre vers Cdiscount. Quitter AWS nécessite un investissement colossal en termes de portage. Un problème de souveraineté et de dépendance économique allant bien au-delà de livraisons de colis.

Enfin, AWS est la poule aux oeufs d’or d’Amazon. Il est le leader du secteur avec [33%](https://www.lesechos.fr/industrie-services/conso-distribution/amazon-lannee-folle-du-geant-mondial-du-commerce-1167954) de part de marché, là où la vente en ligne ne représente que [20%](https://www.capital.fr/entreprises-marches/quel-est-le-poids-damazon-en-france-1384912). Il a rapporté 2,6 milliards de dollars à l’entreprise, deux fois plus que la vente en ligne (seuls les États-Unis sont rentables, Amazon continue de perdre de l’argent dans le reste du monde).

En résumé, méfiez-vous de l’offre AWS d’Amazon plus que de la vente en ligne, là ce situe le vrai danger pour notre économie.