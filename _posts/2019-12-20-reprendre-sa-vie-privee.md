---
title: Reprendre sa vie privée
subtitle: Comment se détacher des GAFA et renforcer sa cybersécurité
date: 2019-12-20
layout: post
background: /img/2019/12/big-brother.png
categories: [article]
tags: [vie privée, société, chiffrement]

---
La vie privée n’est plus acquise, nous devons la cultiver par nous-mêmes. Depuis Cambridge Analitica, j’essaye de rehausser ma sécurité et de me passer des GAFAM pour maîtriser au mieux mes données.


Malgré mes compétences en informatique, cela n’a pas été facile. Il faut bien leur reconnaître les GAFAMS sont très utiles et facile d’utilisation. J’ai passé plus d’un an à chercher des solutions.
On va voir un résumé de ce parcours, les bonnes idées et les mauvaises idées.

Pour chaque solution j’apporterai des moyens pour différents budgets (![coin]({{ site.baseurl }}/img/2019/12/coin.png){:width="30px"}) et compétences techniques (![tech]({{ site.baseurl }}/img/2019/12/tech.png){:width="30px"}).

# Réseaux sociaux & messageries
## Facebook
Pour les réseaux sociaux, ça ne sert à rien de passer par quatre chemins ou de se trouver des excuses : il faut fermer Facebook. Son principe même est de collecter vos données. On peut se dire qu’en le fermant, vous risquez de perdre des amis. C’est vrai j’ai perdu des amis… Facebook. Soit des personnes dont notre lien d’amitié ne tenait que par Facebook. Je n’ai perdu aucun ‘vrai’ ami. Facebook n’apporte rien dans l’amitié, autant le fermer.


Il existe des alternatives open source à Facebook, il y a par exemple Mastalab, un twitter open source et distribué ou encore WT:Social, mais la communauté est assez réduite dessus. Il n’existe actuellement pas de concurrent open source à Facebook.

## Messageries
Pour les messageries, on a le choix. Une petite avancée passe par l’utilisation de Whatapps, l’application de messagerie de Facebook qui est en théorie chiffrée de bout en bout.


Le chiffrement de bout en bout repose sur le fait, que tout ce qui est sur le serveur doit-être chiffré par l’utilisateur. Même l’administrateur de la plateforme ne peut pas lire les messages. Le message est chiffré puis déchiffré uniquement sur l’appareil (ordi, téléphone) de l’utilisateur. Par ce procédé, l’hébergeur est incapable d’intercepter des données, ni même de répondre à une demande de perquisition du gouvernement.

![E2E]({{ site.baseurl }}/img/2019/12/e2e.png){:width="100%"}

Pour Whatapps, je dis *en théorie*, car le chiffrement de bout en bout (End 2 End en anglais) ne peut être garanti que si l’application est inspectée pour respecter le chiffrement E2E. Comme Facebook ne divulgue pas le code source de l’application Whatapps, personne ne peut savoir si Facebook ne fait pas une copie en claire avant de chiffrer. D’ailleurs, Snowden ne semble pas trop aimer Whatapps. À la place, il préfère Signal.


[Signal](https://signal.org) est aussi une application de messagerie chiffrée E2E, mais open source ! De plus Signal envoie des messages chiffrés si le destinataire à également Signal, sinon il enverra un simple SMS. On peut donc mettre Signal comme app SMS par défaut, il choisira par lui-même le meilleur moyen de communiquer avec votre destinataire.

![signal]({{ site.baseurl }}/img/2019/12/signal.png){: .center-image height="150px"}



[Telegram](https://telegram.org) est aussi une application de messagerie chiffrée E2E open source. Elle propose beaucoup plus de fonctionnalité que Signal comme les GIF, les bots ou les stickers. Par contre, seuls les gens de Telegram peuvent communiquer entre eux.

![telegram]({{ site.baseurl }}/img/2019/12/telegram.jpg){: .center-image height="150px"}


## Mails
Pour les mails, le meilleur choix est [Protonmail](https://protonmail.com). Il s’agit d’une entreprise suisse qui propose des boîtes mail hébergées en Suisse (loin du Patriot Act ou du Cloud Act) et totalement chiffrées. L’application chiffre automatiquement les échanges entre deux utilisateurs Protonmail. Si une des personnes n’est pas dans Protonmail, l’hébergeur dispose de palliatifs:

- le mail sera envoyé/reçu en clair dans le serveur de Protonmail. Protonmail s’engage à aussitôt le chiffrer pour vous. Ainsi aucun mail n’est stocké en clair.
- Vous pouvez aussi chiffre le message avec un mot de passe. Le destinataire recevra un mail contenant un lien vers une page où il pourra lire le message et y répondre en introduisant le mot de passe. C’est un peu fastidieux, mais ça permet de conserver un chiffrement E2E.

![protonmail]({{ site.baseurl }}/img/2019/12/protonmail.jpg){: .center-image height="150px"}


![coin]({{ site.baseurl }}/img/2019/12/coin.png){:width="30px"} : Protonmail propose une offre premium 4€/mois avec plus de stockage, et le fait de pouvoir lier le mail à votre nom de domaine. Par exemple si vous avez monsieur@protonmail.com, n’importe qui peut essayer de vous usurper en créant un mon**s**sieur@protonmail.com. Alors que si vous utilisez votre propre nom de domaine personne ne peut vous usurper monsieur@mondomain.fr


![tech]({{ site.baseurl }}/img/2019/12/tech.png){:width="30px"} : Pourquoi ne pas utiliser directement des clés GPG ?

C’est ce que j’ai fait au début et le résultat est mitigé. Quand tout va bien, vous êtes au sommet du mouvement cypherpunk : un échange chiffré, une indépendance totale, une création DIY. Sauf que c’est impraticable dans 99% des cas:

- **Personne n’utilise GPG** C’est trop technique pour monsieur Tout-le-Monde. Vous avez peut-être une chance de convaincre vos proches de passer à Protonmail, mais aucune pour qu’ils utilisent GPG
- **Accessible nulle part** Pour utiliser GPG, il vous faut un ordi avec Thunderbird et Enigma (ou Mail chez Apple), il faut aussi installer les clés sur la machine. Cela interdit l’utilisation par smartphone ou par navigateur.
- **Le meilleur ou rien** Au final soit vous êtes sur votre ordi à discuter avec un autre geek. Dans ce cas sécurité maximale. Soit vous êtes dans les 99% autres cas à envoyer des mails en clair parfaitement lisibles par votre hébergeur.

# Naviguer sur internet

## Eviter d'être tracké
Pour éviter de se faire espionner, il existe plusieurs solutions:
- On peut passer par un VPN comme [ProtonVPN](https://protonvpn.com) (la même entreprise que Protonmail). Le VPN va permettre de passer par un canal sécurisé pour sortir de votre box incognito. Attention toutefois, il faut avoir une foi absolue dans l’hébergeur qui peut vous aiguiller comme il veut.


- On peut passer par [TOR](https://www.torproject.org/), un protocole qui fera rebondir votre requête partout dans le monde vous rendant intraçable. TOR est toutefois assez lent. De plus, comme avec le VPN, une personne mal intentionnée sur TOR pourrait essayer de mal vous aiguiller.


## Choisir un bon navigateur
Un navigateur propriétaire comme Chrome est la pire chose à faire. Il est aux premières loges pour tout siphonner. Le maître des navigateurs open source est Firefox de Mozilla. Personnellement, j’utilise aussi [Brave](https://brave.com) auquel je lui trouve plusieurs qualités:

- il a la même base que Chrome. On a donc tous les avantages de Chrome sans l’espionnage.
- il possède un mode TOR en un click très pratique
- Il stoppe par défaut tous traqueur, pub et cookies.

![brave]({{ site.baseurl }}/img/2019/12/brave.jpg){: .center-image height="150px"}

## Clé U2F
Le Second Facteur Authentification (2AF) est rapidement devenu un atout pour la sécurité. En plus de votre mot de passe, certains sites vous demandent une seconde étape. Il peut s’agit d’un code envoyé par SMS ou d’un code généré quelque part. Il peut aussi s’agir de connecter votre clé USB U2F. Dans un cas, vous devez sortir votre téléphone, le déverrouiller, attendre le SMS, ouvrir le SMS, puis taper le SMS sur le site. Dans l’autre vous branchez votre clé USB et c’est fini ! Pensez-y.
On peut trouver ce genre de clé pour [~15€](https://www.amazon.com/Thetis-Universal-Authentication-Protection-SalesForce/dp/B06XHTKFH3/ref=sr_1_5?keywords=U2F&qid=1576967723&s=electronics&sr=1-5).

![coin]({{ site.baseurl }}/img/2019/12/coin.png){:width="30px"} : la meilleure reste la [Yubikey](https://www.yubico.com/products/) pour ~50€

![tech]({{ site.baseurl }}/img/2019/12/tech.png){:width="30px"} : la Yubikey fait aussi office de smartcard pour stocker vos clés GPG, comme celles de mails par exemple ;)

![yubikey]({{ site.baseurl }}/img/2019/12/yubikey.jpg){: .center-image height="150px"}

## Paiements en ligne

Avec les Néo banques, il est maintenant ultra simple de créer des comptes en banque et de les gérer depuis son téléphone. Ca tombe bien on va pouvoir encore séparer les usages ! J'utilise deux banques : Boursorama qui gère mon compte courant avec un carte de crédit qui ne quitte jamais la maison. Et j'ai Revolut pour ma banque de tous les jours avec sa carte de crédit.

[Revolut](https://www.revolut.com/) joue le rôle d'un sas entre mon gros compte en banque et la vie courante. Il n'y a que quelques centaines d'euros dessus. Ainsi je peux me faire voler ma carte de crédit sur internet ou dans la rue, le voleur ne pourra jamais remonter à mon compte en banque principal.

![coin]({{ site.baseurl }}/img/2019/12/coin.png){:width="30px"} : Revolut propose aussi des cartes virtuelles payantes spécialement faites pour payer sur internet, afin de séparer encore plus les usages.

# Choisir son ordinateur et son smartphone
À notre époque, choisir un ordinateur et un smartphone sécurisé devient compliqué. Des solutions existent analysons les !

## Ordinateur
Bien évidemment, je ne vais pas vous conseiller Windows. La sécurité, tout comme la qualité du logiciel laisse vraiment à désirer. D'un autre côté, Ubuntu, une distribution Linux grand public open source, a bien des avantages. Le système est un modèle de fluidité et de longévité. Avec Ubuntu, dites adieu aux virus, bug et à l'obsolescence programmée.

Les logiciels disponibles sont également open source. Vous pouvez ainsi disposer d'un ordinateur parfaitement fiable, transparent sur son fonctionnement, le tout gratuitement.

Le gros désavantage d’Ubuntu réside aussi dans ses logiciels disponibles. Vous ne pouvez pas utiliser des logiciels comme Words, Outlook ou Acrobat Reader. Il existe bien sûr des équivalents open source : LibreOffice pour Office, Thunderbird pour Outlook ou PDFViewer pour Acrobat Reader. Cependant les équivalents ne sont jamais pleinement compatibles ni aussi productifs.

Heureusement ce fossé entre Windows et Linux tente de se combler. D'une part, car beaucoup d'applications tournent maintenant dans le navigateur indépendamment de l'OS utilisé. Par exemple, Office dispose maintenant de Office360 pour profiter de Words et autres directement dans le Cloud. Dans le monde du développement, il existe aussi des outils pour concevoir d'une traite un programme pour Windows, macOS ou Ubuntu, éliminant ainsi l'ancienne obligation de devoir recoder pour chaque système.

L'utilisation d'Ubuntu pour un particulier qui passe son temps sur Gmail, Netflix ou Facebook est réalisable. Le logiciel est gratuit, ça ne coûte rien d'essayer.

![Ubuntu]({{ site.baseurl }}/img/2019/12/ubuntu.png){: .center-image height="150px"}

## Android
Dans le monde d'Android, les grandes marques comme Xiaomi ou Samsung sont à proscrire. Toutes vos données partent directement vers l'Asie.

Il existe en théorie des smartphones Android sécurisés comme le BlackPhone ou BlackBerry. Entre la théorie et la pratique, il y a quelques ajustements. J'ai personnellement opté pour un BlackBerry Priv depuis 3 ans et le constat est décevant :

- **Pas de mise à jour** Les mises à jour Android dépendent du bon vouloir du fabricant. Avec les petits fabricants, ça se transforme vite en silence radio. Visiblement, dans mon cas, un bug interne empêche toute mise à jour de l'appareil, et ce depuis l'achat.

- **Une dépendance forte à Google** Le compte Google est obligatoire pour installer des applications à partir du Play Store. Ce n'est que le début. Ensuite vous utiliserez Calandar, Gmail, Maps, Photo... Vous ne pouvez rien faire sans passer par Google !

- **Une myriade d'acteurs** Le harware provient souvent d'une entreprise chinoise en marque blanche. Ensuite, Google vient apporter son OS, BlackBerry ajoute juste sa surcouche. Enfin, vous aurez besoin d'une myriade d'applications tierces plus ou moins douteuses pour utiliser la lampe torche, prendre des notes, prendre des rendez-vous, utiliser la boussole.

Au final, en vue des failles non-patchées et de la ribambelle d'acteurs gravitant dans votre téléphone, la sécurité vantée par le fabriquant est quelque peu illusoire sur Android.

## Les cas Apple
![coin]({{ site.baseurl }}/img/2019/12/coin.png){:width="30px"} : Historiquement contre Apple, j'ai revu mes positions. Tim Cooks suit des choix prometteurs : vendre du haut de gamme, mais pas du luxe ! Je n'ai rien contre payer un bon produit plus cher que les autres, mais Apple était parti dans des sphères stratosphériques avec des Iphones à 1500€ et des macs à 3000€, impensables... D'autre part, Tim Cooks prend la sécurité et la vie privée très au sérieux. Tout cela est peut-être du vent. Cependant, à bien y regarder, Apple a tout ce qu'il faut pour y réussir.

- **Un business model compatible** A la différence de Google et de Facebook, Apple vend des produits et services, non des données. De plus, il utilise maintenant la sécurité comme argument marketing pour justifier son extravagante marge. Apple a donc tout intérêt économiquement à garder les mains propres pour conserver ses marges.

- **Un écosystème contrôlé** C'est bien connu Apple est un univers fermé. Pour la sécurité, c'est un avantage. Du processeur, à la carte mère en passant par l'OS jusqu'à l'application de messagerie tout est Apple. Il n'y a pas d'acteurs tiers douteux. Cela permet aussi de rajouter des features bien commodes comme des puces sécurisées pour le stockage des clés de chiffrement.

- **Du chiffrement** Justement, j'aime bien le chiffrement et *en théorie* Apple en met partout : les messages sont chiffrés de bout en bout, la mémoire est aussi chiffrée, l'historique de géolocalisation est chiffré... Je continue de dire en théorie, car, comme Whatsapp, en dépit de code open source, le chiffrement n'est qu'une promesse.

C'est fini pour cette partie. Comme prévenu, les solutions néophytes et économiques n'existent pas. La solution la plus crédible reste Apple, loin de notre souhait de quitter les GAFAM...

Je vous retrouve pour la dernière partie pour construire son infrastructure sécurisée chez soi.

![Apple]({{ site.baseurl }}/img/2019/12/apple.jpg){: .center-image height="150px"}


# Protéger son infrastructure perso

## Relais TOR

![tech]({{ site.baseurl }}/img/2019/12/tech.png){:width="30px"} : En plus des points avant, vous pouvez aussi installer votre propre noeud TOR. D’une part vous participez à ce beau réseau qu’est TOR. Mais aussi vous engendrez ainsi un trafic chaotique sur votre box rendant votre navigation plus compliqué à filtrer. Un ordinateur à 40$ comme une [raspberry](https://www.ldlc.com/fiche/PB00246555.html) pi peut suffire.

![coin]({{ site.baseurl }}/img/2019/12/coin.png){:width="30px"} : un vrai ordi comme un nuc ou autres fanless à [440€](https://www.ldlc.com/fiche/PB00260502.html) ne sera pas du luxe pour faire tourner TOR.

## IoT
On peut continuer à être bien sécurisé tout en utilisant des gadgets . Il faut faire attention au potentiel d’espionnage de l’objet. Par exemple, une enceinte connectée ? C’est non direct ! Elle est équipée d’un micro connecté à internet. Comme son but est justement de nous écouter et d’envoyer la séquence, on ne peut pas savoir si elle nous écoute sur notre ordre ou à notre insu.


Un autre exemple, un bracelet connecté ? La Mi Band 3 de Xiaomi est inoffensif, pas de micro, seulement du Bluetooth. Mais la Mi Band 4 a maintenant un micro et le wifi, donc c’est non, elle pourrait nous espionner. Pour l’aspirateur Xiaomi il a certes le wifi, mais pas de micro ni de webcam. On peut parfaitement auditer l’appareil et vérifier son comportement.


L’autre piège survient dans l’application pour contrôler l’objet. D’une manière générale, aucune application n’est inoffensive. Juste une vérification sur les permissions demandées par l’application Xiaomi MiHome pour voir qu’elle pompe toutes les données. La solution que j’ai retenue est de séparer les usages. J’utilise mon téléphone de tous les jours avec mes localisations, mes photos, contacts et autres sans la moindre application pirate. Puis je réutilise mon ancien téléphone, remis à zéro, comme hub pour mes objets connectés et les applications louches.


![coin]({{ site.baseurl }}/img/2019/12/coin.png){:width="30px"} : Vous pouvez aussi acheter un deuxième router pour avoir deux wifis différents. Le wifi principal est le saint, le sous wifi est pour les objets connectés et le hub. Ainsi un objet qui tenterait de sniffer le réseau ne trouverait rien à se mettre sous la dent.

![tech]({{ site.baseurl }}/img/2019/12/tech.png){:width="30px"} : Pour ceux qui ont installé le noeud TOR, vous pouvez réutiliser le serveur, pour en faire un hotspot wifi.


## Rapatrier son Cloud
On peut très bien avoir du cloud chez soi ! Pour cette partie, je n’ai pas trouvé de méthode économique et  néophyte.

![tech]({{ site.baseurl }}/img/2019/12/tech.png){:width="30px"} : Je me suis d’abord tourné vers l’open source avec la solution [NextCloud](https://nextcloud.com). NextCloud est une plateforme cloud open source à installer chez vous. Elle fonctionne par plug-in pour faire du traitement de texte, du multimédia et autres. J’ai eu plusieurs déboires avec cette solution, dont plusieurs bugs lorsque je voulais chiffrer les données ou faire des mises à jour. Sur les derniers mois, j’étais même incapable d’utiliser les clients Android et Ubuntu, car ils bugaient.

![nexcloud]({{ site.baseurl }}/img/2019/12/nextcloud.png){: .center-image height="150px"}


Au final bien utilisé,  NextCloud devient un véritable cloud open source. Mais ça demande un investissement en temps pour la maintenance. Avec tous mes problèmes, j’avais peur même de perdre mes données, je faisais donc une copie vers Google Drive…

![coin]({{ site.baseurl }}/img/2019/12/coin.png){:width="30px"} : Maintenant, je suis passé sur un [NAS Synalogy](https://www.ldlc.com/fiche/PB00238226.html). Certes ce n’est plus open source, mais la solution est clé en main ! On branche le NAS chez soi, et aussitôt on profite de pleins de fonctionnalités cloud : drive, serveur multimédia, serveur mail, traitement texte en ligne … . Ses fonctionnalités rivalisent avec Google Drive !

![nas]({{ site.baseurl }}/img/2019/12/synalogy.jpg){: .center-image height="150px"}

En conclusion pour notre réseau, on peut résumer par ce schémas.

![network]({{ site.baseurl }}/img/2019/12/network.png){: .center-image width="100%"}


## Clé USB chiffrée
Il y a souvent plein d’informations personnelles sur vos clés USB. Parfois même, la clé USB est accrochée sur votre porte-clés et un document dedans (facture, cv …) contient votre adresse.
Le mieux est donc de n’utiliser que des clés chiffrées. Pour ce faire rien de plus simple le logiciel [Veracyrpt](https://www.veracrypt.fr/) permet justement de faire ce boulot.

![coin]({{ site.baseurl }}/img/2019/12/coin.png){:width="30px"} : VeraCrypt suffit, mais il faut VeraCrypt pour déchiffrer… logique. Or, si vous utilisez votre clé USB chez un ami, il n’aura pas VeraCrypt vous devrez l’installer. Pour ~50€, il existe des clés USB avec chiffrement matériel. La clé possède un digipad et chiffre elle-même les données. Cela évite de dépendre de VeraCrypt et contourne aussi les KeyLoggers.

La marque [IStorage](https://istorage-uk.com/) vend ce type de clé.

![istorage]({{ site.baseurl }}/img/2019/12/istorage.jpg){: .center-image height="150px"}


## Harware Wallet
Enfin pour les bitcoiner et autres, je vous recommande vivement de stocker vos assets dans un harware wallet, comme le [Ledger nano](https://www.ledger.com/).

![ledger]({{ site.baseurl }}/img/2019/12/ledger.webp){: .center-image height="150px"}



# Conclusion
Des solutions existent pour retrouver sa vie privée, elles mélangent du chiffrement et du bon sens. Il ne faut pas voir internet comme le mal incarné, il permet d’innombrables facilités. Mais oui, si vous ne prenez pas le temps de maîtriser la bête, d’autres le feront pour vous, ou plutôt contre vous.
