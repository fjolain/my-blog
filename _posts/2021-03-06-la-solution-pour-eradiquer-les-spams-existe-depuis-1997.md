---
title: La solution pour éradiquer les spams existe depuis 1997
subtitle: Mais personnes ne l'utilise, sauf la plus célèbre des crypto-monnaies
date: 2021-02-18
layout: post
background: /img/2021/03/spam.jpg
categories: [article]
tags: [société, informatique]
---

Dans le dernier numéro d’Usbek & Rica (03/2021), un article parle du fléau des spams. On apprend notamment que Bill Gates a échoué à éradiquer les spams. Pourtant une méthode existe depuis 1997.


Ce qui rend le spam possible, c’est l’incroyable rapidité d’envoi d’un message, quelques microsecondes. La solution [Hashcash](https://en.wikipedia.org/wiki/Hashcash), proposée en 1997, est donc très simple : il faut ralentir l’envoi d’un mail.

Pour ce faire, un mail est considéré comme valide, si l’ordinateur de l’expéditeur résout un petit jeu mathématique. Le jeu n’a aucun intérêt si ce n’est de perdre du temps. La solution du jeu sert de preuve de travail (Proof of Work).

Ainsi, le destinataire d’un mail commence par vérifier la preuve de travail avant d’accepter le mail.

Ce dispositif est indolore pour les particuliers. Si nous fixons la difficulté du jeu mathématique à ~1 seconde de calcul. Alors les mails de tous les jours sont envoyés dans la seconde. Mais si quelqu’un souhaite spamer un million de destinataires, il devrait calculer pendant 11 jours consécutifs pour tout envoyer.

Le problème du Hashcash est l’hétérogénéité des solutions mails. Entre Outlook qui a accès aux ressources du PC (processeur et carte graphique), et Gmail qui ne s’exécute que dans le navigateur, les deux ne disposent pas de la même puissance de calcul. Fixe-t-on une difficulté élevée, et tous les utilisateurs Gmail sont ralentis ? Ou fixe-t-on une difficulté basse, mais n’importe qui avec un bon PC peut de nouveau spamer ?

Bien que ce système ne fut pas adopté pour les mails, il resurgit 10 ans plus tard avec Bitcoin qui l’utilise à merveille pour orchestrer sa blockchain entre tous ses participants.