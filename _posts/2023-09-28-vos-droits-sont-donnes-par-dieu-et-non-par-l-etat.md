---
title: Vos droits sont donnés par Dieu et non par l'État.
subtitle: Un État ne sert qu'à protéger nos droits naturels.
date: 2023-09-14
layout: post
background: /img/2023/09/lacroix.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-LJRY" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=yOQnru3pZGr3&v=3&playerId=ausha-LJRY"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Commençons par un rappel, si [la constitution suisse](https://www.fedlex.admin.ch/eli/cc/1999/404/fr) commence par

```
Au nom de Dieu Tout-Puissant!
Le peuple et les cantons suisses,
conscients de leur responsabilité envers la Création,
[...]
arrêtent la Constitution que voici:
```

C'est pour rappeler un point fondamental sur nos droits en tant qu'Européens des Lumières.

Ce que l'on nomme "droits naturels" ne sont pas donnés par l'État, mais par Dieu. Ou plus généralement par ce que vous considère comme transcendant à l'humain comme Allah, Eli, la nature, l'humanité ou Gaia.

Le rôle du gouvernement est de garantir que personne ne vienne nous retirer de cet état naturel qui donne le meilleur de l'humanité. Ce qui est parfaitement décrit dans [la Déclaration des droits de l'homme de 1789, article II](https://www.elysee.fr/la-presidence/la-declaration-des-droits-de-l-homme-et-du-citoyen) :

```
Le but de toute association politique est la conservation des droits naturels et imprescriptibles de l'homme. Ces droits sont la liberté, la propriété, la sûreté et la résistance à l'oppression.
```

L'état ne nous donne pas de droits, nos droits sont acquis dès la naissance en tant qu'être humain.

Nous ne devons rien à l'État, rien aux bureaucrates, rien aux députés. Par contre le peuple peut et doit destituer son État dès lors qu'il ne protège plus ses droits fondamentaux.


Un État qui bafoue nos droits fondamentaux ne sert plus à rien et devient illégitime. Ce qui est là aussi parfaitement écrit dans [la déclaration d'indépendance des États-Unis de 1776](https://fr.wikipedia.org/wiki/D%C3%A9claration_d%27ind%C3%A9pendance_des_%C3%89tats-Unis) :
```
Lorsque, dans le cours des événements humains, il devient nécessaire pour un peuple de dissoudre les liens politiques qui l'ont attaché à un autre et de prendre, parmi les puissances de la Terre, la place séparée et égale à laquelle les lois de la nature et du Dieu de la nature lui donnent droit.
```

Maintenant place aux exemples.

<h2>Macron et le harcèlement</h2>

Macron ne se cache pas de son dégoût pour l'anonymat en ligne sous prétexte de lutter contre la haine et le harcèlement. [Il disait](https://www.euractiv.fr/section/plateformes/news/emmanuel-macron-se-prononce-a-nouveau-pour-une-levee-de-lanonymat-sur-internet/) même récemment : "Dans une société démocratique, il ne devrait pas y avoir d’anonymat.". Il a eu la volonté de censurer les réseaux sociaux avec [la loi Avia](https://fr.wikipedia.org/wiki/Loi_contre_les_contenus_haineux_sur_internet), stoppée par le Conseil Constitutionnel. Cette tentative [a été remise sur la table](https://www.leparisien.fr/politique/la-majorite-enclenche-la-bataille-contre-lanonymat-sur-internet-18-09-2023-2WJWUYUMZVFVHMJ5GL7CKGX2TQ.php) durant les émeutes.

Macron pousse à un identifiant unique en ligne, projet déjà en courts avec [France Connect](https://fr.wikipedia.org/wiki/FranceConnect). L'état intimide tant tôt [les sites pornographiques](https://www.lemonde.fr/pixels/article/2023/07/07/blocage-de-sites-porno-ou-en-est-l-experimentation-d-outils-de-verification-d-age-en-double-anonymat_6180902_4408996.html), tant tôt [les réseaux sociaux](https://www.nouvelobs.com/economie/20230921.OBS78461/loi-sur-la-securite-numerique-comment-le-gouvernement-veut-instaurer-des-controles-d-identite-sur-internet.html).

Maintenant, il souhaite contrôler notre navigation soit en [interdisant le VPN](https://www.clubic.com/pro/technologie-et-politique/actualite-484939-comment-le-gouvernement-veut-brider-les-vpn-en-france.html), soit en forçant la censure directement [dans votre navigateur](https://foundation.mozilla.org/fr/campaigns/sign-our-petition-to-stop-france-from-forcing-browsers-like-mozillas-firefox-to-censor-websites/?sp_ref=817319645.352.227468.t.691190.2&source=twitter).

Rappelons que l'État espionne tout l'internet français depuis [la loi renseignement](https://fr.wikipedia.org/wiki/Loi_relative_au_renseignement) de 2015.

Dans le même temps, la République en Marche collectionne les membres sulfureux. Comme [Avia](https://fr.wikipedia.org/wiki/Laetitia_Avia#Pol%C3%A9miques_et_affaires_judiciaires) accusée de harcèlement et de raciste au travail, ainsi que d'avoir mordu un chauffeur de taxi. Nous pouvons aussi citer [toutes ces personnes du gouvernement](https://fr.wikipedia.org/wiki/Laetitia_Avia#Pol%C3%A9miques_et_affaires_judiciaires) inculpées de harcèlement, viols ou prise illégale d'intérêts : Chrysoula Zacharopoulou secrétaire d’État à la Francophonie, Catherine Colonna ministre des Affaires étrangères, Gérald Darmanin ministre de l’Intérieur, Damien Abad ministre des Solidarités, Eric Dupond-Moretti ministre de la Justice, Sébastien Lecornu, ministre des Armées, Olivier Dussopt ministre du Travail et Justine Benin secrétaire d’Etat chargée de la Mer

Le bouquet final est atteint par [Charline Avenel](https://fr.wikipedia.org/wiki/Charline_Avenel) parachutée par Macron au rectorat de Versailles. Devant le harcèlement subi par Samuel Patty, elle avait organisé un rappel à l'ordre du professeur annulé après son assassinat.

Puis devant le harcèlement du jeune Nicolas, elle envoya une lettre aux parents, leur menaçant de poursuite judiciaire pour calomnies ! Nicolas se suicida 5 mois après.

Nous sommes donc devant un gouvernement inquisiteur foulant aux pieds notre droit à la vie privée et la liberté d'expression au nom du harcèlement, alors que de nombreux politiques ont montré leur haine et leur mépris envers autrui.

<h2>L'UE et le financement illicite</h2>

L'avenir que nous réserve UE est un traçage complet de notre argent. D'abord en voulant rendre illégal tout [payement en espèce de plus de 3000€](https://currencyassociation.org/article/cash-payment-limits-disempower-citizens). On contraignant au maximum les cryptomonnaies surtout sur [l'achat anonyme](https://www.bfmtv.com/crypto/regulation/les-plateformes-d-echanges-devront-declarer-les-transactions-en-cryptos-de-leurs-clients-europeens_AD-202305160712.html) et la possibilité de détenir ses crypto soi-même. Enfin en proposant son propre e-euro, que [Christine Lagarde avoue](https://www.juristique.org/opinion/christine-lagarde-piegee-wowan-lexus) sans gêne vouloir en faire une arme de surveillance et de spoliation.

Toutes ces envies totalitaires sont pour lutter contre les différents trafics illégaux : corruption, blanchiment, etc.

Connaître et contrôler chaque euro n'est pas suffisant, car l'EU souhaite aussi [interdire les messageries chiffrées](https://www.01net.com/actualites/leurope-envisage-de-casser-le-chiffrement-des-messages-pour-detecter-des-contenus-pedopornographiques.html) afin d'espionner chaque conversation des citoyens européens.

Dans le même temps, on découvre 600 000€ en cash dans l'appartement de l'euro député grec [Eva Kaili](https://fr.wikipedia.org/wiki/%C3%89va_Ka%C3%AFl%C3%AD#Accusations_de_corruption_dans_le_Qatargate). Cet argent venait du Qatar pour acheter la bienveillance de la députée vis-à-vis de la monarchie.

Aujourd'hui, 6 mois après la découverte [la députée est libre](https://www.touteleurope.eu/institutions/qatargate-l-eurodeputee-grecque-eva-kaili-de-retour-au-parlement-europeen-la-semaine-prochaine/) dans l'attente du procès, elle est même de retour au parlement.

Aujourd'hui, nous attendons toujours, que Ursula von der Leyen montre [sa conversation SMS avec le patron de Pfizer](https://www.euractiv.fr/section/sante/news/le-new-york-times-saisit-la-justice-sur-laffaire-des-sms-entre-ursula-von-der-leyen-et-pfizer/). Outre le fait que Ursula n'avait strictement aucun mandat pour négocier avec une entreprise. Cette conversation secrète a débouché sur un juteux contrat à hauteur de 1,8 milliard d'euros pour le vaccin covid.

Ursula veut savoir tout de nos conversations, mais refuse de montrer une conversation qui outrepasse son mandat et se termine par un énorme contrat payé avec nos impôts, les termes du contrat sont d'ailleurs toujours tenus secrets également.

Nous sommes donc devant des bureaucrates fous, obnubilés par la volonté de traquer nos moindres conversations, transactions, déplacements au nom d'arrêter des trafics. Mais dans le même temps, ils rendent leurs gestions opaques et s'adonnent aux trafics.

Nous pouvons conclure par le préambule de la déclaration de 1789 :
```
Les représentants du peuple français, constitués en Assemblée nationale, considérant que l'ignorance, l'oubli ou le mépris des droits de l'homme sont les seules causes des malheurs publics et de la corruption des gouvernements, ont résolu d'exposer, dans une déclaration solennelle, les droits naturels, inaliénables et sacrés de l'homme, afin que cette déclaration, constamment présente à tous les membres du corps social, leur rappelle sans cesse leurs droits et leurs devoirs
```

