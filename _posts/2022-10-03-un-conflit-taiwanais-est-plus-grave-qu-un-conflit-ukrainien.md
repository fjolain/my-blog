---
title: Le conflit taïwanais est plus grave que le conflit ukrainien
subtitle: L'électronique est une ressource plus tendue que le gaz ou le blé.
date: 2022-10-03
layout: post
background: /img/2022/10/taiwan.webp
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
---

<iframe frameborder="0" loading="lazy" id="ausha-2VSb" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=b3n5LuPv3L1l&v=3&playerId=ausha-2VSb"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Avec le conflit ukrainien, le monde a les yeux rivés sur le gaz et le blé. Pourtant, le gaz russe peut être remplacé par n'importe quel autre gaz, que ce soit du gaz naturel norvégien ou qatari, ou même du gaz de schiste américain. Tous sont remplaçables, le problème est logistique, voire financier.

Pareil pour le blé, un grain de blé est le même qu'il vienne de France ou d'Ukraine. On peut aussi raisonner par calorie, dans ce cas la calorie alimentaire est la même qu'elle vienne du blé, du riz ou du maïs. Encore une fois, le problème reste logistique et financier.

L'électronique ne répond pas à une telle interchangeabilité. Nous allons voir les différentes problématiques sur les composants électroniques en cas de conflit à Taiwan.

<h3>Niveau 0: aucun problème</h3>
Pour des composants très simples comme des diodes, des résistances, mais aussi les composants grand publique tels les écrans, claviers ou souris. Il n'y aura que peut d'impact. Ces composants sont fabriqués dans le monde entier. De plus, ils sont tous interchangeables facilement en cas de pénurie.

Votre clavier peut se brancher sur tous les ordinateurs, de même que votre écran.

<h3>Niveau 1: pénurie</h3>
Les composants plus évolués comme les ordinateurs, cartes mères, cartes graphiques, SSD et autres sont majoritairement faits par des entreprises taïwanaises. On peut citer Asus, Acer, MSI, Gigabyte ou Kingstom. De plus elles fabriquent aussi pour des entreprises américaines telles AMD, Nvidia, HP ou encore Dell.

En cas de conflit, ces composants vont manquer. Heureusement, un disque dur 1To Kingstom est remplaçable par un disque dur 1To de Western Digital de marque américaine. Un ordinateur Windows d'Acer est remplaçable par un ordinateur Windows de la marque chinoise Lenovo. On peut donc imaginer à terme une reprise du volume d'exportation de Taiwan par d'autres marques.

<h3>Niveau 2: revoir les produits</h3>

Le problème se complexifie avec des composants plus petits comme les microprocesseurs embarqués. On peut citer les fabricants taïwanais comme MediaTek ou TSCM qui distribuent leurs propres produits ou fabriquent pour Apple, NXP ou Qualcomm.

Par exemple, si Airbus se base sur un microprocesseur MediaTek quelque part dans l'avion. Alors en cas d'arrêt de la production à Taiwan. Airbus devra modifier ses cartes électroniques et ses programmes informatiques pour migrer vers un autre microprocesseur.

En cas de conflit, on ne peut pas changer un produit MediaTek pour un produit Texas Instrument. Il faut revoir le PCB et le code. Les usines du monde entier seront à l'arrêt, car il faut reconcevoir des milliers de produits pour se passer de la production taïwanaise.

<h3>Niveau 3: impossibilité</h3>

Enfin le problème devient insoluble, lorsqu'une technologie de fabrication n'est disponible qu'à Taiwan. C'est le cas, par exemple, de la gravure à [5nm](https://fr.wikipedia.org/wiki/5_nm), disponible que par TSCM à Taiwan. Cette technologie équipe les derniers processeurs d'Apple M1.

En cas de conflit, il n'aura pas de concurrent proposant le même produit même en reprenant la conception.

Apple connaît sa trop grande dépendance vis-à-vis de Taiwan. À ce titre l'iPhone 14 est le premier iPhone assemblé en Inde. C'est le premier pas dans la stratégie d'entreprise de mieux répartir sa supply chain. Mais, puisqu'il reste encore, des composants taïwanais dans l'iPhone, Apple sera à l'arrêt en cas de conflit.


En conclusion, l'électronique n'est pas une ressource vitale comme les autres. À la différence du pétrole, du gaz ou des calories alimentaires que l'on peut se procurer partout sur Terre, l'électronique repose sur Taiwan et sa région.

Or les ressources électroniques ne sont pas aussi facilement interchangeables, le moindre conflit avec Taiwan va mettre à l'arrêt des millions d'usines. Les contre-mesures ne seront efficaces qu'après plusieurs mois voire années.
