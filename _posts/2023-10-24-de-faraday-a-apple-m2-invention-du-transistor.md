---
title: De Faraday à l'Apple M2, l'invention du transistor
subtitle: Comment l'Innovation Émerge sans Etats. (2/3)
date: 2023-10-24
layout: post
background: /img/2023/10/pcbC.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-ltSI" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=B4kMYSpdOKY3&v=3&playerId=ausha-ltSI"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

L'innovation ne peut guère être planifiée, encore moins par un État. Par exemple, une simple amélioration sur les radios a permis l'essor de toute l'électronique et de l'informatique.

<h2>Le long parcourt du transistor</h2>
L'étude de l'électricité a rangé les matériaux selon leur résistance au courant. Ceux qui ont une résistance basse sont des conducteurs comme le cuivre, ils laissent passer l'électricité. Ceux qui ont une résistance élevée seront des isolants comme le caoutchouc.

Cependant, on remarque que dans certains cas cette résistance est pilotable.

En 1833, [Faraday](https://fr.wikipedia.org/wiki/Michael_Faraday) s'est rendu compte que la résistance d'un métal décroît avec la température. [Edwin Hall](https://fr.wikipedia.org/wiki/Effet_Hall) découvrit l'effet Hall en 1879, la résistance d'un métal change en fonction du champ magnétique autour.

En 1904, Flemig invente le premier tube à vide. Cet appareil permet d'amplifier un courant. Autrement dit, un courant de puissance faible peut faire varier la résistance d'un composant dans lequel circule un courant fort.

La possibilité qu'un faible courant pilote un plus gros courant est la clé de voûte de la radio -télécommunication. Une radio capte un signal électromagnétique dans l'air et doit l'amplifier pour l'utiliser dans un haut-parleur. Le tube à vide est l'élément clé entre l'antenne et le haut-parleur de la radio.

En 1947, des chercheurs américains découvrent les propriétés semi-conductrices du silicium pour fabriquer le premier [transistor](https://fr.wikipedia.org/wiki/Transistor). Un transistor est un amplificateur de courant comme le tube à vide, mais plus simple, plus fiable, plus petit et moins coûteux. Il remplaça le tube à vide dans les radios et radars.

<h2>La force vient du nombre</h2>

Le transistor de 1947 ne mesure que quelques millimètres et ses transitions d'états sont presque instantanées. Aussi une propriété mathématique devient intéressante.

Un transistor reçoit la puissance à piloter sur une entrée (A), le signal pilote sur l’entrée (B) et le signal amplifié sur la sortie (C). Dans une logique binaire, avec des entrées soient ouvertes soient fermées, il faut que A et B soient ouverts pour ouvert C. Le transistor peut donc aussi être vu comme un opérateur mathématique élémentaire que l'on nomme "AND". Une variante du transistor existe aussi en opposé qu'on nomme "[NAND](https://fr.wikipedia.org/wiki/Fonction_NON-ET)" (Not-AND).

Il a été prouvé, qu’avec suffisamment d'opérateurs NAND intégrés les uns aux autres, il est possible de concevoir un circuit réalisant n'importe quelle fonction logique.

On peut donc concevoir des mémoires, des additions, des multiplications et autres à partir de NAND. Or justement en 1958, le premier [circuit intégré](https://fr.wikipedia.org/wiki/Circuit_int%C3%A9gr%C3%A9) est produit. Il s'agit d'un processus industriel qui permet de concevoir de minuscules transistors directement sur une plaque de silicium.

On peut facilement chaîner des milliers de NAND sur quelques centimètres et ainsi réaliser n'importe quelle fonction logique.

<h2>Toujours plus de transistors</h2>
On peut concevoir n'importe quelle fonction logique, à condition de chaîner suffisamment de transistors NAND. Un premier phénomène d'émergence est présent avec quelques centaines pour faire des calculatrices. Ou plus généralement un calcul précis.

En passant vers le millier de transistors, on arrive à concevoir des fonctions logiques programmables. Un même circuit intégré peut maintenant avoir plusieurs usages, en fonction de comment on le programme.

En 1971, Intel sort le [Intel 4004](https://fr.wikipedia.org/wiki/Intel_4004), le premier microcontrôler programmable avec 2300 transistors. Aujourd'hui, il y a 65 milliards de transistors dans le processeur Apple M2.

Cette évolution spectaculaire du nombre de transistors a corroboré la prédiction du président d'Intel [Gordon Moore](https://fr.wikipedia.org/wiki/Gordon_Earle_Moore) qui envisageait un doublement du nombre de transistors dans les ordinateurs tous les 6 mois. Cette explosion a ouvert la voie à l'informatique. Cette fonction logique présent dans nos processeurs permet maintenant de programmer n'importe quel cas d'usage.

L'informatique a été possible, car un élément fort simple comme un opérateur NAND a pu être fabriqué et assemblé par millier, puis millions et maintenant milliards sur une plaque de silicium de quelques centimètres.

Par sa multitude, l'opérateur NAND peut devenir n'importe quelle fonction logique, y compris des fonctions logiques programmables pour faire fonctionner nos ordinateurs et nos smartphones.

Le rôle du transistor aujourd'hui se retrouve très éloigné du simple remplacement du tube à vide dans les radios.
