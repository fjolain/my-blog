---
title: La vie privée est devenue un crime
subtitle: Mettre en prison des développeurs qui conçoivent des outils de cryptage, revient à mettre en prison des fabricants de coffres, sous prétexte que leurs coffres seraient trop solides et opaques.
date: 2024-07-15
layout: post
background: /img/2024/07/cell.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-dVIS" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=omkv1SMrGNYM&v=3&playerId=ausha-dVIS"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>


<h2>Construire une prison à ciel ouvert</h2>

Une mécanique de la surveillance a lieu depuis 20 ans toujours de la même manière. La surveillance est confinée sur des personnes, des lieux ou des évènements précis. Puis, une fois socialement acceptée, elle se généralise pour tout le monde.

Ainsi vous, honnête citoyen, vous faites déjà traquer comme le plus grand des criminels. Toutes vos transactions de plus de 1000€ sont analysées par [Trakfin](https://www.economie.gouv.fr/tracfin/communication-systematique-dinformations-cosi-relative-aux-transmissions-fonds), (il vous est aussi [interdit de payer plus de 1000€](https://www.latribune.fr/entreprises-finance/banques-finance/banque/le-paiement-en-especes-interdit-au-dela-de-1-000-euros-a-partir-du-1er-septembre-487685.html) en liquide). Votre historique de navigation est analysé puis conservé pendant 5 ans depuis 2015 et [la loi Renseignement](https://fr.wikipedia.org/wiki/Loi_relative_au_renseignement). Vos déplacements sont analysés par les antennes mobiles et la reconnaissance faciale. Laquelle, une fois de plus ne [devait être "testée" que durant les JO à Paris](https://www.amnesty.fr/liberte-d-expression/actualites/jo-paris-de-la-videosurveillance-algorithmique-a-la-reconnaissance-faciale-il-n-y-a-qu-un-pas), mais vient d'être pérennisée au-delà.

Le fisc analyse même les images satellites de votre maison pour [repérer les piscines](https://www.capital.fr/votre-argent/sa-piscine-est-reperee-par-le-fisc-grace-aux-images-satellites-mais-elle-ne-serait-pas-imposable-1474612) ou extensions n'ont déclarées.

Sans parler de ces bases de données administratives regroupant tous vos revenues, vos actifs ou votre santé.

Un tel espionnage était jadis réservé au pire criminel. Il est aujourd'hui appliqué à chaque citoyen. Puisqu'aux yeux de l'État, nous sommes tous des potentiels criminels, terroristes, pédophiles à surveiller. Quand bien même des affaires comme les Pandora Paper, Paname Paper ou affaire Epstein semble davantage pointer l'élite que le citoyen lambda comme source de crime.


Bien que le droit à la vie privée figure clairement dans [l'article 12](https://www.un.org/fr/universal-declaration-human-rights/#:~:text=Article%2012,honneur%20et%20%C3%A0%20sa%20r%C3%A9putation.) des droits de l'Homme (*Nul ne sera l'objet d'immixtions arbitraires dans sa vie privée, sa famille, son domicile ou sa correspondance*), ce droit est bafoué au bulldozer dans l'indifférence générale.

<h2>Y mettre des criminels de la vie privée</h2>

Maintenant que la prison à ciel ouvert est construite, il est temps d'y mettre des fautifs. Vouloir une vie privée, faisait de vous un suspect, vous voilà maintenant criminel. Les dirigeants mondiaux viennent de franchir cette ligne en même temps.

Dans l'univers des cryptomonnaies [les développeurs de Samourai Wallet ont été arrêtés](https://www.coindesk.com/fr/policy/2024/04/24/samourai-wallet-founders-arrested-and-charged-with-money-laundering/) aux États-Unis, un an après l'arrestation aux Pays-Bas d'[un développeur de Tornado Cash](https://www.lemonde.fr/pixels/article/2024/05/15/cybercriminalite-un-cofondateur-de-tornado-cash-condamne-aux-pays-bas_6233436_4408996.html).

Ces deux affaires se ressemblent, puisque les suspects sont des développeurs qui proposent des outils open source pour renforcer la vie privée lorsqu'on utilise des cryptomonnaies.

Il s'agit uniquement et seulement de développeurs aucun d'eux n'est lié à un trafic, aucun d'eux n'héberge un site en ligne illégale. Leur tort est d'avoir codé des logiciels pour renforcer l'anonymat. Ils ne traitent aucune transaction personnellement ni ne gèrent aucun actif pour le compte de quelqu'un. Ils développent des lignes de codes.

Leurs torts est de vouloir anonymiser un endroit que les gouvernements tentent de contrôler à savoir les cryptomonnaies. Leur tort est de vouloir garder leur vie privée, faire valoir leur droit fondamental face à des dirigeants de plus en plus despotiques.

Mettre en prison des développeurs qui conçoivent des outils de cryptage, revient à mettre en prison des fabricants de coffres, sous prétexte que leurs coffres seraient trop solides et opaques.

<h2>Aussi stupide qu'interdire des formules mathématiques</h2>

À la différence près qu'un développeur ne fabrique rien de physique, il conçoit des lignes de code mathématique. L'État décide donc de mettre en prison des mathématiciens pour avoir inventé des formules mathématiques protégeant trop bien la vie privée.

Le cryptage est un domaine des mathématiques et de l'informatique qui permet de protéger des données. Aussi bien des fichiers sur une clé USB, des messages sur un smartphone ou des transactions dans une blockchain.

Le cryptage n'est que mathématique, vouloir interdire du cryptage est aussi stupide qu'interdire une formule mathématique. Malgré l'absurdité, nos dirigeants ont déjà tenté. Le cryptage RSA (inventé en 1977) était interdit à l'export des USA car [considéré comme des munitions](https://fr.wikipedia.org/wiki/Crypto_Wars). La France a interdit aussi le cryptage. De nombreux pays souhaitent interdire Bitcoin la première cryptomonnaie ([Chine, Inde, Indonésie, Turquie, Maroc, Algérie, Egypte, etc](https://fr.statista.com/infographie/25097/pays-qui-interdisent-le-bitcoin-ou-qui-ont-pris-des-mesures-contre-utilisation-cryptomonnaies/)). Et même [l'EU souhaite interdire le chiffrement](https://www.letemps.ch/suisse/le-chiffrement-des-messages-une-cible-recurrente-des-forces-de-l-ordre) dans les apps de messagerie.


Puisque ni leurs missiles, ni leurs services secrets, ni leurs prisons ne parviennent pas à stopper des formules mathématiques. Ils condamnent ceux qui les implémentent dans des outils pour le grand public. Ils condamnent les développeurs.


Face à leur impuissance devant l'universalité d'une formule, la stratégie mise en place par nos despotes est une stratégie de la terreur. Les mises en examen des développeurs de Samourai Wallet et Tornado Cash vont dans ce sens. La volonté de l'UE de contraindre les développeurs d'app de messagerie comme Signal pour qu'ils sabotent leurs chiffrements va aussi dans ce sens.

Puisque l'espionnage de masse ne suffit plus, puisque le cryptage reste le meilleur moyen de nous protéger, les dirigeants optent pour une stratégie de la terreur. Nous sommes déjà tous suspects potentiels à perpétuité, ceux qui utilisent le cryptage deviennent criminels et ceux qui diffusent ce cryptage pour protéger notre vie privée seront jugés comme traîtres.


Il ne reste plus qu'une chose à faire, utiliser davantage ces outils accessibles à n'importe qui. Utilisez signal.org pour vos messages chiffrés, proton.me pour vos services cloud (mail, calendrier, fichier), Bitcoin pour épargner et payer à l'abri de l'État ([cf mon livre](https://amzn.eu/d/0cqPdizk)) ou encore torproject.org pour cacher votre navigation.



