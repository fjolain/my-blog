---
title: Le crypto-euro amène le communiste 2.0
subtitle: Si l'état contrôle la monnaie, il nous contrôlera tous.
date: 2022-03-02
layout: post
background: /img/2022/03/yuan.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [société, blockchain, survaillance]
---

<iframe frameborder="0" loading="lazy" id="ausha-qwG4" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=oagXMsE323Zm&v=3&playerId=ausha-qwG4"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Le crypto-euro plutôt que d'ouvrir une nouvelle ère d'innovation promet de nous plonger dans un communiste 2.0. L'état pourra contraindre ses citoyens et son économie sans effort juste par la monnaie.

<h2>Qu'est-ce qu'une MNBC</h2>
Dans [le dernier article](https://www.codable.tv/monnaie-numerique-de-banque-centrale-remede-ou-poison/), nous avons soulevé le voile derrière les très paradoxales monnaies numériques de banque centrale (MNBC) également appelées cryptomonnaies de banque centrale. Paradoxales, car une cryptomonnaie est par nature décentralisée et anarchique. Rien à voir donc avec une banque centrale.

Les MNBC sont donc plus une innovation linguistique qu'une innovation monétaire. Mais derrière le cosmétique, se cache un danger. Avec les MNBC, l'état obtiendra un contrôle absolu sur sa monnaie. Il conservera bien sûr son monopole sur la régulation de la création monétaire. Mais, il obtiendra également le rôle de gestion de compte client, auparavant dans les mains des banques. Ainsi, que le rôle de vérification des transactions, auparavant tenu par SWIFT, Visa ou Mastercard.

Dans le précédent article, nous avons vu comment l'état pourrait forcer le passage vers son crypto-euro en la faveur d'une crise d'hyperinflation. Aussi, nous allons voir dans cet article, comment les MNBC pourraient donner lieu à un communiste 2.0.

<h2>Communiste 1.0 : Contrôler par les moyens de production</h2>

Le communiste 1.0 a parfaitement montré qu'on pouvait contrôler l'intégralité d'un pays et la vie de tous ses citoyens en contrôlant l'information et son économie.

Cependant le communiste 1.0 a voulu planifier et contrôler les moyens de production pour asseoir son pouvoir sur l'économie. Il faut lui reconnaître quelques réussites dans le domaine du nucléaire, du spatial ou de l'industrie. Des avancées dissoutes dans l'incapacité de l'état à s'occuper de son peuple. Le communiste 1.0 est une succession de pénuries et famines sous Staline comme sous Mao.

Le rêve d'une bureaucratie capable de tout produire est bien mort. Toutefois, le désir de contrôler totalement l'économie reste présent pour de nombreux dirigeants.

Et si grâce aux MNBC, les états pouvaient de nouveau contraindre l'économie juste en contrôlant la monnaie ?

<h2>Communiste 2.0: Contrôler par la monnaie</h2>

L'état laissera donc les chefs d'entreprises et leurs salariés produire ce qu'ils veulent.

Il se contenterait d'imposer son crypto-euro à tous. Plus aucune personne ne peut payer ou être payée dans une autre monnaie. Or on le rappelle, avec une MNBC, le gouvernement contrôle les dépôts, les comptes et les transactions.

Le gouvernement a donc un aperçu instantané de la vie privée de ses citoyens, que ce soit l'achat d'une maison ou d'une baguette de pain.

Il peut exclure n'importe quels citoyen, entreprise ou association du système monétaire, asphyxiant monétairement ses victimes jusqu'à la mort.

L'état peut aussi imposer très facilement des quotas arbitraires à l'ensemble de la population. On ne pourra dépenser que 50 € d'essence par semaine, 500 € de chauffage par an ou 1 restaurant par mois. Dans cette optique, les mesures écologiques se marieront à merveille avec le communiste 2.0 pour nous retirer des libertés.

Enfin, l'état pourra aussi prendre ce qu'il veut quand il veut sur l'ensemble des comptes des citoyens.

Tous ces cas d'usage sont impossibles avec l'or, le cash ou le Bitcoin, compliqués avec la finance actuelle, mais très facile en imposant une MNBC.

Le contrôle de l'économie se retrouve donc renforcer avec les MNBC sans avoir besoin de gérer les moyens de production, c'est la naissance d'un communiste 2.0.

<h2>Une vision déjà en marche</h2>
Une telle perspective est déjà en marche en Chine. Le pays a bien renoncé au communiste 1.0, il regorge d'entrepreneurs, startup ou entreprises privées qui innovent et produisent à la place de l'état.

Bien évidemment tout le monde est contrôlé par la surveillance de masse, le crédit social ou la peur d'être arrêté. Comme ce fut le cas avec les disparitions de [Jack Ma](https://fr.wikipedia.org/wiki/Jack_Ma#D%C3%A9part_de_la_direction_d'Alibaba_et_activit%C3%A9s_post%C3%A9rieures) fondateur Alibaba ou la tennis women [Peng Shuai](https://fr.wikipedia.org/wiki/Peng_Shuai#Accusation_de_viol,_disparition_puis_r%C3%A9apparition_publique).

Le contrôle de la monnaie, si central pour le communiste 2.0, faisait longtemps défaut au Parti, notamment à cause de l'utilisation massive du cash.

En quelques années, grâce à la fulgurante ascension des paiements mobile, le Parti put disposer d'un meilleur contrôle de sa monnaie. En enfonçant le clou avec le cryptoyuan, Xi Jimping prend directement la place de WeChat (WePay) et Alibaba (AliPay). Il récupère tous les rôles et un contrôle total sur sa monnaie. La Chine est une vitrine du communiste 2.0.

<h3>Bonus canadien</h3>
Cet article devait s'arrêter là, mais Trudeau a décidé de devenir le premier dirigeant occidental à utiliser le communiste 2.0. En effet, pour casser le Convoi de la Liberté, le Premier ministre canadien a décidé de [geler les comptes](https://fr.wikipedia.org/wiki/Convoi_de_la_libert%C3%A9#Financement) en banque des manifestants. Il a aussi mis sur liste noire les adresses de cryptomonnaies liées aux manifestations. Les détenteurs de ses bitcoins gardent toujours leurs actifs, ils ne pourront plus les déposer sur un cryptoéchange.

Le contrôle total de la monnaie est bien dans le viseur des dirigeants occidentaux. Leur MNBC n'aura comme seul but d'amener vers une communiste 2.0 où la liberté et la vie privée seront inexistantes.

