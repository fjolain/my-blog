---
title: Les données, nouveau pétrole, nouvelle arme
subtitle: Comment fonctionne le business de la donnée ?
date: 2016-08-12T13:40:37+01:00
layout: post
categories: [article]
tags: [économie, data, machine learning]
---

Les données numériques, celles qui nous laissons derrière nous sur internet sont devenues un nouvel eldorado. La Silicon Valley n&rsquo;est plus le haut lieu de la technologie, mais plutôt le haut lieu de l&rsquo;extraction de données. De même tous les gouvernements s&#8217;emparent de cette nouvelle technologie comme d&rsquo;une arme redoutable, pour pouvoir écouter son peuple et les autres pays.

## Nouveau Pétrole

En 2012 l&rsquo;ancien CEO de LinkedIn disait : « Les données seront le pétrole du XXI siècle ». On y voit toute de suite le côté ruée vers l&rsquo;or de ce phénomène. Certains poids lourds comme Facebook, Google, Apple ont fait fortune par cette nouvelle matière. Et comme au début pour le pétrole, il suffit de creuser. Une petite application comme Instagram se vend 1 milliards en quelques mois et se rentabilise en seulement 2 ans. Et comme pour le pétrole, le gisement semble à première vue inépuisable.

En plus le timing est parfait, les ventes de matériel informatique stagnent, les entreprises ne voient pas pourquoi elles devraient investir dans du matériel ou dans du personnel. C&rsquo;est là qu&rsquo;entre en jeu le BigData ! Le BigData est la réponse pour utiliser ce gisement de données et ce matériel invendu. Plutôt que de laisser pourrir une base de données fournisseurs ou clients, nous pouvons l&rsquo;analyser pour en extraire des données.

Car oui de même qu&rsquo;il ne suffit de mettre une pompe à essence au-dessus d&rsquo;un gisement pour avoir une énergie exploitable. Il faut une énorme infrastructure pour analyser toutes ces données. Premièrement il faut un forage, un réseau social est un forage judicieux, une application pour téléphone voire même l&rsquo;OS du téléphone complet c&rsquo;est encore mieux. Ensuite il faut penser au stockage pour réguler la production, ici on n’a pas le choix on doit aligner par millions des disques durs. Un data center moyen possède 10 000 To de stockage. L&rsquo;étape suivante est le raffinage, savoir que Marion a acheté du Cochonou mardi et qu&rsquo;avant elle est allée sur internet n&rsquo;est pas très intéressant. Par contre si en traitant les données on découvre que sa recherche était saucissons.com et que sur le site traînait une pub Cochonou, alors les annonceurs se battront pour être affichés lors de sa prochaine recherche. Cette étape est la plus cruciale, en plus d&rsquo;aligner les processeurs pour tout traiter, il faut également des algorithmes d&rsquo;une très grande complexité.

Enfin, on n&rsquo;y pense pas toujours à notre échelle du tout sans fil, mais il est nécessaire de tirer des pipelines de fibre optique entre pays pour connecter les serveurs. Ce réseau représente 25 millions de kilomètres et compte pour 99 % du transfert de données. Au XX siècle, c&rsquo;était les pipelines de pétrole ou de gaz qui dictaient la diplomatie avec ses voisins. Au XXI siècle ce sont les fibres optiques. On a par exemple plusieurs projets de câbles sous-marins entre le Brésil et L&rsquo;Union Européenne, pour éviter de passer par les Etats-Unis et les serveurs de la NSA. Ou encore la Chine qui dépense fortement pour connecter l&rsquo;Asie du Sud à son réseau et faciliter son pacte économique.

## Une nouvelle arme de dissuasion

Comment choisissez-vous votre président ? Comment sont prises les décisions dans les entreprises ? Comment achetez-vous votre dernier téléphone ? Au début de chaque raisonnement, il y a une phase d&rsquo;information, à l&rsquo;heure actuelle l’information est avant tout des données disponibles sur internet. Nos sociétés reposent énormément sur l&rsquo;information, par conséquent contrôler ou pirater les données sur internet font rêver plus d&rsquo;un gouvernement.

Le fait d&rsquo;espionner citoyens et entreprises par la NSA a permis aux Etats-Unis d&rsquo;avoir une longueur d&rsquo;avance dans des discussions diplomatiques et de s&rsquo;en servir comme d&rsquo;une arme de dissuasion. Le piratage de Sony dévoilant quantité d&rsquo;informations personnelles a permis d&rsquo;annuler la sortie de The Interview. Ou encore le virus américain Stuxnet a été conçu comme une arme pour modifier et espionner les informations des centrales nucléaires iraniennes. L&rsquo;entreprise Zerodium est connue pour la revente de failles informatiques. Elle a promis 1 millions de dollars à quiconque serait capable de trouver une faille dans IOS 9 permettant de pirater les données du téléphone. Une nouvelle sorte de marchand d&rsquo;arme.

Pour finir, une petite anecdote :). La physique n&rsquo;a jamais passionné les politiques. Pourtant quand cette même physique a pu concevoir une bombe nucléaire capable de faire plier n&rsquo;importe quel pays, l&rsquo;argent a afflué en provenance des gouvernements. L&rsquo;histoire se répète avec une autre arme de destruction massive : l&rsquo;ordinateur quantique. Ils ne se préoccupaient guère de cette technologie, mais quand avant même le premier ordinateur, on a mis au point un algorithme  »quantique » capable de casser n&rsquo;importe quel cryptage, l&rsquo;argent a afflué. La future arme de destruction massive ne détruit rien… Elle permet juste d&rsquo;écouter les données top secrètes de ses voisins sans être écoutée.

Les données sont donc bien le nouvel or noir du XXIeme siècle, elles en ont l&rsquo;aspect, la valeur, l&rsquo;espoir que l&rsquo;on porte au pétrole. Elles sont, par contre, maintes fois plus inflammables.  
François Jolain