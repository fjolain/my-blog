---
title: À partir du 8 février, WhatsApp ne sera plus un lieu sûr
subtitle: La messagerie réputée protectrice de la vie privée vient de muter en une collecte de données massive.
date: 2021-01-08
published: fjolain.medium.com/%C3%A0-partir-du-8-f%C3%A9vrier-whatsapp-ne-sera-plus-un-lieu-s%C3%BBr-60ee88643c35
layout: post
background: /img/2021/01/whatsapp.jpg
categories: [article, podcast]
tags: [vie privée, whatsapp, chiffrement]
---

<iframe frameborder="0" loading="lazy" id="ausha-YCcw" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%233F51B5&dark=true&podcastId=B6V2WskZGaMB&v=3&playerId=ausha-YCcw"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>



Vous avez sans doute vu cette page en ouvrant WhatsApp récemment. 

![screenshot](/img/2021/01/screenshot.png)

## Que se cache-t-il derrière ?

Avec le rachat de WhatsApp par Facebook en [2014](https://fr.wikipedia.org/wiki/WhatsApp), il fallait s’y attendre. Le réseau social n’allait pas rester indifférent devant les données de [2 milliards d’utilisateurs](https://www.lemonde.fr/pixels/article/2020/02/12/whatsapp-franchit-la-barre-des-2-milliards-d-utilisateurs_6029349_4408996.html). Malgré [les demandes de la FTC](https://www.ftc.gov/news-events/press-releases/2014/04/ftc-notifies-facebook-whatsapp-privacy-obligations-light-proposed) et [les promesses faites à l’EU](https://es.reuters.com/article/idFRKCN18E0FA-OFRBS) de ne jamais regrouper les données entre ses services, Facebook va bien collecter les données de WhatsApp, les regrouper avec les siennes et les revendre à des tiers.

Le changement est brutal. Il s’opère par une simple modification des conditions d’utilisations. Le pas est forcé. Les utilisateurs ont jusqu’au 8 février pour accepter ou partir. 
On peut même y voir une provocation. En regroupant les données, Facebook accroît son monopole, à l’heure où les législateurs [le poursuivent pour anti-concurrence](https://www.lesechos.fr/tech-medias/hightech/facebook-poursuivi-par-la-ftc-et-48-etats-americains-pour-pratiques-anticoncurrentielles-1272587). 

## Quelles données vont-ils collecter ?

Dans l’application WhatsApp, les contenus des messages sont chiffrés, Facebook ne peut donc pas les lire. Par contre, la date, les destinataires, la géolocalisation et d’autres informations appelées méta-données ne sont pas chiffrées et donc à la portée de Facebook.

Les méta-données sont aussi riches d’informations que le contenu du message. Il suffit de connaître la fréquence de vos messages et leurs destinataires pour établir vos cercles privés. Rien que la localisation de vos messages permet de suivre tous vos déplacements. Par exemple, un appel à minuit à SOS Amitié depuis un pont décrit toute la situation sans même avoir besoin d’intercepter le contenu de l’appel.

Il ne faut pas sous-estimer la valeur de ces méta-données, qui décrivent nos vies même sans voir le contenu. Maintenant qu’elles vont tomber dans les mains de Facebook, il est souhaitable de quitter WhatsApp. Il existe d’autres messageries tout aussi fonctionnelles et bien plus sécurisées comme [Signal](https://www.signal.org/fr/) ![signal](/img/2021/01/signal.jpg){: style="height: 25px"} ou [Telegram](https://telegram.org/) ![telegram](/img/2021/01/telegram.jpg){: style="height: 25px"}, à vous de choisir.
