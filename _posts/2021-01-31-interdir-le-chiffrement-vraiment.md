---
title: Interdire le chiffrement, vraiment ?
subtitle: La pire idée voulue par les pires humaines
date: 2021-01-31
layout: video
background: /img/2021/01/chiffrement.jpeg
categories: [video]
tags: [société, vie privée, chiffrement]
yt: ky9g7XIvGxU
---
