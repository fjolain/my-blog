---
title: Les Fléaux de la démocratie
subtitle: Tout système a ses points faibles, la démocratie a donc les siens.
date: 2016-08-12T14:04:58+01:00
layout: post
categories: [article]
tags: [politique, démocratie]
---
Tout système a ses points faibles, la démocratie a donc les siens. Ces Fléaux frappent la France depuis quelques mois mettant à mal notre démocratie. Au regard de l&rsquo;Histoire, on observe qu&rsquo;une démocratie est quelque chose de fragile, qui peut facilement se détruire elle-même, comme en Allemagne 1933 ou en Turquie en ce moment même. Il ne faut donc pas prendre ces Fléaux à la légère.

## Le Vote

Le vote est justement une condition nécessaire à la démocratie, pourtant c&rsquo;est aussi son point faible. Quels reproches incessants fait-on aux concours et au bac ? Celui de ne pas noter les connaissances de l&rsquo;élève sur le long termes, mais de le noter ponctuellement plus sur son état d&rsquo;esprit lors du test. On pourrait faire la même remarque sur le vote. On ne vote pas pour une idée politique qui nous tient à coeur depuis toujours, nous votons en fonction de nos sentiments sur le moment.

Nous reviendrons sur le gros problème qu&rsquo;un peuple ne résonne pas, mais agit uniquement par sentiments, et ce même dans l&rsquo;urne. Concentrons nous sur le problème de ponctualité. En fonction des événements des derniers jours, nous votons une politiques pour plusieurs années. C&rsquo;est ainsi que Sarkozy peut encore se présenter, malgré une liste d&rsquo;affaires de corruption et de détournement de fond longue comme le bras. Du moment qu&rsquo;il ne fasse pas les gros titres dans le mois précédent l&rsquo;élection, les gens le verrons comme un saint-homme. Les attentats de Paris ont joué une part non négligeable dans le score du FN, car ils étaient fort présents dans la tête des électeurs au moment de voter.

Le vote retire aussi le pouvoir aux citoyens. En allant voter, vous allez donner votre infime pouvoir à une personne de votre choix, vous ne retrouverez votre pouvoir qu&rsquo;à la prochaine élection. D&rsquo;ici là vous ne servez plus à rien, votre avis ne compte plus, le gouvernement peut faire ce qu&rsquo;il veut sans votre accord. En démocratie, le pouvoir appartient au peuple… oui, mais un jour tous les 5 ans seulement. Une solution serait de faire plus de référendum comme c&rsquo;est le cas en Suisse. Ainsi le gouvernement reste soumis constamment à la voix du peuple.

## La Peur

On ne pense pas forcément à ce Fléau pourtant les médias et politiciens l&rsquo;utilisent à tout-va. Ils le désignent sous le terme _insécurité_, ce qui revient au même : provoquer la peur chez les électeurs.

Le FN est devenu maître dans le domaine de contrôler la peur (avec l&rsquo;aide bienveillante de nos médias). En 2002, la peur avait pris les électeurs avec le matraquage visuel sur les violences dans les banlieues. La machine FN s&rsquo;était mise en place pour transformer cette peur en vote, résultat Le Pen au deuxième tour. Aujourd&rsquo;hui entre l&rsquo;afflux de migrants et les terroristes, le FN a de la matière première à profusion, ce qui nous donne : _La France et les Français ne sont plus en sécurité, des mesures d’urgence s’imposent._ De quoi faire un beau score, non ?

Si la peur n&rsquo;est pas utilisée pour le vote, on s&rsquo;en sert pour retirer des libertés pour une illusion de sécurité. La loi sur le renseignement est un très bel exemple, sous prétexte que l&rsquo;on souhaite trouver les terroristes, il faut espionner tout le peuple. C&rsquo;est d&rsquo;une part extrêmement stupide quand on sait le peu d’efficacité de la méthode, autant chercher une aiguille dans une botte de foin. Mais ça l&rsquo;a fait mal au pays des droits de l&rsquo;homme, quand on connaît l&rsquo;article 12 de la déclaration : Nul ne sera l&rsquo;objet d&rsquo;immixtions arbitraires dans sa vie privée, sa famille, son domicile ou sa correspondance, ni d&rsquo;atteintes à son honneur et à sa réputation. Oups…

Visiblement on n&rsquo;est pas allé assez fort pour bloquer les terroristes, on a donc décrété l&rsquo;état d&rsquo;urgence. A lui tout seul, ce statuts bafoue une multitude de nos libertés et droits dont l&rsquo;article 20 de la déclaration : Toute personne a droit à la liberté de réunion et d&rsquo;association pacifiques. Oups encore… Et comme un mois de régime plus totalitaire que démocratique ne suffit pas, on a prolongé le décret. Manuel Valls est venu en personne devant les députés pour rallonger l&rsquo;état d&rsquo;urgence, il leurs a demandé de ne pas aller chercher le conseil constitutionnel car selon lui certains actes dans l&rsquo;état d&rsquo;urgence ne sont pas « constitutionnels ». Quand la peur gagne les gens, il n&rsquo;est même plus besoin de cacher ses réelles intentions.

## Le Peuple

Et quelle est l&rsquo;opinion de la foule sur ces lois contre la liberté ? Cela nous donne : 63% des Français favorables à une limitation des libertés individuelles pour lutter contre le terrorisme. Alors d&rsquo;une part, j&rsquo;aimerai bien que l&rsquo;on me dise quel panel de français à répondu à cette question pour oser employer « des Français » au sens général. Poser cette question au public de TF1 ou à celui d&rsquo;Arté change le résultat, un sondage est toujours truqué par nature. Mais 63 % n&rsquo;est pas anecdotique. Le français en répondant à la question a fait comme pour voter, il n&rsquo;a pas raisonné, il a ressenti. Si il aurait réfléchi 2 seconde sa réponse aurait été différente.

L&rsquo;être humain se comporte différemment en tant qu&rsquo;individu et en tant que groupe. Ce comportement a été très bien décrit dans _Psychologie des Foules_ de Gustave Le Bon. L&rsquo;intelligence dépend de la personne, or lorsque nous somme en foule nous tendons vers une intelligence commune. Nous faisons donc un nivellement par le bas en choisissant un intelligence réduite. Même si la foule est constituée de savant, l&rsquo;intelligence de la foule sera faible.

Il est donc impensable de raisonner une foule, elle ne comprendra rien. Le seul moyen de lui faire passer un message est par les émotions. Nous avons l&rsquo;outil de la peur très plébiscité par nos politiciens. L&rsquo;autre le plus connu est la répétition, il suffit de répéter une débilité suffisamment de fois, pour que le peuple prenne la débilité comme vérité. En France, nous avons « les musulmans sont un problème pour notre pays» comme belle débilité répétée depuis plusieurs année, et qui commence à bien porter ses fruits.

Alors nos politiciens sont tous des pourris avec leur paroles conçus pour le vote impulsif ? Là encore, il y a une belle analogie. Dans l&rsquo;agro-alimentaire, les producteurs jettent d&rsquo;énormes quantités de produit mangeable car il y a un minuscule défaut. Ils les jettent car ils savent que le consommateur ne va pas l&rsquo;acheter à cause du défaut. Ce n&rsquo;est donc pas la faute des producteurs, mais de nous qui sommes trop exigeants. Si un homme politique vous dirait qu&rsquo;il souhaite annuler les 35h, freiner les aides, arrêter les lois débiles sur la sécurité et mettre les lois au référendum il ne se ferait pas élire malgré son programme réfléchi et raisonné. Nos politiciens nous illusionnent car, le français préfère les illusions à la réalité. Comme le producteur, le politicien fait les mauvais choix pour répondre à la demande de ses clients.

Selon Sartre _l&rsquo;enfer c&rsquo;est les autres_, je dirais que l&rsquo;enfer c&rsquo;est surtout nous-même.