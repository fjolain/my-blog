---
title: Quelle place a ChatGPT dans le progrès technique ?
subtitle: Trop intelligente pour certain, trop stupide pour d'autre. Révolution ou arnaque ?
date: 2023-04-02
layout: post
background: /img/2023/04/robot.png
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-6RTO" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&v=3&playerId=ausha-6RTO"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

En [1143](https://fr.wikipedia.org/wiki/Arbal%C3%A8te), le pape Innocent II interdit l'arbalète. L'innovation de l'époque permettait à n'importe qui de tirer comme le meilleur archer : "Avec cette arme perfide, un poltron peut tuer sans risque le plus vaillant homme."

En [1878](https://fr.wikipedia.org/wiki/Locomotive_Act#:~:text=Cette%20loi%20disposait%2C%20entre%20autres,locomotive%20d%C3%A9signait%20tout%20v%C3%A9hicule%20motoris%C3%A9.), la voiture était jugée inutile et dangereuse. Il fallait la faire précéder d'un marcheur agitant un drapeau rouge afin de signaler le danger.

En [1965](https://fr.wikipedia.org/wiki/Plume_Sergent-Major), le stylo à bille fut enfin autorisé à l'école. On le jugeait auparavant dangereux pour le poignet des élèves, sans parler de la fin de pleins et des déliées qui allaient réduire l'intelligence de l'enfant.

À notre époque, Bitcoin semble le mal indépassable qui nous emmènera vers l'apocalypse. Mais depuis 2 mois, on a trouvé un diable encore plus sournois : ChatGPT.

Que vous souhaitez une recette de cuisine, un cours sur la physique quantique ou une estimation immobilière, ChatGPT a réponse à tout. Il a su se faire autant de partisans que de détracteur criant à l'arnaque. Essayons de comprendre où se situe cette innovation dans le progrès technique.

<h2>Le progrès suit son cours </h2>

L'ingénierie consiste à automatiser le travail.

Que ce soit un travail musculaire avec les moteurs (machine à vapeur de [1780](https://fr.wikipedia.org/wiki/Machine_de_Watt)). Un travail de dextérité avec les automates (Jacquard de [1801](https://fr.wikipedia.org/wiki/M%C3%A9tier_Jacquard)). Ou plus récemment un travail intellectuel avec l'informatique.

Ainsi, l'informatique a déjà remplacé bon nombre d'emplois. Tels les assistants, les commerces ou les postiers. ChatGPT continue ce remplacement avec les médecins ou avocats.

Chose que l'on perçoit moins, l'informatique remplace aussi les développeurs. Un logiciel ou site web demande beaucoup moins de développeurs qu'il y a 20 ans. Pour beaucoup de cas (blog, site d’entreprise, etc.) on peut même se passer de développeurs.

Là aussi, l'arrivée de ChatGPT ne change donc pas la tendance, les développeurs vont encore une fois se concentrer sur le code plus complexe.

Bref, ChatGPT prolonge le progrès. Un seul humain a déjà la force de millier aux manettes d'une grue. Un seul humain peut connaître et réfléchir comme des milliers aux manettes de ChatGPT.

Vouloir un moratoire sur l'IA sera tout aussi utile que les régulations imposées à l'arbalète ou la voiture.

D'ailleurs certains vont même jusqu'à critiquer chatGPT. Il ne serait bon qu'à prédire statistiquement les mots d'une phrase. Que vaut cette critique ?

<h2>Une vraie révolution</h2>

Il est vrai que le "cerveau" de chatGPT consiste uniquement à prédire la suite des phrases en fonction de ce qu'il a lu auparavant sur internet. Il peut donc paraître éloigné d'une quelconque forme d'intelligence. Ses prouesses ressemblent à des supercheries.

Or, toutes révolutions technologiques sont des "supercheries". Pour imiter la marche, on inventa la roue. Une solution parfaitement fonctionnelle pour doter les objets de jambe, et pourtant très éloignés de la marche humaine.

Les lave-vaisselle ne lavent pas comme les humains. Ils sont incapables de distinguer les assiettes dans l’évier, incapable de prendre délicatement l'assiette, incapable de passe l'éponge ou le torchon. On a repensé intégralement le lavage pour à la fin avoir des assiettes propres.

Les premières IA cherchaient à apprendre, comme : détecter des objets dans une image ou jouer au jeu de Go. Il fallait des années pour leur faire apprendre une discipline. Et surtout l'IA restait cantonné à sa discipline. AlphaGo de Google a battu le champion du monde de Go, mais ne peut même pas décrire les règles d'un jeu d'échecs.

Plusieurs projets se sont lancés à la recherche d'un cerveau numérique pour imiter l'intelligence humaine. Comme le projet européen [Human Brain Project](https://fr.wikipedia.org/wiki/Human_Brain_Project). Les avancées dans une intelligence humaine synthétique semblaient tellement tourner en rond, que des sommités du domaine comme Yann LeCun ne [prédisait pas une IA généraliste avant plusieurs décennies](https://www.letemps.ch/economie/cyber/yann-lecun-machines-manquent-sens).

Puis des entreprises comme OpenIA ont cherché d'autres chemins.

Oui ChatGPT ne connaît rien, il ne réfléchit pas comme le ferait un humain. Il se contente de prédire la suite d'une phrase avec ce qu'il a lu sur internet. Et pourtant à la fin, il a réponse à tout.

Et oui une roue ne marche pas comme les humains. Et pourtant à la fin, elle permet de se mouvoir.


La force humaine a été décuplée par les roues et les engrenages. L'invention ultime a été le moteur qui permet à un humain de disposer d'autant de force qu'il veut. Les limites pour mouvoir des choses sont maintenant déterminées par l'ingénierie (faire de gros moteurs) et l'énergie (alimenter ces gros moteurs).

L'intelligence humaine a été décuplée par le langage et l'écriture. L'invention ultime risque d'être ChatGPT qui permet à un humain de disposer d'autant de cerveau qu'il veut. Les limites pour réfléchir seront maintenant imposées par l'ingénierie (faire des grosses IA) et l'énergie (alimenter ces grosses IA).

ChatGPT marque la première IA généraliste. Nous sommes bien sûr une révolution, qui marquera le progrès technique.

