---
title: Nvidia est un colosse aux pieds d'argile.
subtitle: Nvidia peut facilement se faire doubler
date: 2024-07-28
layout: post
background: /img/2024/07/nvidia.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-qM1s" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=B19mZFVXNJmN&v=3&playerId=ausha-qM1s"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>


Nvidia est la surprise de la décennie. L'entreprise conçoit des cartes graphiques, un composant optionnel sur les ordinateurs, nécessaire uniquement pour jouer aux jeux vidéo, faire du montage vidéo ou de la conception 3D. L'entreprise restait depuis des années sur ce "gros marché de niche" avec  une forte concurrence de la part d'AMD, Gygabyte, ASUS ou autre.

Un "gros marché" de [3.6 milliards de dollars](https://www.mordorintelligence.com/fr/industry-reports/gaming-gpu-market), mais de "niche" comparer à l’électronique grand public comme les ordinateurs portables, smartphones ou TV.

On imaginait donc mal Nvidia, la troisième entreprise tech la plus valorisée, devant des mastodontes présents sur des marchés plus gros comme Google ou Samsung. Tout le monde possède un produit Apple, Samsung, Google ou Microsoft, mais qui possède une carte graphique Nvidia chez lui ?

Pour comprendre l'ascension du fabricant, il faut comprendre la différence entre les cartes graphique, appelées Graphical Process Unit (GPU) et les processeurs, appelés Central Process Unit (CPU). Les deux exécutent du code pour réaliser des calculs.

Les tâches comme lire les mails, ouvrir un document ou naviguer sur internet tournent sur CPU qui ont 4, 8, 16 voire 38 coeurs puissants. Les rendus graphiques demandent beaucoup de calculs simples en même temps. On les lance donc sur les GPU qui disposent de [10 000 coeurs](https://www.nvidia.com/fr-fr/geforce/graphics-cards/30-series/rtx-3090-3090ti/) de faibles puissances.

Historiquement dédiées aux calculs graphiques, les GPU s'ouvrent à d'autres domaines, comme les [simulations physiques](https://developer.nvidia.com/gpugems/gpugems2/part-vi-simulation-and-numerical-algorithms/chapter-43-gpu-computing-protein), le [minage de cryptomonnaies](https://minerstat.com/hardware/nvidia-rtx-3080-ti) ou encore le [cassage de mot de passe](https://www.tomshardware.com/pc-components/gpus/nvidias-flagship-gaming-gpu-can-crack-complex-passwords-in-under-an-hour).

Maintenant c'est autour de l'IA de migrer ses tâches de calcul vers les GPU pour gagner en performance. Cela fait des années que l'on entraîne les IA sur GPU, par exemple une IA type yolov5 pour détecter des objets demande [4 GPUs](https://docs.ultralytics.com/yolov5/tutorials/multi_gpu_training/#results) pour être entraînée. Puis cette IA peut tourner sur n'importe quel CPU (ordinateur ou smartphone) sans besoin de GPU.

Cependant les nouvelles IA comme chatGPT ou Midjourney sont des orges de puissance. Une IA type chatGPT-4 nécessite [4096 GPUs](https://huggingface.co/tiiuae/falcon-180B) pour être entraînée et [8 GPUs](https://huggingface.co/tiiuae/falcon-180B) pour être utilisée.

Or actuellement, seule Nvidia dispose de GPU assez puissant pour l'IA. L'entreprise vend par palette entière son best-seller, le [H100](https://www.digitec.ch/fr/s1/product/nvidia-h100-80-go-carte-graphique-32012280?utm_campaign=organicshopping&utm_source=google&utm_medium=organic&utm_content=406802&supplier=406802) pour 30,000€ pièce. Rien qu'Elon Musk en a commandé [100 000](https://www.businessinsider.com/elon-musk-hints-how-much-grok-ai-chatbot-training-costs-2024-7) pour concevoir sa propre IA.

<h2>Les IA n'ont pas besoin de Nvidia</h2>

Les briques logiques qu'on assemble pour former une IA ne sont pas liées à des GPU Nvidia. On les construit à travers un framework logiciel open source comme [pyTorch](https://fr.wikipedia.org/wiki/PyTorch) de Meta ou [Tensorflow](https://fr.wikipedia.org/wiki/TensorFlow) de Google.

Un modèle IA est concrètement une suite d'instructions pour pyTorch ou Tensorflow. Par la suite, le framework va convertir ses instructions en instructions pour CPU ou GPU.

Ces frameworks sont open source, utilisable et modifiable par n'importe qui. Nvidia dispose du meilleur hardware dans l'IA, mais n'est pas "vendor lock". Si demain, une meilleure alternative débarque, on l'intégrera dans les frameworks et toutes les IA pourront tourner sur le nouveau hardware sans modifications.

Si l'IA était une voiture, Nvidia est les roues, le framework est le moteur. Changer de framework demande de refaire toute la voiture. Mais passer d'un GPU Nvidia à autre chose revient juste à changer les pneus.

La situation de monopole de Nvidia est moins prospère qu'imaginée, le moindre concurrent peut tout faire basculer, et les concurrents sont déjà là.

<h2>Nvidia déjà sous pression</h2>

Sans modifier le hardware, on peut déjà optimiser le software. Soit en concevant des IA plus frugales, comme la société Mistral. Son modèle [Mistral 7B tourne sur un mac mini M1](https://www.codable.tv/chatgpt-chez-soi/) d'une valeur de 800€. On peut aussi optimiser le framework et ses instructions. Une équipe de chercheurs pense pouvoir contourner un blocage qui [réduira le calcul d'une IA](https://intelligence-artificielle.developpez.com/actu/359612/Des-chercheurs-bouleversent-le-statu-quo-sur-la-formation-des-LLM-en-eliminant-du-processus-la-multiplication-matricielle-ce-qui-reduit-les-couts-lies-aux-exigences-elevees-en-matiere-de-calcul-et-de-memoire/) au point de réduire par 100 sa consommation d'énergie.

Nvidia peut voir son chiffre d'affaires fondre, avec une seule mise à jour des frameworks apportant des optimisations dans les calculs.

Ensuite, on peut changer de hardware. Des entreprises travaillent sur du hardware dédié à l'IA, appelé Tensor Process Unit (TPU). Google était le pionner dans ce domaine avec sa filiale [Coral.ia](https://coral.ai/). Apple semble avoir repris le flambeau en proposant ses TPU (appelés [Neuronal Engine](https://en.wikipedia.org/wiki/Neural_Engine)) dans ses produits iPhone, iPad ou Mac. L'entreprise souhaite rester dans la course en musclant toujours plus son hardware afin de suivre les IA types chatGPT.

Enfin, une myriade de startups se lancent sur le secteur des TPU tels [Hailo](https://hailo.ai/products/generative-ai-accelerators/hailo-10h-m-2-generative-ai-acceleration-module/), [Groq](https://wow.groq.com/why-groq/), [Tenstorrent](https://tenstorrent.com/hardware/grayskull) ou [Sima](https://sima.ai/mlsoc-boards/). Encore une fois, si un seul TPU est plus performant que les GPU Nvidia, toutes les IA pourront tourner sur ce nouveau TPU, laissant Nvidia sur le carreau.

Nvidia peut bien sûr riposter en proposant ses propres TPU ou en rachetant des startup prometteuses. Elle va devoir se battre pour rester au niveau, car il sûr que de meilleures alternatives que ses GPU arriveront.

L'entreprise fait la course en tête, mais pour combien de temps encore ?
