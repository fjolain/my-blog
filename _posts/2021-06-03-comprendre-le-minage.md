---
title: Pourquoi ne peut-on pas prédire la pollution du Bitcoin ?
subtitle: La rentabilité d'une mine de Bitcoin est fragile. Dès lors le futur du minage est imprédictible.
date: 2021-06-03
layout: post
background: /img/2021/06/money.jpeg
published: www.contrepoints.org/2021/07/06/401012-pourquoi-ne-peut-on-pas-predire-la-pollution-de-bitcoin
categories: [article, podcast]
tags: [bitcoin]
---

<iframe frameborder="0" loading="lazy" id="ausha-j0NT" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=b7NXvSexYDOL&v=3&playerId=ausha-j0NT"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Toutes les prédictions sur la consommation du réseau bitcoin se sont avérées fausses. En 2017, un "scientifique" défrisait la chronique en prédisant que [Bitcoin consommerait toute l'électricité mondiale en 2020](https://www.newsweek.com/bitcoin-mining-track-consume-worlds-energy-2020-744036). Depuis, bien que totalement fausse, cette "étude" fait encore office de sainte parole pour toute personne souhaitant dénigrer le Bitcoin s'en trop se fouler.

Pour prédire la pollution du Bitcoin, il faut prédire son minage et sa rentabilité. Nous allons donc nous intéresser à la rentabilité de notre future ferme de minage bitcoin.

<h1>Qu'est-ce que le minage ?</h1>

Il existe de nombreuses explications sur ce qu'est le minage (par exemple [celle-ci](https://www.youtube.com/watch?v=0uYJQpuNxDs)). On va tenter de faire vite et de ne garder que les caractéristiques importantes qui impactent la rentabilité de notre mine de Bitcoin.

**Le minage est indispensable au Bitcoin.** Il s'agit d'un jeu mathématique que se livre les mineurs. La résolution du jeu nécessite une recherche collective de 10min. À la fin, l'heureux élu ayant trouvé la solution, peut mettre à jour le registre et se rétribuer des bitcoins. Une fois la solution et le registre propagés sur le réseau, les mineurs se mettent en quête de la prochaine solution.

Il y a donc, en continu, des mineurs qui se livrent une bataille pour résoudre un jeu mathématique. **Dès qu'un mineur trouve la solution, il fait perdre les autres. Et tout le monde recommence une nouvelle partie du jeu.**

Dans ce jeu, votre chance de gagner équivaut à votre puissance de calcul. Si vous disposez de 30% de la puissance de calcul du réseau, vous avez 30% de chance de résoudre le jeu à chaque partie. Point important, la difficulté évolue avec la puissance de calcul globale. Ainsi **la résolution prend toujours 10 minutes, peu importe la puissance de calcul du réseau.**


<h1>Une Rentabilité soumise aux règles du minage</h1>

Le minage a donc de très fortes contraintes techniques qui vont impacter la rentabilité de notre ferme de minage.

<h2>Une mine commune</h2>
C'est le gros inconvénient du minage de Bitcoin. Admettons que l'on se lance dans une mine d'or. Notre mine produit 2 tonnes d'or par an. Si une mine chinoise, brésilienne ou canadienne se lance à leur tour, ça n'impacte pas notre production. Dans le cas du Bitcoin si !

Nous pouvons nous lancer dans le minage du Bitcoin avec 20% de la puissance de calcul du réseau ce qui fait (à titre d'exemple) 1000 bitcoins par an. Mais si des mines de Bitcoin s'installent à travers le monde, notre part dans le réseau descend à 10%, et par conséquent notre production descend à 500 Bitcoins.

Avec le bitcoin chacun est en compétition dans la même mine. Plus il y a de monde, moins nous gagnons... Ainsi une ruée vers le bitcoin, avec une hausse exponentielle des mineurs, risque de se finir en faillite collective.

<h2>Une récompense qui décroît</h2>

Dans le minage, on peut prédire avec certitude une seule chose, votre récompense décroît. C'est codé dans le logiciel, la récompense est divisée par deux tous les quatre ans (appelé [halving](https://fr.wikipedia.org/wiki/Bitcoin#Cr%C3%A9ation_de_bitcoins)). Un mineur gagnait 50 BTC par solution trouvée en 2008, puis 25 BTC en 2012, puis 12.5 BTC en 2016 et 6.125 BTC en 2020, etc.

L'industrie du Bitcoin est donc à l'heure actuelle, la seule basée sur une décroissance ! Les écolos ont de quoi se réjouir. Pour nous, cela n'arrange pas nos affaires. Même si personne ne vient nous embêter, notre production sera divisée par deux tous les quatre ans.

Alors, il reste un espoir, car en tant que mineur, on gagne les frais liés aux transactions. Ces frais sont libres et dépendent du niveau de sollicitation du registre. Plus il y a du monde utilisant Bitcoin, plus il faudra payer des frais pour avoir la priorité.


Cependant, rien ne garantit une hausse des frais dans le futur. D'ailleurs les problèmes de saturation sur le registre ont donné lieu à beaucoup d'améliorations (SigWit, Lightning, ....) pour y remédier. Encore une fois ça n’arrange pas nos affaires.

<h2>La technologie, toujours la technologie</h2>

Le Bitcoin se mine avec des ordinateurs. Et notre rémunération dépend de notre puissance de calcul par rapport au reste du réseau. Or de nouveaux ordinateurs plus efficaces pour miner arrivent tous les jours. Les mineurs entrants font déjà baisser notre production, et ils arrivent mieux équipés que nous, aggravant encore notre manque à gagner.

La production de Bitcoin a déjà connu deux sauts technologiques avec un gain de 1000 entre les deux (du CPU au GPU puis du GPU au ASCI). Si un tel saut arrive durant notre activité, on est bon pour faire faillite ou changer tout notre matériel.


<h1>Une rentabilité soumise à deux prix volatiles</h1>

<h2>Le prix Bitcoin</h2>
Notre gain est en Bitcoin, mais toutes nos dépenses (salaires, local, électricité, matériel, etc) sont en euro. Ainsi même avec une production de Bitcoin stable dans le temps, notre rentabilité est liée au prix du Bitcoin. Or comme vous le savez, le prix du Bitcoin peut tout aussi bien grimper de 200% en un mois, comme perdre 60% et nous mettre en faillite.

<h2>Le prix de l'électricité</h2>
Et si je vous disais qu'il existe un marché encore plus volatile que le Bitcoin : celui de l'électricité. Pour nous simple particulier, le prix est fixe, mais en réalité le prix évolue énormément.

La raison vient que l'électricité ne peut se stocker. Il faut donc à chaque seconde trouver preneur pour tous les MW produits. Heureusement certaines productions comme les centrales à gaz ou au charbon peuvent s'éteindre et s'allumer facilement. Mais les éoliennes, les barrages hydrauliques ou les panneaux solaires  ne peuvent être régulés.

Comme vous pouvez le voir sur le graphique venant de [RTE](https://www.rte-france.com/eco2mix/les-donnees-de-marche#), le prix peut débuter la semaine à 60€/MW et terminer à 20€/MW voire à -60€/MW comme pour l'Allemagne.

![rte](/img/2021/06/rte.png)

Un prix négatif de l'électricité et personne n'en parle ! Pour le pétrole c'est historique et ça a fait la une, mais pour l'électricité c'est très courant. Si un producteur d'éolien à trop d'électricité, et que la consommation est au plus bas, il est bien obligé de la céder, voire de payer pour s'en débarrasser, d'où les prix négatifs.

Alors pour nos affaires que signifie tout ça, bon ou mauvais ? Et bien un peu des deux. Nous consommons beaucoup d'électricité, notre rentabilité dépend du prix de l'électricité. Une incertitude financière de plus ! Mais notre ferme de minage est très souple, on peut l'installer un peu partout, et on peut faire varier sa consommation d'une manière très flexible.

Nous pouvons donc pleinement profiter de la surproduction des énergies renouvelables, c'est-à-dire le moment où le prix est au plus bas. On peut même se lier avec un producteur pour lui garantir un prix de rachat de son énergie, ça lui évitera de la céder gratuitement.

C'est ainsi que l'industrie du minage utilise [74% d'énergies](https://journalducoin.com/bitcoin/actualites-bitcoin/coinshares-rapport-74-energie-utilise-bitcoin-energie-renouvelable/) renouvelables, majoritairement de l'hydraulique. **Ce n'est pas une idéologie, mais une nécessité de rentabilité.**

<h2>Bonus: volatilité des lois</h2>
Et enfin pour finir, il y a les lois. Les gouvernements trouvent toujours une bonne raison pour interdire le minage du Bitcoin. Par exemple, on assiste actuellement à un revirement de situation en [Chine qui l'a interdit](https://investir.lesechos.fr/marches/bitcoin-cryptomonnaies/nouvelle-offensive-de-la-chine-contre-le-bitcoin-1964051.php), alors qu'elle était le premier mineur de Bitcoin au monde. [L'Iran lui l'interdit jusqu'à l'automne.](https://www.capital.fr/entreprises-marches/le-minage-de-cryptomonnaies-interdit-en-iran-jusqua-lautomne-1404346)

L'Europe n'est pas épargnée, la France crée des réglementations toujours plus contraignantes comme le [PSAN](https://www.amf-france.org/fr/espace-professionnels/fintech/mes-relations-avec-lamf/obtenir-un-enregistrement-un-agrement-psan). [En Allemagne, les écolos souhaitent tout simplement l'interdire](https://www.20minutes.fr/insolite/3029419-20210426-allemagne-millionnaire-grace-bitcoin-milite-interdiction-cryptomonnaies).


<h1>Conclusion</h1>
Alors notre ferme de minage sera-t-elle rentable ? Combien de mineurs peuvent être en concurrence sans finir en faillite ? Quel est le prix du Bitcoin minimum pour être rentable ? Quel est le prix de l'électricité maximal pour être rentable ?

Toutes ces questions n'ont pas de réponse. Il n’existe aucune boule de cristal pour connaître la consommation future du minage. C'est une toute nouvelle industrie avec des contraintes et dynamiques complexes. Les conclusions chocs comme "Bitcoin va polluer comme tout un continent" relèvent plus d'un combat idéologique que d'une reflexion approfondie.

