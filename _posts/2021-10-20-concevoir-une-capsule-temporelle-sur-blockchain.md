---
title: Concevoir une capsule temporelle sur blockchain
subtitle: Le dernier numéro d'Usbek & Rica souhaite le retour des capsules temporelle. Je vous propose un tutoriel pour en faire sur blockchain.
date: 2021-10-20
layout: post
background: /img/2021/10/bouteille.jpeg
categories: [article, podcast]
tags: [société, blockchain]
---

<iframe frameborder="0" loading="lazy" id="ausha-8rce" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=brDkxfYO7d5W&v=3&playerId=ausha-8rce"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Le dernier numéro de septembre 2021 d'Usbek & Rica montre le besoin de faire des capsules temporelles pour se reconnecter avec une vision long terme. Il se trouve que l'écosystème blockchain a fécondé des innovations pour pérenniser de l'information sur le long terme, outils parfaits pour réaliser une capsule temporelle.

Premièrement, une blockchain est un registre mathématique immutable décentralisé, dont le contenu est répliqué sur plusieurs noeuds à travers le monde. Aujourd'hui, il n'a pas mieux pour assurer la survie de l'information sur des générations.

Deuxièmement, il faut savoir que l'on ne détient pas de cryptomonnaie en propre. Les cryptomonnaies restent sur les blockchains, l'utilisateur détient le moyen de s'identifier sur la blockchain. Un peu comme avec votre CB, l'argent n'est pas dedans. La CB ne sert qu'à donner un ordre de transaction à votre banque.

Pour les cryptomonnaies, le moyen d'identification est une clé secrète de cryptage faisant 256 bits. C'est là que le problème arrive. Comment conserver secrètement 256  bits sur du long terme ?

Il a d'abord eu l'idée de convertir ces 256 bits en quelque chose de plus *humain*. Ainsi chaque détenteur de cryptomonnaie, lors de sa première utilisation, reçoit 24 mots à conserver. Ces 24 mots sont une sauvegarde, qui avec une moulinette permet de reconstruire la clé secrète de 256 bits.

Ensuite, comme ces 24 mots étaient très importants, la conservation sur du papier ne suffisait pas. Des entreprises ont donc conçu des moyens pour stocker ces 24 mots sur du métal. De tels objets prennent la forme de plaques ou de tubes, où l'on vient enfiler les mots, lettre par lettre comme des perles.

![capsule de la marque cryptosteel](/img/2021/10/cryptosteel.png)
*capsule de la marque cryptosteel*

Les voilà nos capsules temporelles. Elles sont faites pour durer des siècles. De plus, elles stockent une clé secrète pour cryptomonnaie, ce qui permet de faire voyager aussi bien de l'argent, des oeuvres d'art (par NFT) ou même des fichiers quelconques (avec [Sia](https://www.sia.tech) ou [FileCoin](https://filecoin.io/)).

**Vers la chasse au trésor avec Shamir**

Dans le paysage crypto, on trouve des inventions étonnantes. Ainsi l'algorithme de [Shamir](https://fr.wikipedia.org/wiki/Partage_de_cl%C3%A9_secr%C3%A8te_de_Shamir) permet de diviser une clé secrète en plusieurs sous-clés. Par exemple, avec un Shamir *2/3*, la clé est divisée en 3 sous-clés dont il faut au moins 2 pour reconstruire la clé principale.

Ainsi la capsule prend des allures de chasse au trésor, il faut retrouver plusieurs clés éparpillées pour mettre la main sur le butin.
