---
title: L'écologie détruit notre monnaie et notre épargne.
subtitle: Notre monnaie faite de fiction et de mensonges a donné lieu à des utopies faites de fictions et de mensonges.
date: 2023-03-16
layout: post
background: /img/2023/03/dollar.png
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-H72k" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=brDkxf5zgDD6&v=3&playerId=ausha-H72k"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

En soi, l'écologisme religieux promet bien des manières pour finir pauvre au plutôt en "sobriété choisie", pour reprendre la novlangue gouvernemantale.

On peut bien sûr [raréfier l'accès à l'énergie](https://www.lemonde.fr/energies/article/2020/02/19/ce-qu-il-faut-savoir-sur-l-arret-du-reacteur-de-fessenheim_6030117_1653054.html), imposer toujours [plus de taxes](https://www.ecologie.gouv.fr/fiscalite-des-energies) pour racheter nos péchés ou créer des pénuries dans [les logements](https://www.bfmtv.com/immobilier/logement-le-marche-de-la-location-au-bord-de-la-penurie-a-la-rentree_AV-202208250292.html) ou l'[alimentaire](https://www.contrepoints.org/2022/10/30/378519-pour-moi-agriculteur-fini-la-betterave) par des règles écolos punitives au nom de Gaia.

Oui on peut le faire et ça marche. Mais il y a mieux. Le dogmatisme écolo est entrain de détruire notre monnaie et notre épargne. Explication.

<h2>Nos monnaies sont des châteaux de cartes</h2>

Notre monnaie est dite "scripturale", car il n'est pas nécessaire de faire le moindre effort pour en créer. Elle apparaît comme par magie dans le bilan des banques lors de [l'octroi d'un crédit](https://fr.wikipedia.org/wiki/Cr%C3%A9ation_mon%C3%A9taire).

En vous accordant un prêt, la banque ne déplace pas de l'épargne ni ne demande de l'argent à la banque centrale. Elle déclare au passif un nouveau dépôt sur votre compte et à l'actif une créance envers vous.

Il a donc création monétaire lors de l'octroi d'un crédit et destruction monétaire lors du remboursement. Ce système fort atypique permet de tout financer sans limites budgétaires. Mais encore faut-il rembourser son crédit !

Si vous investissez dans l'industrie, des écoles ou des hôpitaux, vous allez accroître le PIB, créer de la valeur et ainsi rembourser votre dette en dégageant un bénéfice pour vous et les intérêts du banquier.

Cette situation a donné les 30 glorieuses. La France voyait son PIB explosé grâce à la relance de son industrie. La dette par rapport au PIB restait stable.

Aujourd'hui, avec les 30 piteuses, nous voyons l'inverse. Le PIB n'augmente plus, mais la dette explose.

<h2>Les châteaux de cartes ne tiennent qu'avec de la croissance.</h2>

Pour en arriver là, rien de plus simple, il suffit d'investir dan des projets qui ne rapportent rien. Comme les [600 milliards d'euros](https://www.nouvelobs.com/economie/20220421.OBS57437/600-milliards-de-dettes-supplementaires-faut-il-vraiment-s-en-inquieter.html) empruntés par Macron durant le covid. Avez-vous vu les nouvelles centrales nucléaires, les nouveaux hôpitaux ou programmes de recherche ? Et non, tout est parti dans des aides superflues, des vaccins douteux et des conseils médiocres de McKinsey.

Ce déluge de mauvais investissement est un avant-goût de l'écologisme. On a investi des milliards dans des panneaux solaires ou des éoliennes qui ne rapportent guère. Il reste encore à subventionner la voiture électrique pour enrichir les fabricants de batteries chinois. Installer toujours plus de renouvelables pour enrichir les producteurs de gaz américain. Ou saboter notre agriculture pour enrichir les agriculteurs brésiliens.

Tous ces beaux projets ont le mérite de ne pas être rentables. Alors que se passe-t-il, si on ne rembourse pas le crédit ?

Avec l'État, dans un premier temps, il va juste rembourser le crédit par un nouveau crédit plus grand. On retrouve ce mécanisme avec l'explosion de la dette actuelle.

Cet accroissement de la masse monétaire par le crédit va créer de l'inflation. L'état peut aussi directement "imprimer" de l'argent par la banque centrale. C'est le quantitative easing de la BCE, utilisé à hauteur de [3
500  milliards d'euros](https://www.ecb.europa.eu/mopo/implement/app/html/index.en.html) depuis 2008. Ce second levier gonfle aussi l'inflation.

L'inflation nous plonge déjà dans la pauvreté, mais le bouquet final va venir quand l'État, les entreprises ou les citoyens ne pourront plus rembourser leurs crédits.

Dans ce cas, même avec une perte de 10%, les banques font faillite et sans doute ce qui reste de l'euro aussi.

Notre système monétaire actuel ne peut supporter des projets douteux et encore moins une décroissance. Nous devons toujours créer plus de richesse pour rembourser nos crédits.

Alors pourquoi, toute la classe politique fonce dans l'écologisme et impose leur mauvaise gestion aux acteurs privés ? Le fait qu'il faut [forcer les banques à financer](https://fincley.com/le-green-asset-ratio-un-premier-pas-vers-davantage-de-transparence-des-banques-europeennes-dans-la-lutte-contre-le-rechauffement-climatique/) ce type de projet montre qu'ils ne sont clairement pas rentables.

<h2>Les fausses monnaies donnent de fausses utopies</h2>

En soit, il existe des monnaies, comme l'or et le bitcoin qui s'accommodent de ces choix calamiteux.

L'État peut financer autant d'éoliennes qu’il veut, mon or et mes bitcoins resteront à moi, contrairement à mon argent en banque, qui va disparaître lors de la faillite. De plus, l'or et le bitcoin ne tomberont pas à zéro lors de la faillite de l'État, contrairement à nos monnaies scripturales.

Avant de financer le désastre écologique, il faudrait au moins vider les banques pour revenir à l'or. Mais s’il n’a plus d’argent en banque, il n’y a plus de  monnaie scripturale, plus de planche à billets ou de planche à dette, comment l'écologisme va trouver des financements ?

On s'aperçoit, que tous ces courants de pensés dispendieux (socialisme, écologisme, wokisme) existent uniquement dans un monde où la monnaie et la dette s'impriment en illimité.

Notre monnaie faite de fiction et de mensonges a donné lieu à des utopies faites de fictions et de mensonges.  L'un ne va pas sans l'autre, les deux vont dans le mur.



