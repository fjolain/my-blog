---
title: Contrer les pirates russes grâce à 3 bonnes pratiques
subtitle: Gardez vos logiciels à jour. Protégez vos comptes en ligne. Faites des sauvegardes de vos données.
date: 2022-03-11
layout: post
background: /img/2022/03/pirate.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [sécurité]
---

<iframe frameborder="0" loading="lazy" id="ausha-uUQZ" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=od5jXtRLxXVp&v=3&playerId=ausha-uUQZ"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

La Russie est connue pour ses compétences en piratages informatiques. Elle les utilise actuellement en Ukraine et pourrait se lancer à l'assaut de l'Europe pour déstabiliser des pays comme la France.

Aussi, il est de notre devoir de ne pas faciliter la vie des pirates russes en optant pour 3 bonnes pratiques.

<h2>I - Garder les systèmes à jour</h2>
Si un pirate russe tombe sur une faille, il n'y a plus rien à faire. Il peut corrompre l'ordinateur de sa victime en appuyant sur une seule touche.
On en découvre fréquemment de nouvelles. Une des plus graves et aussi la plus récente. La faille Log4shell touche beaucoup de logiciels et permet de prendre le contrôle des ordinateurs à distance.

Il est déjà avéré que les pirates russes utilisent cette faille pour pirater des serveurs non mis à jour en Ukraine.

À nous donc de ne pas faciliter la vie des pirates en mettant régulièrement nos systèmes à jour afin de colmater les nouvelles failles. Que ce soit les mobiles (Android, iOS), les ordinateurs (Windows, macOS ou Linux) ou les navigateurs (Chrome, Safari, Firefox), il faut toujours garder à jour nos systèmes.

Évidement que le piratage d'un serveur d'entreprise est la cible numéro 1 d'un pirate. Mais le piratage d'un ordinateur de particulier est important. Ce sont depuis des ordinateurs de particuliers que les pirates lancent leurs attaques massives contre les serveurs.

De plus avec le télétravail, pirater l'ordinateur d'un particulier peut très facilement mener aux réseaux d'entreprises. Nous avons donc un rôle majeur à jouer en gardant nos systèmes à jour.

<h2>II - Protéger vos comptes en ligne</h2>

Les mots de passe du type `piano123` ou `marseille13!` ne sont pas de bon mot de passe. Un pirate peut les retrouver facilement.

De même si on utilise le même mot de passe pour notre club de sport et notre boîte Gmail. Le pirate a juste, pour lire vos mails, à pirater le modeste site web de votre club de sport au lieu de Google. On lui mâche le travail !

Dans ce cas, la bonne pratique est d'utiliser un gestionnaire de mot de passe comme [Bitwarden](https://bitwarden.com/) (gratuit et open source). Il va générer un mot de passe compliqué et différent pour chaque compte en ligne.

Tous vos mots de passe sont chiffrés dans le cloud pour être synchronisés sur vos appareils. Si le cloud ne vous tente pas, un carnet d'adresses est parfait pour conserver vos mots de passe en fonction du site web.

Cependant, ce n'est pas tout ! Les attaques par hameçonnage se multiplient. Le pirate envoie un mail urgent imitant le code graphique de votre banque. Le mail stipule que vous devez au plus vite vous connecter et il n'oublie pas de vous donner le lien.

Or le lien est frauduleux. Il vous entraîne vers un faux site web ressemblant à votre banque. Lorsque vous entrez vos identifiants, ils seront récupérés par le pirate russe.

Évidemment dans ce cas, la première chose à faire est de ne jamais cliquer sur un lien d'un mail suspect.

Mais personne n'est infaillible. Aussi depuis quelques années est apparu le second facteur d'authentification (2FA). En plus d'entrer votre mot de passe, votre banque vous demande un code qui change à chaque connexion. Le code peut venir d'une application mobile, d'un mail ou d'un SMS.

Ainsi même en possession du mot de passe, le pirate ne pourra pas se connecter à votre compte. Le 2FA est réputé réduire de [99%](https://published-prd.lanyonevents.com/published/rsaus20/sessionsFiles/18466/2020_USA20_IDY2-F03_01_Breaking-Password-Dependencies-Challenges-in-the-Final-Mile-at-Microsoft.pdf) les usurpations de compte. Il est donc indispensable de l'utiliser pour votre banque, messagerie, réseau social, drive, etc.

<h2>III - Faire des sauvegardes</h2>

L'attaque informatique en pleine tendance est le rançongiciel. Le pirate s'introduit sur l'ordinateur et lance un logiciel qui va chiffrer toutes vos données contre votre gré.

Seul le pirate dispose de la clé pour déchiffrer, qu'il vous donne (ou pas) contre une rançon.

À partir du moment où vous êtes victimes d'un rançongiciel, vous pouvez considérer vos données comme perdues. Le fait de payer la rançon garantit d'alimenter un réseau criminel, mais aucune ne garantit de retrouver vos données.

Aussi dans ce cas, la solution est l'anticipation. Il faut faire des sauvegardes de vos appareils sur des disques durs externes. Heureusement, cela est devenu très facile. Maintenant tous les systèmes Android, iOS, Windows, macOS ou Linux proposent des outils de sauvegarde automatique. Il suffit de [se procurer un disque dur externe](https://www.boulanger.com/ref/1136409) et de prendre le temps de configurer.

En résumé pour compliquer la vie d'un pirate russe, c'est très simple. Il faut :

- garder ses appareils et logiciels à jour
- utiliser un gestionnaire de mot de passe et activer le second facteur d'authentification partout où c'est possible
- faire des sauvegardes régulières sur un disque dur externe.

Des choses très simples qui vont rendre le pirate fou et vous mettre à l'abri de bien des problèmes.


