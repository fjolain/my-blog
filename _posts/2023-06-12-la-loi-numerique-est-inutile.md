---
title: Comment contourner les 12 propositions de la loi numérique
subtitle: 12 propositions dont pas une n'est utile voire applicable.
date: 2023-06-12
layout: post
background: /img/2023/06/macron.png
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-FbVc" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=brDkxfq0e0Y6&v=3&playerId=ausha-FbVc"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

    Les combats très intimes que nous menons [...] sont à chaque fois bousculés parce que des contenus qui disent exactement le contraire circulent librement sur les plateformes

Cette phrase ne provient ni de Poutine ni de Xi Jinping, mais de Macron lui-même dans le [dossier de presse](https://www.economie.gouv.fr/files/files/2023/dp_projet_loi_numerique.pdf?v=1683794564) de la proposition de loi du numérique.

Nos députés vont avoir la chance de débattre d'une loi fourre-tout qui souhaite censurer la liberté d'expression, combattre le cyberharcèlement, stopper la pornographie, résister à AirBnB et bien d'autres.

Puisque nos députés sont trop occupés à [vider le bar de l'assemblée](https://www.lepoint.fr/politique/retraites-la-consommation-d-alcool-en-hausse-a-la-buvette-de-l-assemblee-27-02-2023-2510135_20.php), nous allons voir par nous mêmes, si techniquement les 12 propositions sont applicables.

(Les rubriques *Problème* et *Proposition* viennent directement du dossier de presse)


<h2>1. Créer un filtre de cybersécurité antiarnaque</h2>
**Problème :** Après avoir reçu un faux SMS de l’assurance maladie l’invitant à cliquer sur un lien, l’internaute risque d’y déposer ses coordonnées bancaires.

**Proposition :** Au moment de cliquer sur le lien, il ou elle recevra un message lui indiquant que le site vers lequel il se dirige est compromis.

**Analyse :** Il y a déjà un gros problème constitutionnel. Comment peut-on informer que le site est nuisible sans espionner les messages des citoyens ? [La loi du renseignement](https://fr.wikipedia.org/wiki/Loi_relative_au_renseignement) de 2015 oblige déjà les Fournisseurs d'Accès Internet (FAI) à espionner les citoyens pour lutter contre le terrorisme. Cette proposition ne généralise-t-elle pas insidieusement cette écoute à tout internet ?

De plus un site web avec un nouveau nom de domaine se fait en seulement 10 minutes. Les sites d'arnaques n'auront aucun mal à changer de site autant de fois que nécessaire s'ils se retrouvent blacklister.

Sans même parler des raccourcisseurs d'URL comme bit.ly qui pourront être utilisé pour cacher l'URL de l'arnaqueur dans les messages.


<h2>2. Choisir librement son moteur de recherche, son navigateur, sa messagerie</h2>
**Problème :** Pour communiquer avec ses proches qui ont installé une application comme WhatsApp ou Olvid, il est nécessaire d’installer la même application qu’eux.

**Proposition :** Comme c’est déjà le cas pour les mails, il sera possible de communiquer avec ses proches sans avoir à disposer de la même messagerie qu’eux.

**Analyse :** Voilà que le gouvernement veut normer les applications de messageries ! Il y a un problème technique. Les mails utilisent le même protocole ce qui les rend interopérables. Or une application comme Olvid ne communique pas de la même manière que Whatsapp.

Le projet open source matrix.org souhaite justement faire une sorte de pont entre toutes les apps. Un autre projet nostr veut reproduire un protocole unique comme les mails, mais pour les messageries.

La France sombre dans une administration absolue qui veut normer jusqu'au protocole de messagerie. Ce n'est pas son rôle.


<h2>3. Bannir des réseaux sociaux les personnes condamnées pour cyberharcèlement</h2>
**Problème :** Une personne condamnée pour cyberharcèlement sur un réseau social peut continuer à y propager la haine.

**Proposition :** Le juge pourra prononcer une peine complémentaire de suspension de l’accès au compte du réseau social pendant 6 mois, et un an en cas de récidive.

**Analyse :** N'importe qui peut créer autant de comptes anonymes en ligne. Un cyberharceleur pourra toujours créer un autre compte s'il se retrouve banni du premier.

Macron a déjà fait part de son [dégoût pour l'anonymat](https://www.euractiv.fr/section/economie/news/emmanuel-macron-se-prononce-a-nouveau-pour-une-levee-de-lanonymat-sur-internet/) : "Dans une société démocratique, il ne peut pas y avoir d'anonymat." . Cette proposition s'annonce le cheval de Troie pour arrêter l'anonymat en ligne. Une fois promulguée, Macron légitimera qu'il faille maintenant interdire l'anonymat pour pouvoir appliquer cette mesure.

Mais cela ne sera même pas suffisant, l'interdiction de l'anonymat ne sera que pour les citoyens français. Or sur internet, il suffit d'utiliser un VPN (comme protonVPN) pour enjamber les frontières et les législations. Le harceleur devra juste passer par un VPN.


<h2>4. Encadrer les nouveaux types de jeux en ligne </h2>
**Problème :** Le cadre juridique d’interdiction par défaut pénalise l’innovation numérique dans les jeux et le divertissement sans protéger efficacement les utilisateurs.

**Proposition :** La France demeurera une terre d’innovation, tout en étant l’un des premiers pays à adopter une législation protectrice des utilisateurs de jeux fondés sur le Web3.

**Analyse :** Au milieu de tout ce fourbie, cela ne m'étonne pas de trouver une n-ième réglementation des cryptomonnaies. Car oui, le web3 est le nom des nouveaux sites web qui utilisent des cryptomonnaies pour fonctionner. J'ai d'ailleurs fait [un article](https://www.codable.tv/a-quoi-sert-le-web3/) entier sur le web3 pour plus de détails.

Une chose important avec le web3 est qu'il est décentralisé. Comme le paiement se fait par cryptomonnaie et que les données sont sur des blockchaines. Il n'a pas besoin de laisser son identité pour louer un serveur en ligne ou accepter les cartes bancaires.

Je souhaite donc bonne chance au gouvernement pour réguler un site décentralisé.


<h2>5. Mettre fin à l’exposition des enfants aux contenus pornographiques en ligne</h2>
**Problème :** Les sites pornographiques ne mettent pas en œuvre les solutions techniques de vérification d’âge existant pourtant sur le marché.

**Proposition :** Les sites qui persisteront à violer la loi pourront être bloqués sous un mois et être redevables d’une amende allant jusqu’à 4% du chiffre d’affaires mondial.

**Analyse :** Entre les livres érotiques de [Schiappa](https://www.lepoint.fr/culture/marlene-schiappa-ces-livres-erotiques-qu-elle-assume-avoir-ecrits-06-10-2020-2395154_3.php) ou de [Bruno Le Maire](https://www.francetvinfo.fr/politique/bruno-le-maire/un-passage-erotique-du-dernier-roman-de-bruno-le-maire-affole-les-reseaux-sociaux_5799725.html), ce gouvernement semble obnubilé par le porno. Les *techniques de vérification d'âge existant* ne permettent pas de protéger l'identité de l'utilisateur. Les sites pornos n'en veulent pas au risque de perdre tous leurs clients.

Et encore une fois, si la France met une limite d'âge sur les sites pornos, un simple VPN permet de changer de législation.

<h2>6. Retirer plus rapidement les contenus pédopornographiques en ligne</h2>
**Problème :** Les prestataires hébergeurs doivent participer à la lutte contre la diffusion de contenus pédopornographiques. Mais les défaillances sont impunies.

**Proposition :** Les hébergeurs défaillants pourront être poursuivis pour non-respect en 24 heures d’une demande de retrait présentée par la police ou la gendarmerie.

**Analyse :** Cette proposition retente la loi Avia pourtant retoquée par le Conseil Consitutionnel. Il s'agissait au nom de la pédophilie, du terroriste ou du harcèlement de punir les plateformes. Les contraintes, en termes de temps de réponse et d'amende, entraîneraient une censure par les plateformes.

On pourrait croire au rôle magique de l'IA pour analyser le contenu. Sauf que l'IA n'est pas aussi fiable. Il existe déjà [des méthodes qui rajoutent du bruit](https://www.youtube.com/watch?v=SA4YEAWVpbk) dans les images pour duper les IA.

C'est un véritable travail de Sisyphe qui attend les plateformes avec cette proposition. Le seul gagnant sera l'état qui pourra facturer 250 000€ par contenu non retiré à temps.

<h2>7. Interdire la publicité ciblée sur les mineurs ou utilisant des données sensibles</h2>
**Problème :** Les mineurs sont considérés comme des consommateurs précoces et directement ciblés sur la plupart des réseaux sociaux.

**Proposition :** Les plateformes auront l’interdiction absolue de pratiquer le ciblage publicitaire en direction des mineurs, sous peine de lourdes sanctions.

**Analyse :** C'est une bonne chose, je trouve ça scandaleux que le parti au pouvoir se crée un compte Tiktok pour endoctriner les jeunes. Cela ressemble aux pratiques de régimes totalitaires.

Toutefois, si cette proposition ne s'adresse pas à la propagande macronienne, ça se complique. Imaginons qu'une influenceuse à Dubai arnaque les enfants français sur une plateforme chinoise comme Tiktok. Le problème est déjà de savoir dans quel pays porter plainte, en France, aux Émirats ou en Chine ?

Et encore une fois l'administration française met les pieds en dehors du régalien. Elle se substitue à l'éducation par les parents sur les dangers et menaces qui entoure leurs enfants.


<h2>8. Interdire aux géants du numérique de privilégier leurs services sur leurs plateformes</h2>
**Problème :** Les grandes plateformes abusent de leur position dominante d’intermédiaires pour avantager commercialement leurs propres offres.

**Proposition :** La réglementation  commerciale la plus  ambitieuse depuis un siècle,  adoptée pendant la  présidence française de l’UE,  protégera nos entreprises  grâce à une concurrence  équitable.

**Analyse :** Une fois de plus on passe du coq-à-l'âne dans cette loi. Tout y est mis, des markeplaces aux navigateurs en passant par l'hébergeur cloud. Alors que chaque cas est différent.

- La plupart des marketplaces n'ont pas de monopole. On peut installer une app android sans passer par Google Play, on peut acheter un jeux vidéo sans passer par Steam. Le seul acteur qui ne respecte pas la libre concurrence est Apple qui impose son store pour installer une app sur iPhone. Inutile de faire une loi, il a juste à condamner Apple.

- Il n’a rien de plus simple que de changer de moteur de recherche. Au lieu de se rendre sur google.com, il suffit de taper duckduckgo.com, c'est tout. Google est donc à un clic de la concurrence.

- Pour les hébergeurs cloud, nous voilà devant un épineux problème de choix des technologies d'infrastructure.

Il existe des alternatives pour tout. Si vous prenez une base de données DynamoDB sur Amazon Web Service, alors oui vous allez vous retrouver coincer cher eux. Mais si vous choisissez une Postgres, vous pouvez la migrer où vous voulez. Tout dépend des choix technologiques faits.

Plutôt que de pondre des lois, le gouvernement devrait montrer l'exemple et arrêter de courir chez Microsoft pour tous leurs besoins. L'État utilise Microsoft pour tout, y compris pour stocker les [données de santé des Français](https://fr.wikipedia.org/wiki/Dossier_m%C3%A9dical_partag%C3%A9#Le_Health_Data_Hub).

<h2>9. Réduire la dépendance des entreprises aux fournisseurs de cloud</h2>

**Problème :** Pour changer de fournisseur cloud, une entreprise doit payer des frais représentant 125% de son coût d’abonnement annuel.

**Proposition :** Aucun frais de transfert ne sera facturé en changeant de fournisseur. Le non-respect de cette interdiction sera sanctionné d’une amende allant jusqu’à 1M€, et 2M€ en cas de récidive.

**Analyse :** Cela ressemble à la proposition précédente. Il n'y a aucune taxe à la migration vers un autre cloud, ça n'existe pas. Le prix vient du travail pour changer le code ou les données vers la nouvelle plateforme. Encore une fois, il existe des technologies open source qui couvrent 99% des besoins informatiques (linux, docker, kubernetes, postgres, redis, etc) et qui sont disponible dans tous les clouds y compris les Français tels OVH ou Scaleway.

<h2>10. Soutenir les collectivités dans la régulation des meublés de tourisme</h2>

**Problème :** Il est coûteux et complexe pour une collectivité, notamment à faibles moyens, de faire appliquer la régulation des meublés de tourisme.

**Proposition :** Grâce à la création d’un intermédiaire chargé de standardiser et partager les données, la régulation sera plus efficace et moins coûteuse.

**Analyse :** On repasse du coq-à-l'âne avec l'immobilier et les locations de courtes durées comme AirBnB. Difficile d'analyser une proposition aussi floue : "création d'un intermédiaire chargé de standardiser et partager les données". On parle d'une n-ième Haut Autorité de machin ou d'un Conseil Régional de truc ?

Je me demande toutefois pourquoi l'état se bat contre AirBnB tout en détruisant la location classique. Son DPE vient de rendre inlouable des millions de logements. Tout est fait pour rendre la location déficitaire et inciter AirBnB.


<h2>11. Interrompre la diffusion de médias étrangers faisant l’objet de sanctions internationales</h2>

**Problème :** Des médias frappés par les sanctions européennes peuvent continuer à relayer leur propagande grâce à des rediffusions indirectes sur internet.

**Proposition :** L’ARCOM pourra enjoindre aux acteurs de faire cesser la diffusion des contenus sanctionnés sous 72h. En l’absence d’exécution, l’ARCOM pourra ordonner le blocage du site concerné.

**Analyse :** On ne va pas tergiverser, encore une fois un VPM permettra de regarder le média interdit en France.

<h2>12. Lutter contre la désinformation sur les réseaux sociaux</h2>

**Problème :** Les actions de lutte contre la désinformation et les ingérences numériques étrangères ne sont pas coordonnées entre les pouvoirs publics, l’industrie et les organismes de recherche.

**Proposition :** Les principales plateformes en ligne, les acteurs du secteur de la publicité, les organisations de recherche et de la société civile collaboreront plus efficacement pour lutter contre la désinformation.

**Analyse :** Il fallait bien finir ce chef d'oeuvre par un bouquet final. La loi Avia réapparaît clairement : censurer internet au nom de la désinformation. Ce texte oublie le principal sujet. Qui définira une information comme fausse ?

Est-ce de la désinformation :

- [les propos de Gilbert Deray](https://twitter.com/cavousf5/status/1463211197055053833) qui jurait sur France 5 que les enfants atteints d'omicron ont les mêmes cerveaux que des vieillards atteints d'Alzheimer.

- [les propos de Véran](https://www.huffingtonpost.fr/actualites/article/olivier-veran-exhorte-une-nouvelle-fois-les-femmes-enceintes-a-se-faire-vacciner_190488.html) déclarant que les femmes non-vaccins ont plus de chance de faire de fausse couche. Espérons que ces femmes n'ont pas attrapé de [thrombose avec le vaccin](https://www.vidal.fr/actualites/27117-effets-thrombotiques-des-vaccins-astrazeneca-et-janssen-quelle-prise-en-charge.html).

- [les propos de Darmanin](https://www.francetvinfo.fr/sports/foot/ligue-des-champions/ligue-des-champions-gerald-darmanin-denonce-l-attitude-des-milliers-de-supporters-britanniques-sans-billet-ou-avec-de-faux-tickets_5166007.html) sur la mise en cause des supporters anglais lors des scènes de violence de la finale de la Ligue des Champions.

- [les propos de Bruno Le Maire](https://www.lefigaro.fr/conjoncture/bruno-le-maire-dit-viser-5-d-inflation-debut-2023-4-fin-2023-et-2-courant-2024-20221020) sur une inflation de 5% en 2023.

<h2>Conclusion</h2>

La volonté totalitaire des paroles de Macron introduisant la proposition de loi se retrouve dans chaque mesure. Pour pouvoir les appliquer, il va falloir :

- Interdire les VPN et isoler l'internet français de l'internet mondial comme en Chine.
- Interdire l'anonymat avec une carte d'identité numérique comme en Chine.
- Scanner le contenu de chaque français comme en Chine afin d'éviter les arnaques, la pédocriminalité ou les annonces AirBnB.
- Imposer les technologies défini par le Parti pour les messageries ou de cloud, comme en Chine.

Finalement, le fait de pondre ces lois inapplicables ne serait pas une première étape avant de sombrer dans le totalitarisme pour justifier la mise en place de ces mêmes lois ?

