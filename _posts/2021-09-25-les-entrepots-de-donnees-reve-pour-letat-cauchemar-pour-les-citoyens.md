---
title: Faille AP-HP, Les entrepôts de données, rêve pour l'état, cauchemar pour les citoyens
subtitle: Ces pots de miel attirent les pirates comme les dictateurs.
date: 2021-09-25
layout: post
background: /img/2021/09/honey.jpeg
published: www.contrepoints.org/2021/09/28/406831-faille-ap-hp-entrepots-de-donnees-reve-de-letat-cauchemar-des-citoyens
categories: [article, podcast]
tags: [survaillance, technologie]
---

<iframe frameborder="0" loading="lazy" id="ausha-LfnQ" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=Bq1dZFJg9Qzd&v=3&playerId=ausha-LfnQ"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Décidément nos données de santé ne sont clairement plus de l'ordre du privé. Récemment [nous parlons des données de santé](https://www.contrepoints.org/2021/09/05/404600-qr-code-et-pass-sanitaire-vers-une-surveillance-de-masse) présentes dans le QR Code, lisibles par tous. Maintenant c'est une [fuite des hôpitaux de Paris](https://cyberguerre.numerama.com/13342-14-million-de-tests-covid-ont-fuite-4-questions-pour-comprendre-laffaire.html) qui laisse échapper les données de 1,4 million de français dans la nature.

Depuis toujours l'informatique promet de stocker les données dans d'énormes entrepôts, des bases de données hébergées dans le cloud. Ces entrepôts sont fantasmés comme une étape nécessaire vers un plus grande modernité et efficacité de l'état.

Leur désavantage eux, sont bien réels. Ils créent de véritables pots de miel attirant les pirates. De plus, ils se transforment en arme de surveillance contre les citoyens.

<h2>Les pots de miel en forte croissance</h2>

L'état a toujours voulu collecter un maximum de données sur sa population. On repense aux projets avortés de la police [SAFARI](https://fr.wikipedia.org/wiki/Syst%C3%A8me_automatis%C3%A9_pour_les_fichiers_administratifs_et_le_r%C3%A9pertoire_des_individus) en 1974, puis [EDVIGE](https://fr.wikipedia.org/wiki/Exploitation_documentaire_et_valorisation_de_l%27information_g%C3%A9n%C3%A9rale) en 2008, qui souhaitaient centraliser les données de tous les Français. En outre, chaque levée d'impôts ou obtention d’aides ont été de bonnes raisons pour collecter toujours plus de données.

Ainsi l'état connaît précisément, le revenu, le patrimoine, l'emploi, les résidences de tous ces citoyens. Il connaît aussi tous les virements bancaires au-dessus de 1000€ grâce à [Trakfin](https://fr.wikipedia.org/wiki/Tracfin). Il peut [analyser vos réseaux sociaux](https://www.europe1.fr/emissions/L-edito-eco2/impots-lexploitation-des-donnees-des-reseaux-sociaux-par-le-fisc-entre-vigueur-4026276). Depuis [avril 2020](https://www.contrepoints.org/2020/05/24/372068-detention-armes-et-maintenant-le-fichage-de-votre-orientation-sexuelle), les détenteurs d'armes ont droit à un gros bonus, puisque l'état s'autorise le droit de connaître : "les convictions religieuses, origine ethnique, opinions publiques ou orientation sexuelle".


Récemment, la santé est devenue une priorité. On peut noter le [Health Data Hub](https://fr.wikipedia.org/wiki/Dossier_m%C3%A9dical_partag%C3%A9#Le_Health_Data_Hub), agrégeant toutes les données de santé liées au covid (et hébergées sur un serveur américain). Il y a aussi le projet [France Médecine Génomique 2025](https://pfmg2025.aviesan.fr/) voulant collecter le génome d'un maximum de citoyen.

Or ces énormes bases de données se transforment en véritables pots de miel pour pirates. Il suffit de trouver une seule faille pour accéder à des millions de données. Le jeu en vaut la chandelle. Ce mois-ci, les hôpitaux de Paris ont vu leur base de résultats de test piratée, des millions de données de santé se retrouvent ainsi en accès libre.

Malheureusement, de tels accidents sont de plus en plus fréquents dans le monde aussi bien dans le public que le privé. Une faille chez Facebook a ainsi fait fuiter les données de [2,8 milliards](https://www.phonandroid.com/facebook-est-a-laube-dune-enieme-fuite-de-donnees-massive.html) utilisateurs. En Finlande, un pirate a dérobé les dossiers psychiatriques de [36 000](https://korii.slate.fr/tech/milliers-patients-psychiatrie-finlande-victimes-chantage-hack-vol-donnees) citoyens. Par la suite, le pirate a fait du chantage auprès des victimes.

Au Brésil, ce sont les données de [220 millions](https://securite.developpez.com/actu/311975/Une-fuite-de-donnees-gigantesque-expose-les-informations-personnelles-de-220-millions-de-Bresiliens-l-incident-a-ete-signale-par-le-dfndr-lab-le-laboratoire-de-cybersecurite-de-Psafe/) de citoyens qui ont été piratées. En [Bulgarie](https://www.bfmtv.com/tech/vie-numerique/un-hacker-a-derobe-les-donnees-fiscales-de-millions-de-bulgares_AN-201907160051.html), ce sont les impôts qui se sont fait pirater. Vous imaginez les conséquences d'un tel piratage en France ? Tout le monde sera les sources de revenus et le patrimoine des Français sur plusieurs générations.

<h2>L'ours étatique</h2>

Permettez aussi une piqûre de rappel... historique.

Le régime de Weimar avait justement entrepris de construire des registres mentionnant la religion, le patrimoine ou les armes de ses citoyens. Ces registres se sont [montrés d'une aide précieuse](https://www.amazon.fr/Bas-armes-d%C3%A9sarmement-ennemis-int%C3%A9rieurs/dp/2832107486) pour Hitler. En France sous le régime de Vichy, c'est le [fichier Tulard](https://fr.wikipedia.org/wiki/Andr%C3%A9_Tulard) qui répertoria les juifs d'Île-de-France.

Plus récents, les Américains ont mis en place une base de données de leurs partenaires afghans. Cette base se retrouve maintenant [dans les mains des talibans](https://www.challenges.fr/high-tech/l-afghanistan-illustre-le-danger-des-donnees-biometriques-qui-sont-non-revocables_780111), leur donnant  nom, prénom, empreinte digitale et portrait de tous les Afghans ayant travaillé pour l'ennemi.

Il existe toujours d'excellentes raisons (lutte terroriste ou pédophile, efficacité, modernisation, etc) pour regrouper un maximum de données. Leurs avantages sont souvent réduits, mais leurs désavantages sont bien réels. À la fin, ces données se retrouvent toujours entre de mauvaises mains ruinant notre vie privée voire notre vie tout court.



