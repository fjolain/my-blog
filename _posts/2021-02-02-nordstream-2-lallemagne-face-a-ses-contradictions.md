---
title: NordStream 2, l'Allemagne face à ses contradictions
subtitle: Il y a 10 ans, l’Allemagne s'indignait de la vente de deux Mistral français à la Russie. Aujourd’hui, sa dépendance au gaz russe ne semble pas l'indigner.
date: 2021-02-02
layout: post
published: contrepoints.org/2021/02/05/390374-nord-stream-2-lallemagne-face-a-ses-contradictions
background: /img/2021/02/pipeline.jpg
categories: [article]
tags: [politique]
---

# Contradictions sur les partenaires commerciaux

En pleine crise de Crimée, l’Allemagne faisait pression sur la France pour [annuler la livraison de deux porte-hélicoptères Mistral](https://fr.wikipedia.org/wiki/Affaire_des_Mistral) à la Russie. Aujourd’hui, les rôles s’inversent. Suite à l’affaire Navalny, la France demande officiellement à l’Allemagne [d’arrêter NordStream 2](https://www.francetvinfo.fr/monde/russie/alexei-navalny/video-affaire-navalny-la-france-favorable-a-l-abandon-du-projet-de-gazoduc-nord-stream-2-selon-le-secretaire-d-etat-charge-des-affaires-europeennes_4279383.html), son projet de gazoduc russe.

Or, le gouvernement d’Angela Merkel, si prompt à pointer du doigt les partenaires français peu fréquentables comme la Russie ou [l’Arabie Saoudite](https://www.lesechos.fr/industrie-services/air-defense/defense-bae-plombe-par-lembargo-allemand-sur-larabie-saoudite-993028), semble se contredire sur son propre partenariat russe.

Il faut dire que là où, la France pouvait se permettre de ne pas livrer les bâtiments militaires, **l’Allemagne se retrouve coincée dans une dépendance énergétique au gaz russe.**


# Contradiction sur son mix énergétique

Tout a commencé par l’arrêt du nucléaire en [2011](https://fr.wikipedia.org/wiki/%C3%89lectricit%C3%A9_en_Allemagne), puis par un investissement massif des énergies renouvelables. Or, **le renouvelable n’a pas remplacé les anciennes centrales à charbon**, elles restent toujours disponibles pour pallier la volatilité des productions éoliennes ou solaires.

C’est ainsi que la capacité de production d’énergie verte est arrivée [au même niveau](https://fr.wikipedia.org/wiki/%C3%89lectricit%C3%A9_en_Allemagne#R%C3%A9partition_de_la_puissance_install%C3%A9e) que la capacité de productions d’énergies fossiles ou nucléaires, sans la remplacer. Si l’on installe 1 GW d’éolien, il faut aussi 1 GW de charbon pour sécuriser la production lors de vent calme. Ainsi, les deux infrastructures marchent de concert par intermittence. À la fin il a même [plus de production par énergie fossile que par énergie renouvelable](https://fr.wikipedia.org/wiki/%C3%89lectricit%C3%A9_en_Allemagne#R%C3%A9partition_de_la_production_par_source).

Avec l’abandon des centrales au charbon, trop polluantes, pour des centrales au gaz, le mix énergétique allemand dépend plus que jamais du gaz russe. **Les mêmes qui protestaient contre le nucléaire puis le charbon, se retrouvent à protester impuissants contre NordStream 2.**

L’Allemagne s’enfonce dans une dépendance au gaz russe. Un partenariat bien plus nocif que la vente de deux Mistrals…