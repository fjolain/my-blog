---
title: Comment protéger vos cryptos ?
subtitle: Les failles ou faillites se suivent et se ressemblent. Comment éviter de se faire détrousser ?
date: 2022-07-25
layout: post
background: /img/2022/08/coffre.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [blockchain, internet]
---

<iframe frameborder="0" loading="lazy" id="ausha-Wrim" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=BMPL1SnOrJ8d&v=3&playerId=ausha-Wrim"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

On dit qu'une blockchain est sûre, que personne ne peut voler vos cryptos sur blockchain. Alors pourquoi les personnes de Celsius ont perdu leur argent ? Pourquoi les personnes de Voyager ont perdu leur argent ? Pourquoi les utilisateurs de Slope ont perdu leur argent ? Pourquoi les utilisateurs de Terra ont perdu leur argent ?

Depuis quelques semaines, les failles et les faillites se succèdent et les victimes se comptent en nombre. Une blockchain est sûre à condition de bien s’en servir ! Nous allons voir comment sécuriser vos cryptos.


<h2>Not your key, not your coin</h2>

La première faille intervient si vous mettez un intermédiaire entre vous et la blockchain. Par défaut, il n'en a pas, et c'est la grande qualité de la blockchain.

Mais le corollaire de cette qualité peut être la complexité d'utilisation. À la place du login / mot de passe, vous avez une adresse crypto et une clé de cryptage privée. Et à la place de "mot de passe oublié", vous n’avez...rien.

Lorsque vous utilisez la blockchain par vous-même, vous êtes responsable de vos clés de cryptage, qu'il ne faut ni perdre ni se faire voler. Sinon adieux à vos cryptomonnaies.

Aussi plusieurs entreprises se proposent de gérer vos clés pour vous (service de *custody*). Il s'agit bien souvent de plateforme d'échange comme CoinHouse (français), SwissBorg (Suisse) , Paymium (français). Mais aussi des plateformes d'investissement comme JustMining (français).

Ainsi vous retrouvez votre traditionnel login / mot de passe pour vous connecter, mais vous devez faire pleinement confiance à cet intermédiaire. Au risque qu'il parte avec votre argent comme [MtGox](https://en.wikipedia.org/wiki/Mt._Gox) (perte de 750 000 bitcoins, 2014) ou [QuadrigaCX](https://en.wikipedia.org/wiki/Quadriga_Fintech_Solutions) (215 millions de dollars canadien, 2019). Ou qu'il fasse des placements désastreux comme [Voyager](https://journalducoin.com/exchanges/voyager-270-millions-dollars-restitues-clients/) (270 millions de dollars perdus, 2022) ou [Celsius](https://www.cointribune.com/analyses/institutions-entreprises/crypto-ce-quon-doit-comprendre-de-la-banqueroute-de-celsius-cel/) (17.8 milliard de dollar perdus, 2022).

Comme on dit dans la crypto-sphère, *not your key, not your coin* (pas vos clés, pas votre argent). Derrière la simplicité du *custody* se cache un risque pour votre argent.

<h2>Un bon porte-monnaie crypto</h2>

L'idéal est donc de disposer soi-même de ses clés de cryptage pour ne plus avoir aucun intermédiaire avec la blockchain. Encore faut-il correctement stocker vos clés. Elles ne doivent être ni perdues ni volées.


Pour éviter de les perdre, chaque porte-monnaie crypto vous donne une série de 12 ou 24 mots à conserver. Il s'agit ni plus ni moins que de votre clé de cryptage secrète mise sous une forme "lisible" pour les humains. Il faut à minima en faire des copies sur du papier. Il existe aussi des CryptoSteel qui vous permet de conserver ces mots sur des supports acier à l'abri des inondations et incendies.

Votre clé est donc à l'abri de la perte, mais pour utiliser vos cryptos, vous avez besoin d'utiliser cette clé pour signer numériquement des transactions. N'importe quel porte-monnaie *software* fait cette opération de signature. Le problème est que les porte-monnaie *software* s’exécutent sur ordinateur et smartphone, endroit réputé pour contenir des virus.

Si vous disposez de plusieurs centaines d'euros en crypto. Il vaut mieux les mettre ailleurs, ou vous finirez ruiner comme les utilisateurs du porte-monnaie [Slope](https://fr.cryptonews.com/news/solana-blames-slope-wallet-for-hack-while-slope-says-that-nothing-is-yet-firm-fr.htm) (8 millions de dollars, 2022). La semaine dernière une faille a été découverte, les porte-monnaie ont été siphonés.

Heureusement la solution existe, il faut passer aux porte-monnaie *hardware*, le meilleur dans se domaine est français, il s'agit de l'entreprise Ledger.

Il s'agit d'une clé USB électronique qui va contenir vos clés et réaliser elle-même les opérations de cryptage en son sein. Ainsi vos clés ne quittent jamais le Ledger. Jamais elles n'atterrissent, même temporairement, sur votre ordinateur ou smartphone.


<h2>Choisir ses projets avec précaution</h2>

Tous les projets crypto ne se valent pas. Même en disposant de vos clés sur un Ledger. Si le projet que vous avez investi est bancal, vous risquez de tout perdre.

Soit, une faille existe sur le projet et vous perdez vos cryptos, comme les utilisateurs de [Nomad](https://journalducoin.com/defi/hack-nomad-36-millions-dollars-restitues-white-hats/) (190 millions de dollars piratés, 2022). Soit le projet fait faillite, vous conservez vos cryptos mais elles ne valent plus rien. Ça a été le cas des utilisateurs de Terra et du [TerraUSD](https://coinmarketcap.com/currencies/terrausd/) tombé à zéro (18 milliards de dollars évaporés, 2022).

**En résumé, si vous commencez, partez sur un porte-monnaie crypto *software* comme l'application Mt Perlerin (Suisse). Si vous accumulez plusieurs centaines d'euros en crypto, basculez sur un ledger, en générant une nouvelle adresse depuis le Ledger. Commencez par investir principalement sur Bitcoin avant de passer à une autre cryptomonnaie.**


