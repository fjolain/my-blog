---
title: Le paradoxe des prix libres
subtitle: Pourquoi les prix libres est une invention capitaliste
date: 2016-10-29T22:16:48+01:00
layout: post
categories: [article]
tags: [économie]
---
Une mode en chasse une autre, aujourd&rsquo;hui les initiatives de ventes au prix libre se multiplient. Pour ceux qui n&rsquo;ont jamais testé, il s&rsquo;agit d&rsquo;acheter un bien ou service au prix que l&rsquo;on souhaite. Il y avait par exemple une soupe au prix libre durant Nuit Debout. Les gérants ont vite arrêté devant le gouffre financier.

![image]({{ site.baseurl }} /img/2016/10/prix-libre.jpg)


Plus malin, il y a les offres _Humble Bundle_. Pour un temps donné, des jeux indépendants seront à prix libre. Pour n’importe quel prix on a le droit à 3 jeux, mais si on met plus que la moyenne on a le droit à 7 jeux. Le prix reste libre, mais il ne s&rsquo;effondre pas vers 0€, au contraire il augmente constamment car on est poussé à mettre plus que la moyenne.

Près de chez moi un magasin à prix libre est apparu, le prix n&rsquo;est pas totalement libre. Chaque objet a un prix minimum et maximum. Ça gâche un peu le principe, la liberté totale reste une utopie face à la réalité des mentalités.

Ce qui m&rsquo;intrigue le plus c&rsquo;est la logique du principe. En soi proposer un prix libre est justement le principe du libéralisme et sa logique de marché. Que le prix libre se retrouve dans les événements anti-capitalisme est bizarre.

Le prix libre permet à la personne le choix de payer un service/bien au juste prix voire en quasi-gratuit. On se retrouve dans la logique de l&rsquo;économie d&rsquo;internet et sa gratuité. Deezer, LeBonCoin, Youtube, Spotify et toutes les applications mobiles reposent sur le _use for free_. On a le choix de payer ou non un service. Cette économie est une nouveauté du néolibéralisme qui voit la gratuité comme un moyen de rémunération indirect (c&rsquo;est gratuit mais en échange je récupère tes informations, ou c&rsquo;est gratuit mais je te rends accro au produit, &#8230;).

Le prix libre est donc un mélange des logiques du capitalisme moderne, et il se veut anti-capitalisme, drôle d&rsquo;ambition&#8230; D’ailleurs, on peut dire qu&rsquo;il a échoué. Le principe selon lequel le prix est TOTALEMENT libre a été remplacé par une fourchette de prix, un simple marché en somme. Et le fait que le bien/service ne change pas en fonction du prix payé, a été modifié pour inclure une sorte d&rsquo;offre graduelle.

Le prix libre était donc un enfant rebelle du libéralisme. Après un fugue hors de son foyer, il a échoué et y retourne sagement.