---
title: Bienvenue à Bruxellinegrad
subtitle: En théorie, l'UE rejette la Russie. En pratique Bruxelle imite Moscou.
date: 2022-03-21
layout: post
background: /img/2022/03/eu.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [surveillance, société]
---

<iframe frameborder="0" loading="lazy" id="ausha-ess3" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=b7NXvSxRAR4q&v=3&playerId=ausha-ess3"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Selon l'empereur Marc Aurel : "la meilleure façon de se venger d'un ennemi, c'est de ne pas l'imiter". Dans ce cas la Russie est-elle vraiment l'ennemi de l'UE, tant nos gouvernements imitent Putin dans ses lois liberticides ?


<h2>Vérouiller les médias</h2>

Commençons par la liberté d'expression et la pluralité des médias. L'état français contrôle déjà la télé, 5 des 7 premières chaînes sont étatiques (France 2, France 3, France 4, France 5, Arte), le reste est détenu par des oligarques français : [TF1](https://fr.wikipedia.org/wiki/TF1) avec Bouygues, [C8, CNews](https://fr.wikipedia.org/wiki/C8_(cha%C3%AEne_de_t%C3%A9l%C3%A9vision)) avec Bolloré, [BFMTV](https://fr.wikipedia.org/wiki/BFM_TV) avec Drahi. [M6](https://fr.wikipedia.org/wiki/M6) est quant à elle détenue par le groupe allemand Bertelsmann.

L'état est aussi présent à la radio avec France Inter, France info, France bleue, France culture, France musique, Fib et Mov. Le reste étant là aussi possédé par des oligarques. [RTL](https://fr.wikipedia.org/wiki/RTL) par le même groupe allemand Bertelsmann, [RMC](https://fr.wikipedia.org/wiki/RMC) par le même Drahi, [Europe 1, RFM et Virgin Radio](https://fr.wikipedia.org/wiki/Europe_1) par Lagardère.

Pour la presse, l'état se contente de verser d'énormes subventions aux journaux, lui donnant un pouvoir de vie ou de mort sur chaque titre. De toute manière, là aussi on retrouve les oligarques. [Le Monde](https://fr.wikipedia.org/wiki/Le_Monde) est détenu par Xavier Niel, [Libération](https://fr.wikipedia.org/wiki/Lib%C3%A9ration_(journal)) par le même Drahi, [Le Figaro](https://fr.wikipedia.org/wiki/Le_Figaro) par Dassault, [Les Échos](https://fr.wikipedia.org/wiki/Les_%C3%89chos) par Bernard Arnault, [Le Journal du Dimanche](https://fr.wikipedia.org/wiki/Le_Journal_du_dimanche) par le même Lagardère, [CNews, Gala, Télé Loisir](https://fr.wikipedia.org/wiki/CNews_(journal)) par le même Bolloré.

Le paysage médique français constitué exclusivement d'état et d'oligarques n'a rien à envier au paysage médiatique russe. Mais comme Putin, nos gouvernements souhaitent encore restreindre l'accès à l'information.

<h2>Etendre la censure par la loi</h2>

Il faut se rappeler de la loi [Avia](https://fr.wikipedia.org/wiki/Loi_contre_les_contenus_haineux_sur_internet). Le gouvernement voulait imposer une liste de sujet haineux interdit sur internet. Les plateformes telles YouTube ou Twitter devaient supprimer le contenu en moins d'une heure. Le râteau de la censure serait donc particulièrement large pour éviter toute amende.

Cette  loi, bien qu’adoptée par l'Assemblé, fut annulée par le Conseil Constitutionnel. Cependant même abrogée, elle est tacitement appliquée par les plateformes pour ne pas se mettre le gouvernement à dos.

Durant le covid, des vidéos contraires à la vaccination obligatoire furent retirées. Comme [celle de *Pas Content TV*](https://www.youtube.com/watch?v=_trpt0txIRc) qui partageait un tweet de l'OMS  sur l'inefficacité des vaccins. Ou encore comme Zemmour, qui a vu sa vidéo de candidature [supprimée par Youtube](https://www.rtl.fr/actu/politique/youtube-retire-le-clip-de-campagne-d-eric-zemmour-7900132707).

Cette volonté troublante de contrôler l'information comme le ferait Putin est particulièrement visible avec la guerre en Ukraine.

<h2>Interdire tout ce qu'on ne peut pas censurer</h2>

Les deux camps ont au même moment couper la diffusion de chaînes d'opposition. Dans le camp du mal, Putin a bien évidemment réduit au silence, *Les Échos de Moscou* un média libre et indépendant. Dans le gentil camp du bien européen, Ursula von der Leyne, a stoppé Russia Today, une chaîne de désinformation complotiste.

Les similitudes continuent entre l'UE et le régime autoritaire russe. Actuellement, les ministres de l'Économie française et allemande souhaitent le [départ de Facebook de l'UE](https://actu.orange.fr/politique/menace-de-fermer-facebook-en-europe-on-vivrait-tres-bien-sans-reagit-bruno-le-maire-magic-CNT000001JC6Xd.html). Bien sûr contrairement au camp du mal, qui souhaite bannir Facebook pour censurer la liberté d'expression. Le gentil camp du bien souhaite bannir Facebook pour nous éviter de nous faire voler nos données.

En soi, je partage l'argument de Bruno le Maire, Facebook a plus besoin de l'Europe que l'Europe a besoin de Facebook. Mais peut-on croire que le gouvernement se préoccupe de notre vie privée ?

Après tout, il n'a pas hésité à stocker nos données de santé chez Microsoft avec le [Health Data Hub](https://fr.wikipedia.org/wiki/Dossier_m%C3%A9dical_partag%C3%A9#Le_Health_Data_Hub). Sans oublier [la Loi Renseignement](https://fr.wikipedia.org/wiki/Loi_relative_au_renseignement#Collecte_en_temps_r%C3%A9el_des_donn%C3%A9es_de_connexion) votée en France en 2015, qui légalise la surveillance de masse en continu de tout l’internet français. Les fournisseurs d’accès internet ont également l’[obligation de conserver](https://www.nextinpact.com/article/47917/conservation-donnees-connexion-pistes-gouvernement) pendant un an l’historique de navigation de tous leurs clients.

De plus, de même que Putin [souhaite interdire](https://fr.wikipedia.org/wiki/Telegram_(application)#Censure_en_Russie) la messagerie chiffrée Telegram, l'UE [tente depuis des années d'interdire](https://www.blogdumoderateur.com/europe-affaiblir-chiffrement-messageries/) les messageries chiffrées telles Telegram ou Signal pour mieux nous espionner.

Aussi devant ces faits, on peut se demander si Bruno le Maire souhaite bannir Facebook, cette plateforme qui vole nos données ou Facebook, cette même plateforme qui permet aux informations de circuler et aux mouvements de Gilet jaune ou du Convoi de la Liberté de s'organiser ?

Pareil pour Bitcoin, le camp du mal russe souhaite le bannir pour contrôler toutes les transactions du pays. Pour le gentil camp du bien européen, il faut la bannir ou du moins le réguler fortement pour de *bonnes* raisons : éviter sa pollution comme avec la loi [MICA](https://www.latribune.fr/entreprises-finance/banques-finance/industrie-financiere/regulation-des-crypto-actifs-mica-l-union-europeenne-se-tire-une-balle-dans-le-pied-pierre-person-depute-larem-906101.html) récemment votée à Bruxelles, éviter le financement de tout un tas de fantasmes (y compris le financement du camp du mal) ou nous protéger de sa volatilité.

Les mesures prisent par le camp du bien, qui ressemble pourtant étrangement au camp du mal, sont avant tout là pour nous protéger contre les méchants autour de nous.

Cela résume bien les raisons invoquées suite aux deux ans de confinement, couvre-feux, pass-sanitaire et vaccination obligatoire. À ceci près que pour le coup, ça ne vient pas de Putin. La Russie n'a eu ni confinement général ni pass-sanitaire.

Quant à l'envie d'envahir un autre pays, il semblerait bien que Putin nous ait imités en voyant le [Kosovo](https://fr.wikipedia.org/wiki/Guerre_du_Kosovo) en 1999, l'[Afghanistan](https://fr.wikipedia.org/wiki/Guerre_d%27Afghanistan_(2001-2021)) en 2001, l'[Irak](https://fr.wikipedia.org/wiki/Guerre_d%27Irak) en 2003, la [Libye](https://fr.wikipedia.org/wiki/Intervention_militaire_de_2011_en_Libye) en 2011 ou la [Syrie](https://fr.wikipedia.org/wiki/Guerre_civile_syrienne) en 2011.


En conclusion, mise à part que le camp du bien est forcément le bien car progressiste et dirigé par de gentils technocrates. Et que le camp du mal est forcément le mal car dirigé par un méchant russe. Les deux semblent converger vers une idéologie commune : censurer toutes oppositions et interdire toute technologie pouvant aider le citoyen dans son émancipation face à l'état.

