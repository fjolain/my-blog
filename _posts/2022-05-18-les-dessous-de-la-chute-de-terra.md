---
title: Les dessous de la chute de Terra et des stablecoins.
subtitle: Comme se fait-il qu'une crypto qui promettait un prix fixé à 1$ se retrouve aujourd'hui à 10ct ?
date: 2022-05-18
layout: post
background: /img/2022/05/terrausd.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [société, cryptomonnaie]
nolatex: true
---

<iframe frameborder="0" loading="lazy" id="ausha-PV5T" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=brDkxfROgQpw&v=3&playerId=ausha-PV5T"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Chaque crise crypto apporte son lot de drama. La dernière crise a fait chuter Terra, l'un des plus important et médiatique projet crypto des dernières années. Au coeur de l'écosystème de Terra se trouvait son TerraUSD, un stablecoin qui devait toujours valoir 1$, mais que la crise a pourtant fait chuter à 10ct, conduisant le projet au cimetière.

À quoi sert un stablecoin ? Comment fonctionne-t-il ? Pour celui de Terra, s'est effondré ? Nous allons répondre à toutes ces questions.

<h2>A quoi sert un stablecoin</h2>

Une blockchain est certes, un registre transparent et auditable par tous, mais cela reste un système clos sur lui-même.

Les programmes que l'on déploie sur blockchain (Smart Contract) ne peuvent que manipuler des actifs présents sur la blockchain. Ils n'ont aucune connaissance du monde externe  et ne peuvent pas gérer les monnaies présentes hors blockchain.

Ainsi si vous avez de l'Ethereum, il faut impérativement passer par une plateforme d'échange pour les convertir contre des dollars. Cette opération vous oblige à sortir de la blockchain.

Or on aimerait bien avoir nos dollars sur la blockchain pour éviter les frais, le long processus des plateformes de change. Sans parler de notre anonymat ou de la possibilité de l'état de nous interdire les plateformes comme l'a fait Justin Trudeau avec les participants du  Convoit de la Liberté.

En [2014](https://fr.wikipedia.org/wiki/Tether), l'entreprise Tether a donc conçu le premier stablecoin. Il s'agit d'une cryptomonnaie, le Tether (USDT), que l'entreprise prétend valoir toujours 1$.

Maintenant, si je souhaite convertir mes Ethereums en dollar, je peux rester sur la blockchain en achetant du stablecoin Tether, une sorte de pseudodollar sur blockchain.

C'est bien beau de prétendre valoir toujours 1$, mais le prix est donné par le marché et Tether ne peut pas le fixer. Comment font-ils pour stabiliser sa valeur ?

<h2>Comment fonctionne un stablecoin ?</h2>
Certes, Tether ne peut pas fixer le prix sur le marché, mais il peut intervenir sur les marchés pour influencer sur le prix.

Premièrement, si le Tether augmente et dépasse les 1,01$. Dans ce cas, très  simple, Tether va créer de nouveaux stablecoins, qu'il va vendre à 1$ sur les marchés. Le prix va ainsi redescendre à 1$. C'est le cas simple, car Tether a juste à créer ex-niolo ses stablecoins et les vendre contre d'autres actifs. Il a donc une marche de manoeuvre infinie.

Deuxièment, si le prix descend à 99ct. Dans ce cas, Tether va utiliser sa réserve de change pour racheter son stablecoin à 1$ sur les marchés, puis le détruire. Cette réduction de l'offre va mécaniquement augmenter le prix à 1$.

Contrairement à la création ex-niolo de son stablecoin, ici, Tether doit échanger des dollars, euros, bitcoins, ethereums et autres pour racheter son stablecoin. La marche de manoeuvre de Tether pour contrer les pressions à la baisse dépend de sa réserve de change de Tether et de sa bonne gestion.

Suite au succès de Tether, d'autres projets sont venus proposer leurs stablecoins sur la blockchain en concurrence du Tether. Tous interviennent sur les marchés pour stabiliser le prix de leur stablecoin par un jeu de vente et de rachat.

<h2>Panorama des différents stablecoins</h2>
La clé de voûte d'un stablecoin est la bonne gestion de la réserve de change et comment l'organisme garanti, au détenteur de leur stablecoin, la force de rachat lors de tendance à la baisse.

Pour ce faire, nous allons analyser cinq stablecoins dollar gérés par cinq projets différents. Ils sont classés selon mon avis du plus solide au moins solide.

On commence avec Binance et son stablecoin BUSD. Il est selon moi, le plus sûr, car Binance assure détenir [100%](https://www.binance.com/en/busd) de la supply (soit le nombre de stablecoins en circulation) en dollar. Un BUSD, émit par Binance, a son jumeau dollar stocké dans leur coffre. En fait, Binance peut répondre à une pression totale à la vente où tout le monde souhaite revendre le stablecoin.

Ensuite, nous avons USDC avec l'entreprise Circle en partenariat avec Coinbase. Circle prétend que [23%](https://www.circle.com/blog/how-to-be-stable-usdc-transparency-and-trust) de la supply de son stablecoin est soutenu par autant de dollars. Les 77% restant sont placés dans des bons du trésor américain courts termes, des produits peu risqués labélisés en dollar.

On retrouve enfin Tether, qui détient seulement [6%](https://tether.to/en/transparency/#reports) de la supply en cash. Le reste va du bon du trésor américain, aux actions, en passant par des dettes d’entreprises ou des métaux précieux. Et déjà, les critiques fusent sur une telle gestion. On reproche à Tether d'avoir des produits trop peu liquides voire trop dangereux pour faire face à une forte pression à la baisse. Mais pour l'instant Tether jouit de son statut de pionner et reste de loin le stablecoin le plus utilisé et la troisième cryptomonnaie en termes de valorisation.

Enfin pour les deux derniers, on rentre dans un autre monde. Notre souhait d’être à 100% dans la blockchain et à moitié réaliser avec Tether, Binance ou Circle. Ces entreprises reçoivent des euros, dollar, yuan contre leur stablecoin. Ils ont donc un pied dans la blockchain et un pied à l'extérieur. Ils nous font office de pont pour que nous puissions continuer à rester dans la blockchain.

Cependant si un stablecoin décide de recevoir uniquement des bitcoins, ethereums ou autres cryptomonnaies, alors on peut concevoir un programme sur blockchain qui va algorithmiquement gérer le stablecoin en gardant sa réserve de chance exclusivement en cryptomonnaie.

Une telle approche à l'avantage d'être 100% crypto, il n'y a pas d'entreprise *centralisée* et *opaque* comme Tether. Tout se passe sur la blockchain à la vue de tous par un programme décentralisé et transparent.

Le premier projet de ce type est le stablecoin Dia. Dia est une organisation décentralisée qui a conçu un programme déployé sur blockchain qui gère le stablecoin dia (DAI). La supply de Dia est garanti par une réserve de change de [150%](https://en.wikipedia.org/wiki/Dai_(cryptocurrency)) en divers cryptoactifs. Le fait d'être *sur compensé* permet de garder assez de réserve même quand le Bitcoin ou l'Ethereum se déprécient face au dollar. Dia a aussi un mécanisme d'urgence si la réserve devient trop basse pour protéger les détenteurs.

Et enfin, on arrive sur le canard boiteux des stablecoins. TerraUSD fonctionne comme Dia, il est 100% sur blockchain et géré algorithmiquement par un programme décentralisé. Sauf que, contrairement aux stablecoins *centralisés* qui ont 100% de la supply dans des actifs labélisés en dollar, ou comme Dia qui à 150% de la supply en cryptomonnaie, TerraUSD n'a que [16%](https://cryptoast.fr/terra-4130-bitcoins-reserve-176-millions-dollars/) (en avril) de sa supply comme réserve de change dans des cryptomonnaies.

Depuis les cryptos se sont effondrés, la réserve de change de TerraUSD était trop faible. Il ne restait plus qu'à lui faire une attaque à la Soros pour l'achever.

<h2>Attaque à la Soros</h2>

Il se retrouve que la volonté d'avoir des actifs stables n'est pas propre qu'aux cryptomonnaies. En 1992, les pays de l'Union européenne devaient garantir la stabilité des taux de conversion entre eux. Ainsi la banque d'Angleterre devait garantir la stabilité de la livre sterling contre le franc ou le mark. Or la banque d'Angleterre avait sa réserve de change trop faible pour pallier une forte chute.

Le banquier d'affaire George Soros s'en est aperçu et a organisé [une attaque financière](https://www.lemonde.fr/economie/article/2012/09/17/1992-george-soros-fait-sauter-la-banque-d-angleterre_1761289_3234.html) coordonnée afin de faire plonger la livre sterling. La banque d'Angleterre n'a pas pu garder la stabilité de son actif face à une telle pression et Soros est reparti le jour même avec un milliard de profit. (Le montage pour se faire du profit en pariant à la baisse est hors du scope de cet article).

Actuellement, les rumeurs s'amplifient autour d'une possible attaque *à la Soros* contre TerraUSD. Les attaquants seraient des puissants fonds d'investissement qui ont décelé la faille chez TerraUSD et l'ont utilisé comme Soros en 1992.

