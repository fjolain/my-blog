---
title: Fabriquer son propre Luxo
subtitle: Lampe intelligente robotisée
date: 2017-01-26T19:22:42+01:00
layout: post
background: /img/2017/01/luxo.jpg
categories: [project]
tags: [raspberry, robotique, mathématique]
---
Bien le bonjour,

Je suis un grand fan de Pixar (c.f. article sur Wall-e), et bien sûr de leur mascotte, Luxo.
J&rsquo;ai donc conçu un luxo pour mon projet de fin d&rsquo;étude.
Mon Luxo est une lampe de bureau robotisée et intelligente.

Et voici, mon luxo a base de servo moteur et de raspberry pi 😉

<iframe src="https://www.youtube.com/embed/iEbiNDQ0BFA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Pour le refaire les codes sources, les fichiers CAO et le rapport sont sur [mon github](https://github.com/FrancoisJ/luxo)