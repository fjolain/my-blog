---
title: La Réalité du Prix Dérange
subtitle: Des prix ubuesques cachent toujours des situations ubuesques
date: 2020-05-26
layout: post
background: /img/2020/05/balance.jpg
categories: [article]
tags: [politique, économie]
---

Si nous vivions dans la nature, le sens du vent, l'emplacement des étoiles sont nos signaux. Dans un monde de marchés libres, nos signaux deviennent les prix. À nous de les interpréter.

Oui c’est vrai, le marché fait peur. Personne ne semble en mesure de le contrôler. Et souvent, il nous jette à la figure des prix qui semblent illogiques. Forcément, quand on souhaite vivre uniquement entouré d’ordre et de centralisation, cette singularité déplaît. Néanmoins, le marché par son côté libre nous donne des signaux précieux même sur des prix jugés affligeants.

# Prix de l’insuline aux Etat-Unis

Les États-Unis restent un des derniers pays de l’OCDE où les prix des médicaments ne sont pas régulés. Ce qui donne parfois des moments tragiques. La dose d’insuline a été [multipliée par 10 en 20 ans](https://www.france24.com/fr/focus/20190401-insuline-scandale-etats-unis-diabete-pharmaceutique), provoquant la mort d’Américains. Je suis pour la régularisation des prix des médicaments, un marché où la demande devient vitale rend l’offre biaisée. Néanmoins, tout le monde regarde avec effroi les laboratoires qui maintiennent les prix élevés. Or ce prix montre le vrai problème : le sucre dans la société.

Se concentrer sur le manque d’offre n’est pas suffisant pour expliquer le prix, le problème vient surtout de l’explosion de la demande. Le diabète aux États-Unis a [triplé en 20 ans](https://atlanta.consulfrance.org/spip.php?article3498). Comme le marché est libre, cette demande couplée avec la complexité à faire de l’insuline donne une explosion du prix.

Dans ce cas, le prix révèle les vrais bandits : l’industrie agroalimentaire, bien devant l’industrie pharmaceutique.

# Salaire des invisibles

Encore une fois, je ne jugerai pas le système, je parlerai juste de son fonctionnement. Les salaires sont fixés par l’offre et la demande. Sur le marché du travail, cela donne un marché de compétences et des prix qui vont avec. Pourquoi un trader en analyses quantitatives est-il mieux payé qu’une caissière ? L’un est plus utile que l’autre. Les deux nécessitent des compétences. La différence se situe dans le type de compétences et leur rareté.

Si une personne souhaite nous embaucher, c’est pour lui créer de l’argent. Cette vérité contraint donc le salaire à suivre la valeur ajoutée créée par le job, valeur ajoutée purement financière, non sociale. Ainsi, une caissière même aguerrie ne créera pas une grosse valeur : quelques centaines d’heures par jour. Alors que le trader pourra en une seule journée générer plusieurs millions d’euros de bénéfice. Leur salaire évolue donc en conséquence.

Deuxièmement, la rareté. Dans vos proches, qui peut faire passer des produits devant un scanneur, faire l’acompte et autre ? Tout le monde… et même des machines. Ainsi, si une caissière démissionne, le patron peut la laisser partir en sachant qu’il trouvera rapidement quelqu’un d’autre. Et dans votre entourage qui connaît la bourse et l’analyse quantitative ? Personnellement, je ne connais personne. Ainsi, si un patron a besoin d’un trader, le laisser partir sera problématique, tenter de le faire rester en l’augmentant sera une solution.

Dans ce cas la différence de prix révèle des changements brutaux dans nos sociétés. D’une part, une polarisation des compétences entre des gens peu diplômés et des gens ultra-diplômés avec des compétences de pointe. D’autre part, une captation de valeur ajoutée par des jobs comme marketing, développeur ou trader qui peuvent engranger de l’argent sans limites. Et des jobs très manuels où la valeur ajoutée est basse et limitée mécaniquement par le job en lui-même : caissière, livreur, etc.

# Salaire des infirmières

En théorie, leur valeur ajoutée est sans limites, car elles sauvent des vies. Elles ont des compétences de pointe que peu de gens savent faire. Alors pourquoi sur le marché du travail, leur salaire est-il si bas ?

Tout simplement car leur marché du travail n’est pas libre, il est verrouillé par les syndicats grâce à [une grille salariale](https://www.emploi-collectivites.fr/grille-indiciaire-hospitaliere). Une grille salariale est un bulldozer qui détruit la valeur du travail. Il ne prend pas en compte la motivation ni l’excellence de la personne dans son job. Ainsi, l’employé devient qu’une copie parfaite de l’autre au même salaire. La grille salariale nivelle par le bas les salaires.

Dans ce cas, le salaire de l’infirmière montre le truquage total dû aux grilles salariales présent dans le secteur public.

# Prix du bitcoin

Pourquoi une monnaie « qui ne sert à rien » coûte-t-elle aujourd’hui (mai 2020) 9 000 $ ? En analysant le cours en fonction de l’actualité on se rend compte qu’il monte dès qu’une crise éclate dans le monde : Iran, Chine, Venezuela, etc. Dans ce sens, Bitcoin joue bien son rôle de monnaie internationale non censurable.

Bitcoin est aussi une monnaie spéculative où tout le monde rêve du meilleur investissement. Le paysage d’investissement est particulièrement morose: livret A à 0.75%, assurance vie à 1% voire négative. La banque européenne vient même d’instaurer des rendements négatifs pour l’épargne. Bref, tout ce qui peut faire rêver un peu les investisseurs bondit. Le marché immobilier dans des villes « spéculatives » comme Paris, Londres, Hong Kong a explosé [créant des bulles immobilières](https://www.liberation.fr/france/2019/10/11/immobilier-paris-au-plus-haut-avant-la-descente_1756693). Le marché des actions a aussi explosé avec des titres surévalués comme celui de Tesla.

Il ne faut pas voir là seulement le comportement de petits épargnants. Non, la vraie montée vient des grosses institutions qui reçoivent depuis 2008 des [flots de liquide](https://www.challenges.fr/finance-et-marche/ou-sont-passes-les-4-000-milliards-d-euros-injectes-dans-l-economie-par-la-bce_520551) de la banque centrale à placer et qui ne savent pas où le placer. C’est cette première raison qui a fait gonfler l’immobilier par la création massive de crédit, mais également les actions. Le bitcoin n’échappe pas à la règle et voit son prix gonflé par [la demande institutionnelle](https://journalducoin.com/bitcoin/ils-mettent-la-main-sur-33-des-bitcoins-mines-ces-3-derniers-mois/).

Dans ce cas, le prix provient d’un climat de défiance de plus en plus grand envers les États, et d’une raréfaction des investissements rentables surgonflés par une inondation de liquidité.
