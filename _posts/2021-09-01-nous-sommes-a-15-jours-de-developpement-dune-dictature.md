---
title: Nous sommes à 15 jours de développement d'une dictature.
subtitle: Le pass sanitaire est un outil de surveillance clé en main.
date: 2021-09-01
layout: post
background: /img/2021/09/jumelles.jpeg
published: www.contrepoints.org/2021/09/05/404600-qr-code-et-pass-sanitaire-vers-une-surveillance-de-masse
categories: [article, podcast]
tags: [survaillance, technologie]
---

<iframe frameborder="0" loading="lazy" id="ausha-PjRh" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=yg2zjtNJlw8Z&v=3&playerId=ausha-PjRh"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

[Dans un précédent article](https://www.contrepoints.org/2021/08/07/402940-que-contient-le-qr-code-du-pass-sanitaire), je décortiquais le contenu du QRCode, révélant son manque total d'anonymat. Ceci rabaisse une fois de plus notre liberté et notre vie privée, mises à mal depuis le début de la pandémie.

Aussi en tant que développeur, je constate que nous sommes maintenant à une étape d'une surveillance de masse, que je chiffre à seulement 15 jours de travail.

En effet aujourd'hui, à cause du pass sanitaire, les citoyens reçoivent un QRCode nominatif à scanner régulièrement. De l'autre côté du QRCode, il y a une application mobile gouvernementale ([TousAntiCovid Verif](https://play.google.com/store/apps/details?id=com.ingroupe.verify.anticovid&hl=fr&gl=FR)) qui vérifie les informations du scan. Elle fonctionne comme une boîte noire, son code [n'est  pas totalement public](https://www.developpez.com/actu/317358/Fausse-bonne-nouvelle-le-code-source-de-TousAntiCovid-Verif-publie-mais-il-est-incomplet-et-pire-encore-sous-licence-proprietaire/). Et du point de vue d’utilisateur, chaque scan affiche l'identité et un simple "correct" ou "incorrect", ne précisant nullement la raison du refus.

Nous sommes face à deux menaces extrêmes pour notre vie privée, qui sont techniquement rapides à mettre en place.

<h2>Rapatrier les données</h2>

Cette application peut à tout moment lors de la prochaine mise à jour rapatrier les données des scans sur un serveur étatique central. Un tel serveur et une telle fonctionnalité sont très faciles à mettre en place en seulement 15 jours de travail. L'état aura alors un point de vue omniscient sur sa population, nous rentrerons dans une surveillance de masse.

<h2> Rendre le QRCode opaque </h2>

Le QRCode laisse les données médicales lisibles par n'importe qui. Il existe deux voies pour mieux le sécuriser. Premièrement, avoir deux QRCode, l'un avec données médicales, l'autre non, ainsi nous pouvons présenter le QRCode sans données médicales aux restaurateurs, agents SNCF, employeurs sans danger.

Deuxièmement, nous pouvons aussi demander un mot de passe au détenteur du QRCode pour chiffrer son contenu. Ainsi le citoyen peut lire les données de son QRCode et le partager avec qui il veut.

Mais l'état peut aussi suivre une troisième voie : chiffrer le contenu du QRCode sans donner le mot de passe au citoyen. Ainsi le QRCode se transforme en boîte noire. L'état peut y rajouter discrètement des données sur l'orientation sexuelle, la couleur de peau, la nationalité ou les opinions politiques ou religieuses, sans que personne ne le sache.

Sachant que l'application de scan affiche déjà un simple "correct" ou "incorrect" sans plus de détails, le combo QRCode chiffré plus application au code source fermé permet au gouvernement de discriminer n'importe qui sur des critères arbitraires en toute discrétion.

Ici encore, il ne faut que quelques jours pour mettre en place une telle fonctionnalité.

Nous avons déjà fait le plus dur. Aujourd'hui il faut rentrer son identité dans une application étatique dès lors que l'on souhaite prendre le train ou manger au restaurant. C'est une chose épouvantable, inconcevable il y a encore un an, et pourtant acceptée par [64%](https://www.lefigaro.fr/actualite-france/passe-sanitaire-64-des-francais-approuvent-son-extension-dans-les-bars-et-restaurants-selon-un-sondage-20210823) des Français. Il ne reste que quelques jours de développement pour que cet outil nous fasse sombrer dans une surveillance de masse aux persécutions arbitraires.


