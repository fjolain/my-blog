---
title: Le business derrière nos données. Enjeux sur nos vies privées 1/3
subtitle: Votre vie privée est collectée, agrégée et même mise aux enchères. Découvrons l'état du marché autour de ce business.
date: 2022-12-28
layout: post
background: /img/2023/01/masque1.jpeg
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe frameborder="0" loading="lazy" id="ausha-hKfs" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=ypNRpSN63M2j&v=3&playerId=ausha-hKfs"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Drôle d'époque, notre vie privée attire les convoitises. Pourtant elle est justement "privée", mais depuis l'arrivée d'internet et de l'informatique, nous pouvons être traqués dans nos moindres faits et gestes dans nos vies en ligne.

Gallimard ne sait rien de nos lectures. Amazon connaît tous nos achats et nous observe lire sur sa liseuse Kindle. Universal ne connaît pas nos goûts musicaux. Spotify sait chaque second d'écoute. Le barman n'a que faire de nos conversations entre amis. Facebook les analyse toutes.

Maintenant qu'une grande partie de nos vies est numérique, notre vie privée est devenue une bataille où s'affrontent les violeurs de vie privée et les défenseurs.

Nous allons analyser les différents acteurs et le business des données sur internet. Pour ceux qui veulent protéger sa vie privée, je vous invite à regarder [une de mes vidéos](https://youtu.be/Ky0JBbEeSwQ) sur le sujet.

<h2>Les violeurs de vie privée</h2>
<h3>Marketing</h3>
Bien sûr, on pense à Facebbok, Google ou Microsoft. À eu trois, ils gèrent les principaux messages envoyés par internet avec Messanger, Whatsapp, Gmail et Outlook. Ils disposent de [84%](https://fr.statista.com/infographie/8204/parts-de-marche-des-systemes-exploitation-pour-smartphones/) des smartphones dans le monde avec Android et [75%](https://fr.statista.com/infographie/20455/parts-de-marche-des-systemes-exploitation-pour-ordinateurs-dans-le-monde/) des ordinateurs avec Windows et ChromeOS. Ils contrôlent les principaux réseaux sociaux avec Facebook, Instagram, YouTube et LinkedIn. Ils ont même un droit de regard sur nos documents avec Office ou GSuite.

Tout cela est déjà énorme, et pourtant cela reste la partie émergée de l'iceberg.

Premièrement, la BigTech espionne nos mouvements en dehors de leurs sites. Google dispose de [Google Analytics](https://fr.wikipedia.org/wiki/Google_Analytics), un module à installer sur un site tiers pour analyser le trafic. En échange de laisser Google espionner les visites sur le site web. Le gestionnaire du site peut visualiser les métriques de ses visites.

Google Analytics est installé sur 80% des sites web, ce qui permet à Google de surveiller 80% d'internet. Sans oublier le navigateur Google Chrome qui doit permettre de surveiller les 20% restant.

Facebook n'est pas en reste, lui aussi propose un module à installer sur les sites, le [Facebook Pixel](https://developers.facebook.com/docs/meta-pixel/). Ce composant se transforme en mouchard pour analyser le trafic des sites où il est présent.

Deuxièment, à côté des BigTech, il existe des milliers d'entreprises pour collecter nos données, les revendre, les aggers, puis les mettre aux enchères.

![landscape](/img/2023/01/martech-landscape.webp)

Oui vous avez bien lu, nos données finissent aux enchères, il en a à chaque fois que vous visitez un site web.

Par exemple, vous naviguez sur internet à la recherche de vos prochaines vacances. Tout votre parcours est analysé, un profil marketing circule avec vous sur internet (homme à 80%, CSP+ à 60%, recherche vacances à 90%).

À chaque site visité, ce profil est mis aux enchères ([Programmatic advertising](https://www.ibm.com/watson-advertising/thought-leadership/what-is-programmatic-advertising)), des robots vont se battre durant quelques millisecondes pour le compte d'annonceurs. Dans votre cas, une bataille entre hotels.com contre trivago.com. À la fin, le gagnant aura le droit d'afficher sa pub devant vos yeux.

Tout ce processus se rejoue sur chaque site entre le moment où le site s'affiche et la pub apparaît.

<h3>Mercenaire 2.0</h3>
On restait jusqu'ici dans la sphère du marketing. Il existe aussi des entreprises de surveillances d'internet au service des gouvernements, des mercenaires du numérique 2.0.

L'entreprise [Palantir](https://fr.wikipedia.org/wiki/Palantir_Technologies) propose d'analyser les immenses données des citoyens en ligne pour les états, une sorte de NSA clé en main. Le gouvernement français l'utilise, et même le NHS britannique a utilisé ses services durant le covid.

On peut aussi citer [NSO](https://fr.wikipedia.org/wiki/NSO_Group) avec son logiciel Pegasus. Il sert à pirater le téléphone de n'importe qui. Son but est bien sûr de traquer les terroristes et les pédophiles. Il n'a toujours pas permis de retrouver les clients d'Epstein. Mais il a déjà été utilisé 50 000 fois au moins pour espionner des journalistes et militants des droits de l'homme comme Ahmed Mansoor ou même des politiques. Le Maroc a utilisé ce logiciel pour espionner [le téléphone de Macron](https://www.lemonde.fr/projet-pegasus/article/2021/07/20/projet-pegasus-un-telephone-portable-d-emmanuel-macron-dans-le-viseur-du-maroc_6088950_6088648.html).

<h2>Les défenseurs</h2>
Il existe de vrais acteurs qui militent pour nos vies privées. Que ce soit des associations comme [EFF](https://fr.wikipedia.org/wiki/Electronic_Frontier_Foundation) aux États-Unis, la [Quadrature du Net](https://fr.wikipedia.org/wiki/La_Quadrature_du_Net) et [Framasoft](https://fr.wikipedia.org/wiki/Framasoft) en France. Ou des entreprises comme [Proton](https://fr.wikipedia.org/wiki/Proton_Mail) qui propose un concurrent entièrement chiffré à Gmail et Drive.

Encore une fois pour ceux qui veulent plus de détails sur les applications et outils pour protéger sa vie privée, je vous renvoie vers [ma vidéo](https://youtu.be/Ky0JBbEeSwQ) à ce sujet.

Mais certaines entreprises interrogent, telle [Apple](https://fr.wikipedia.org/wiki/Apple#Questions_relatives_%C3%A0_la_vie_priv%C3%A9e). Sur le papier Apple se veut un anti-Google. Son business modèle repose sur la vente de produits et services aux consommateurs et non sur la collecte de données.

D'un côté Apple se donne les moyens: puces de sécurité T2 et capteurs biométriques sur tous leurs appareils pour chiffrer et mieux sécuriser; mis à jour fréquent et bug bunty pour éviter les failles; implémentation du chiffrement de bout en bout dans diverses app pour éviter que des données en claires ne circulent dans leurs serveurs; meilleur cadrage et limite du traçage des apps tierces sur AppStore.

De l'autre, Apple reste une entreprise fermée, ses appareils sont difficilement auditables. L'entreprise revendique la collecte de données sur l'AppStore et l'app Bourse. Elle était bien dans l'affaire Snowden comme entreprise qui travaille avec la NSA. Le FBI lui demande de retirer le chiffrement. La Chine lui demande de retirer des apps pouvant aider les manifestants.

Est-ce qu'Apple fait tout son possible pour protéger nos vies privées ou surfe-t-elle sur un simple argument marketing ?

En tout cas pour certaines entreprises la question ne se pose même plus. L'entreprise [Qwant](https://fr.wikipedia.org/wiki/Qwant) se voulait un Google français respectueux de la vie privée.

Après 25 millions investi par l'UE, le produit reste bancal, trop dépendant de Bing. Alors que d'autres solutions similaires ont réussi à émerger comme DuckDuckGo ou Brave Search.

Pire [le salaire élevé de ses dirigeants](https://www.nouvelobs.com/economie/20190701.OBS15228/qwant-accuse-de-trop-payer-ses-salaries-le-pdg-repond-a-la-polemique.html) a été révélé, alors que la boîte ne gagne toujours pas un rond.

La situation de Qwant, malgré l’afflux d'argent public, est telle que l'entreprise à [emprunter 8 millions d'euros à Huawei](https://www.politico.eu/article/french-search-engine-qwant-huawei-bailout-loan/) pour éviter la faillite.

Enfin le [fondateur vient de partir pour lancer une entreprise de cybersurveillance.](https://www.politico.eu/article/comment-lancien-patron-de-qwant-champion-de-la-vie-privee-se-reinvente-dans-la-cybersurveillance/) La crédibilité de Qwant est vivement touchée. Cela ressemble à une entreprise ramassant les subventions pour les "nobles causes".

<h2>Conclusion</h2>
Il ne sert à rien de pointer du doigt un acteur ou une plateforme en particulier comme Tiktok. Tout internet viole votre vie privée, repartie à travers des milliers d'acteurs !

D'ailleurs il manque l'acteur le plus important. Dans un prochain article, nous verrons quels rôles ont les États dans ce business de nos vies privées.

