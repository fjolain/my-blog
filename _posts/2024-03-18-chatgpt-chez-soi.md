---
title: Comment déployer son propre ChatGPT ?
subtitle: Il est possible de reprendre le contrôle sur l'IA, en personalisant la votre sur mesure. 
date: 2024-02-06
layout: post
background: /img/2024/03/robot.png
categories: [article, podcast]
published: www.contrepoints.org
tags: [économie, société]
nolatex: true
---

<iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-VJ25" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/?showId=Bndd5fqXpjnw&color=%23d7d7d7&dark=true&podcastId=bP8OptG6aZg1&v=3&playerId=ausha-VJ25"></iframe><script src="https://player.ausha.co/ausha-player.js"></script>

Difficile de passer à côté de chatGPT. Cette IA conversationnelle permet de répondre à tout, de la programmation à la médecine, en passant par le juridique.

ChatGPT a beaucoup de qualités. Cependant, il bute sur deux problématiques. Il appartient à OpenAI une entreprise américaine et toutes vos données se retrouvent chez elle. Il reste général et ne peut guère répondre sur une connaissance pointue spécifique à un métier.

Aussi pour des besoins de souveraineté, de confidentialité ou de spécialisation dans un domaine particulier, il peut être nécessaire d'avoir sa propre IA.

Comment faire ? Quelles sont les alternatives ? Peut-on personnaliser une IA ? Peut-on déployer son IA dans son infrastructure ?

<h2> Quelles IA existent ?</h2>

ChatGPT fait office d'arbre qui cache la forêt. Il existe d'autres propositions. Il faut commencer par élaguer. Nous parlerons des robots conversationnels de dernière génération. Ces robots manipulent du texte pour répondre aux demandes des utilisateurs à travers une technologie appelée Large Language Model (LLM).

On n'abordera pas les IA capables de générer des images ni les technologies plus anciennes devenues obsolètes après les LLM.

En fin de compte, il existe 4 alternatives à chatGPT, que l'on peut installer soi-même :

- [LLaMa](https://fr.wikipedia.org/wiki/LLaMA), l'IA de Facebook
- [Falcon](https://huggingface.co/tiiuae/falcon-180B-chat), l'IA du Technology Innovation Institute des Emirates.
- [Gimini](https://fr.wikipedia.org/wiki/Google_Gemini) de Google
- [Mistral](https://mistral.ai/fr/) de la startup française éponyme.

J'exclus Copilot de Microsoft, car il s'agit simplement d'une surcouche de ChatGPT.

Premièrement dans le choix de l'IA, il faut faire attention aux licences. Gimini de Google peut être intégrée dans des produits, mais n'est pas open source. Il vous faudra acquérir une suscription qui [n'est pas encore disponible](https://ai.google.dev/pricing?hl=fr) (21 mars 2024).

LLaMa bien que décrit comme open source par Meta [ne l'est pourtant pas](https://opensource.org/blog/metas-llama-2-license-is-not-open-source). Premièrement vous devez vous inscrire auprès de Meta pour l'acquérir. Deuxièmement sa licence comporte bon nombre d'interdictions. Vous ne devez pas l'utiliser pour entraîner une autre IA ou encore vous ne devez pas l'utiliser si vous avez plus de 700 millions d'utilisateurs. En bref, Meta veut bien que vous jouiez avec son IA tant que vous ne devenez pas son concurrent.


Falcon est lui aussi dit "open source", cependant [il n'autorise pas l'usage commercial en SaaS](https://huggingface.co/spaces/tiiuae/falcon-180b-license/blob/main/LICENSE.txt). Vous ne pouvez pas proposer une plateforme type chatGPT utilisant Falcon.

Heureusement, je vous ai gardé le meilleur pour la fin, Mistral est complètement open source sous licence [Apache 2.0](https://github.com/mistralai/mistral-src/blob/main/LICENSE), aucune restriction même pour un usage commercial.

<h2>Personnaliser son IA</h2>
On peut spécialiser une IA de deux manières différentes.

Le plus simple est de personnaliser le prompt. Ces IA s'utilisent par un prompt. Bien que de prime abord enfantin, des prompts simples comme "rédige un article court sur l'IA", pourront décevoir par la platitude. Le prompt devient alors une science pour atteindre des réponses satisfaisantes. Dans notre exemple un meilleur prompt peut être : "Rédige un article de 300 mots compréhensible par un public non technique avec une problématique comme introduction".

Pour aider l'utilisateur, on peut ajouter un contexte au prompt pour guider l'IA. Cela peut être une simple phrase ou le résultat d'une recherche internet qu'on ajoutera en plus du prompt de l'utilisateur. En soi l'IA reste inchangée, on peaufine juste ses paramètres d'entrées.

La deuxième méthode nécessite de modifier l'IA pour reprendre son entraînement sur des données plus précises venant du métier.

Imaginons que vous vendez des machines-outils, vous souhaitez ajouter une IA sur votre support client. Dans ce cas, vous pouvez prendre un modèle type Mistral et le réentraîner sur l'historique de vos messages supports précédents. Après un certain temps, l'IA sera optimisée pour pouvoir répondre aux questions du support propre à vos produits.

Cette seconde personnalisation prend plus de temps, mais permet de concevoir une IA unique que vos concurrents n'auront pas.

<h2>Déployer son IA</h2>
Enfin après avoir sélectionné une IA, configuré son prompt ou réentraîné sur vos données, il est temps de déployer votre IA. L'IA demande beaucoup de puissance de calcul pour fonctionner.

Il est possible de déployer une IA à moindre coût dans son infrastructure. J'ai pu déployer une IA sur un ordinateur à 800 CHF, elle est accessible en ligne ici : [app.geniusheidi.ch](https://app.geniusheidi.ch)

Premièrement le choix de l'IA est important. Les IA vues plus haut comme LLaMa, Falcon, Mistral sont disponibles en plusieurs tailles, que l'on compte en milliards de paramètres. On parle donc de Falcon 180B pour 180 milliards de paramètres ou de Mistral 7B pour 7 milliards de paramètres.

Plus un IA dispose de paramètres, plus elle devient efficace et nécessite de la puissance de calcul. Par exemple un modèle 7B demande une carte graphique Nvidia A100. Les modèles 40B nécessitent 3 cartes graphiques. Enfin les plus gros modèles rivalisant avec chatGPT-4 avec une taille de 180B demandent des ordinateurs avec 10 cartes graphiques.

Une carte graphique Nvidia A100 coût [10 000 CHF pièce](https://www.ldlc.com/fr-ch/fiche/PB00462665.html), sur un hébergeur en ligne elle se loue pour [2000 CHF / mois](https://www.ovhcloud.com/fr/public-cloud/prices/).

Et oui, votre requête chatGPT-4 tourne sur un ordinateur coûtant 100,000 CHF. D'ailleurs la facture cloud d'OpenIA est estimée à [700,000$ par jour](https://www.businessinsider.com/how-much-chatgpt-costs-openai-to-run-estimate-report-2023-4?r=US&IR=T). Cela représente donc un coût pour déployer son IA. Heureusement encore une fois, je vous laisse le meilleur pour la fin.

Les ordinateurs Apple avec les puces M1/M2/M3 semblent être aussi performants que des cartes graphiques.
Mon IA [app.geniusheidi.ch](https://app.geniusheidi.ch) tourne actuellement sur un Mac Mini M1 d'une valeur de 800 CHF avec l'IA Mistral 7B. On peut donc drastiquement réduire la facture en reprenant son infrastructure chez soi.

<h2>Conclusion</h2>
Derrière la simplicité de chatGPT se cache tout un monde qu'il va bien falloir  intégrer dans les entreprises pour rester concurrentiel. Utiliser sa propre IA n'est ni une tâche triviale ni une tâche d'expert en IA. Il s'agit plutôt d'un outil informatique comme un autre à personnaliser et intégrer dans son infrastructure.





